//
//  ReadPListPlugin.swift
//  App
//
//  Created by Taylor Armstrong on 6/25/20.
//

import Foundation
import Capacitor

/**
 * Please read the Capacitor iOS Plugin Development Guide
 * here: https://capacitor.ionicframework.com/docs/plugins/ios
 */
@objc(pListReader)
public class ReadPListPlugin: CAPPlugin {

     @objc func readVariable(_ call: CAPPluginCall) {
       let value = call.getString("value") ?? ""
        call.success([
            "value": value
        ])
    }
    
}
