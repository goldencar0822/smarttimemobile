//
//  Plugin.swift
//  App
//
//  Created by Taylor Armstrong on 6/25/20.
//

import Foundation
import Capacitor

@objc(PListPlugin)
public class PListPlugin: CAPPlugin {
    
    @objc func testPlugin(_ call: CAPPluginCall) {
      let value = call.getString("value") ?? ""
      call.resolve([
          "value": value
      ])
    }
    
  @objc func readVariable(_ call: CAPPluginCall) {
    let appVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion")
    call.success([
        "appVersion": appVersion
    ])
  }
    
}
