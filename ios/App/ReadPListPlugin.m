//
//  ReadPListPlugin.m
//  App
//
//  Created by Taylor Armstrong on 6/25/20.
//

#import <Foundation/Foundation.h>
#import <Capacitor/Capacitor.h>

CAP_PLUGIN(ReadPListPlugin, "ReadPListPlugin",
  CAP_PLUGIN_METHOD(readVariable, CAPPluginReturnPromise);
)
