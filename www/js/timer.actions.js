//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.timer) { swp.timer = {} }

swp.timer.actions = function () {
    let arrEntries = swpSettings.arrEntries,
        arrEntriesTimers = swpSettings.arrEntriesTimers,
        activeEntryPosition = swpSettings.activeEntryPosition,
        activeTimerPosition = swpSettings.activeTimerPosition,
        start, currentEntryIndicatorId, sortOption, sortDir;

    function calcTimerStart(timer) {


        start = new Date().getTime();


        if (timer !== "" && timer !== ':00')  /* took out this constraint: [&& parseInt(timer) !== 0] */ {

            var total = swp.shared.Rounding.msFromTimer(timer);

            start = new Date().getTime();

            if (!isNaN(total))
                start = start - total;

        } else {
            start = new Date().getTime();
        }

        return start;
    }

    //public interface
    var module = {

        addTimer: function () {
            log.debug('adding new timer')
            arrEntriesTimers = swpSettings.arrEntriesTimers

            var entry1 = swp.entry.newEntry(),
                shortdate = moment().format();

            /* if time is on then use the time as well */
            if (swpSettings.selectTime == "true" || swpSettings.selectTime == "True") {
                shortdate = moment(shortdate).add((new Date()).getMinutes(), 'm').add((new Date()).getHours(), 'h').toDate();
            }

            arrEntriesTimers.push(entry1)
            entry1.eventDT = shortdate
            entry1.sortDate = moment()
            entry1.timer = "00:00:00"
            entry1.timerId = `${arrEntriesTimers.length}`
            entry1.eventTitle = "Mobile Timer"
            entry1.notes = `Timer ${arrEntriesTimers.length}`
            entry1.color = "white"
            entry1.type = "Timer"
            swpSettings.activeTimerPosition = arrEntriesTimers.length - 1;

            module.displayTimerPhone()

            if ($("#session-entries-list-timer").data("kendoMobileListView")) {
                $("#session-entries-list-timer").data("kendoMobileListView").setDataSource(arrEntriesTimers)

                sortOption = swpSettings.timerSortOption.split("_")[1]
                sortOption === "sortDate"
                    ? sortDir = "desc"
                    : sortDir = "asc"

                $("#session-entries-list-timer").data("kendoMobileListView").dataSource.sort([
                    { field: `${sortOption}`, dir: `${sortDir}` },
                ])
            }

            $('#fieldTimestamp_timer').show();
            $('#lblHours-phone').text('Timer:');
            $('#divTimerButtons').show();
            $("#fieldDuration_timer").hide();
            $('fieldDurationTime-phone').show();
            //$("#timersView #divTouchTimerTemplate-phone").kendoTouch({ enableSwipe: true, swipe: function (e) { if(e.direction === left) {console.log(e);} } })
            //checkForTimeToBeVisable()

            //$("#mobi_datetime").change(function () {
            //    log.info("EVENT DT CHANGE");
            //    checkForTimeToBeVisable()
            //})
        },

        clearTimer_Phone: function () {
            arrEntriesTimers = swpSettings.arrEntriesTimers
            activeTimerPosition = swpSettings.activeTimerPosition

            // Your moment
            var mmt = moment();
            // Your moment at midnight
            var mmtMidnight = mmt.clone().startOf('day');
            // Difference in minutes
            var diffMinutes = mmt.diff(mmtMidnight, 'minutes');

            var startTimeIsSameDay = moment(arrEntriesTimers[activeTimerPosition].timerStart).isSame(mmtMidnight, "day");
            var eventDateIsSameDay = moment(arrEntriesTimers[activeTimerPosition].eventDT).isSame(mmtMidnight, "day");
            if (eventDateIsSameDay === true) {
                //if (1 == arrEntriesTimers[activeTimerPosition].set("timerRunning") ? stopTimer_Phone() : $("#liDuration-phone").attr("href", "#duration-phone"), "Released" != arrEntriesTimers[activeTimerPosition].status && "Posted" != arrEntriesTimers[activeTimerPosition].status) {
                if (1 == arrEntriesTimers[activeTimerPosition].set("timerRunning")) { stopTimer_Phone() }
                if (arrEntriesTimers[activeTimerPosition].status != "Released" && arrEntriesTimers[activeTimerPosition].status != "Posted") {
                    var a = arrEntriesTimers[activeTimerPosition].get("timerId");
                    if (a !== 0 && a !== "") {
                        arrEntriesTimers[activeTimerPosition].set("timerRunning", false)
                        arrEntriesTimers[activeTimerPosition].set("timerId", 0)
                        arrEntriesTimers[activeTimerPosition].set("timer", "00:00:00")
                        arrEntriesTimers[activeTimerPosition].set("duration", "")
                        arrEntriesTimers[activeTimerPosition].set("durationTimeDisplay", "00:00:00")
                        arrEntriesTimers[activeTimerPosition].set("timerStart", "")
                        arrEntriesTimers[activeTimerPosition].set("timeStamp", "")
                        arrEntriesTimers[activeTimerPosition].set("unsavedFlagClass", "hidden-image")
                    }
                }
                $("#btnStartTimer-phone").css("display", "inline-block"), $("#btnPauseTimer-phone").css("display", "none")
            }

        },

        displayTimerPhone: function (a) {
            $("#timeEntryView #timeEntryDateInput").val(moment().format("l").toString())
            $("#timeEntryView #pageTitleTop").text(swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].notes)
            if (location.hash === "#views/timeEntryViews/timeEntryView.html") swp.stackView.loadTimeForm()
            
            if ($("#timeEntryView #timeEntryDateInput").data("kendoDatePicker")) $("#timeEntryView #timeEntryDateInput").data("kendoDatePicker").enable(false)
            // swp.timeentry.actions.clearAllFields()
            $("#timeEntryView #backNavButton").hide()
            $("#fieldDurationUnits-phone").hide()
            $("#fieldDurationTime-phone").hide()
            $("#timeEntryView #btnTimerToggle-phone").hide()
            $("#btnPauseTimer-phone").hide()
            $("#fieldTimer-phone").show()
            $("#timeEntryView #timerColorIndicatorTouch").show()
            $("#btnStartTimer-phone").show()
            $("#spanTimerPhone").show()
        },

        loadDefaultSession: function () {
            if (swpSettings.timerSettings.LoadRadio === 1) {
                swp.timer.actions.loadBlankTimers()
            } else {
                $.ajax({
                    type: "GET",
                    url: `${swpSettings.service_url}defaultsession/${swpSettings.objTook.token}`,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        
                        swpSettings.arrEntriesTimers = []
                        let shortdate = moment().format('l').toString();
                        // let tempArr

                        for (let i = 0; i < data.length; i++) {
                            let entry = swp.entry.newEntry(),
                                entryCD = moment()
                                

                            // if (entryCD == null || entryCD == undefined) {
                            //     entryCD = moment(shortdate).add((new Date()).getMinutes(), 'm').add((new Date()).getHours(), 'h').toDate();
                            // }

                            if (arrEntriesTimers.filter(x => x.id === entry.id)) {
                                // entryCD = moment(shortdate).add((new Date()).getMinutes(), 'm').add((new Date()).getHours(), 'h').toDate()
                                shortdate = moment(shortdate).add(1, "s")
                                entryCD = shortdate.toString()
                            }

                            entry = swp.utilities.setTimeToLocal(entry, entryCD);
                            entry.eventDT = moment(entryCD).format()
                            entry.sortDate = entryCD
                            entry.id = new Date(entryCD).getTime() + swpSettings.swp_username.replace('@', '').replace('.', '')
                            entry.origId = data[i].ID
                            entry.client = data[i].Client //changing this back to client / matter from CientName and parsing in template.
                            entry.matter = data[i].Matter //assigning the number only here will cause inconsistency with MRUs /saved client matters
                            entry.custom1 = data[i].Custom1
                            entry.custom2 = data[i].Custom2
                            entry.custom3 = data[i].Custom3
                            entry.custom4 = data[i].Custom4
                            entry.custom5 = data[i].Custom5
                            entry.duration = data[i].Hours
                            entry.narrative = data[i].TimeEntryDesc
                            entry.eventTitle = "Mobile Timer"
                            entry.type = "Timer"
                            entry.timer = data[i].Timer
                            entry.color = data[i].Color
                            entry.notes = data[i].Notes
                            entry.returned = data[i].Returned

                            if (entry.client === null || entry.client === undefined) entry.client = ""
                            if (entry.matter === null || entry.matter === undefined) entry.matter = ""
                            if (entry.custom1 === null || entry.custom1 === undefined) entry.custom1 = ""
                            if (entry.custom2 === null || entry.custom2 === undefined) entry.custom2 = ""
                            if (entry.custom3 === null || entry.custom3 === undefined) entry.custom3 = ""
                            if (entry.custom4 === null || entry.custom4 === undefined) entry.custom4 = ""
                            if (entry.custom5 === null || entry.custom5 === undefined) entry.custom5 = ""
                            if (entry.narrative === null || entry.narrative === undefined) entry.narrative = ""

                            //Timer check and set logic
                            if (entry.timer === "" || entry.timer === null || entry.timer === undefined) entry.timer = "00:00:00"
                            if (entry.color === "" || entry.color === null || entry.color === undefined) entry.color = "white"
                            if (entry.notes === "" || entry.notes === null || entry.notes === undefined) entry.notes = `Timer ${i + 1}`

                            entry.status = data[i].Status
                            entry.statusImage = `img/${data[i].Status}.png`

                            // entry = swp.utilities.setTimeToLocal(entry, data[i].CreateDate)
                            // tempArr.push(entry)
                            swpSettings.arrEntriesTimers.push(entry)
                        }
                        // swpSettings.arrEntriesTimers = tempArr
                    }
                })
            }

            setTimeout(() => {
                $("#session-entries-list-timer").data("kendoMobileListView").setDataSource(swpSettings.arrEntriesTimers)

                sortOption = swpSettings.timerSortOption.split("_")[1]
                sortOption === "sortDate"
                    ? sortDir = "desc"
                    : sortDir = "asc"

                $("#session-entries-list-timer").data("kendoMobileListView").dataSource.sort([
                    { field: `${sortOption}`, dir: `${sortDir}` },
                ])

                $(`#timersView #userFeedbackMessages`).data("kendoMobileCollapsible").collapse()
                $('.k-overlay').remove()
            }, 350)
        },

        loadBlankTimers: function () {
            let entryCD,
            newDate = moment();

            swpSettings.arrEntriesTimers.length = 0
            arrEntriesTimers = swpSettings.arrEntriesTimers

            for (let i = 0; i < swpSettings.timerSettings.BlankRows; i++) {
                var entry1 = swp.entry.newEntry(),
                    shortdate = moment().format('l').toString();

                if (arrEntriesTimers.filter(x => x.id === entry1.id)) {
                    // entryCD = moment(shortdate).add((new Date()).getMinutes(), 'm').add((new Date()).getHours(), 'h').toDate()
                    entryCD = moment(newDate).add(1, "ms")
                    newDate = entryCD
                }

                /* if time is on then use the time as well */
                if (swpSettings.selectTime == "true" || swpSettings.selectTime == "True") {
                    shortdate = moment(shortdate).add((new Date()).getMinutes(), 'm').add((new Date()).getHours(), 'h').toDate();
                }

                entry1.id = new Date(entryCD).getTime() + swpSettings.swp_username.replace('@', '').replace('.', '')
                entry1.eventDT = shortdate
                entry1.sortDate = moment(swpSettings.defaultDate)
                arrEntriesTimers.push(entry1)
                entry1.timer = "00:00:00"
                entry1.timerId = `${arrEntriesTimers.length}`
                entry1.eventTitle = "Mobile Timer"
                entry1.type = "Timer"
                entry1.notes = `Timer ${arrEntriesTimers.length}`
                entry1.color = "white"
                swpSettings.activeTimerPosition = arrEntriesTimers.length - 1
                entry1.returned = false
                // swp.timer.actions.addTimer()
            }

        },

        loadTimerPreset: function () {
            $.ajax({
                type: "GET",
                url: `${swpSettings.service_url}getpreset/${swpSettings.objTook.token}/${swpSettings.timerSettings.PresetOption}`,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    let shortdate = moment().format('l').toString(),
                        entryCD = moment().format();
                    // let tempArr

                    for (let i = 0; i < data.length; i++) {
                        swp.timer.actions.addTimer()
                        activeTimerPosition = swpSettings.activeTimerPosition
                        entryCD = moment(entryCD).add(1, "s")

                        // if (entryCD == null || entryCD == undefined) {
                        //     entryCD = moment(shortdate).add((new Date()).getMinutes(), 'm').add((new Date()).getHours(), 'h').toDate();
                        // }
                        // let temp = arrEntriesTimers.filter(x => x.id === swpSettings.arrEntriesTimers[activeTimerPosition].id && x !== swpSettings.arrEntriesTimers[activeTimerPosition])
                        if (arrEntriesTimers.filter(x => x.id === swpSettings.arrEntriesTimers[activeTimerPosition].id && x !== swpSettings.arrEntriesTimers[activeTimerPosition]).length > 0) {
                            entryCD = moment(entryCD).add(1, "s")
                        }
                            
                        // swpSettings.arrEntriesTimers[activeTimerPosition] = swp.utilities.setTimeToLocal(swpSettings.arrEntriesTimers[activeTimerPosition], entryCD);
                        swpSettings.arrEntriesTimers[activeTimerPosition].eventDT = moment(entryCD).format()
                        swpSettings.arrEntriesTimers[activeTimerPosition].sortDate = entryCD
                        swpSettings.arrEntriesTimers[activeTimerPosition].id = new Date(entryCD).getTime() + swpSettings.swp_username.replace('@', '').replace('.', '')

                        swpSettings.arrEntriesTimers[activeTimerPosition].client = data[i].Client
                        if (swpSettings.arrEntriesTimers[activeTimerPosition].client === null) swpSettings.arrEntriesTimers[activeTimerPosition].client = ""
                        swpSettings.arrEntriesTimers[activeTimerPosition].matter = data[i].Matter
                        if (swpSettings.arrEntriesTimers[activeTimerPosition].matter === null) swpSettings.arrEntriesTimers[activeTimerPosition].matter = ""
                        swpSettings.arrEntriesTimers[activeTimerPosition].custom1 = data[i].Custom1
                        swpSettings.arrEntriesTimers[activeTimerPosition].custom2 = data[i].Custom2
                        swpSettings.arrEntriesTimers[activeTimerPosition].custom3 = data[i].Custom3
                        swpSettings.arrEntriesTimers[activeTimerPosition].custom4 = data[i].Custom4
                        swpSettings.arrEntriesTimers[activeTimerPosition].custom5 = data[i].Custom5
                        swpSettings.arrEntriesTimers[activeTimerPosition].duration = data[i].Hours
                        swpSettings.arrEntriesTimers[activeTimerPosition].narrative = data[i].Narrative
                        swpSettings.arrEntriesTimers[activeTimerPosition].returned = data[i].Returned
                        swpSettings.arrEntriesTimers[activeTimerPosition].type = "Timer"

                        if (data[i].Timer == null || data[i].Timer == "undefined") data[i].Timer = "00:00:00"
                        swpSettings.arrEntriesTimers[activeTimerPosition].timer = data[i].Timer
                        if (data[i].Notes == "" || data[i].Notes == null || data[i].Notes == "undefined") data[i].Notes = `Timer ${swpSettings.arrEntriesTimers.length}`
                        swpSettings.arrEntriesTimers[activeTimerPosition].notes = data[i].Notes
                        if (data[i].Color == "" || data[i].Color == null || data[i].Color == "undefined") data[i].Color = `white`
                        swpSettings.arrEntriesTimers[activeTimerPosition].color = data[i].Color

                        swpSettings.arrEntriesTimers[activeTimerPosition].status = data[i].Status
                        swpSettings.arrEntriesTimers[activeTimerPosition].statusImage = `img/${data[i].Status}.png`

                        // swp.utilities.setTimeToLocal(arrEntriesTimers[activeTimerPosition], data[i].CreateDate)
                    }
                },
                error: function (errMsg) {
                }
            })

        },

        loadPresetsButton: function () {
            swpSettings.loadTimerPreset = true
            location.href = "#views/settingsViews/timersSettings/timersPresetsView.html"
        },

        loadPreviousSession: function () {
            //set settings to load previous session
            // swpSettings.timerSettings.LoadRadio = 4
            // //updates settings on server for user
            // swp.timer.actions.updateTimerSettings()

            $.ajax({
                type: "GET",
                url: `${swpSettings.service_url}previousSession/${swpSettings.objTook.token}`,
                contentType: "application/json",
                dataType: "json",
                success: (data) => {
                    let entry, entryCD, shortdate;
                    let tempArr = [],
                        newDate = moment();

                    swpSettings.arrEntriesTimers.length = 0

                    for (let i = 0; i < data.length; i++) {
                        entry = swp.entry.newEntry()
                        entryCD = moment(data[i].CreateDate)
                        shortdate = moment().format('l').toString()

                        // entryCD = moment(shortdate).add((new Date()).getMinutes(), 'm').add((new Date()).getHours(), 'h').toDate()
                        if (tempArr.filter(x => x.id === entry.id)) {
                            // entryCD = moment(shortdate).add((new Date()).getMinutes(), 'm').add((new Date()).getHours(), 'h').toDate()
                            entryCD = moment(newDate).add(1, "ms")
                            newDate = entryCD
                        }
                        
                        // entry = swp.utilities.setTimeToLocal(entry, entryCD)
                        entry.createDate = new Date(entryCD)
                        entry.id = new Date(entryCD).getTime() + swpSettings.swp_username.replace('@', '').replace('.', '')
                        // entry.id = data[i].EventKey.split("-")[0] + swpSettings.swp_username.replace('@', '').replace('.', '')
                        entry.eventDT = moment(entryCD).format()
                        entry.sortDate = entryCD
                        entry.origId = data[i].ID
                        entry.client = data[i].Client //changing this back to client / matter from CientName and parsing in template.
                        entry.matter = data[i].Matter //assigning the number only here will cause inconsistency with MRUs /saved client matters
                        entry.custom1 = data[i].Custom1
                        entry.custom2 = data[i].Custom2
                        entry.custom3 = data[i].Custom3
                        entry.custom4 = data[i].Custom4
                        entry.custom5 = data[i].Custom5
                        entry.duration = data[i].Hours
                        entry.narrative = data[i].Narrative
                        entry.eventTitle = "Mobile Timer"
                        
                        entry.color = data[i].Color
                        entry.notes = data[i].Notes
                        entry.returned = data[i].Returned
                        entry.type = "Timer"
                        // let time = data[i].Hours
                        //time = moment.duration(data[i].Hours, "minutes")

                        // let sec = Number(time[2])
                        // let min = Number(time[1])
                        // let hr = Number(time[0])

                        let time = data[i].Hours
                        time = moment.duration(data[i].Hours, "minutes")

                        let sec = time.seconds()
                        let min = time.minutes()
                        let hr = time.hours()

                        hr = (hr < 10 ? "0" : "") + hr;
                        min = (min < 10 ? "0" : "") + min;
                        sec = (sec < 10 ? "0" : "") + sec;


                        entry.timer = `${hr}:${min}:${sec}`

                        //Timer check and set logic
                        if (entry.timer === "" || entry.timer === null) entry.timer = "00:00:00"
                        if (entry.color === "" || entry.color === null) entry.color = "white"
                        if (entry.notes === "" || entry.notes === null) entry.notes = `Timer ${i + 1}`
                        if (entry.client === null) entry.client = ""
                        if (entry.matter === null) entry.matter = ""
                        

                        entry.status = data[i].Status
                        entry.statusImage = `img/${data[i].Status}.png`

                        tempArr.push(entry)
                        // swpSettings.arrEntriesTimers.push(entry)
                    }
                    
                    swpSettings.arrEntriesTimers = tempArr
                    $("#session-entries-list-timer").data("kendoMobileListView").setDataSource(swpSettings.arrEntriesTimers)
                    swp.timersView.showTimers()

                    $(`#timersView #userFeedbackMessages`).data("kendoMobileCollapsible").collapse()
                    $('.k-overlay').remove()
                },
                error: (errMsg) => {

                }
            })
        },

        removeAllTimers: function () {
            console.log("removeAllTimers function hit")
            swpSettings.arrEntriesTimers.length = 0
            //$("#session-entries-list-timer").data("kendoMobileListView").setDataSource(arrEntriesTimers)
            $("#session-entries-list-timer").data("kendoMobileListView").setDataSource(swpSettings.arrEntriesTimers)

        },

        removeZeroTimeTimers: function () {
            console.log("removeZeroTimeTimers function hit")
            swpSettings.arrEntriesTimers = swpSettings.arrEntriesTimers.filter(x => x.timer !== "00:00:00")
            $("#session-entries-list-timer").data("kendoMobileListView").setDataSource(swpSettings.arrEntriesTimers)
        },

        roundTimer: function (timer, increment) {
            log.debug('rounding increment: ' + increment);

            if (increment === '' || increment === undefined)
                increment = swpSettings.set_DefaultIncrement;

            return swp.shared.Rounding.roundTimer(timer, increment);
        },

        selectTimer: function (id) {
            let index = swpSettings.arrEntriesTimers.indexOf(swpSettings.arrEntriesTimers.find(x => x.id === id))
            swpSettings.activeTimerPosition = index
        },

        startTimer_Phone: function () {
            arrEntries = swpSettings.arrEntries;
            activeEntryPosition = swpSettings.activeEntryPosition;

            arrEntriesTimers = swpSettings.arrEntriesTimers
            activeTimerPosition = swpSettings.activeTimerPosition

            // Your moment
            var mmt = moment(),
                // Your moment at midnight
                mmtMidnight = mmt.clone().startOf('day'),
                // Difference in minutes
                diffMinutes = mmt.diff(mmtMidnight, 'minutes'),
                eventDateIsSameDay = moment(arrEntriesTimers[activeTimerPosition].eventDT).isSame(mmtMidnight, "day"),
                a = arrEntriesTimers[activeTimerPosition].get("status");

            if (a != "Released" && a != "Posted" && eventDateIsSameDay === true) {
                if (swpSettings.set_MultiTimers == "false" || swpSettings.set_MultiTimers == "False") {
                    for (var b = 0; b < arrEntriesTimers.length; b++) {
                        if (arrEntriesTimers[b].timerRunning == true && arrEntriesTimers[b].id != arrEntriesTimers[activeTimerPosition].id) {
                            log.debug("stopping a timer")
                            module.stopTimer_PhoneTemplate(arrEntriesTimers[b].id)
                        };
                    };
                };

                $(`#timersView #${arrEntriesTimers[activeTimerPosition].id} .timerValueBorder`).css("background-color", "var(--customRed)")
                $(`#timersView #${arrEntriesTimers[activeTimerPosition].id} #timerTemplateTimerValue`).css("color", "white")
                $(`#timersView #${arrEntriesTimers[activeTimerPosition].id} #timerTemplateTimerValue`).css("font-weight", "700")
                $("#btnStartTimer-phone").css("display", "none");
                $("#btnPauseTimer-phone").css("display", "inline-block");
                arrEntriesTimers[activeTimerPosition].timerRunning = true;

                //let c = arrEntries[activeTimerPosition].get("timerStart")
                let d = arrEntriesTimers[activeTimerPosition].get("timer")
                //fix for new entries not having a timer value defined, this may be fixed by the same bug preventing the timer value from displaying for saved items from the allsaved.
                if (!d) d = arrEntriesTimers[activeTimerPosition]
                c = calcTimerStart(d);

                if (arrEntriesTimers[activeTimerPosition].timeStamp === "") {
                    arrEntriesTimers[activeTimerPosition].timeStamp = moment().format("h:mm A")
                    //arrEntries[activeTimerPosition].set("timerStart", c)
                };
                //moved outside to fix start/stop adding time between stop / start
                arrEntriesTimers[activeTimerPosition].timerStart = c

                let e = arrEntriesTimers[activeTimerPosition].id,
                    f = self.setInterval("swp.timer.actions.timerTickPhone('" + e + "')", 1e3);

                arrEntriesTimers[activeTimerPosition].timerId = f

            }
        },

        stopAllTimers_Phone: function () {
            arrEntriesTimers = swpSettings.arrEntriesTimers;

            $.each(arrEntriesTimers, function (i, item) {

                $("#btnStartTimer-phone").css("display", "inline-block")
                $("#btnPauseTimer-phone").css("display", "none")

                var iInt = arrEntriesTimers[i].get("timerId")
                arrEntriesTimers[i].set("timerRunning", false)
                window.clearInterval(iInt)

                arrEntriesTimers[i].set("timerRunning", false)
                arrEntriesTimers[i].set("timerId", 0)

                swp.timer.actions.stopTimer_PhoneTemplate(item.id)

            })
        },

        stopTimer_Phone: function () {
            arrEntriesTimers = swpSettings.arrEntriesTimers;
            activeTimerPosition = swpSettings.activeTimerPosition;
            activeEntryPosition = swpSettings.activeEntryPosition;

            if ($("#btnPauseTimer-phone")) {
                $("#btnStartTimer-phone").css("display", "inline-block");
                $("#btnPauseTimer-phone").css("display", "none");
            };
            
            var a = arrEntriesTimers[activeTimerPosition].timerId;
            arrEntriesTimers[activeTimerPosition].timerRunning = false;
            window.clearInterval(a);

            arrEntriesTimers[activeTimerPosition].timer === "00:00:00"
                ? ($(`#timersView #${arrEntriesTimers[activeTimerPosition].id} #timerTemplateTimerValue`).css("font-weight", "100"),
                    $(`#timersView #${arrEntriesTimers[activeTimerPosition].id} #timerTemplateTimerValue`).css("color", "black"),
                    $(`#timersView #${arrEntriesTimers[activeTimerPosition].id} .timerValueBorder`).css("background-color", "inherit"))
                : ($(`#timersView #${arrEntriesTimers[activeTimerPosition].id} #timerTemplateTimerValue`).css("font-weight", "700"),
                    $(`#timersView #${arrEntriesTimers[activeTimerPosition].id} #timerTemplateTimerValue`).css("color", "black"),
                    $(`#timersView #${arrEntriesTimers[activeTimerPosition].id} .timerValueBorder`).css("background-color", "inherit"));

            if (swpSettings.set_TimerAutoSave == true) {
                swp.timeentry.actions.saveEntry_Phone(true) //pass true to override validation
            }
        },

        stopTimer_PhoneTemplate: function (a) {

            arrEntriesTimers = swpSettings.arrEntriesTimers;

            for (var b, c = 0; c < arrEntriesTimers.length; c++) {
                if (arrEntriesTimers[c].id == a) {
                    b = c
                };
            };

            //$("#liDuration-phone").attr("href", "#views/timeEntryViews/durationView.html");
            var d = arrEntriesTimers[b].timerId

            setTimeout(() => {
                // Your moment
                var mmt = moment()
                // Your moment at midnight
                var mmtMidnight = mmt.clone().startOf('day')
                var eventDateIsSameDay = moment(arrEntriesTimers[b].eventDT).isSame(mmtMidnight, "day")

                $(`#timersView #${arrEntriesTimers[b].id} .timerValueBorder`).css("background-color", "inherit")
                $(`#timersView #${arrEntriesTimers[b].id} #timerTemplateTimerValue`).css("color", "black")

                if (arrEntriesTimers[b].timer === "00:00:00") {
                    $(`#timersView #${arrEntriesTimers[b].id} #timerTemplateTimerValue`).css("font-weight", "100")
                } else {
                    $(`#timersView #${arrEntriesTimers[b].id} #timerTemplateTimerValue`).css("font-weight", "700")
                }

                //logic here to change style to past day style on timer stop (midnight check?)
                if (eventDateIsSameDay === false) {
                    $(`#timersView #${arrEntriesTimers[b].id} .timerValueBorder`).css("background-color", "var(--customGrey)")
                    $(`#timersView #${arrEntriesTimers[b].id} #timerTemplateTimerValue`).css("font-weight", "700")
                }

            }, 150)

            return arrEntriesTimers[b].timerRunning = false, window.clearInterval(d), false
        },

        timerColorUpdate: function () {
            let color = swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].color;

            if (color !== "") {
                $("#timeEntryView #timerColorIndicator").css("background-color", color)
                $(`#timersView #timerColorDisplay-${currentEntryIndicatorId}`).css("background-color", color)
                swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].color = color
            }
        },

        timerNameCheck: function () {
            let view = window.location.hash.split("/")[window.location.hash.split("/").length - 1].split(".")[0]
            let timerName = $(`#${view} #timerNameInput`).val()
            let regex = /^Timer [0-9]*$/g
            let found = timerName.match(regex)

            if (found) {
                $(`#${view} #timerNameInput`).val("")
            }
        },

        timerNameColorUpdate: function () {
            let notes = swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].notes,
                color = swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].color

            if (notes !== "") {
                $("#timeEntryView #pageTitleTop").text(`${notes}`)
                $(`#timersView #${currentEntryIndicatorId} #timerTemplateTimerName`).text(notes)
                swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].notes = notes
            }
            if (color !== "") {
                $("#timeEntryView #timerColorIndicator").css("background-color", color)
                $(`#timersView #timerColorDisplay-${currentEntryIndicatorId}`).css("background-color", color)
                swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].color = color
            }
        },

        timerTickPhone: function (a) {
            let found = arrEntriesTimers.find(e => e.id == a)
            try {
                found.tickTimer()
            } catch (err) {
                console.log("timer failed to tick", err)
            }
        },

        togglePreviousSession: function () {
            $(`#timersView #userFeedbackMessages .km-collapsible-content`).children().hide()
            $("#timersView #loadPreviousSessionButtons .loadPreviousHeaderText").text("Load Previous Session?")
            $(`#timersView #loadPreviousCollapsibleHeader`).show()
            $("<div class='k-overlay' id='greyOverlay'></div>").appendTo($(`#timersView .km-content`))
            $("<div class='k-overlay' id='greyOverlay'></div>").appendTo($(`#timersView #divStackButtons-phone`))
            swp.utilities.toggleUserFeedbackMessages(this)
            $("#timersView .km-collapsible-header .km-x-swp").css("top", "0em")
        },

        toggleTimerCheck: function (e) {
            let id = e.sender.element[0].id
            swpSettings.activeTimerPosition = swpSettings.arrEntriesTimers.map((e) => { return e.id }).indexOf(id)
            //swpSettings.activeTimerPosition = swpSettings.arrEntriesTimers.map((e) => { return e.id.split("t")[0] }).indexOf(id)
            if (swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].timerRunning === true) {
                module.stopTimer_Phone()
                // $("#session-entries-list-timer").data("kendoMobileListView").refresh()
            } else if (swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].timerRunning === false)
                module.startTimer_Phone()
        },

        toggleTimerColor: function (e) {
            if (window.location.hash.includes("timersView") === true) swpSettings.activeTimerPosition = swpSettings.arrEntriesTimers.indexOf(swpSettings.arrEntriesTimers.find(entry => entry.id == currentEntryIndicatorId))
            // swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].color = e.target.css("backgroundColor")
            swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].color =e.target[0].id.split('Color')[1]
            // updating color indicator on toggle, may want to add selected viewport and then set on header close
            $("#timerCollapsibleHeader #timerColors a").css("border", "solid .5px black")
            e.sender.element.css("border", "solid 3px black")
            module.timerColorUpdate()
        },

        toggleTimerDropdown: function (a) {
            a.preventDefault()
            let view = window.location.hash.split("/")[window.location.hash.split("/").length - 1].split(".")[0]

            if (view === "timersView" && swpSettings.timerDropDown.open === false) {
                if (this.element.context) {
                    currentEntryIndicatorId = this.element.context.id.split("-")[1]
                } else if (this.element) {
                    currentEntryIndicatorId = this.element[0].id.split("-")[1]
                }

                swpSettings.activeTimerPosition = swpSettings.arrEntriesTimers.indexOf(swpSettings.arrEntriesTimers.find(x => x.id === currentEntryIndicatorId))
                $("#timersView #fieldClientCollapsible").attr("placeholder", swpSettings.clientLabel)
                $("#timersView #fieldMatterCollapsible").attr("placeholder", swpSettings.matterLabel)
            }

            kendo.bind($("#timersView #timerCollapsibleHeader"), swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition])
            $(`#${view} #userFeedbackMessages .km-collapsible-content`).children().hide()
            $(`#${view} #timerCollapsibleHeader`).show()

            if ($(`#${view} #userFeedbackMessages`).hasClass("km-collapsed") === true) {
                $(`#${view} #userFeedbackMessages`).data("kendoMobileCollapsible").expand()
                $("#timersView #timerCollapsibleHeader #timerColors a").css("border", "solid .5px black")
                document.querySelectorAll("#timersView #timerCollapsibleHeader #timerColors a").forEach(x => $(x).css("background-color") === swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].color && (x.style.border = "solid 3px black"))
                $("<div class='k-overlay' id='greyOverlay'></div>").appendTo($(`#${view} .km-content`))
                $("<div class='k-overlay' id='greyOverlay'></div>").appendTo($(`#${view} #divStackButtons-phone`))
                swpSettings.timerDropDown.open = true
                swpSettings.timerDropDown.entryId = currentEntryIndicatorId
                setTimeout(() => {
                    $("#greyOverlay").click(swp.utilities.closeTimerDropDown)
                    $("#greyOverlayFoot").click(swp.utilities.closeTimerDropDown)
                }, 300)
            } else {
                $(`#${view} #userFeedbackMessages`).data("kendoMobileCollapsible").collapse()
                $('.k-overlay').remove()
            }
            $(`#${view} .km-collapsible-header .km-x-swp`).css("top", "0em")

        },

        toggleTimerPhone: function (a) {
            log.debug("show phone timer")
            swpSettings.bTimers = true

            //remove entry from arrEntries and update activeEntryPosition
            let tempEntry = swpSettings.arrEntries.splice(swpSettings.activeEntryPosition, 1)[0]
            swpSettings.activeEntryPosition = swpSettings.arrEntries.length - 1
            if ($("#session-entries-phone").data("kendoMobileListView")) $("#session-entries-phone").data("kendoMobileListView").setDataSource(swpSettings.arrEntries)
            //Add entry to arrEntriesTimers and update activeTimerPosition
            swpSettings.arrEntriesTimers.push(tempEntry)
            swpSettings.activeTimerPosition = swpSettings.arrEntriesTimers.length - 1
            activeTimerPosition = swpSettings.activeTimerPosition

            setTimeout(() => {
                swpSettings.arrEntriesTimers[activeTimerPosition].timer = "00:00:00"
                swpSettings.arrEntriesTimers[activeTimerPosition].timerId = `${swpSettings.arrEntriesTimers.length}`
                swpSettings.arrEntriesTimers[activeTimerPosition].eventTitle = "Mobile Timer"
                swpSettings.arrEntriesTimers[activeTimerPosition].color = 'white'
                swpSettings.arrEntriesTimers[activeTimerPosition].notes = `Timer ${swpSettings.arrEntriesTimers.length}`
                // need to add update to sortDate here as well? maybe it will update with the eventDT change watcher on the viewmodel?
                swpSettings.arrEntriesTimers[activeTimerPosition].sortDate = moment()
                swpSettings.arrEntriesTimers[activeTimerPosition].eventDT = moment().format()

                module.timerNameColorUpdate()
                module.displayTimerPhone(a)
                module.startTimer_Phone()
            }, 500)
        },

        updateTimerSettings: function () {
            $.ajax({
                type: "POST",
                url: swpSettings.service_url + `savetimersettings`,
                data: JSON.stringify(swpSettings.timerSettings),
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    console.log('savetimersettings', data)
                    $("#timersSettingsView #openBlankTimersView").text(`Blank Timers: (${swpSettings.timerSettings.BlankRows})`)
                    $("#timersSettingsView #openLastPostedCMView").text(`Last Posted Client Matters: (${swpSettings.timerSettings.LastPostedClientMatterNumbers})`)
                },
                failure: function (errMsg) {
                    alert(errMsg)
                }
            })
        },
    }

    return module;

}();
