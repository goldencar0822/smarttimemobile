//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.entry) { swp.entry = {} }

swp.entry = function () {
    "use strict";
    let service_url = swpSettings.service_url,
        arrEntriesTimers,
        activeTimerPosition,
        oldStatus;

    var module = {

        addNewCal: function(e) {
            console.log('addNewCal function hit')
            swpSettings.defaultDate = moment(e)._d;
            module.addNewTimeEntry(e)
        },

        addEntry: function() {
            
            let defaultDate = swpSettings.defaultDate,
                entry1 = module.newEntry();

            if (defaultDate === "") {
                defaultDate = moment()._d
            }

            swpSettings.arrEntries.push(entry1)
            swpSettings.activeEntryPosition = swpSettings.arrEntries.length - 1

            if (defaultDate !== "") {
                log.debug("Adding new entry for date " + moment(defaultDate).toString())
                swp.debugLogView.addToDebugLog("Adding new entry for date " + moment(defaultDate).toString())
                var shortdate = moment(defaultDate).format()
                // var shortdate = moment(defaultDate).format("l").toString()

                if (swpSettings.selectTime == "true" || swpSettings.selectTime == "True") {
                    //potential error
                    shortdate = moment(shortdate).add((new Date).getMinutes(), "m").add((new Date).getHours(), "h").toDate()
                }

                //entry1.set("eventDT", shortdate) <- from old code, may need to convert back to this format here but this is causing stack to not sort properly, altering to (Tue Dec 24 2019 04:00:00 GMT-0700 (Mountain Standard Time)) format
                entry1.set("eventDT", shortdate)
                entry1.set("sortDate", moment(defaultDate))
                entry1.set("type", "Mobile")
                entry1.set("typeImage", "MobileEntry.png")
            }

            kendo.bind($("#timeEntryView"), swpSettings.arrEntries[swpSettings.activeEntryPosition])
            $(".te-button-phone").css("color", "black")
            swp.stackView.loadTimeForm()
            module.updateReferences()
            $("#timeEntryView #fieldNarrative-phone").removeAttr('readonly')

            if (swpSettings.set_DurationInUnits == true) {
                $("#fieldDurationUnits-phone").show()
            } else {
                $("#fieldDurationTime-phone").show()
            }

            $("#toggleBillingGuideInfo").hide()

            $("#fieldTimestamp-phone").hide()
            $("#lblHours-phone").text("Hours:")

            log.debug("set_Timers = " + swpSettings.set_Timers.toString())
            swp.debugLogView.addToDebugLog("set_Timers = " + swpSettings.set_Timers.toString())
            ////toggle Timer show-hide check
            //if ("true" !== swpSettings.set_Timers && swpSettings.set_Timers !== true) {
            //    log.debug("Exposing timer button")
            //    $("#timeEntryView #btnTimerToggle-phone").show()
            //}
            if (!swpSettings.arrEntries[swpSettings.activeEntryPosition].durationTimeDisplay && !swpSettings.arrEntries[swpSettings.activeEntryPosition].duration) {
                $("#timeEntryView #btnTimerToggle-phone").show()
            }

            $("#spanTimerPhone").hide()

            //trying to set a timeout here to avoid setDatasource error (entry.js line 82 $("#session-entries-phone").data("kendoMobileListView").setDataSource(swpSettings.arrEntries)) on initial selection of date with no entries in it, may actually have to alter the sequence of calls so the kendo widget is available
            setTimeout(() => {
              if (typeof $("#session-entries-phone").data("kendoMobileListView") !== "undefined") {
                $("#session-entries-phone").data("kendoMobileListView").setDataSource(swpSettings.arrEntries)
                $("#session-entries-phone").data("kendoMobileListView").refresh()
                $("#divTimeForm-Phone textarea").blur()
              } 
            //   else {
            //     //MW:Trying to avoit the error mentioned above when clicking from the scheduler without
            //     //time entry or stack being initialized, otherwise error forces the user back to login screen
            //     // if (location.hash === "#views/homeViews/homeView.html") {
            //     //   location.href = "#views/timeEntryViews/timeEntryView.html"
            //     // }
            //   }

            }, 500)
        },

        addNewEntry: function() {

            log.debug('adding new entry')
            swp.debugLogView.addToDebugLog("addNewEntry function hit")

            swp.timeentry.actions.clearValidation()
            swp.entry.setupBillDisplay("empty <br />", '#billingGuidelines')

            if (swpSettings.bTimers === true)
                swp.timer.actions.addTimer()
            else
                module.addEntry()

            //UpdateNavTitle();
            setTimeout(() => {
                swp.utilities.refreshMatters()
                swp.utilities.refreshCustomMenus()
            }, 300)
            

        },

        addNewTimeEntry: function(e) {
            // e.preventDefault()
            console.log('addNewTimeEntry function hit')

            setTimeout(() => {
                let view = window.location.hash.split("/")[window.location.hash.split("/").length - 1].split(".")[0]
               
                if (view === "homeView" || view === "stackView") {
                    swpSettings.bTimers = false
                    $("#timeEntryView #pageTitleTop").text("Time Entry")
                    $(`#timeEntryView #backNavButton`).show()
                    $(`#timeEntryView #timerColorIndicatorTouch`).hide()
                    swp.timeentry.actions.clearValidation()
                    
                    setTimeout(() => location.href = "#views/timeEntryViews/timeEntryView.html", 150)
                } else if (view === "timersView"){
                    setTimeout(() => location.href = "#views/timersView.html", 150)
                }
            
                module.addNewEntry()
            }, 500)

        },

        checkTotalDayHours: function (data) {
            let tempTotal = +swpSettings.totalCurDayHrs + +data.Hours,
                maxHrs = +swpSettings.set_MaxDayHours;
            if (tempTotal > maxHrs) {
                let timeOver = (tempTotal - maxHrs).toFixed(2);
                $("#timeEntryView #userFeedbackMessages .km-collapsible-content").children().hide();
                $("#timeEntryView #userFeedbackMessages #userFeedbackMessageContent").text(`Max Hours Per Day Exceeded By ${timeOver} Hours`);
                $("#timeEntryView #userFeedbackMessages #userFeedbackMessageContent").show();
                swp.utilities.toggleUserFeedbackMessages(this);
            }
        },

        checkWorkingAtty: function (item, e, source, status) {
            if (status === "new" || status === "saved") {
                log.debug("URL: " + swpSettings.service_url + "workingatty")
                swp.debugLogView.addToDebugLog("URL: " + swpSettings.service_url + "workingatty")

                var objWA = new kendo.data.ObservableObject({
                    Client: item.client,
                    Matter: item.matter,
                    Username: swpSettings.swp_username
                })

                if (objWA.Matter === "") objWA.Matter = null

                $.ajax({
                    type: "POST",
                    url: swpSettings.service_url + "workingatty",
                    data: JSON.stringify(objWA),
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        console.log("data", data)
                        if (!data) return

                        var result = data,
                            obj = jQuery.parseJSON(data);
                            
                        log.debug(obj)
                        swp.debugLogView.addToDebugLog(obj)

                        source.matterIncrement = obj.Increment.toString().replace(",", ".")
                        result = obj.WorkingAtty

                        source.blockBill = obj.BlockBill
                        source.billType = obj.BillType

                        if (typeof obj.EthicalWall !== 'undefined') source.bEthicalWall = obj.EthicalWall
                        if (typeof obj.BadWords !== 'undefined') source.badWords = obj.BadWords.toString().toLowerCase()

                        if (result === false) {
                            source.bValidAtty = false
                            let view = window.location.hash.split("/")[window.location.hash.split("/").length - 1].split(".")[0]

                            if (view === "timeEntryView") {
                                /* only show message if we are on the time entry form */
                                $("#timeEntryView #userFeedbackMessages .km-collapsible-content").children().hide()
                                $("#timeEntryView #userFeedbackMessages #userFeedbackMessageContent").text(swpSettings.error_WorkingAtty);
                                $("#timeEntryView #userFeedbackMessages #userFeedbackMessageContent").show()
                                swp.utilities.toggleUserFeedbackMessages(this)
                                // $("#timeEntryView #fieldNarrative-phone").attr('readonly', true)
                            }

                        } else {

                            source.bValidAtty = true
                            /* clear the error */
                            $('#divValidation').text('')
                            $('#divValidation_timer').text('')
                        }
                    },
                    failure: function (errMsg) {
                        alert(errMsg)
                    }
                })

            }
        },

        entryType: function (x) {
            if (x.type == "Timer" || x.timerEntry == true) {
                $(`#stackView #${x.id} #stackTimerIdentifier`).css("display", "inline-flex")
                $(`#stackView #${x.id} #stackTimeEntryIdentifier`).css("display", "none")
                $(`#stackView #${x.id} #stackTimeCaptureIdentifier`).css("display", "none")
            } else if (x.type == "Mobile") {
                $(`#stackView #${x.id} #stackTimerIdentifier`).css("display", "none")
                $(`#stackView #${x.id} #stackTimeEntryIdentifier`).css("display", "inline-flex")
                $(`#stackView #${x.id} #stackTimeCaptureIdentifier`).css("display", "none")
            } else {
                $(`#stackView #${x.id} #stackTimerIdentifier`).css("display", "none")
                $(`#stackView #${x.id} #stackTimeEntryIdentifier`).css("display", "none")
                $(`#stackView #${x.id} #stackTimeCaptureIdentifier`).css("display", "inline-flex")
            }
        },
        // "http://www.stdemo.smarttimeapps.com:8080/wsRest.svc/"
        //allSaved_Phone
        getAllSavedEntries: function() {
            $.ajax({
                type: 'POST',
                traditional: true,
                url: swpSettings.service_url + "allsaved",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(swpSettings.objTook),
                dataType: "json",
                success: function (data) {
                    console.log('success allSaved Call', data)
                    log.debug('Received ' + data.length.toString() + ' saved items.')
                    swp.debugLogView.addToDebugLog('getAllSavedEntries success: ', data)

                    let arrEntriesTemp = [],
                        arrTimersTemp = [];

                    for (var i = 0; i < data.length; i++) {
                        var entry = module.newEntry(),
                            entryDTF = data[i].DateToFrom;

                        if (entryDTF != null) {
                            entry = swp.utilities.setTimeToLocal(entry, entryDTF);
                            entry.set("eventDT", moment(entryDTF).format("l").toString());
                            entry.set("sortDate", moment(entryDTF));
                        }

                        entry.set("id", data[i].ID);
                        entry.set("origId", data[i].ID);
                        entry.set("client", data[i].ClientName);
                        entry.set("matter", data[i].MatterName);
                        entry.set("custom1", data[i].Custom1);
                        entry.set("custom2", data[i].Custom2);
                        entry.set("custom3", data[i].Custom3);
                        entry.set("custom4", data[i].Custom4);
                        entry.set("custom5", data[i].Custom5);
                        entry.set("duration", data[i].Hours);
                        entry.set("eventTitle", data[i].eventTitle);
                        entry.set("narrative", data[i].TimeEntryDesc);
                        entry.set("timer", data[i].Timer);
                        entry.set("color", data[i].Color);
                        entry.set("notes", data[i].Notes);
                        // entry.set("type", data[i].Type);

                        entry.set("status", data[i].Status);
                        entry.set("statusImage", "img/" + data[i].Status + ".png");

                        entry = swp.utilities.setTimeToLocal(entry, data[i].DateToFrom)

                        /* Replaced with buttonReleaseClass()/buttonsSaveDeleteClass() functions
                        if (entry.get("status") == "Posted" || entry.get("status") == "Released"){
                        entry.set("buttonClass", "button-div disabled-button");
                        entry.set("buttonClassTimer", "button-timer button-div disabled-button");}
                        */

                        if (entry.timer === "" || entry.timer === null) {
                            arrEntriesTemp.push(entry);
                        } else {
                            arrTimersTemp.push(entry);
                        }

                        //arrEntriesTemp.push(entry);

                        swpSettings.arrEntries = arrEntriesTemp
                        swpSettings.arrEntriesTimers = arrTimersTemp

                        //fix for stackView loading all entries
                        swpSettings.defaultDate = moment()._d;
                        // swp.homeView.daySearch_Phone(swpSettings.defaultDate)
                    }

                    //if (arrEntriesTemp.length === 0) {
                    //    var entry1 = module.newEntry();
                    //    entry1.set("eventDT", new Date());
                    //    entry1.set("sortDate", new Date());
                    //    arrEntriesTemp.push(entry1);
                    //    swpSettings.arrEntries = arrEntriesTemp
                    //}
                    // swp.dataSources.stackDataSource.sync()
                    if (typeof $("#session-entries-phone").data("kendoMobileListView") !== "undefined") {
                        $("#session-entries-phone").data("kendoMobileListView").setDataSource(swpSettings.arrEntries)
                        //swpSettings.arrEntries = []
                    }

                    if (typeof $("#session-entries-list-timer").data("kendoMobileListView") !== "undefined") {
                        $("#session-entries-list-timer").data("kendoMobileListView").setDataSource(swpSettings.arrEntriesTimers)
                    }

                    //var buttongroup = $("#select-entries-sort").data("kendoMobileButtonGroup");
                    //buttongroup.select(0); /* default to date */
                },
                error: function (x, textStatus, e) {
                    kendo.ui.progress($('#scheduler-phone'), false);
                }
            })
        },

        newEntry: function() {

            var dt = new Date()
            var _id = dt.getTime();
            let c1 = swpSettings.custom1
            let c2 = swpSettings.custom2
            let c3 = swpSettings.custom3
            let c4 = swpSettings.custom4
            let c5 = swpSettings.custom5

            var viewModel = new kendo.data.ObservableObject({
                badWords: "",
                billType: "",
                blockBill: "", /* if this is anything but blank it means we need to enforce block billing */
                cleanId: function () { return this.id.replace(/\+/g, "").replace(/=/g, "").replace(/</g, "").replace(/>/g, "") }, //clean id which can be used in html
                client: "",
                clientName: function () {
                    if (this.client !== null && this.client.toString().indexOf("|") != -1)
                        return this.client.toString().split(" | ")[1];
                    else
                        return this.client;  /* may not have | if user slugged it in (offline mode) */
                },
                clientNum: function () {
                    if (this.client !== null && this.client.toString().indexOf("|") != -1)
                        return this.client.toString().split(" | ")[0] + " |";
                    else
                        return "";
                },
                createDate: dt,
                custom1: c1.set_Custom1Default,
                custom2: c2.set_Custom2Default,
                custom3: c3.set_Custom3Default,
                custom4: c4.set_Custom4Default,
                custom5: c5.set_Custom5Default,
                dateTime: function () { /* date and time (if time flag is set), otherwise just returns long date */
                    if (this.eventDT != null && this.eventDT != "" && typeof this.eventDT != "undefined") {
                        if (swpSettings.selectTime == "true" || swpSettings.selectTime == "True") {
                            /* don't parse time if it's 0:00*/
                            if (moment(this.eventDT).format('H') !== "0" || moment(this.eventDT).format('m') !== "0") {
                                return moment(this.eventDT).format('MMMM D, YYYY  h:mm A').toString();
                            }
                            else {
                                return this.eventDate();
                            }

                        }
                        else {
                            return this.eventDate();
                        }
                    }
                    else {
                        return "";
                    }
                },
                description: "",
                duration: "",
                durationUnitDisplay: "",
                durationTimeDisplay: "",
                durationFont: "",
                durationSize: "",
                durationValue: function (e) {
                    let dur
                    /* 6/30/2014 -- show timer even if it is not running */
                    if ((this.timerId === '') || this.status === 'Posted' || this.status === 'Released' || (this.timerId !== '' && this.color === "")) {
                        swpSettings.decimalComma === true
                        ? dur = this.duration.replace(".", ",")
                        : dur = this.duration
                    } else {
                        dur = '';
                    } 
                    return dur
                },
                eventDate: function () {
                    //add logic here to display time field for time captures and time entries.
                    if (this.eventDT != null && this.eventDT != "" && typeof this.eventDT != "undefined") {
                        let evTime = moment(this.eventDT)
                        evTime.hours() == 0 && evTime.minutes() == 0 && evTime.seconds() == 0 && this.timer !== null
                            ? evTime = moment(this.eventDT).format(`${swpSettings.dateFormat}`).toString()
                            : evTime = moment(this.eventDT).format(`${swpSettings.dateFormat} @ h:mm a`).toString()

                        return evTime
                    }
                    else {
                        return ""
                    }
                },
                eventDT: "",
                eventTitle: "Time Entry",
                flagged: false,
                id: _id + swpSettings.swp_username.replace('@', '').replace('.', ''),
                includeInMassAction: false,
                matter: "",
                matterIncrement: "",
                matterName: function () {
                    if (this.matter != null)
                        if (this.matter.toString().indexOf("|") != -1)
                            return this.matter.toString().split(" | ")[1];
                        else
                            return this.matter;
                },
                matterNum: function () {
                    if (this.matter != null)
                        if (this.matter.toString().indexOf("|") != -1)
                            return this.matter.toString().split(" | ")[0];
                        else
                            return "";
                },
                narrative: "",
                origId: "",
                plan1: "",
                plan2: "",
                plan3: "",
                plan4: "",
                prebillNarrative: "",
                returned: false,
                returnedShow: function () {
                    let retDisp 
                    //add logic here to display time field for time captures and time entries.
                   this.returned === true
                       ? retDisp = "inline-block"
                       : retDisp = "none"

                    return retDisp
                },
                shortDate: function () { /* short date, for phone time entry */
                    if (this.eventDT != null && this.eventDT != "" && typeof this.eventDT != "undefined") {
                        if (swpSettings.selectTime == "true" || swpSettings.selectTime == "True") {
                            /* don't parse time if it's 0:00*/
                            if (moment(this.eventDT).format('H') !== "0" || moment(this.eventDT).format('m') !== "0") {
                                return moment(this.eventDT).format(swpSettings.dateFormat + '  h:mm A').toString();
                            }
                            else {
                                return moment(this.eventDT).format(swpSettings.dateFormat).toString();
                            }

                        }
                        else {
                            return moment(this.eventDT).format(swpSettings.dateFormat).toString();
                        }
                    }
                    else {
                        return "";
                    }
                },
                sortDate: 0,
                status: "New",
                statusImage: "img/New.png",
                timer: "",
                color: "",
                timerEntry: false,
                timerId: "",
                notes: "",
                timerRunning: false,
                timeStamp: "",
                timerStart: "",
                type: "",
                typeImage: "",
                username: swpSettings.swp_username,

                /* show timestamp or duration, depending on status  */
                bEthicalWall: true,
                bValidAtty: true,
                timerHours: function () {
                    /* we have to make sure we "get" the status here so that we trigger the change and the viewmodel updates */
                    if (this.get("status") === 'Posted' || this.get("status") === 'Released')
                        return this.get("duration");
                    else
                        return ''; //this.get("timeStamp"); -- we aren't doing timestamp here anymore
                },
                timerStartButton: "",
                timerStopButton: "",
                unsavedFlag: false,
                unsavedFlagClass: "hidden-image",

                /* duration\timer value functions */                
                buttonReleaseClass: function (e) {
                    if (this.get("status") === "Posted" || (this.get("status") === "Released" && swpSettings.unrelease === false))
                        return "button-div disabled-button";
                    else
                        return "button div";
                },

                buttonsSaveDeleteClass: function (e) {
                    if (this.get("status") === "Released" || this.get("status") === "Posted")
                        return "button-div disabled-button";
                    else
                        return "button div";
                },

                buttonTimerClass: function (e) {
                    if (this.get("status") === "Posted" || (this.get("status") === "Released" && swpSettings.unrelease === false))
                        return "button-div disabled-button";
                    else
                        return "button div";
                },

                /* try to make narrative read-only for posted/released */
                narReadOnly: function (e) {
                    if (this.status === "Posted" || this.status === "Released") {
                        return true;
                    }
                    else {
                        return false;
                    }
                },

                maxNarrCheck: function () {
                    let maxNarr = bgArr.filter(x => x.includes("Only", "characters allowed"))[0]
                    !!maxNarr
                    ? swpSettings.set_MaxNarrative = maxNarr.match(/(\d+)/)[0]
                    : swpSettings.set_MaxNarrOverride == "True"
                        ? swpSettings.set_MaxNarrative = swpSettings.maxNarrServer
                        : swpSettings.set_MaxNarrative = "10000"
                },

                releaseButtonCaption: function (e) {
                    if (this.get("status") === "Released" && swpSettings.unrelease === true)
                        return "Unrelease";
                    else
                        return "Release";
                },

                releaseEntry: function (ev) {
                    swp.debugLogView.addToDebugLog('entry releaseEntry event fired' + ev)

                    //track old status in case we need to revert
                    oldStatus = this.status;

                    /*
                       There are two stages of validation -- client side and server
                       server side is only required if offline mode is available and it causes a slight delay
                       so if we don't have offline we will trust the client side validation and mark the item
                       as released right away

                        2015-4-8: BG validation also in second stage
                    */
                    //bg flag is comping across as a string
                    if (swpSettings.cacheOffline === false && (swpSettings.billingGuidelines == "false" || swpSettings.billingGuidelines == "False")) {
                        let disabledPhoneButtonColor = swpSettings.styles.disabledPhoneButtonColor

                        if (swpSettings.bTimers === true) {
                            $('#fieldTimestamp_timer').hide();
                            $('#divTimerButtons').hide();
                            $("#fieldDuration_timer").show();
                            $('#lblHours-phone').text('Hours:');
                        }
                    }
                    log.debug('making call to release event.')
                    swp.debugLogView.addToDebugLog('making call to release event.')

                    //for foley only, call delayed release
                    if (swpSettings.service_url.toLowerCase().indexOf("foley") > -1) {
                        swp.timeentry.actions.releaseEntryDelayed(this, ev)
                    }
                    else {
                        swp.timeentry.actions.releaseEntry(this, ev)
                    }
                },

                roundEntry: function (e) {
                    swp.debugLogView.addToDebugLog('entry roundEntry event fired')
                    let inc = swpSettings.set_DefaultIncrement,
                        duration = this.duration,
                        tempTime, hr, min;

                    /* get duration for timer */
                    if (this.get("timer") != '') {
                        var hrs = swp.timer.actions.roundTimer(this.get("timer"), this.get("matterIncrement"));
                        this.set("duration", hrs);
                    }

                    if (swpSettings.decimalComma === true) {
                        duration = this.duration.toString().replace(",", ".");
                    }

                    if (this.matterIncrement !== '' && this.matterIncrement !== undefined)
                        inc = this.matterIncrement;

                    if (duration !== "" && +duration != 0) {

                        var hrs = swp.shared.Rounding.roundHours(duration, inc);

                        log.debug("Setting hours to " + hrs);
                        swp.debugLogView.addToDebugLog("Setting hours to " + hrs);
                        tempTime = hrs.split(".")
                        hr = tempTime[0]
                        min = (Number(tempTime[1]) / 100) * 60
                        if (hr.length === 1) { hr = "0" + hr }
                        if (min < 10) { min = "0" + min }
                        tempTime = `${hr}:${min}:00`
                        this.set("duration", hrs); //change event handles putting back the comma if applicable
                        this.set("durationTimeDisplay", tempTime)
                    }
                    else {
                        //KP 2018-9-26: blank for running timer on save, default to just an increment
                        if (inc == .3) { inc = ".10"; }
                        this.set("duration", inc);
                    }
                },

                saveEntry: function (e) {
                    swp.debugLogView.addToDebugLog('entry saveEntry event fired')
                    let valid = true

                    if (swpSettings.set_ValSave == true || swpSettings.set_ValSave == "true" || swpSettings.set_ValSave == "True") {
                        this.validate(e, true)
                        ? valid = true
                        : valid = false
                    }

                    if (this.status !== "Released" && this.status !== "Posted" && valid === true) {

                        /* round duration */
                        this.roundEntry();

                        var ev = new module.newEventObject()

                        /* parse date */
                        var tempDate = moment(this.eventDT).format();
                        //var tempDate = new Date()

                        /* trim timezone, could be a + or  - */
                        if (tempDate.indexOf('+') > 1) {
                            tempDate = tempDate.substring(0, tempDate.lastIndexOf('+'));
                        }
                        else {
                            tempDate = tempDate.substring(0, tempDate.lastIndexOf('-'));
                        }

                        if (this.eventDT != null) { ev.set("eventDT", tempDate); }

                        ev.client = this.client;
                        ev.matter = this.matter;
                        ev.custom1 = this.custom1;
                        ev.custom2 = this.custom2;
                        ev.custom3 = this.custom3;
                        ev.custom4 = this.custom4;
                        ev.custom5 = this.custom5;
                        ev.duration = this.duration;
                        ev.eventTitle = this.eventTitle;
                        ev.timer = this.timer;
                        ev.color = this.color;
                        ev.notes = this.notes;
                        ev.username = this.username;
                        ev.narrative = this.narrative;
                        ev.id = this.id
                        ev.returned = this.returned

                        this.unsavedFlag = false;

                        var that = this;

                        // if we have billing guidelines we need two factor validation
                        // web service returns bool but somehow this becomes a string
                        //if (swpSettings.billingGuidelines.toString().toLowerCase() === "true" && this.BillingGuidelineError !== "IGNORE") {
                        if (swpSettings.set_ValSave.toLowerCase() == 'true') {

                            $.ajax({
                                type: "POST",
                                url: swpSettings.service_url + "validateevent",
                                data: JSON.stringify(ev),
                                contentType: "application/json",
                                dataType: "json",
                                success: function (data) {
                                    let objResult = data,
                                        invalidFieldColor = swpSettings.styles.invalidFieldColor,
                                        errArr,
                                        errDisplay;

                                    log.debug("validateevent result: " + data)
                                    swp.debugLogView.addToDebugLog("validateevent result: " + data)

                                    if (objResult.EnableExport === false) {
                                        /* at this point billing guidelines should error */
                                        /* if we start getting more errors here we should add a function to handle all the crap release validation does */
                                        if (objResult.BillingGuidelineError !== "") {
                                            //$("#stackView #userFeedbackMessages .km-collapsible-content").children().hide()
                                            //$("#userFeedbackMessages .km-collapsible-content p").append("<p></p>")
                                            if (swpSettings.isTablet) {
                                                if (swpSettings.bTimers) {
                                                    $("#divValidation_timer").text("Billing Guidelines Errors")
                                                    $("#divValidation_timer").show()
                                                }
                                                else {
                                                    $("#divValidation").text("Billing Guidelines Errors")
                                                    $("#divValidation").show()
                                                }
                                                //$("#timeEntryView #userFeedbackMessages p").text((objResult.BillingGuidelineError.replace("<br />", ".")))
                                                //errArr = objResult.BillingGuidelineError.split("<br />")
                                                module.setupBillDisplay(objResult.BillingGuidelineError, "#billingGuidelineErrors")
                                                $("#timeEntryView #bgViolationContainer").show()
                                                $("#toggleBillingGuideInfo").show()

                                                //$("#timeEntryView #userFeedbackMessages p").text((objResult.BillingGuidelineError.replace(new RegExp(String.fromCharCode(13), 'g'), "<br/>")))
                                                $("#spanIgnore-tab").text("Ignore & Save")
                                                //bg errors on save will now still save but show the error, no need for ignore and save button
                                                $("#spanIgnore-tab").hide()
                                                innerSave()
                                            }
                                            else {

                                                log.debug(objResult.BillingGuidelineError)
                                                swp.debugLogView.addToDebugLog("Billing Guideline Error:" + objResult.BillingGuidelineError)
                                                $("#toggleBillingGuideInfo .km-warning-swp").css("color", "red")
                                                $("#timeEntryView #billingGuidelineErrors").empty()
                                                module.setupBillDisplay(objResult.BillingGuidelineError, "#billingGuidelineErrors")

                                                $("#toggleBillingGuideInfo").show()
                                                $("#timeEntryView #bgViolationContainer").show()
                                                innerSave()
                                            }

                                        } else {
                                            /* client, matter and/or custom field(s) invalid, figure out which and mark it accordingly */
                                            if (objResult.DateColor === "Red") {
                                                $("#timeEntryDatePicker-phone-row").addClass('invalidField')

                                                $("#fieldDate_timer").css("background-color", invalidFieldColor)
                                                $("#fieldDate_timer").css("border", "1px solid red")
                                            }

                                            if (objResult.ClientColor === "Red") {
                                                $("#client-timeentry-row").addClass('invalidField')

                                                $("#fieldClient_timer").css("background-color", invalidFieldColor)
                                                $("#fieldClient_timer").css("border", "1px solid red")
                                            }

                                            if (objResult.MatterColor === "Red") {
                                                $("#matter-timeentry-row").addClass('invalidField')

                                                $("#fieldMatter_timer").css("background-color", invalidFieldColor)
                                                $("#fieldMatter_timer").css("border", "1px solid red")

                                            }

                                            if (objResult.Custom1Color === "Red") {
                                                $("#custom1-timeentry-row").addClass('invalidField') //liCustom1-phone-row

                                                $("#fieldCustom1_timer").css("background-color", invalidFieldColor)
                                                $("#fieldCustom1_timer").css("border", "1px solid red")
                                            }

                                            if (objResult.Custom2Color === "Red") {
                                                $("#custom2-timeentry-row").addClass('invalidField')

                                                $("#fieldCustom2_timer").css("background-color", invalidFieldColor)
                                                $("#fieldCustom2_timer").css("border", "1px solid red")
                                            }

                                            if (objResult.Custom3Color === "Red") {
                                                $("#custom3-timeentry-row").addClass('invalidField')

                                                $("#fieldCustom3_timer").css("background-color", invalidFieldColor)
                                                $("#fieldCustom3_timer").css("border", "1px solid red")
                                            }

                                            if (objResult.Custom4Color === "Red") {
                                                $("#custom4-timeentry-row").addClass('invalidField')

                                                $("#fieldCustom4_timer").css("background-color", invalidFieldColor)
                                                $("#fieldCustom4_timer").css("border", "1px solid red")
                                            }

                                            if (objResult.Custom5Color === "Red") {
                                                $("#custom5-timeentry-row").addClass('invalidField')

                                                $("#fieldCustom5_timer").css("background-color", invalidFieldColor)
                                                $("#fieldCustom5_timer").css("border", "1px solid red")
                                            }

                                            if (objResult.HoursColor === "Red") {
                                                $("#duration-timeentry-row").addClass('invalidField')

                                                $("#fieldDuration_timer").css("background-color", invalidFieldColor)
                                                $("#fieldDuration_timer").css("border", "1px solid red")

                                                module.checkTotalDayHours(data);

                                            }
                                        }
                                    } else {

                                        $("#toggleBillingGuideInfo .km-warning-swp").css("color", "black")
                                        $("#timeEntryView #billingGuidelineErrors").empty()
                                        $("#timeEntryView #bgViolationContainer").hide()
                                        innerSave()
                                    }
                                },
                                failure: function (errMsg) {
                                    alert(errMsg)
                                }
                            })

                        }
                        else {
                            log.debug('skipping web service validation')
                            swp.debugLogView.addToDebugLog('skipping web service validation')
                            innerSave()
                        }

                        /* just calls the service, creating this function so I dont have to duplicate this code twice above */
                        //var that = this;

                        function innerSave() {

                            log.debug('saving item')
                            swp.debugLogView.addToDebugLog("saving item")

                            //for foley only, call delayed release
                            if (swpSettings.service_url.toLowerCase().indexOf("foley") > -1) {
                                swp.timeentry.actions.saveEntryDelayed(that)
                            }
                            else {
                                swp.timeentry.actions.saveEntry(that)
                            }


                        }
                    }
                },

                saveLocal: function () {

                    log.debug("entry saving locally event fired")
                    swp.debugLogView.addToDebugLog('entry saving locally event fired')

                    this.roundEntry()
                    this.unsavedFlag = false
                    this.set("status", "Saved")
                    this.set("statusImage", "img/Saved.png")
                    this.set("unsavedFlagClass", "hidden-image")
                    $('.imgSave').css("visibility", "visible")

                    setTimeout(() => {
                        $('.imgSave').css("visibility", "hidden")
                    }, 1000)

                    var entries = JSON.parse(localStorage.getItem("swp-entries"))

                    if (entries === null || typeof entries === "undefined") {
                        /* no existing data so we just create a new array */
                        log.debug("Creating new entries array")
                        swp.debugLogView.addToDebugLog("Creating new entries array")
                        entries = []
                    }
                    else {

                        /* check to see if this entry already exists, remove it */
                        for (var i = 0; i < entries.length; i++) {
                            log.debug(entries[i].id)
                            swp.debugLogView.addToDebugLog(entries[i].id)
                            if (entries[i].id === this.id) {
                                log.debug("removing entry from array")
                                swp.debugLogView.addToDebugLog("removing entry from array")
                                entries.splice(i, 1)
                            }
                        }
                    }

                    /* add the entry */
                    entries.push(this)
                    localStorage.setItem("swp-entries", JSON.stringify(entries))

                    $("#imgStatus-phone").css("visibility", "hidden")
                    $("#save-confirm-phone").css("visibility", "visible")
                    $("#timersView #save-confirm-phone")
                    setTimeout(() => {
                        $("#save-confirm-phone").css("visibility", "hidden")
                    }, 1000)


                    //if (isTablet) {
                    //    /* show save flag in stack */
                    //    $('#' + this.cleanId() + 'timer' + ' img.imgSaveStack').css("display", "inline-block");
                    //    $('#' + this.cleanId() + 'timer' + ' img.imgSaveStack').delay(1000).fadeOut(700);
                    //    $('#' + this.cleanId() + ' img.imgSaveStack').css("display", "inline-block");
                    //    $('#' + this.cleanId() + ' img.imgSaveStack').delay(1000).fadeOut(700);
                    //}

                    setTimeout(() => {
                        $("#imgStatus-phone").css("visibility", "visible")
                    }, 2000)
                },

                tickTimer: function () {
                    // swp.debugLogView.addToDebugLog('entry tickTimer event fired')
                    // Your moment
                    var mmt = moment();
                    // Your moment at midnight
                    var mmtMidnight = mmt.clone().startOf('day');
                    // Difference in minutes
                    var diffMinutes = mmt.diff(mmtMidnight, 'minutes');

                    var startTimeIsSameDay = moment(this.timerStart).isSame(mmtMidnight, "day");
                    var eventDateIsSameDay = moment(this.eventDT).isSame(mmtMidnight, "day");

                    if (this.timerRunning == true) {
                        var time = new Date().getTime() - this.timerStart;
                        if (startTimeIsSameDay === false || eventDateIsSameDay === false) { //remove any time after midnight
                            time = moment(new Date().getTime() - this.timerStart, "x").subtract(diffMinutes.valueOf(), "minutes").format("x")
                        }

                        var hrs = 0;
                        var mins = 0;
                        var secs = 0;

                        secs = Math.floor(time / 1000)

                        if (secs > 3600) {
                            hrs = Math.floor(secs / 3600);
                            secs = secs % 3600;
                        }

                        if (secs > 60) {
                            mins = Math.floor(secs / 60);
                            secs = secs % 60;
                        }

                        hrs = hrs.toString()
                        mins = mins.toString()
                        secs = secs.toString()

                        if (hrs.length === 1) {
                            hrs = "0" + hrs
                        }

                        if (mins.length === 1)
                            mins = "0" + mins;

                        if (secs.length === 1)
                            secs = "0" + secs;

                        this.set("timer", `${hrs}:${mins}:${secs}`);

                    }

                    $(`#timersView #${this.id} .timerValueBorder`).css("background-color", "var(--customRed)")
                    $(`#timersView #${this.id} #timerTemplateTimerValue`).css("color", "white")
                    $(`#timersView #${this.id} #timerTemplateTimerValue`).css("font-weight", "700")

                    if (startTimeIsSameDay === false || eventDateIsSameDay === false) {
                        $(`#timersView #${this.id} .timerValueBorder`).css("background-color", "inherit")
                        $(`#timersView #${this.id} #timerTemplateTimerValue`).css("color", "black")
                        $(`#timersView #${this.id} #timerTemplateTimerValue`).css("font-weight", "100")
                        //swp.timer.actions.stopAllTimers();
                        swp.timer.actions.stopAllTimers_Phone();
                    }

                },

                timerValue: function () {
                    /* 6/30/2014 -- show timer even if it is not running */
                    if ((this.timerId !== '') && (this.status === 'New' || this.status === 'Saved'))
                        return this.timer;
                    else
                        return '';
                },

                timerValueTab: function () {
                    if (this.status === "Released")
                        return this.duration;
                    else
                        return this.timer;
                },

                unrelease: function (e) {
                    swp.debugLogView.addToDebugLog('entry unrelease event fired')
                    /* {key}/{user}/{token} */
                    var url = swpSettings.service_url + "unrelease/" + this.id + "/" + swpSettings.swp_username + "/" + swpSettings.objTook.token;
                    var that = this;

                    log.debug(url);
                    swp.debugLogView.addToDebugLog(url)
                    $.ajax({
                        type: "GET",
                        url: url,
                        contentType: "application/json",
                        dataType: "json",
                        timeout: 4000,
                        success: function (data) {
                            if (data.toString().toLowerCase() == 'true') {
                                that.set("status", "New"); //status set properly in save -- needs to be something other than release/posted

                                that.BillingGuidelineError = "IGNORE"
                                that.saveEntry(true)
                                swp.stackView.loadTimeForm()
                                $('.te-button-phone').css('color', 'black')

                                /* enable fields */
                                module.updateReferences()
                            }
                            else {
                                /* mark as posted */
                                that.set("status", "Posted");
                                that.set("statusImage", "img/Posted.png");
                            }
                        },
                        error: function (result) {
                            if (result.status != 200) {
                                that.set("status", "Released");
                                that.set("statusImage", "img/Released.png");
                                alert("Un-release failed, you may be experiencing poor network connectivity. ");
                            }
                        },
                        failure: function (errMsg) {
                            alert(errMsg);
                        }
                    });

                },

                validate: function (e, overrideAtty) {
                    swp.debugLogView.addToDebugLog('entry validation event fired')
                    ///* clear everything first */
                    swp.timeentry.actions.clearValidation()

                    let invalidField = swpSettings.styles.invalidFieldColor

                    log.debug('Validating event');
                    swp.debugLogView.addToDebugLog('Validating event');
                    
                    // /* get duration for timer */
                    // if (this.timer != '' && this.timer != null) {
                    //     var hrs = swp.timer.actions.roundTimer(this.timer, this.matterIncrement)
                    //     this.duration = hrs
                    // }

                    var result = true;

                    /* bad words */
                    var errorWords = "";
                    if (this.badWords !== "") {

                        /* don't apply the error yet because we may combine it with the BB error */
                        var words = this.badWords.split(";");

                        for (var i = 0, l = words.length; i < l; i++) {
                            if (words[i] !== "" && this.narrative.toLowerCase().indexOf(words[i]) !== -1) {
                                log.debug("Error Bad Words")
                                swp.debugLogView.addToDebugLog("Error Bad Words")
                                result = false;

                                if (errorWords === "")
                                    errorWords = words[i];
                                else
                                    errorWords = errorWords + ", " + words[i];
                            }
                        }

                    }

                    /* check block billing */
                    if (this.blockBill !== '' && this.narrative.indexOf(';') !== -1) {
                        log.debug("Invalid Block Billing")
                        swp.debugLogView.addToDebugLog("Invalid Block Billing")
                        result = false

                        /* combine with bad words error if appropriate */
                        var errorBB = error_BlockBilling
                        if (errorWords !== "") errorBB = "Block Billing and Stop Word Error: " + errorWords

                        $('#divValidation').text(errorBB)
                        $('#divValidation_timer').text(errorBB)

                    } else {
                        /* apply just bad words error if needed */
                        if (errorWords !== "") {
                            errorWords = "Stop Word Error: " + errorWords
                            $('#divValidation').text(errorWords)
                            $('#divValidation_timer').text(errorWords)
                        }
                    }

                    /* check working atty flag which is set in the change event */
                    if (this.bValidAtty === false && overrideAtty !== true) {
                        log.debug('Invalid working atty')
                        swp.debugLogView.addToDebugLog('Invalid working atty')
                        result = false

                        //workingAtty Warning 
                        $("#timeEntryView #userFeedbackMessages .km-collapsible-content").children().hide()
                        $("#timeEntryView #userFeedbackMessages #userFeedbackMessageContent").text(swpSettings.error_WorkingAtty);
                        $("#timeEntryView #userFeedbackMessages #userFeedbackMessageContent").show()
                        swp.utilities.toggleUserFeedbackMessages(this)
                    }

                    if (this.bEthicalWall === false) {
                        log.debug('Invalid Ethical Wall')
                        swp.debugLogView.addToDebugLog('Invalid Ethical Wall')
                        result = false
                        // $("#modalErrorText-phone").text(swpSettings.error_EthicalWall)
                        $("#timeEntryView #userFeedbackMessages .km-collapsible-content").children().hide()
                        $("#timeEntryView #userFeedbackMessages #userFeedbackMessageContent").text(swpSettings.error_WorkingAtty)
                        $("#timeEntryView #userFeedbackMessages #userFeedbackMessageContent").show()
                        swp.utilities.toggleUserFeedbackMessages(this)
                    }

                    /* date validation */
                    var validDate = true

                    /* make sure entry is not too old */
                    if ((moment(this.eventDT) < moment(swpSettings.set_OldestValidDate)) || (this.eventDT == "")) {
                        log.debug("Entry Too Old")
                        swp.debugLogView.addToDebugLog("Entry Too Old")
                        validDate = false
                    } else {

                        /* KP 2019-1-22: check this against current date not including timestamp, otherwise next day stuff can pass validation */
                        var dtToCompare = moment().startOf('day')

                        /* check for future date -- only check this if date is still valid after above check */
                        if (this.billType === "billable" || this.billType === "" || typeof this.billType == "undefined") {
                            if (moment(this.eventDT).diff(dtToCompare, "Days") > swpSettings.set_futureDateBill) {
                                log.debug("Date too futuristic")
                                swp.debugLogView.addToDebugLog("Date too futuristic")
                                validDate = false
                            }
                        }
                        else {
                            if (moment(this.eventDT).diff(dtToCompare, "Days") > swpSettings.set_futureDateNonBill) {
                                log.debug("Date too futuristic")
                                swp.debugLogView.addToDebugLog("Date too futuristic")
                                validDate = false
                            }
                        }
                    }


                    if (validDate === false) {

                        result = false
                        $("#timeEntryDatePicker-phone-row").addClass('invalidField')

                    }

                    if (this.client == "") {
                        result = false

                        log.debug('invalid client')
                        swp.debugLogView.addToDebugLog('invalid client')
                        $("#client-timeentry-row").addClass('invalidField')

                    }


                    if (this.matter == "" || this.matter == null) {
                        result = false

                        log.debug('invalid matter')
                        swp.debugLogView.addToDebugLog('invalid matter')
                        $("#matter-timeentry-row").addClass('invalidField')

                    }

                    for (const property in this) {
                        if (property.includes("custom")) {
                            let num = property.slice(-1)
                            if ((this[property] == "" || this[property] == undefined) && swpSettings[`custom${num}`][`set_Custom${num}Reqs`].toString().toLowerCase() == 'true' && swp.dataSources[`custom${num}_dataSource`]._data.length > 0) {
                                result = false
                                log.debug(`invalid custum${num}`)
                                swp.debugLogView.addToDebugLog(`invalid custum${num}`)
                                $(`#custom${num}-timeentry-row`).addClass('invalidField')
                            }

                        }
                    }

                    /* for purpose of this validation, put decimal back as normal */
                    let durationToValidate = this.duration.toString().replace(",", ".")

                    if ((durationToValidate == "" || durationToValidate == "0" || isNaN(durationToValidate) == true || durationToValidate == "0.00")
                        && swpSettings.set_PostZero == 'false' && (this.timer === "" || this.timer == "00:00:00")) {
                        result = false

                        log.debug('invalid duration')
                        swp.debugLogView.addToDebugLog('invalid duration')

                        /* tablet validation */
                        $("#duration-timeentry-row").addClass('invalidField')

                        //$("#fieldDuration_timer").css("background-color", invalidField);
                        //$("#fieldDuration_timer").css("border", "1px solid red");
                    }

                    /* check for > 24 hr duration */
                    if (swpSettings.set_AllowOver24Hrs === false) {
                        if (parseFloat(this.duration) > 24) {

                            result = false
                            log.debug('invalid duration over 24hr')
                            swp.debugLogView.addToDebugLog('invalid duration over 24hr')
                            $("#duration-timeentry-row").addClass('invalidField')
                        }
                    }

                    if (this.narrative == "" && swpSettings.set_PostEmpty == 'false') {

                        log.debug('invalid narrative')
                        swp.debugLogView.addToDebugLog('invalid narrative')
                        result = false
                        $("#liNarrative-phone").addClass('invalidField')

                    }

                    log.debug('Returning: ' + result.toString())
                    swp.debugLogView.addToDebugLog('Returning: ' + result.toString())
                    return result
                }

            })

            viewModel.bind("change", function (e) {
                swp.debugLogView.addToDebugLog("viewModel change event triggered")

                //this functionality will run when the associated field is changed
                if (e.field != "status" && e.field != "statusImage" && e.field != "timerStartButton"
                    && e.field != "timerStopButton" && e.field != "timerId" && e.field != "timerRunning"
                    && this.unsavedFlagClass == "hidden-image" && e.field != "unsavedFlagClass") {
                    this.unsavedFlag = true;
                }

                if (e.field === "timer" && this.timer !== ':00') {
                    this.set("unsavedFlagClass", "");
                    if (this.timerRunning === true) {
                        $(`#timersView #${this.id} .timerValueBorder`).css("background-color", "var(--customRed)")
                        $(`#timersView #${this.id} #timerTemplateTimerValue`).css("color", "white")
                        $(`#timersView #${this.id} #timerTemplateTimerValue`).css("font-weight", "700")
                    } else {
                        $(`#timersView #${this.id} .timerValueBorder`).css("background-color", "inherit")
                        $(`#timersView #${this.id} #timerTemplateTimerValue`).css("color", "black")
                        $(`#timersView #${this.id} #timerTemplateTimerValue`).css("font-weight", "100")
                    }

                }

                //removing this to fix error of clearing the defaultDate on an entry date change, may have to reimplement and refactor instead.
                //if (e.field == "sortDate" && swpSettings.defaultDate != "" && this.sortDate != 0 && this.sortDate != "" && this.sortDate != null)
                //    if (this.sortDate.format('YYYY-M-D').toString() != moment(swpSettings.defaultDate).format('YYYY-M-D').toString())
                //        swpSettings.defaultDate = ""

                if (e.field == "duration" || e.field == "status") {

                    swp.entry.updateTotals();

                    //if this site has a comma separator, make sure we don't have a decimal shown
                    if (swpSettings.decimalComma === true) {
                        if (this.duration.toString().indexOf(".") >= 0) {
                            this.set("duration", this.duration.toString().replace(".", ","));
                        }
                    }
                }

                if (e.field == "client") {
                    /* refresh matters list */
                    swp.utilities.refreshBillingGuidelines()
                    swp.utilities.refreshMatters()
                }

                if (e.field == "matter" && swpSettings.offlineMode === false) {
                    log.debug("GETTING MATTER INFO (INCREAMENT, STOPWORDS, ETC")
                    swp.debugLogView.addToDebugLog("GETTING MATTER INFO (INCREAMENT, STOPWORDS, ETC")
                    var source = e.sender.source

                    /* reset increment & BB*/
                    this.matterIncrement = ''
                    this.blockBill = ''

                    if (source == undefined) source = e.sender

                    var status = source.get('status').toLowerCase()

                    log.debug('checking working atty: ' + status)
                    swp.debugLogView.addToDebugLog('checking working atty: ' + status)

                    // module.checkWorkingAtty(this, e, source, status)
                }

                if (e.field === "notes") {
                    arrEntriesTimers = swpSettings.arrEntriesTimers
                    activeTimerPosition = swpSettings.activeTimerPosition
                    let view = window.location.hash.split("/")[window.location.hash.split("/").length - 1].split(".")[0]

                    if (arrEntriesTimers.length) {
                        if (arrEntriesTimers[activeTimerPosition].notes == "") {
                            $("#timeEntryView #pageTitleTop").text("Timer")
                        } else {
                            $("#timeEntryView #pageTitleTop").text(`${arrEntriesTimers[activeTimerPosition].notes}`)
                            if (view === "timersView") {
                                swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].notes = $("#timersView #timerNameInput").val()
                            } else {
                                swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].notes = $("#timeEntryView #timerNameInput").val()
                            }
                        }
                    }
                }
            })

            return viewModel
        },

        newEventObject: function() {
            swp.debugLogView.addToDebugLog("newEventObject function hit")
            var objEvent = new kendo.data.ObservableObject({

                eventDT: "",
                client: "",
                matter: "",
                custom1: "",
                custom2: "",
                custom3: "",
                custom4: "",
                custom5: "",
                duration: "",
                timer: "",
                username: "",
                narrative: "",
                description: "",
                id: "",
                Key: swpSettings.swp_variable,
            })

            return objEvent;
        },

        setupBillDisplay: function (array, list) {
            swp.debugLogView.addToDebugLog("setupBillDisplay function hit")
            let bgArr = array.split("<br />"),
                bgList = $(`#timeEntryView ${list}`)

                if (bgArr.includes("</br>")) {
                    bgArr = bgArr.split("</br>")
                }

            $("#timeEntryView #userFeedbackMessages .km-collapsible-content").children().hide()
            bgArr = bgArr.filter((el) => {
                return el != "";
            })

            for (let i = 0; i < bgArr.length; i++) {
                let li = document.createElement("li")
                li.innerText = bgArr[i]
                bgList.append(li)
            }

            //logic here to adjust
            let maxNarr = bgArr.filter(x => x.includes("Only", "characters allowed"))[0]
            !!maxNarr
            ? swpSettings.set_MaxNarrative = maxNarr.match(/(\d+)/)[0]
            : swpSettings.set_MaxNarrOverride == "True"
                ? swpSettings.set_MaxNarrative = swpSettings.maxNarrServer
                : swpSettings.set_MaxNarrative = "10000"
            // if (swpSettings.set_MaxNarrOverride === "False") {
            // }

            $("#timeEntryView #billingGuidelineErrors").show()
            $("#timeEntryView #billingGuidelines").show()
            $("#timeEntryView #bgHeaderContainer").show()
        },

        updateReferences: function () {
            swp.debugLogView.addToDebugLog("updateReferences function hit")
            if ($("#timeEntryView #timeEntryDateInput").data("kendoDatePicker")) $("#timeEntryView #timeEntryDateInput").data("kendoDatePicker").enable(true)

            let dateField = document.querySelector("#timeEntryView #timeEntryDatePicker-phone-row .phoneTimeInput")
            if (dateField) { dateField.addEventListener("click", swp.timeEntryView.openDatePicker) }
            
            // $("#timeEntryView #timeEntryDatePicker-phone-row .phoneTimeInput").click(() => {
            //     $("#timeEntryView #timeEntryDateInput").data("kendoDatePicker").open()
            // })
            $("#timeEntryView #timeEntryDateInput").attr("readonly", true)
            $("#timeEntryView #liClient-phone").attr("href", "#views/timeEntryViews/clientLookup.html")
            $("#timeEntryView #liMatter-phone").attr("href", "#views/timeEntryViews/matterLookup.html")
            $("#timeEntryView #liCustom1-phone").attr("href", "#views/timeEntryViews/custom1Lookup.html")
            $("#timeEntryView #liCustom2-phone").attr("href", "#views/timeEntryViews/custom2Lookup.html")
            $("#timeEntryView #liCustom3-phone").attr("href", "#views/timeEntryViews/custom3Lookup.html")
            $("#timeEntryView #liCustom4-phone").attr("href", "#views/timeEntryViews/custom4Lookup.html")
            $("#timeEntryView #liCustom5-phone").attr("href", "#views/timeEntryViews/custom5Lookup.html")
            $("#timeEntryView #fieldNarrative-phone").removeAttr("readonly")
        },

        updateTotals: function() {
            swp.debugLogView.addToDebugLog("updateTotals function hit")
            let arrEntries = swpSettings.arrEntries
            let set_DefaultRounding = swpSettings.set_DefaultRounding
            let arrEntriesTimers = swpSettings.arrEntriesTimers

            var saved = 0;
            var released = 0;
            var posted = 0;
            var total = 0;

            for (var i = 0; i < arrEntries.length; i++) {


                if (arrEntries[i].status == "Saved" && arrEntries[i].duration != "")
                    saved = saved + swp.utilities.parseNumValue(arrEntries[i].duration);

                if (arrEntries[i].status == "Released" && arrEntries[i].duration != "")
                    released = released + swp.utilities.parseNumValue(arrEntries[i].duration);

                if (arrEntries[i].status == "Posted" && arrEntries[i].duration != "")
                    posted = posted + swp.utilities.parseNumValue(arrEntries[i].duration);

                if (arrEntries[i].status != "New" && arrEntries[i].duration != "")
                    total = total + swp.utilities.parseNumValue(arrEntries[i].duration);
            }

            saved = saved.toFixed(set_DefaultRounding);
            released = released.toFixed(set_DefaultRounding);
            posted = posted.toFixed(set_DefaultRounding);
            total = total.toFixed(set_DefaultRounding);

            $("#spanSavedTotal").text(saved);
            $("#spanReleasedTotal").text(released);
            $("#spanPostedTotal").text(posted);
            $("#spanTotal").text(total);

            saved = 0;
            released = 0;
            total = 0;

            for (var j = 0; j < arrEntriesTimers.length; j++) {

                if (arrEntriesTimers[j].status == "Saved" && arrEntriesTimers[j].duration != "")
                    saved = saved + swp.utilities.parseNumValue(arrEntriesTimers[j].duration);

                if (arrEntriesTimers[j].status == "Released" && arrEntriesTimers[j].duration != "")
                    released = released + swp.utilities.parseNumValue(arrEntriesTimers[j].duration);

                if (arrEntriesTimers[j].status != "New" && arrEntriesTimers[j].duration != "")
                    total = total + swp.utilities.parseNumValue(arrEntriesTimers[j].duration);
            }

            saved = saved.toFixed(set_DefaultRounding);
            released = released.toFixed(set_DefaultRounding);
            total = total.toFixed(set_DefaultRounding);

            $("#spanSavedTotal_Timer").text(saved);
            $("#spanReleasedTotal_Timer").text(released);
            $("#spanTotal_Timer").text(total);
        },
    }

    return module

}()


