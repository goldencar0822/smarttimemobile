//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.setup) { swp.setup = {} }

swp.setup = function () {
    "use strict";
    let arrEntries = swpSettings.arrEntries,
        arrEntriesTimers = swpSettings.arrEntriesTimers,
        activeEntryPosition = swpSettings.activeEntryPosition,
        activeTimerPosition = swpSettings.activeTimerPosition,
        c1 = swpSettings.custom1,
        c2 = swpSettings.custom2,
        c3 = swpSettings.custom3,
        c4 = swpSettings.custom4,
        c5 = swpSettings.custom5;

    var module = {

        applyHolidayStylePhone: function (result) {
            var slots = $(".k-scheduler-content td[role=gridcell]:not(.k-other-month)")
            var sched = $("#scheduler-phone").data("kendoScheduler")

            var schedSelectedMonth = moment(sched._selectedView._firstDayOfMonth).month();

            $.each(result, function (i, item) {
                var holiday = new Date(parseInt(item.replace('/Date(', '').replace(')/', '')));
                log.info("Setting holiday: " + holiday);

                $.each(slots, function (i, slot) {
                    var currentSlot = $(slots[i]);
                    var slotData = sched.slotByElement(currentSlot)
                    if (slotData && slotData.startDate.getDate() === holiday.getDate() &&
                        slotData.startDate.getMonth() === holiday.getMonth()) {
                        if (schedSelectedMonth === holiday.getMonth()) {
                            //now check day as well for swipe bug where if a user swipes to the next month the
                            //grid cells can be inconsistent. checking the day html fixes this and assures accuracy
                            if (currentSlot[0].children[0].innerHTML.trim() === holiday.getDate().toString()) { //plus 1 for base 0f 0 on get day
                                log.info(slotData.startDate);
                                currentSlot.css('background-color', 'var(--holidayCalColor)')
                                //remove error if needed on holiday
                                currentSlot.children("i.timeViolation").hide()
                            }
                        }
                    }
                })

            })
        },

        FinishInit: function () {

            var dtString = moment().format('YYYYM').toString();
            // kendo.culture(swpSettings.set_Culture)

            swpSettings.sched_dataSource = new kendo.data.SchedulerDataSource({
                batch: true,
                offlineStorage: {
                    getItem: function () {

                        var appts = [];
                        appts = JSON.parse(localStorage.getItem("swp-calendar"));

                        return appts;
                    },
                    setItem: function (item) { }
                },
                transport: {
                    read: {
                        url: swpSettings.service_url + "calendar/" + dtString + "/" + swpSettings.objTook.token,
                        dataType: "json"
                    },
                    parameterMap: function (options, operation) {
                        if (operation !== "read" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        }
                    }
                },
                schema: {
                    model: {
                        fields: {
                            title: { field: "Title", defaultValue: "No title", validation: { required: true } },
                            description: { field: "Description" },
                            start: { type: "date", from: "startDate" },
                            end: { type: "date", from: "endDate" },
                        }
                    }
                },
                change: function (e) {
                    var saved = 0;
                    var released = 0;
                    var posted = 0;
                    var total = 0;

                    for (var i = 0; i < this._data.length; i++) {

                        if (this._data[i].description === "saved-event")
                            saved = saved + swp.utilities.parseNumValue(this._data[i].title);

                        if (this._data[i].description === "released-event")
                            released = released + swp.utilities.parseNumValue(this._data[i].title);

                        if (this._data[i].description === "posted-event")
                            posted = posted + swp.utilities.parseNumValue(this._data[i].title);

                        total = total + swp.utilities.parseNumValue(this._data[i].title);


                        // this._data.start = moment(this._data.start).format(swpSettings.dateFormat)
                        // this._data.end = moment(this._data.end).format(swpSettings.dateFormat)
                    }

                    saved = saved.toFixed(swpSettings.set_DefaultRounding);
                    released = released.toFixed(swpSettings.set_DefaultRounding);
                    posted = posted.toFixed(swpSettings.set_DefaultRounding);
                    total = total.toFixed(swpSettings.set_DefaultRounding);

                    $("#spanSavedMonthly").text(saved);
                    $("#spanReleasedMonthly").text(released);
                    $("#spanPostedMonthly").text(posted);
                    $("#spanTotalMonthly").text(total);

                    /* cache calendar data if needed */
                    if (swpSettings.cacheOffline === true) {
                        //localStorage.setItem('swp-calendar', JSON.stringify(this._data.toJSON()));
                        localStorage.setItem('swp-stat-saved', saved);
                        localStorage.setItem('swp-stat-released', released);
                        localStorage.setItem('swp-stat-posted', posted);
                        localStorage.setItem('swp-stat-total', total);

                        /* make a separate call for data so we can store the raw results */
                        $.ajax({
                            type: "GET",
                            url: this.transport.options.read.url,
                            contentType: "application/json",
                            dataType: "json",
                            success: function (data) {
                                localStorage.setItem("swp-calendar", JSON.stringify(data));
                            },
                            failure: function (errMsg) {
                                alert("Error caching calendar data: " + errMsg);
                            }
                        });
                    }

                }
            });

            /* set online/offline */
            log.debug("Setting sched datasource online to " + !swpSettings.offlineMode.toString());
            swpSettings.sched_dataSource.online(!swpSettings.offlineMode);
        },

        FinishInit_Phone: function () {
            log.info("TRYING TO HIDE STATUS BAR!!")
            //fixAndroidStatusBar()

            function b(a) {
                a.attr("readonly", "readonly")
                a.attr("disabled", "true")
                setTimeout(function () {
                    a.blur()
                    a.removeAttr("readonly")
                    a.removeAttr("disabled")
                }, 100)
            }

            var a = new Date

            log.debug("Finishing init phone after successful login (" + swpSettings.offlineMode.toString() + ")")

            swp.utilities.refreshCustomMenus()
            $("#tabstrip-phone").show()
            //location.href = "#views/homeViews/homeView.html"
            $("#divTimeForm-Phone").show()
            log.debug(swpSettings.service_url + "/hoursstats/" + swpSettings.objTook.token)
            swp.setup.refreshStatsPhone()
            swp.setup.setupTimeEntryLabels()
            swp.setup.setupTimeFormat()
            swp.utilities.checkLocalForCaptureParams()
            swp.utilities.checkLocalForIncludeParams()

            $("#divTimeForm-Phone li").on({
                touchstart: () => {
                    $(this).find(".invalid-icon").hide()
                }
            })

            $("#divTimeForm-Phone textarea").focus(() => {
                $("body").scrollTop(140),
                    $("#divTimeForm-Phone textarea").siblings(".invalid-icon").hide()
            })

            $("#divTimeForm-Phone textarea").on({
                keypress: (a) => {
                    13 === a.keyCode && b($(this))
                }
            })

            // if ($("#scheduler-phone").data("kendoScheduler")) {
            //     $("#scheduler-phone").data("kendoScheduler").destroy()
            //     $("#scheduler-phone").empty()
            // }

            if (swpSettings.bInitComplete) {
                if ($("#scheduler-phone").data("kendoScheduler")) {
                    try {
                        var d = $("#scheduler-phone").data("kendoScheduler"),
                        e = moment(d.date()).format("YYYYM").toString(),
                        f = swpSettings.service_url + "/calendar/" + e + "/" + swpSettings.objTook.token;
                        // kendo.culture(swpSettings.set_Culture)
                        d.dataSource.transport.options.read.url = f,
                        d.dataSource.read()
                    } catch {
                        setTimeout(() => {
                            var d = $("#scheduler-phone").data("kendoScheduler"),
                                e = moment(d.date()).format("YYYYM").toString(),
                                f = swpSettings.service_url + "/calendar/" + e + "/" + swpSettings.objTook.token;
                            // kendo.culture(swpSettings.set_Culture)
                            d.dataSource.transport.options.read.url = f,
                            d.dataSource.read()
                        }, 300)
                    }
                }
            } else {

                // kendo.culture(swpSettings.set_Culture)
                //Set beginning day of month to diff Day. (Mon = 1)
                kendo.culture().calendar.firstDay = swpSettings.set_SchedulerStartDay

                var c = kendo.getCulture().calendars.standard
                c.days.names = c.days.namesAbbr

                $("#scheduler-phone").kendoScheduler({
                    date: new Date,
                    scrollable: false,
                    eventHeight: 10,
                    footer: moment().format('dddd, MMMM Do YYYY'),
                    views: [{
                        type: "month",
                        // dayTemplate: $("#day-template").html(),
                        eventTemplate: $("#event-template").html(),
                        selectedDateFormat: "{0:MMMM yyyy}",
                        workDays: [2, 3, 4, 5, 6, 7, 1]
                    }],
                    dataSource: swpSettings.sched_dataSource,
                    navigate: function (a) {
                        if (log.debug("navigate"), swpSettings.offlineMode === !1) {
                            var b = moment(a.date).format("YYYY").toString() + moment(a.date).format("M").toString(),
                                c = swpSettings.service_url + "/calendar/" + b + "/" + swpSettings.objTook.token;

                            log.debug(c);
                            swpSettings.sched_dataSource.transport.options.read.url = c;
                            swpSettings.sched_dataSource.read();

                        } else a.preventDefault()
                    },
                    add: function (a) {
                        swpSettings.offlineMode === !1
                            ? swp.homeView.daySearch_Phone(a.event.start)
                            : a.preventDefault()
                    },
                    edit: function (a) {
                        swpSettings.offlineMode === !1
                            ? swp.homeView.daySearch_Phone(a.event.start)
                            : a.preventDefault()
                    },
                    dataBound: function (a) {
                        var b = $("#scheduler-phone td[role='gridcell']"),
                            c = $("#scheduler-phone tr[role='row']");

                        if (swpSettings.offlineMode === !1) {
                            b.on({
                                touchstart: function () {
                                    $(this).css("background-color", swpSettings.styles.entryHighlightColor)
                                }
                            })
                        }

                        b.on({
                            touchend: function () {
                                $(this).css("background-color", "white")
                            }
                        })

                        b.off({ click: "" })

                        b.on({
                            click: function () {

                                // $('#scheduler-phone').data("kendoScheduler").calendar.setOptions({ depth: "year" })

                                var a = this.getAttribute("class"),
                                    b = !0;
                                if (null !== a && 1 === a.indexOf("k-other-month")) {
                                    b = false
                                };

                                if (b === true) {
                                    var c = parseFloat($(this).find("span").html()),
                                        d = $("#scheduler-phone").data("kendoScheduler"),
                                        e = moment(d.date()).format("YYYY").toString(),
                                        f = moment(d.date()).format("M").toString();

                                    f = parseInt(f) - 1;

                                    //if the cell has this class it's coming from another month so we need to adjust the search
                                    if ($(this).hasClass("k-other-month")) {
                                        //if its another month and the day is less than 15 its the next month
                                        if (c < 15) {
                                            f = f + 1;
                                        } else {
                                            //else its the previous month
                                            f = f - 1; gridHours_dataSource
                                        }
                                    }

                                    var g = new Date(e, f, c, 0, 0, 0, 0);
                                    swp.homeView.daySearch_Phone(g)
                                    // location.href = "#views/timeEntryViews/timeEntryView.html"
                                }
                            }
                        })

                        if (null != b[6].getAttribute("class") && "k-other-month" == b[6].getAttribute("class").trim()) {
                            $(c[0]).css("display", "none")
                        }
                        if (null != b[35].getAttribute("class")) {
                            $(c[5]).css("display", "none")
                        }

                        swp.setup.getApplyHolidayStylePhone()
                        $("#homeView #calendarLegend").css("display", "inline-flex")
                        $('#greyOverlay').remove()
                    },
                    change: function (a) {
                        if (swpSettings.cacheOffline === true) {
                            localStorage.setItem("swp-calendar", JSON.stringify(this._data));
                            localStorage.setItem("swp-stat-saved", saved);
                            localStorage.setItem("swp-stat-released", released);
                            localStorage.setItem("swp-stat-posted", posted);
                            localStorage.setItem("swp-stat-total", total);
                        }
                    },
                })

                var scheduler = $("#scheduler-phone").data("kendoScheduler")
                scheduler._showCalendar();
                $('.k-animation-container').css('display', 'none');
                scheduler.popup.close();
                var calendar = scheduler.calendar;
                // Customize calendar here
                calendar.options.start = "year";
                calendar.options.depth = "year";
                // calendar.options.format = "MMMM yyyy";
                calendar.navigateUp();

                if (swpSettings.offlineMode === false) {
                    var d = $("#scheduler-phone").data("kendoScheduler"),
                        e = moment(d.date()).format("YYYYM").toString(),
                        f = swpSettings.service_url + "/calendar/" + e + "/" + swpSettings.objTook.token;
                    //sets datasource of scheduler to current month
                    d.dataSource.transport.options.read.url = f;
                    d.dataSource.read();
                }

                swpSettings.bInitComplete = true;
                a = new Date;
                log.debug("Done with init (" + a.toString() + ")")

                //Constant check logic for application
                setInterval(() => {
                    //checks if client/matter field should be active for current entry
                    if (arrEntries.length > 0 && "New" === arrEntries[activeEntryPosition].status && arrEntries[activeEntryPosition].status !== "Released" && arrEntries[activeEntryPosition].status !== "Posted") {
                        $("#liClient-phone").attr("href", "#views/timeEntryViews/clientLookup.html")
                        $("#liMatter-phone").attr("href", "#views/timeEntryViews/matterLookup.html")
                    }

                }, 3000)

                setInterval(() => {
                    swp.auth.backup()
                }, swpSettings.set_BackupInterval)
            };

            $("#scheduler-phone .k-scheduler-navigation .k-nav-current .k-link").click(() => {
                $("<div class='k-overlay' id='greyOverlay' onclick='$(this).remove()'></div>").appendTo($(document.body))
            })
        },

        getApplyHolidayStylePhone: function () {
            //adds red background to holidays
            var url = swpSettings.service_url + "/holidays" + "/" + swpSettings.objTook.token;
            log.info(url, "URL FROM APPLY HOLIDAY")
            $.ajax({
                url: url,
                type: "GET",
                success: function (result) {
                    log.info(result)
                    swp.setup.applyHolidayStylePhone(result);
                },
                error: function (err) {
                    log.info(err)

                }
            })
        },

        getTimerSettings: function () {
            $.ajax({
                type: "GET",
                url: swpSettings.service_url + `GetTimerSettings/${swpSettings.objTook.token}`,
                data: JSON.stringify(),
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    console.log("GetTimerSettings", data)
                    swpSettings.timerSettings.BlankRows = data.BlankRows
                    swpSettings.timerSettings.LastPostedClientMatterNumbers = data.LastPostedClientMatterNumbers
                    swpSettings.timerSettings.PresetOption = data.PresetOption
                    swpSettings.timerSettings.LoadRadio = data.LoadRadio
                    swpSettings.timerSettings.Id = data.Id
                },
                failure: function (errMsg) {
                    alert(errMsg)
                }
            })
        },

        hideEmptyCustomFields: function () {

            for (const property in swpSettings) {
                if (property.includes("custom")) {
                    let num = property.slice(-1)
                    console.log(`${property}: ${this[property]}`)
                    if (swpSettings[`custom${num}`][`set_Custom${num}`] === "" || swpSettings[`custom${num}`][`set_Custom${num}`] == undefined) {
                        $(`#custom${num}-timeentry-row`).hide()
                    }
                }
            }
        },

        hideSplashScreen: function () {
            swpPlugins.default.hideSplashScreen()
        },

        InitPhone: function () {
            if (swpSettings.bInitComplete) {
                kendo.init($("#divTimeForm-Phone"), kendo.mobile.ui);
                kendo.init($("#divStack-Phone"), kendo.mobile.ui);
                swpSettings.set_Timers = localStorage.getItem("swp_phoneTimers");
                if (swpSettings.set_Timers == null) {
                    set_Timers = !0
                };

                // $("#mobi_datetime").mobiscroll().calendar({
                //     display: "top",
                //     controls: ["calendar"],
                //     mode: "mixed",
                //     lang: "en",
                //     minDate: new Date(2014, 3, 10, 9, 22),
                //     maxDate: new Date((new Date).setFullYear((new Date).getFullYear() + 1)),
                //     stepMinute: 1,
                //     dateFormat: "m/d/yy",
                //     timeFormat: "  h:ii A",
                //     closeOnSelect: !0,
                //     buttons: [],
                //     onBeforeShow: (a) => {
                //         return $("#spanTimerPhone")[0].style.display == "none" && "Released" != arrEntries[activeEntryPosition].status && "Posted" != arrEntries[activeEntryPosition].status && ($("#divDupe-phone").hide(), $("#divAddNewEntryPhone").hide(), $("#spanTitle-phone").text("Date"), $("#spanTitle-phone").css("padding-left", "0px"), "True" == swpSettings.selectTime && $("#spanTitle-phone").text("Date & Time"), void $("#backTE-phone").show())
                //     },
                //     onShow: (a) => {
                //         $(".dw-persp").css("background-color", "white")

                //         "" !== arrEntries[activeEntryPosition].eventDT
                //             ? $("#mobi_datetime").mobiscroll("setValues", [moment(arrEntries[activeEntryPosition].eventDT).toDate()])
                //             : $("#mobi_datetime").mobiscroll("setValues", [new Date])

                //         document.addEventListener("hidekeyboard", () => {
                //             $('#fieldNarrative-phone').blur()

                //             setTimeout(() => {
                //                 log.info("TRYING TO HIDE STATUS BAR!!")
                //                 fixAndroidStatusBar()
                //             }, 500)
                //         }, false);
                //     },
                //     onBeforeClose: (a) => {
                //         $("#divDupe-phone").show(),
                //             $("#divAddNewEntryPhone").show(),
                //             $("#backTE-phone").hide(),
                //             $("#spanTitle-phone").text("Time Entry"),
                //             $("#spanTitle-phone").css("padding-left", "30px")
                //     },
                //     onClosed: (a, b) => {
                //         arrEntries[activeEntryPosition].set("eventDT", moment(b.getVal()).toDate())
                //     }
                // })
            }

            $("#txtEmail-phone").bind("focus", (a) => {
                window.scrollTo(0, 0), document.body.scrollTop = 0
            })

            $("#checkRemember-phone").kendoMobileSwitch({
                onLabel: "Yes",
                offLabel: "No"
            })

            $("#checkOffline-phone").kendoMobileSwitch({
                onLabel: "Yes",
                offLabel: "No"
            })

            $("#checkOffline-phone").prop("checked", !1)
            let a = new Date
            log.debug("Done loading phone app (" + a.toString() + ")")
        },

        isTouchDevice: function () {
            try {
                return document.createEvent("TouchEvent"), !0
            } catch (a) {
                return false
            }
        },

        refreshHoursPhone: function () {
            $.ajax({
                type: "GET",
                traditional: true,
                url: swpSettings.hourURLTemplate,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (a) {
                }
            })
        },

        refreshStatsPhone: function () {
            swpSettings.offlineMode === false
                ? $.ajax({
                    type: "GET",
                    traditional: true,
                    url: swpSettings.service_url + "/hoursstats/" + swpSettings.objTook.token,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (a) {
                        //reset stats view table information and set to 2 units after decimal
                        $("#spanSavedMonthly-phone").text(Number(a.Saved).toFixed(2)),
                            $("#spanReleasedMonthly-phone").text(Number(a.Released).toFixed(2)),
                            $("#spanPostedMonthly-phone").text(Number(a.Posted).toFixed(2)),
                            $("#spanTotalMonthly-phone").text(Number(a.All).toFixed(2)),
                            $("#spanBillActMTD-phone").text(Number(a.MTD_BillableActual).toFixed(2)),
                            $("#spanBillBudMTD-phone").text(Number(a.MTD_BillableBudget).toFixed(2)),
                            $("#spanBillVarMTD-phone").text(Number(a.MTD_BillableVariance).toFixed(2)),
                            $("#spanNonBillActMTD-phone").text(Number(a.MTD_NonBillableActual).toFixed(2)),
                            $("#spanNonBillBudMTD-phone").text(Number(a.MTD_NonBillableBudget).toFixed(2)),
                            $("#spanNonBillVarMTD-phone").text(Number(a.MTD_NonBillableVariance).toFixed(2)),
                            $("#spanActActMTD-phone").text(Number(a.MTD_AccountableActual).toFixed(2)),
                            $("#spanActBudMTD-phone").text(Number(a.MTD_AccountableBudget).toFixed(2)),
                            $("#spanActVarMTD-phone").text(Number(a.MTD_AccountableVariance).toFixed(2)),
                            $("#spanBillActYTD-phone").text(Number(a.YTD_BillableActual).toFixed(2)),
                            $("#spanBillBudYTD-phone").text(Number(a.YTD_BillableBudget).toFixed(2)),
                            $("#spanBillVarYTD-phone").text(Number(a.YTD_BillableVariance).toFixed(2)),
                            $("#spanNonBillActYTD-phone").text(Number(a.YTD_NonBillableActual).toFixed(2)),
                            $("#spanNonBillBudYTD-phone").text(Number(a.YTD_NonBillableBudget).toFixed(2)),
                            $("#spanNonBillVarYTD-phone").text(Number(a.YTD_NonBillableVariance).toFixed(2)),
                            $("#spanActActYTD-phone").text(Number(a.YTD_AccountableActual).toFixed(2)),
                            $("#spanActBudYTD-phone").text(Number(a.YTD_AccountableBudget).toFixed(2)),
                            $("#spanActVarYTD-phone").text(Number(a.YTD_AccountableVariance).toFixed(2)),
                            swpSettings.cacheOffline === !0 && (localStorage.MTD_BillableActual = Number(a.MTD_BillableActual).toFixed(2),
                                localStorage.MTD_BillableBudget = Number(a.MTD_BillableBudget).toFixed(2),
                                localStorage.MTD_BillableVariance = Number(a.MTD_BillableVariance).toFixed(2),
                                localStorage.MTD_NonBillableActual = Number(a.MTD_NonBillableActual).toFixed(2),
                                localStorage.MTD_NonBillableBudget = Number(a.MTD_NonBillableBudget.toFixed(2)),
                                localStorage.MTD_NonBillableVariance = Number(a.MTD_NonBillableVariance).toFixed(2),
                                localStorage.MTD_AccountableActual = Number(a.MTD_AccountableActual).toFixed(2),
                                localStorage.MTD_AccountableBudget = Number(a.MTD_AccountableBudget).toFixed(2),
                                localStorage.MTD_AccountableVariance = Number(a.MTD_AccountableVariance).toFixed(2),
                                localStorage.YTD_BillableActual = Number(a.YTD_BillableActual).toFixed(2),
                                localStorage.YTD_BillableBudget = Number(a.YTD_BillableBudget).toFixed(2),
                                localStorage.YTD_BillableVariance = Number(a.YTD_BillableVariance).toFixed(2),
                                localStorage.YTD_NonBillableActual = Number(a.YTD_NonBillableActual).toFixed(2),
                                localStorage.YTD_NonBillableBudget = Number(a.YTD_NonBillableBudget).toFixed(2),
                                localStorage.YTD_NonBillableVariance = Number(a.YTD_NonBillableVariance).toFixed(2),
                                localStorage.YTD_AccountableActual = Number(a.YTD_AccountableActual).toFixed(2),
                                localStorage.YTD_AccountableBudget = Number(a.YTD_AccountableBudget).toFixed(2),
                                localStorage.YTD_AccountableVariance = Number(a.YTD_AccountableVariance).toFixed(2))
                    }
                })
                : ($("#spanBillActMTD-phone").text(Number(localStorage.MTD_BillableActual).toFixed(2)),
                    $("#spanBillBudMTD-phone").text(Number(localStorage.MTD_BillableBudget).toFixed(2)),
                    $("#spanBillVarMTD-phone").text(Number(localStorage.MTD_BillableVariance).toFixed(2)),
                    $("#spanNonBillActMTD-phone").text(Number(localStorage.MTD_NonBillableActual).toFixed(2)),
                    $("#spanNonBillBudMTD-phone").text(Number(localStorage.MTD_NonBillableBudget).toFixed(2)),
                    $("#spanNonBillVarMTD-phone").text(Number(localStorage.MTD_NonBillableVariance).toFixed(2)),
                    $("#spanActActMTD-phone").text(Number(localStorage.MTD_AccountableActual).toFixed(2)),
                    $("#spanActBudMTD-phone").text(Number(localStorage.MTD_AccountableBudget).toFixed(2)),
                    $("#spanActVarMTD-phone").text(Number(localStorage.MTD_AccountableVariance).toFixed(2)),
                    $("#spanBillActYTD-phone").text(Number(localStorage.YTD_BillableActual).toFixed(2)),
                    $("#spanBillBudYTD-phone").text(Number(localStorage.YTD_BillableBudget).toFixed(2)),
                    $("#spanBillVarYTD-phone").text(Number(localStorage.YTD_BillableVariance).toFixed(2)),
                    $("#spanNonBillActYTD-phone").text(Number(localStorage.YTD_NonBillableActual).toFixed(2)),
                    $("#spanNonBillBudYTD-phone").text(Number(localStorage.YTD_NonBillableBudget).toFixed(2)),
                    $("#spanNonBillVarYTD-phone").text(Number(localStorage.YTD_NonBillableVariance).toFixed(2)),
                    $("#spanActActYTD-phone").text(Number(localStorage.YTD_AccountableActual).toFixed(2)),
                    $("#spanActBudYTD-phone").text(Number(localStorage.YTD_AccountableBudget).toFixed(2)),
                    $("#spanActVarYTD-phone").text(Number(localStorage.YTD_AccountableVariance).toFixed(2))
                )
        },

        setupTimeEntryLabels: function () {
            $.ajax({
                type: "GET",
                url: swpSettings.service_url + "labels",
                contentType: "application/json",
                dataType: "json",
                success: function (a) {
                    a.forEach(x => {
                        swpSettings.clientLabel = a.find(x => x.key === "Client").value
                        swpSettings.matterLabel = a.find(x => x.key === "Matter").value
                    })
                    
                    if (swpSettings.clientLabel === "") swpSettings.clientLabel = "Client"
                    if (swpSettings.matterLabel === "") swpSettings.matterLabel = "Matter"
                },
                error: function (err) {
                    console.log(err)
                }
            })
        },

        setupTimeFormat: function () {
            // localStorage.displaySimpleTimeInScheduler = 
        },

        setupApplication: function (obj) {
            //location.href = "#views/homeViews/homeView.html"
            swpSettings.defaultDate = moment().format()
            swpSettings.swp_firmname = obj.Firm
            swpSettings.swp_fullname = obj.Fullname
            swpSettings.objTook.token = obj.Token
            localStorage.swp_Toke = obj.Token

            //Time Between Auto Backups
            !!obj.backupInterval
            ? swpSettings.set_BackupInterval = obj.backupInterval
            : swpSettings.set_BackupInterval = 30000

            //Max Narrative logic
            swpSettings.maxNarrServer = obj.MaxNarrative
            swpSettings.maxNarrServer == "0"
            ? swpSettings.set_MaxNarrOverride = "False"
            : swpSettings.set_MaxNarrOverride = "True"

            swpSettings.set_MaxNarrOverride === "True"
            ? swpSettings.set_MaxNarrative = swpSettings.maxNarrServer
            : swpSettings.set_MaxNarrative = "10000"
            
            swpSettings.timerSettings.TimekeeperUser = swpSettings.swp_username
            swpSettings.stackParams.includeTimeCaptures = obj.Capture

            //Starting day of the week for scheduler 
            !!obj.WeekStart 
                ? swpSettings.set_SchedulerStartDay = Number(obj.WeekStart)
                : swpSettings.set_SchedulerStartDay = 0

            //Display Option for Statistics
            obj.RemoveStats == "true" || obj.RemoveStats == "True"
                ? swpSettings.set_StatsHidden = true
                : swpSettings.set_StatsHidden = false

            //Display Option for Hours
            obj.RemoveHours == "true" || obj.RemoveHours == "True"
                ? swpSettings.set_HoursHidden = true
                : swpSettings.set_HoursHidden = false

            //localstorage setup logic
            localStorage.userDisplaySimpleTimeInScheduler == "true" || localStorage.userDisplaySimpleTimeInScheduler == "True"
                ? swpSettings.set_DurationInUnits = false
                : swpSettings.set_DurationInUnits = true
            // swpSettings.set_DurationInUnits = !localStorage.userDisplaySimpleTimeInScheduler

            if (localStorage.swp_EnableAuth0) swpSettings.enableAuth0 = localStorage.swp_EnableAuth0

            swpSettings.first_stack_visit = true
            swpSettings.first_timers_visit = true

            module.getTimerSettings()

            //Timer Auto Save
            obj.disableTimerAutoSave == "true" || obj.disableTimerAutoSave == "True"
            ? swpSettings.set_TimerAutoSave = false
            : swpSettings.set_TimerAutoSave = true

            /* if name is blank then set it to username */
            if (swpSettings.swp_fullname === '')
                swpSettings.swp_fullname = swpSettings.swp_username;

            swpSettings.set_PostZero = obj.PostZero;
            swpSettings.set_OldestValidDate = obj.OldestValidDate;
            swpSettings.set_PostEmpty = obj.PostEmpty;
            c1.set_Custom1 = obj.Custom1;
            c2.set_Custom2 = obj.Custom2;
            c3.set_Custom3 = obj.Custom3;
            c4.set_Custom4 = obj.Custom4;
            c5.set_Custom5 = obj.Custom5;
            swpSettings.set_ValSave = obj.ValidateSave;
            swpSettings.set_Broadcast = obj.Broadcast;
            swpSettings.set_IgnoreBillingGuidelines = obj.IgnoreBillingGuidelines
            
            swpSettings.swp_emailaddr = obj.EmailAddr;

            swpSettings.set_MultiTimers = obj.MultiTimers;
            swpSettings.timerSortOption = 'sortByTimer_sortDate'
            swpSettings.stackParams.stackSortOption = 'sortByEntry_sortDate'

            c1.set_Custom1Reqs = obj.sCustomReq1;
            c2.set_Custom2Reqs = obj.sCustomReq2;
            c3.set_Custom3Reqs = obj.sCustomReq3;
            c4.set_Custom4Reqs = obj.sCustomReq4;
            c5.set_Custom5Reqs = obj.sCustomReq5;
            typeof c4.set_Custom4Reqs === "undefined" ? c4.set_Custom4Reqs = "false" : c4.set_Custom4Reqs = c4.set_Custom4Reqs;
            typeof c5.set_Custom5Reqs === "undefined" ? c5.set_Custom5Reqs = "false" : c5.set_Custom5Reqs = c5.set_Custom5Reqs;

            /* hide menus we don't use */
            swp.setup.hideEmptyCustomFields()

            /*default values for custom menus */
            c1.set_Custom1Default = obj.Custom1Default;
            c2.set_Custom2Default = obj.Custom2Default;
            c3.set_Custom3Default = obj.Custom3Default;
            c4.set_Custom4Default = obj.Custom4Default;
            c5.set_Custom5Default = obj.Custom5Default;

            c1.set_Custom1OriginalDefault = obj.Custom1Default;
            c2.set_Custom2OriginalDefault = obj.Custom2Default;
            c3.set_Custom3OriginalDefault = obj.Custom3Default;
            c4.set_Custom4OriginalDefault = obj.Custom4Default;
            c5.set_Custom5OriginalDefault = obj.Custom5Default;

            /* menu stickyness setting */
            c1.set_Custom1Sticky = obj.custom1Sticky;
            c2.set_Custom2Sticky = obj.custom2Sticky;
            c3.set_Custom3Sticky = obj.custom3Sticky;
            c4.set_Custom4Sticky = obj.custom4Sticky;
            c5.set_Custom5Sticky = obj.custom5Sticky;
            typeof c5.set_Custom5Sticky === "undefined" ? c5.set_Custom5Sticky = false : c5.set_Custom5Sticky = c5.set_Custom5Sticky;

            /* Gunderson action code shortcut */
            if (typeof obj.Custom1Shortcut !== 'undefined') { c1.custom1Shortcut = obj.Custom1Shortcut; }

            // copy custom menu to narrative
            if (typeof obj.CopyCustom1 !== 'undefined') { c1.CopyCustom1 = obj.CopyCustom1; }
            if (typeof obj.CopyCustom2 !== 'undefined') { c2.CopyCustom2 = obj.CopyCustom2; }
            if (typeof obj.CopyCustom3 !== 'undefined') { c3.CopyCustom3 = obj.CopyCustom3; }
            if (typeof obj.CopyCustom4 !== 'undefined') { c4.CopyCustom4 = obj.CopyCustom4; }
            if (typeof obj.CopyCustom5 !== 'undefined') { c5.CopyCustom5 = obj.CopyCustom5; }

            /*default matter rounding increment */
            swpSettings.set_DefaultIncrement = obj.DefaultIncrement;

            if (swpSettings.set_DefaultIncrement.length > 3) {
                swpSettings.set_DefaultRounding = 3;
            }
            else {
                swpSettings.set_DefaultRounding = 2;
            }

            /* error messages */
            swpSettings.error_WorkingAtty = obj.WorkingAttyError;
            swpSettings.error_BlockBilling = obj.BlockBillingError;
            if (typeof obj.EthicalWallError !== 'undefined') { swpSettings.error_EthicalWall = obj.EthicalWallError; }


            /* flag to show time selector */
            if (typeof obj.SelectTime !== 'undefined') {
                // swpSettings.selectTime = obj.SelectTime;
                swpSettings.selectTime = "false"
                
                if (swpSettings.selectTime == "true" || swpSettings.selectTime == "True") {
                    /* phone date */
                    // $('#mobi_datetime').mobiscroll('option', 'controls', ['calendar', 'time']);
                    // $('#mobi_datetime').mobiscroll('option', 'buttons', ['set']);
                    // $('#mobi_datetime').mobiscroll('option', 'closeOnSelect', false);

                    // /* tablet date */
                    // $('#fieldDate').mobiscroll('option', 'controls', ['calendar', 'time']);
                    // $('#fieldDate').mobiscroll('option', 'buttons', ['set']);
                    // $('#fieldDate').mobiscroll('option', 'closeOnSelect', false);

                    // /* timer date */
                    // $('#fieldDate_timer').mobiscroll('option', 'controls', ['calendar', 'time']);
                    // $('#fieldDate_timer').mobiscroll('option', 'buttons', ['set']);
                    // $('#fieldDate_timer').mobiscroll('option', 'closeOnSelect', false);
                }
            }

            // Set for localization
            /* set date format based on Culture */
            if (typeof obj.Culture !== 'undefined') {
                kendo.ui.progress($("#scheduler-phone"), true);
                swpSettings.set_Culture = obj.Culture
                // swpSettings.set_Culture = "en-IE"
                // swpSettings.set_Culture = "da-DK"
                kendo.culture(swpSettings.set_Culture)
                //set decimal placeholder based on kendo logic
                swpSettings.decimalComma = kendo.culture().numberFormat["."] === ","
                //set date format for app based on culture
                kendo.culture().calendar.patterns.d.split("")[0] === "d" 
                    ? swpSettings.dateFormat = "DD MMMM YYYY"
                    : swpSettings.dateFormat = "MMMM D, YYYY"

                kendo.ui.progress($("#scheduler-phone"), false)
                //     // decimalArr: ["en-AU", "en-CA", es-DO, "cy-GB", "en-GB", "gd-GB", "zh-HK", "en-IE", "ga-IE", "he-IL", "en-NZ", m"i-NZ", "en-US"]
                //     // commaArr: ["fr-BE" , "nl-BE" , "fr-CA" , "da-DK" , "fi-FI" , "se-FI," , "smn-FI" , "sms-FI" , "sv-FI" , "br-FR" , "co-FR" , "fr-FR" , "gsw-FR" , "oc-FR" , "de-DE" , "dsb-DE" , "hsb-DE" , "fy-NL" , "nl-NL" , "nb-NO" , "nn-NO" , "se-NO" , "sma-NO" , "smj-NO" , "pt-PT" , "es-ES" , "se-SE" , "sma-SE" , "smj-SE" , "sv-SE" , "de-CH" , "fr-CH" , "it-CH" , "rm-CH"]
            }

            /* init calendar datasource, use same one for phone and tablet */
            swp.setup.FinishInit()
            $(".km-tabstrip").show()

            swpSettings.set_AllowOver24Hrs = obj.AllowOver24Hrs;
            swpSettings.set_MaxDayHours = Number(obj.MaxDayHours)
            swpSettings.set_MaxEntryHours = Number(obj.MaxEntryHours)

            //Default to 24 hours if no user MaxEntryHour data from login or conflict with Allow Over 24hrs setting
            if (!swpSettings.set_MaxEntryHours) {
                swpSettings.set_MaxEntryHours = 24
            }
            //set maximum time allowed for a single entry
            if (swpSettings.set_AllowOver24Hrs === false && swpSettings.set_MaxEntryHours > 24) {
                swpSettings.set_MaxEntryHours = 24
            }
            //set maximum time allowed for a single day
            if (swpSettings.set_AllowOver24Hrs === false && swpSettings.set_MaxDayHours > 24) {
                swpSettings.set_MaxDayHours = 24
            }

            /* date range allowed into the future */
            swpSettings.set_futureDateBill = obj.futureDateBill;
            swpSettings.set_futureDateNonBill = obj.futureDateNonBill;

            /* check for added search to custom menu */
            c1.set_Custom1Search = swp.utilities.stringToBoolean(obj.customSearch1);
            c2.set_Custom2Search = swp.utilities.stringToBoolean(obj.customSearch2);
            c3.set_Custom3Search = swp.utilities.stringToBoolean(obj.customSearch3);
            c4.set_Custom4Search = swp.utilities.stringToBoolean(obj.customSearch4);
            c5.set_Custom5Search = swp.utilities.stringToBoolean(obj.customSearch5);

            /* text shortcut character (used in offline mode only) */
            if (typeof obj.TextshortcutChar !== 'undefined') {
                swpSettings.textShortcutChar = obj.TextshortcutChar;
            }

            /* billing guidelines flag */
            if (typeof obj.BillingGuidelines !== 'undefined') { swpSettings.billingGuidelines = obj.BillingGuidelines; }

            /* unrelease flag */
            if (typeof obj.Unrelease !== 'undefined') { swpSettings.unrelease = obj.Unrelease; }

            /* budget flag */
            if (typeof obj.ShowBudget !== 'undefined') {

                if (obj.ShowBudget == "false" || obj.ShowBudget == "False") {
                    swpSettings.statParams.showBudget = false;
                    $(".colBudget").hide();
                    $(".colTotalBudget").text("Total");
                    $("#trendHoursGrid").width(370);

                    // adjust dropdown header
                    $("#hoursView #txtCompareActVsBudg").text("Actual Only")
                }
            }

            if (obj.Offline) {
                /* show password box on settings page */
                $("#liOfflinePass-phone").show()
                $("#liOffline-phone").show()

                /* offline mode - store the setting locally */
                localStorage.swp_OfflineMode = true

                if (swpSettings.offlineMode === false) { 
                    swp.offline.cacheDataOffline() 
                }

            } else {
                localStorage.swp_OfflineMode = false
            }

            $("#spanTitleHome").text(swpSettings.swp_fullname)

            // $("#mobi_datetime").change(function () {

            //     log.info("EVENT DT CHANGE")
            //     // Your moment
            //     var mmt = moment()
            //     // Your moment at midnight
            //     var mmtMidnight = mmt.clone().startOf('day')
            //     // Difference in minutes
            //     var diffMinutes = mmt.diff(mmtMidnight, 'minutes')

            //     setTimeout(() => {
            //         var eventDateIsSameDay = moment($("#mobi_datetime").val()).isSame(mmtMidnight, "day")

            //         if (eventDateIsSameDay === false) {
            //             $('#btnTimerToggle-phone').hide()  /* hide timer toggle */
            //         } else {
            //             $('#btnTimerToggle-phone').show()
            //         }
            //     }, 1000)
            // })

            if (obj.ShowAccountable.toString() == "false" || obj.ShowAccountable.toString() == "False") {
                swpSettings.statParams.showAccountable = false
            }

            if (typeof obj.ShowMultiTimer !== 'undefined') {
                if (obj.ShowMultiTimer === false) {
                    $('#liMultTimersSets-phone').hide()
                }
            }

            /* will setup scheduler */
            setTimeout(() => {
                module.FinishInit_Phone();
                swpSettings.arrEntries = []
                swpSettings.arrEntriesTimers = []
                let tempUsername = swpSettings.swp_fullname.split(","),
                    userNameDisplay;

                tempUsername[1] != undefined && tempUsername[1] != null
                    ? userNameDisplay = `${tempUsername[1]} ${tempUsername[0]}`
                    : userNameDisplay = `${tempUsername[0]}`

                // let userNameDisplay = `${tempUsername[1]} ${tempUsername[0]}`
                $("#homeView #usernameDisplay").text(userNameDisplay)

                $("#custom1LookupView #pageTitleTop").text(c1.set_Custom1);
                $("#custom2LookupView #pageTitleTop").text(c2.set_Custom2);
                $("#custom3LookupView #pageTitleTop").text(c3.set_Custom3);
                $("#custom4LookupView #pageTitleTop").text(c4.set_Custom4);
                $("#custom5LookupView #pageTitleTop").text(c5.set_Custom5);
            }, 300)
        },

        setupServiceURL: function () {
            /* setup service URL */
            let local_service_url = localStorage.getItem("swp_wsURL");
            log.debug("SERVICE_URL being found in onload: ", local_service_url)

            //for troubleshooting specific site
            //service_url = "https://lssmarttime.lundgrens.dk"
            //service_url = "https://smarttime.raeder.no"
            //service_url = "https://time.venable.com"
            //service_url = "https://smarttime.sillscummis.com"
            //service_url = "https://smarttime.bristows.com"
            //service_url = "https://mobile.smarttimeapps.com/wsRest.svc/"
            //service_url = "http://www.stdemo.smarttimeapps.com:8585"
            //service_url = "http://localhost:50671"
            //service_url = "https://hannum.smarttimeapps.com"
            // service_url = "https://smarttime.raeder.no"
            
            //service_url = "http://www.stdemo.smarttimeapps.com:8080"
            //service_url = "http://www.st5pro.smarttimeapps.com:8089"

            if (local_service_url == "null" || local_service_url == null || local_service_url == "" || local_service_url == "undefined") {
                local_service_url = "http://www.stdemo.smarttimeapps.com:8080"
                swpSettings.desktop_service_url = local_service_url
            }

            local_service_url = swp.utilities.cleanupURL(local_service_url)
            log.debug("no service url found, setting to default: ", local_service_url)

            swpSettings.service_url = local_service_url
            localStorage.setItem("swp_wsURL", local_service_url)
        },

        touchScroll: function (a) {
            if (isTouchDevice()) {
                var c = (document.getElementById(a), 0);
                document.getElementById(a).addEventListener("touchstart", function (a) {
                    c = this.scrollTop + a.touches[0].pageY
                }, !1), document.getElementById(a).addEventListener("touchmove", function (a) {
                    this.scrollTop = c - a.touches[0].pageY, a.preventDefault()
                }, !1)
            }
        },
    }

    return module

}()

