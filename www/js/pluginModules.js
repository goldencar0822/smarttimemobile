//Copyright 2017 by Smart WebParts LLC
//this file will need to be bundeld via webpack will be spit out in dist/main.js
//NOTE DO NOT ADD A REF TO THIS FILE REF DIST/MAIN.js - not doing so will result in errors
//in console from the root of smarttimemobile run webpack
//entry / exit / variables can be set in webppack.config.
// adding a -w flag to the above command will make webpack watch for changes in the file

///NOTE IF THESE PLUGS INS anre not installed or isUndefined
//install with plain npm commands from www. this will allow the webpack command to work

//to install webpack global: $npm install webpack -g
//local: $npm install webpack --save-dev

//Capacitor Reference
import {
  Plugins,
  PluginResultError
} from '@capacitor/core';

//auth0 References
// var Auth0Cordova = require('@auth0/cordova');
// var App = require('./app');

var swpPlugins = function () {

  var module = {

    addSplashScreenHideListener: function () {
      Plugins.SplashScreen.addListener("hide", () => {
        swp.debugLogView.addToDebugLog("hiding Splash Screen ")
      })
    },

    launchKnowledgebase: function () {
      Plugins.Browser.open({ url: 'https://desk.zoho.com/portal/smarttimeapps/kb/smart-time-users' })
    },

    launchSmartTimeApps: function () {
      Plugins.Browser.open({ url: 'http://www.smarttimeapps.com' })
    },

    hideKeyboard: function (e) {
      Plugins.Keyboard.hide(e)
    },

    hideSplashScreen: function (e) {
      Plugins.SplashScreen.hide()
    },

    keyboardHeight: function () {
      Plugins.Keyboard
    },

    showKeyboard: function () {
      Plugins.Keyboard.show()
    },

    showAccessoryBar: function () {
      try {
        //show keyboard AccessoryBar
        Plugins.Keyboard.setAccessoryBarVisible({ isVisible: true })
      } catch (e) {
        log.debug("No Keyboard available", e)
      }
    },

    checkBiometrics: function () {

      Fingerprint.isAvailable(isAvailableSuccess, isAvailableError)

      function isAvailableSuccess(result) {
        /*
        result depends on device and os. 
        iPhone X will return 'face' other Android or iOS devices will return 'finger'  
        */
        console.log("Biometrics - Fingerprint available");
        Fingerprint.show({
          clientId: "Fingerprint-Demo",
          clientSecret: "password"
        }, successCallback, errorCallback);

        function successCallback() {
          console.log("Biometrics - Authentication successfull");
          swp.auth.appInit(localStorage.faceIDUser, localStorage.faceIDPass, localStorage.swp_token)
        }

        function errorCallback(err) {
          console.log("Biometrics - Authentication invalid " + err);
        }
      }

      function isAvailableError(error) {
        // 'error' will be an object with an error code and message
        alert("Biometrics - Fingerpring Unavailable" + error.message);
      }
    },

    checkFaceID: function () {
      if (Plugins.FaceId) {
        Plugins.FaceId.isAvailable().then(checkResult => {
          if (checkResult.value) {
            Plugins.FaceId.auth().then((resp) => {
              console.log('checkFaceID - authenticated', resp);
              swp.auth.faceIdLoginCheck(localStorage.faceIDUser, localStorage.faceIDPass, localStorage.swp_token)
              // swp.auth.appInit(localStorage.faceIDUser, localStorage.faceIDPass, localStorage.swp_token)
            }).catch((error, message) => {
              // handle rejection errors

              //following two redirects are a fix for login page becoming inactive after cancelling faceIdCheck.
              location.href = "#popover-blank"
              setTimeout(() => {
                location.href = "#popover-login-phone"
                //console.log("checkFaceID - error.message", error.message);
              }, 500)

            });
          } else {
            //following two redirects are a fix for login page becoming inactive after cancelling faceIdCheck.
            location.href = "#popover-blank"
            setTimeout(() => {
              location.href = "#popover-login-phone"
            }, 500)
          }
        });
      }
    },

    testPListPlugin: function() {
      const result = Plugins.PListPlugin.testPlugin({
        value: "Hello World!"
      })
      console.log(result.value) 
    },

    readPList: function() {
      const result = Plugins.PListPlugin.readVariable()
      console.log(result.value)
    },
    // readPList: async function() {
    //   const result = await Plugins.pListReader.readVariable()
    //   console.log("result", result)
    // },


    // auth0: function() {
    //   log.info("!deviceready MAIN function hit in auth.js")
    //   alert("auth0 function hit")

    //   var app = new App();

    //   function intentHandler(url) {
    //     alert("intentHandler function hit")
    //     log.info("intentHandler hit URL:", url);
    //     Auth0Cordova.onRedirectUri(url);
    //   }

    //   window.handleOpenURL = intentHandler;
    //   log.info("loading intentHandler suckahs");

    //   alert("app.run about to be called" + app)
    //   app.run('#app');
    // }

  }

  return module

}()

$(document).ready(swpPlugins.showAccessoryBar)
// $(document).ready(swpPlugins.auth0)

// module.exports = swpPlugins
export default swpPlugins