//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.timeentry) { swp.timeentry = {} }

swp.timeentry.actions = function () {
    let activeEntryPosition = swpSettings.activeEntryPosition,
        arrEntries = swpSettings.arrEntries,
        activeTimerPosition = swpSettings.activeTimerPosition,
        disabledPhoneButtonColor = swpSettings.styles.disabledPhoneButtonColor,
        _saveReleaseRecursiveAttempts = 0,
        _saveReleaseRecursiveLimit = 2,
        sortOption,
        sortDir;

    
    //public interface
    var module = {

        checkConnection: function () {

            $.ajax({
                type: "GET",
                url: swpSettings.service_url + "hello",
                contentType: "application/json",
                dataType: "json",
                timeout: 500,
                success: function (data) {
                    /* don't do anything */
                    console.log("Connection Checked")
                    swp.debugLogView.addToDebugLog("Connection Checked")
                },
                error: function (errMsg) {
                    /* we are offline - who cares */
                    if (errMsg.status == 200) {
                        console.log("Connection Checked")
                        swp.debugLogView.addToDebugLog("Connection Checked")
                    } else {
                        console.log("Connection Check Failed")
                        swp.debugLogView.addToDebugLog("Connection Check Failed")
                    }

                }
            });
        },

        clearValidation: function () {
            $("#list-timeform-phone li").removeClass('invalidField')

            /* working atty error */
            $('#divValidation').text('');
            $('#divValidation_timer').text('');
        },

        clearCustomMenuValues: function () {
            let activeEntry

            if (swpSettings.bTimers === true) {
                activeEntry = swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition]
            } else {
                activeEntry = swpSettings.arrEntries[swpSettings.activeEntryPosition]
            }

            activeEntry.custom1 = ""
            activeEntry.custom2 = ""
            activeEntry.custom3 = ""
            activeEntry.custom4 = ""
            activeEntry.custom5 = ""

            $("#fieldCustom1_phone").val("")
            $("#fieldCustom2_phone").val("")
            $("#fieldCustom3_phone").val("")
            $("#fieldCustom4_phone").val("")
            $("#fieldCustom5_phone").val("")
        },

        clearAllFields: function () {
            swpSettings.bTimers === true
                ? activeEntry = swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition]
                : activeEntry = swpSettings.arrEntries[swpSettings.activeEntryPosition]

            activeEntry.client = ""
            activeEntry.matter = ""
            $("#fieldClient_phone").val("")
            $("#fieldMatter_phone").val("")
            module.clearCustomMenuValues()
        },

        clearEntryField: function (data) {
            let activeEntry
            console.log("clearEntryField" + data)

            if (swpSettings.bTimers === true) {
                activeEntry = swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition]
            } else {
                activeEntry = swpSettings.arrEntries[swpSettings.activeEntryPosition]
            }

            let field = data.target[0].id.split("clear")[1]
            field === "Client"
                ? (activeEntry.client = "",
                    activeEntry.matter = "",
                    $(`#fieldClient_phone`).val(""),
                    $(`#fieldMatter_phone`).val(""))
                : (activeEntry.matter = "",
                    $(`#fieldMatter_phone`).val(""))

            swp.utilities.refreshBillingGuidelines()
            swp.utilities.refreshMatters()
        },

        deleteActiveEntry: function () {
            let activeEntry;
            console.log('deleteActiveEntry Clicked')
            if (swpSettings.set_narFullScreen === true) {
                swp.timeEntryView.narClose_Phone()
            }

            if (swpSettings.bTimers === true) {
                activeEntry = swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition]
            } else {
                activeEntry = swpSettings.arrEntries[swpSettings.activeEntryPosition]
            }

            if (!activeEntry) {
                module.removeActiveEntry()
                return
            }

            var tempId = activeEntry.id

            if (activeEntry.status == "New") {

                // var b = new kendo.data.ObservableObject({
                //     TimeEntryDesc: entryArray[entryPosition].get("narrative"),
                //     Username: swpSettings.swp_username
                // })

                $.ajax({
                    type: "POST",
                    url: swpSettings.service_url + "deleteevent",
                    data: JSON.stringify(activeEntry),
                    contentType: "application/json",
                    dataType: "json",
                    timeout: 4000,
                    success: (a) => {
                        kendo.ui.progress($("#stackView"), false)
                    },
                    failure: (a) => {
                        alert(a)
                    },
                    error: (err) => {
                        if (err.status != 200) {
                            //set item back to new
                            alert("Delete failed, you may be experiencing poor network connectivity. Item removed from session only.");
                        }
                    },
                })
                module.removeActiveEntry()

            } else if (activeEntry.status !== "New" && activeEntry.status !== "Released" && activeEntry.status !== "Posted") {
                log.debug("Deleting active entry")

                log.debug("delete entry: " + tempId)

                $.ajax({
                    type: "POST",
                    url: swpSettings.service_url + "delete/" + swpSettings.swp_username + "/" + tempId,
                    data: "",
                    contentType: "application/json",
                    dataType: "json",
                    timeout: 4000,
                    success: (a) => {
                        kendo.ui.progress($("#stackView"), false)
                    },
                    failure: (a) => {
                        alert(a)
                    },
                    error: (err) => {
                        if (err.status != 200) {
                            //set item back to new
                            alert("Delete failed, you may be experiencing poor network connectivity. Item removed from session only.");
                        }
                    },
                })

                module.removeActiveEntry()
            }
            swp.stackView.checkIncludeToggles()
        },

        releaseEntry: function (that, ev) {
            var objResult
            let invalidFieldColor = swpSettings.styles.invalidFieldColor

            swpSettings.bTimers === true
                ? (entryArray = swpSettings.arrEntriesTimers,
                    entryPosition = swpSettings.activeTimerPosition)
                : (entryArray = swpSettings.arrEntries,
                    entryPosition = swpSettings.activeEntryPosition)

            //remove timestamp
            that.eventDT = moment(that.eventDT).format('YYYY-MM-DDTHH:mm:ss')

            $.ajax({
                type: "POST",
                url: swpSettings.service_url + "releaseevent",
                data: JSON.stringify(that),
                contentType: "application/json",
                dataType: "json",
                timeout: 4000,
                success: function (data) {
                    console.log('data response to releaseevent post', data)
                    _saveReleaseRecursiveAttempts = 0;
                    objResult = data;

                    log.debug(objResult);

                    if (objResult.EnableExport === false) {
                        log.debug(objResult)
                        //implement release fix here if necessary - Need to check if release is currently behaving properly 3/26/20 - should it release if invalid field is thrown?
                        for (const property in objResult) {
                            if (objResult[property] === "Red") {
                                let rowName = property.split("Color")[0].toLowerCase()
                                console.log(`${rowName} -> ${property}: ${objResult[property]}`)
                                $(`#${rowName}-timeentry-row`).addClass('invalidField');
                            }
                        }

                        if (objResult.HoursColor === "Red") {
                            $("#duration-timeentry-row").addClass('invalidField');

                            $("#fieldDuration_timer").css("background-color", invalidFieldColor);
                            $("#fieldDuration_timer").css("border", "1px solid red");

                            swp.entry.checkTotalDayHours(data);
                        }

                        /* billing guidelines */
                        if (objResult.BillingGuidelineError !== "" && swpSettings.supressError === false) {

                            if (swpSettings.isTablet) {
                                if (swpSettings.bTimers) {
                                    $("#divValidation_timer").text("Billing Guidelines Errors");
                                    $("#divValidation_timer").show();
                                }
                                else {
                                    $("#divValidation").text("Billing Guidelines Errors");
                                    $("#divValidation").show();
                                }

                                $("#timeEntryView #fieldNarrative-phone").attr('readonly', true)
                                swp.utilities.toggleBGReleaseOverride()
                            }
                            else {
                                log.debug("Opening BG error")
                                $("#toggleBillingGuideInfo .km-warning-swp").css("color", "red")
                                $("#timeEntryView #billingGuidelineErrors").empty()
                                swp.entry.setupBillDisplay(objResult.BillingGuidelineError, "#billingGuidelineErrors")
                                swp.utilities.toggleBGReleaseOverride()
                            }
                        }
                    } else {

                        /* update status */
                        that.set("status", "Released")
                        that.set("statusImage", "img/Released.png")
                        that.set("unsavedFlagClass", "hidden-image")
                        swpSettings.massReleaseCount++

                        //logic here to remove timer from timer array and add into time entry array
                        if (swpSettings.bTimers === true) {
                            let tempEntry = entryArray.splice(entryPosition, 1)
                            swpSettings.arrEntries.push(tempEntry[0])
                            swpSettings.arrEntriesTimers = entryArray
                            if (swpSettings.arrEntriesTimers.length > 0) swpSettings.activeTimerPosition = swpSettings.arrEntriesTimers.length - 1

                            $(`#timeEntryView #backNavButton`).show(),
                                $("#timeEntryView #backNavButton").attr("href", "#views/stackView.html"),
                                $("#timeEntryView #backNavButton").removeAttr("onclick"),
                                $(`#timeEntryView #timerColorIndicatorTouch`).hide(),
                                $("#timeEntryView #pageTitleTop").text("Time Entry")

                            if ($("#session-entries-phone").data("kendoMobileListView")) {
                                $("#session-entries-phone").data("kendoMobileListView").setDataSource(swpSettings.arrEntries)

                            }

                            if ($("#session-entries-list-timer").data("kendoMobileListView")) {
                                $("#session-entries-list-timer").data("kendoMobileListView").setDataSource(swpSettings.arrEntriesTimers)
                            }

                            entryArray = swpSettings.arrEntries
                            entryPosition = swpSettings.arrEntries.length - 1
                            swpSettings.activeEntryPosition = entryPosition

                            swp.stackView.checkReturnedStatus(entryArray[entryPosition])
                            // swp.stackView.checkFlaggedStatus(entryArray[entryPosition])
                            swpSettings.bTimers = false
                        } else {
                            swp.stackView.checkReturnedStatus(entryArray[entryPosition])
                            // swp.stackView.checkFlaggedStatus(entryArray[entryPosition])
                        }

                        // swpSettings.bTimers === true
                        //     ? (tempEntry = entryArray.splice(entryPosition,1),
                        //         swpSettings.arrEntries.push(tempEntry[0]),
                        //         entryArray = swpSettings.arrEntriesTimers,
                        //         entryPosition = swpSettings.activeTimerPosition)
                        //     : (entryArray = swpSettings.arrEntries,
                        //         entryPosition = swpSettings.activeEntryPosition)

                        // swpSettings.arrEntries.push(tempEntry[0])

                        // swp.stackView.checkReturnedStatus(entryArray[entryPosition])
                        // swp.stackView.checkFlaggedStatus(entryArray[entryPosition])

                        $("#save-confirm-phone").css("visibility", "hidden") /* force save check to hide in case user is going quickly from save to release */
                        $("#imgStatus-phone").css("visibility", "hidden")
                        $('#btnTimerToggle-phone').hide();  /* hide timer toggle */
                        $('#spanTimerPhone').hide()
                        $("#fieldDurationTime-phone").hide()
                        $("#fieldTimer-phone").hide()
                        $("#fieldDurationUnits-phone").show()
                        $('#lblHours-phone').text('Hours:')
                        $("#fieldDurationUnits-phone").val(entryArray[entryPosition].duration)
                        // $("#timeEntryView #button-ReleaseUnrelease-phone span").text(entryArray[0].releaseButtonCaption())
                        $('#button-save-phone').css('color', disabledPhoneButtonColor)
                        $('#button-Delete-phone').css('color', disabledPhoneButtonColor)
                        $('#timeEntryView #btnTimerToggle-phone').hide();  /* hide timer toggle */
                        $('#btnRemove-phone').css('color', 'black')
                        $('#timeEntryView #btnTimerToggle-phone').hide()
                        $("#timeEntryView #list-timeform-phone a.clearEntryField").hide()

                        /* disable fields */
                        let dateField = document.querySelector("#timeEntryView #timeEntryDatePicker-phone-row .phoneTimeInput")
                        if (dateField) dateField.removeEventListener("click", swp.timeEntryView.openDatePicker)
                        $("#timeEntryView #timeEntryDateInput").data("kendoDatePicker").enable(false)
                        $("#timeEntryView #timeEntryDateInput").attr("readonly", true)
                        $('#list-timeform-phone a').removeAttr('href')
                        $('#fieldNarrative-phone').attr("readonly", true)
                        $('#btnSave-phone').css('color', disabledPhoneButtonColor)
                        $('#btnDelete-phone').css('color', disabledPhoneButtonColor)

                        if (swpSettings.unrelease === false) { $('#btnRelease-phone').css('color', disabledPhoneButtonColor) }
                        swp.auth.backup()
                        /* show phone confirmation flag */
                        $('.imgRelease').css("visibility", "visible")

                        setTimeout(function () {
                            $('.imgRelease').css("visibility", "hidden")
                            $("#imgStatus-phone").css("visibility", "visible")
                        }, 2000)
                    }
                },
                error: function (result) {

                    console.log('result', result)
                    if (result.status != 200) {
                        console.log('error release Event', result)
                        //pulling folley stuff for now
                        //return status
                        if (_saveReleaseRecursiveAttempts < _saveReleaseRecursiveLimit) {
                            _saveReleaseRecursiveAttempts = _saveReleaseRecursiveAttempts + 1;
                            module.releaseEntry(that, ev);
                        } else {
                            _saveReleaseRecursiveAttempts = 0;
                            that.status = "Saved"
                            that.statusImage = "img/Saved.png"
                            alert("Release failed, you may be experiencing poor network connectivity. ")
                        }
                    }
                }
            });
        },

        releaseEntryDelayed: function (that, ev) {
            module.checkConnection();

            setTimeout(this.releaseEntry(that, ev), 1000);
        },

        removeActiveEntry: function (e) {

            console.log('removeActiveEntry Clicked')

            if (swpSettings.set_narFullScreen === true) {
                swp.timeEntryView.narClose_Phone()
            }

            swpSettings.bTimers === true
                ? (entryArray = swpSettings.arrEntriesTimers,
                    entryPosition = swpSettings.activeTimerPosition)
                : (entryArray = swpSettings.arrEntries,
                    entryPosition = swpSettings.activeEntryPosition)

            log.debug("Remove entry: " + entryPosition)
            swp.timeentry.actions.clearValidation()

            entryArray.splice(entryPosition, 1)
            0 === entryArray.length || (entryPosition >= entryArray.length && (
                entryPosition = entryArray.length - 1),
                entryArray[entryPosition].status === "Released" || entryArray[entryPosition].status === "Posted"
                    ? ($("#list-timeform-phone a").removeAttr("href"),
                        $("#timeEntryView #fieldNarrative-phone").attr('readonly', true))
                    : ($("#liDate-phone").attr("href", "#datepicker-phone"),
                        $("#liClient-phone").attr("href", "#views/timeEntryViews/clientLookup.html"),
                        $("#liMatter-phone").attr("href", "#views/timeEntryViews/matterLookup.html"),
                        $("#liCustom1-phone").attr("href", "#views/timeEntryViews/custom1Lookup.html"),
                        //$("#liDuration-phone").attr("href", "#views/timeEntryViews/durationView.html"),
                        $("#timeEntryView #fieldNarrative-phone").removeAttr("readonly"))
            )

            swpSettings.bTimers === true
                ? (location.href = "#views/timersView.html",
                    setTimeout(() => {
                        swpSettings.activeTimerPosition = entryPosition
                        // $("#session-entries-list-timer").data("kendoMobileListView").setDataSource(entryArray)
                        // $("#session-entries-list-timer").data("kendoMobileListView").refresh()
                    }, 300))
                : (location.href = "#views/stackView.html",
                    setTimeout(() => {
                        swpSettings.activeEntryPosition = entryPosition
                        $("#session-entries-phone").data("kendoMobileListView").setDataSource(entryArray)
                        $("#session-entries-phone").data("kendoMobileListView").refresh()
                        swp.stackView.checkIncludeToggles()

                        sortOption = swpSettings.stackParams.stackSortOption.split("_")[1]
                        sortDir = "asc"

                        $("#session-entries-phone").data("kendoMobileListView").dataSource.sort([
                            { field: `${sortOption}`, dir: `${sortDir}` },
                        ])

                        kendo.ui.progress($("#stackView"), false)

                    }, 300))
        },

        saveEntry: function (that) {

            swpSettings.bTimers === true
                ? (entryArray = swpSettings.arrEntriesTimers,
                    entryPosition = swpSettings.activeTimerPosition)
                : (entryArray = swpSettings.arrEntries,
                    entryPosition = swpSettings.activeEntryPosition)

            arrEntries = swpSettings.arrEntries
            arrEntriesTimers = swpSettings.arrEntriesTimers
            activeEntryPosition = swpSettings.activeEntryPosition
            activeTimerPosition = swpSettings.activeTimerPosition

            that.set("status", "Saved")
            that.set("statusImage", "img/Saved.png")
            that.set("unsavedFlagClass", "hidden-image")
            swpSettings.massSaveCount++
            // that.set("returned", false)
            $("#timeEntryView #btnTimerToggle-phone").hide()
            
            swp.stackView.checkReturnedStatus(entryArray[entryPosition])
            // swp.stackView.checkFlaggedStatus(entryArray[entryPosition])

            $(`#timersView #${that.id}-save-confirm-phone`).css("display", "inline-flex")
            setTimeout(() => {
                $(`#timersView #${that.id}-save-confirm-phone`).css("display", "none")
            }, 1000)

            $('#timeEntryView .imgSave').css("visibility", "visible")
            $('#timeEntryView .imgSave').delay(1000).css("visibility", "hidden")

            $.ajax({
                type: "POST",
                url: swpSettings.service_url + "saveevent",
                data: JSON.stringify(that),
                contentType: "application/json",
                dataType: "json",
                timeout: 3000,
                success: function (data) {

                    _saveReleaseRecursiveAttempts = 0;
                    log.debug(data); //doesn't work because "saveevent" doesn't return anything
                    console.log('save entry success', data)

                },
                error: function (result) {

                    console.log('result', result)
                    if (result.status != 200) {
                        //pulling folley stuff
                        //set item back to new
                        if (_saveReleaseRecursiveAttempts < _saveReleaseRecursiveLimit) {
                            _saveReleaseRecursiveAttempts = _saveReleaseRecursiveAttempts + 1;
                            module.saveEntry(that)
                        } else {
                            _saveReleaseRecursiveAttempts = 0;
                            that.set("status", "New");
                            that.set("statusImage", "img/New.png");
                            alert("Save failed, you may be experiencing poor network connectivity. ");
                        }
                    }
                }
            })

            // $("#timeEntryView #fieldNarrative-phone").attr('readonly', true)
            module.showSaveConfirmation()
            //$("#fieldDurationTime-phone").val(swpSettings.arrEntries[swpSettings.activeEntryPosition].durationTimeDisplay)

            if (swpSettings.bTimers === false) {
                arrEntries[activeEntryPosition].BillingGuidelineError = ""; //reset error in case it was ignored
            }
            else {
                arrEntriesTimers[activeTimerPosition].BillingGuidelineError = ""; //reset error in case it was ignored
            }
        },

        saveEntryDelayed: function (that) {
            module.checkConnection();

            setTimeout(this.saveEntry(that), 1000);
        },

        saveEntry_Phone: function (overrideValidation) {
            log.debug("set_ValSave: " + swpSettings.set_ValSave + ".")
            let entryArray, entryPosition;

            swpSettings.bTimers == true
                ? (entryArray = swpSettings.arrEntriesTimers,
                    entryPosition = swpSettings.activeTimerPosition)
                : (entryArray = swpSettings.arrEntries,
                    entryPosition = swpSettings.activeEntryPosition)

            swpSettings.offlineMode === false
                ? entryArray[entryPosition].saveEntry(overrideValidation)
                : entryArray[entryPosition].saveLocal()

            swp.auth.backup()
            
            if (swpSettings.set_ValSave == "false" || swpSettings.set_ValSave == "False") {
                swp.timeentry.actions.clearValidation()
            }
        },

        showSaveConfirmation: function () {

            /* show phone confirmation flag */
            $("#imgStatus-phone").css("visibility", "hidden")
            $("#save-confirm-phone").css("visibility", "visible")

            setTimeout(function () {
                $("#save-confirm-phone").css("visibility", "hidden")
                $("#imgStatus-phone").css("visibility", "visible")
            }, 2000)
        },
    }

    return module;

}();
