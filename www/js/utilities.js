//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.utilities) { swp.utilities = {} }

swp.utilities = function () {
    //private stuff
    let arrEntries = swpSettings.arrEntries,
        activeEntryPosition = swpSettings.activeEntryPosition,
        activeTimerPosition = swpSettings.activeTimerPosition,
        entryArray,
        entryPosition,
        massArr, massCount;

        function updateActiveItem() {
            swpSettings.bTimers === true ?
                (entryArray = swpSettings.arrEntriesTimers,
                    entryPosition = swpSettings.activeTimerPosition) :
                (entryArray = swpSettings.arrEntries,
                    entryPosition = swpSettings.activeEntryPosition)
        }

    //public interface
    var module = {

        addItemToCustomMenu: function (ds, filterTerm) {

            /* if length is greater than 1 we have already added someting at the top, remove it */
            if (filterTerm.length > 1) {
                var item, i;
                var raw = ds.data();

                for (i = raw.length - 1; i >= 0; i--) {
                    item = raw[i];

                    if (item.flag === true) {
                        ds.remove(item);
                    }
                }
            }

            ds.insert(0, {
                text: filterTerm,
                flag: true /* flag for removal */
            })
        },

        // checkMatterRef: function () {
        //     let includesMatter = swp.dataSources.matterDataSource.data()
        //     includesMatter = includesMatter.filter(x => x.text === swpSettings.arrEntries[swpSettings.activeEntryPosition].matter)

        //     if (includesMatter.length < 1) {
        //         entryArray[entryPosition].matter = ""
        //         $(`#fieldMatter_phone`).val("")
        //     }
        // },

        cancelMassAction: function () {
            swpSettings.set_MassActionsEnabled = false
            // $("#session-entries-phone #divTouchEntryTemplate-phone").kendoTouch({ tap: '' })

            $("#stackView #statusImageTemplate").css("left", "16px"),
                $("#stackView #returnedEntryIndicator").css("left", "-1px"),
                $("#stackView .stackViewField").css("margin-left", "36px"),
                $("#stackView .stackViewField").css("width", "75%"),
                $("#stackView #dateTemplate").css("margin-left", "36px"),
                $("#stackView #entriesTemplateNarrative-phone").css("margin-left", "0px"),
                $("#stackView #entriesTemplateNarrative-phone").css("width", "83%")

            $("#session-entries-phone .massActionCheckbox").css("display", "none")
            $("#session-entries-phone .massActionCheckboxLabel").css("display", "none")

            $("#stackView #timeEntryAddDefaultDiv").show()
            $("#stackView #massActionCancelTouch").hide()

            $("#stackView #button-massSave-phone").hide()
            $("#stackView #button-massRelease-phone").hide()
            $("#stackView #button-massFlag-phone").hide()
            $("#stackView #button-massRemove-phone").hide()

            $("#stackView #button-massAction-phone").show()
            $("#stackView #button-journal-phone").show()
            $("#stackView #button-clear-phone").show()

            $("#session-entries-phone .massActionCheckbox").prop("checked", false)
            swpSettings.arrEntries.forEach(x => x.includeInMassAction = false)
        },

        checkLocalForCaptureParams: function () {

            if (localStorage.timeCaptureParams) {
                let params = JSON.parse(localStorage.timeCaptureParams)
                params = Object.keys(params)
                params.forEach(x => {
                    swpSettings.timeCaptureParams.x = x.value
                })
            }
            // localStorage.setItem("timeCaptureParams", JSON.stringify(swpSettings.timeCaptureParams))
        },

        checkLocalForIncludeParams: function () {


            if (localStorage.includeTimeEntries !== undefined) {
                localStorage.includeTimeEntries == true || localStorage.includeTimeEntries == "true" || localStorage.includeTimeEntries == "True"
                    ? swpSettings.stackParams.includeTimeEntries = true
                    : swpSettings.stackParams.includeTimeEntries = false
            }

            if (localStorage.includeTimeCaptures !== undefined) {
                localStorage.includeTimeCaptures == true || localStorage.includeTimeCaptures == "true" || localStorage.includeTimeCaptures == "True"
                    ? swpSettings.stackParams.includeTimeCaptures = true
                    : swpSettings.stackParams.includeTimeCaptures = false
            }
        },

        checkDefaultInCustomResult: function (_ds, menu_num) {
            updateActiveItem()

            // for (var menu_num = 1; menu_num < 6; menu_num++) {
                if (swpSettings[`custom${menu_num}`][`set_Custom${menu_num}`] != "") {

                    let input

                    if (swpSettings[`custom${menu_num}`][`set_Custom${menu_num}Default`]) {
                        input = swpSettings[`custom${menu_num}`][`set_Custom${menu_num}Default`].slice();
                    } else if (entryArray[entryPosition] && entryArray[entryPosition].client !== "" && entryArray[entryPosition].client !== null && !_ds.data().map(x => x.text).includes(input)) {
                        input = ""
                    } 

                    if (entryArray[entryPosition]  && (!entryArray[entryPosition][`custom${menu_num}`] || entryArray[entryPosition][`custom${menu_num}`].length < 1)) {
                        entryArray[entryPosition][`custom${menu_num}`] = input
                        $(`#fieldCustom${menu_num}_phone`).val(input)
                    }

                    module.checkSingleCustomResult(_ds, menu_num)
                }
            // }
            // if (swpSettings.custom5.set_Custom5 !== "") {
            //     swp.utilities.refreshCustom(swp.dataSources.custom5_dataSource, '5');
            //     $(".lblfieldCustom5").text(swpSettings.custom5.set_Custom5 + ":");
            // }

            // let input = swpSettings[`custom${menu_num}`][`set_Custom${menu_num}Default`].slice()

            // if (entryArray[entryPosition].client !== "" && entryArray[entryPosition].client !== null && !_ds.data().map(x => x.text).includes(input)) {
            //     input = ""
            // }

            // entryArray[entryPosition][`custom${menu_num}`] = input
            // $(`#fieldCustom${menu_num}_phone`).val(input)
            // if (_ds.data().map(x => x.text).includes(input)) {
            //     entryArray[entryPosition][`custom${menu_num}`] = input
            //     $(`#fieldCustom${menu_num}_phone`).val(input)
            // }
        },

        checkEmptyCustom: function () {
             for (var i = 1; i < 6; i++) {
                 if (swpSettings[`custom${i}`][`set_Custom${i}`] != "") {

                     let _ds = swp.dataSources[`custom${i}_dataSource`],
                         input = swpSettings[`custom${i}`][`set_Custom${i}Default`].slice();

                     if (entryArray[entryPosition].client !== "" && entryArray[entryPosition].client !== null && !_ds.data().map(x => x.text).includes(input)) {
                         input = ""
                     } else {
                         input = swpSettings[`custom${i}`][`set_Custom${i}Default`].slice()
                     }

                     entryArray[entryPosition][`custom${i}`] = input
                     $(`#fieldCustom${i}_phone`).val(input)
                 }
             }
        },

        checkSingleCustomResult: function (_ds, menu_num) {
            updateActiveItem()

            if (_ds.data().length === 1 && entryArray[entryPosition] && entryArray[entryPosition].matter !== "" && entryArray[entryPosition].matter !== null) {
                entryArray[entryPosition][`custom${menu_num}`] = _ds.data()[0].value
                $(`#fieldCustom${menu_num}_phone`).val(_ds.data()[0].value)
            }
        },

        checkSingleMatter: function () {
            if (swp.dataSources.matterDataSource.data().length == 1) {
                ((entryArray[entryPosition].matter = swp.dataSources.matterDataSource.data()[0].text,
                    $("#fieldMatter_phone").val(swp.dataSources.matterDataSource.data()[0].text),
                    $("#fieldMatter_phone").siblings(".invalid-icon").hide(),
                    swp.utilities.refreshBillingGuidelines(),
                    // swp.utilities.refreshCustomMenus(),
                    swpSettings.offlineMode === true && (
                        entryArray[entryPosition].plan1 = ev.dataItem.Plan1,
                        entryArray[entryPosition].plan2 = ev.dataItem.Plan2,
                        entryArray[entryPosition].plan3 = ev.dataItem.Plan3,
                        entryArray[entryPosition].plan4 = ev.dataItem.Plan4)))
            }
        },

        checkTimeFormat: function (num) {
            if (!num || num === "undefined") {
                num = 0
            }

            if (num < 10) {
                num = "0" + num
            }

            return num
        },

        clearButton_Phone: function (e) {
            arrEntries = swpSettings.arrEntries;
            activeEntryPosition = swpSettings.activeEntryPosition;

            if (activeEntryPosition = 0, "true" === swpSettings.set_Timers || swpSettings.set_Timers === !0) {
                for (var a = arrEntries.length; a--;) {
                    "" !== arrEntries[a].get("timerId") && "Released" !== arrEntries[a].get("status") && "Posted" !== arrEntries[a].get("status") || arrEntries.splice(a, 1);
                }
            } else {
                swpSettings.arrEntries = []
                swpSettings.activeEntryPosition = 0
            }
            $("#session-entries-phone").data("kendoMobileListView").setDataSource(arrEntries);
            $("#session-entries-phone").data("kendoMobileListView").refresh();
            kendo.ui.progress($("#stackView"), false)
        },

        cleanupURL: function (url) {
            if (url.substring(0, 7).toLowerCase() != 'http://' && url.substring(0, 7).toLowerCase() != 'https:/') {
                url = 'http://' + url;
            }

            if (url.substring((url.length - 12), url.length) != '/wsRest.svc/') {
                swpSettings.desktop_service_url = url
                url = url + "/wsRest.svc/";
            }

            if (url[url.length - 1] != '/') {
                url = url + "/";
            }
            return url
        },

        closeUserFeedbackMessages: function (e) {
            let view = window.location.hash.split("/")[window.location.hash.split("/").length - 1].split(".")[0]
            $(`#${view} #userFeedbackMessages`).data("kendoMobileCollapsible").collapse()
            $('.k-overlay').remove()
        },

        closeTimerDropDown: function (e) {
            swpSettings.timerDropDown.open = false
            swpSettings.timerDropDown.entryId = ""

            let view = window.location.hash.split("/")[window.location.hash.split("/").length - 1].split(".")[0]
            $(`#${view} #userFeedbackMessages`).data("kendoMobileCollapsible").collapse()
            $('.k-overlay').remove()
        },

        copyMenuToNarrative: function (target, newVal) {
            //place text from cst in narative as well if needed
            var oldVal = $("#timeEntryView #fieldNarrative-phone").val()

            //for the sake of the comparison make sure there is a space in front of any possible delimeter
            var comparVal = newVal.replace(":", " :");
            comparVal = newVal.replace("-", " -");

            //now find the first space
            var firstSpace = comparVal.indexOf(" ");

            //parse text from full code
            newVal = newVal.substr(firstSpace + 2);

            if (oldVal.trim().replace(/&nbsp;/g, '').length === 0) {
                if (oldVal.indexOf(newVal) === -1) {
                    $("#timeEntryView #fieldNarrative-phone").val(newVal.trim())
                    swpSettings.bTimers === true
                        ? swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].narrative = newVal.trim()
                        : swpSettings.arrEntries[swpSettings.activeEntryPosition].narrative = newVal.trim()
                }
            }
        },

        fixAndroidStatusBar: function () {
            //this function will make sure full screen is enabled and the status bar is hidden
            if (window.Capacitor.platform === "android") {
                $("#list-timeform-phone").css("min-height", "600px") //first set min height for android small devices
                var matches = device.version.match(/[0-9]+(\.[0-9]+)?/i);

                if (matches.length && parseFloat(matches[0]) < 4.2) {
                    document.body.style.zoom = 1 / window.devicePixelRatio;
                }

                StatusBar.backgroundColorByHexString("#00000000");
                StatusBar.hide();

                log.info("tYPE OF FULL SCREEN", typeof window.AndroidFullScreen)
                log.info(typeof window.AndroidFullScreen)

                if (window.AndroidFullScreen) {
                    // Extend your app underneath the status bar (Android 4.4+ only)
                    window.AndroidFullScreen.showUnderStatusBar();

                    // Extend your app underneath the system UI (Android 4.4+ only)
                    window.AndroidFullScreen.showUnderSystemUI();

                    // Hide system UI and keep it hidden (Android 4.4+ only)
                    window.AndroidFullScreen.immersiveMode();
                }
            }
        },

        generalViewShow: function () {
            let view = window.location.hash.split("/")[window.location.hash.split("/").length - 1].split(".")[0]
            document.scrollingElement.scrollTo(0, 0)
            $(`#${view} #timerColorIndicatorTouch`).hide()
            swpSettings.searchParams.searchAllEntries = false
        },

        isAndroid: function () {
            if (window.Capacitor.platform === "android") {
                return true;
            }

            return false;
        },

        isIOS: function () {

            var isIphone = navigator.userAgent.indexOf("iPhone") != -1;
            var isIpod = navigator.userAgent.indexOf("iPod") != -1;
            var isIpad = navigator.userAgent.indexOf("iPad") != -1;

            // now set one variable for all iOS devices
            var isIos = isIphone || isIpod || isIpad;

            if (isIos) {
                return true
            }

            return false
        },

        journalButton_Phone: function (e) {
            let a = [];

            arrEntries = swpSettings.arrEntries
            activeEntryPosition = swpSettings.activeEntryPosition
            $("#stackView #userFeedbackMessages .km-collapsible-content").children().hide()

            for (b = 0; b < arrEntries.length; b++) {
                let c = new swp.entry.newEventObject;
                c.eventDT = arrEntries[b].eventDT;
                c.client = arrEntries[b].client;
                c.matter = arrEntries[b].matter;
                c.custom1 = arrEntries[b].custom1;
                c.custom2 = arrEntries[b].custom2;
                c.custom3 = arrEntries[b].custom3;
                c.custom4 = arrEntries[b].custom4;
                c.custom5 = arrEntries[b].custom5;
                c.duration = arrEntries[b].duration;
                c.username = arrEntries[b].username;
                c.narrative = arrEntries[b].narrative;
                c.id = arrEntries[b].id;
                a.push(c);
            }

            $.ajax({
                type: "POST",
                url: swpSettings.service_url + "journal/" + swpSettings.swp_emailaddr,
                data: JSON.stringify(a),
                contentType: "application/json",
                dataType: "json",
                success: function (a) {
                    alert(a)
                },
                failure: function (a) {
                    alert(a)
                }
            })

            setTimeout(() => {
                $("#stackView #userFeedbackMessages p").text("Report Emailed");
                $("#stackView #userFeedbackMessages p").show()
                swp.utilities.toggleUserFeedbackMessages(this)
            }, 500)
        },

        massAction_Phone: function (e) {
            console.log(e)
            swpSettings.set_MassActionsEnabled = true
            // $("#session-entries-phone #divTouchEntryTemplate-phone").kendoTouch({ tap: 'swp.stackView.handleMassActionToggle' })
            // $(e)._click = $(e).click
            // $(e).click = null;

            $("#stackView .round").kendoTouch({
                touch: (e) => {
                    swp.stackView.handleMassActionToggle(e)
                },
                tap: (e) => {
                    swp.stackView.handleMassActionToggle(e)
                }
            })

            $("#stackView #statusImageTemplate").css("left", "55px"),
                $("#stackView #returnedEntryIndicator").css("left", "38px"),
                $("#stackView .stackViewField").css("margin-left", "75px"),
                $("#stackView .stackViewField").css("width", "68%"),
                $("#stackView #dateTemplate").css("margin-left", "75px"),
                $("#stackView #entriesTemplateNarrative-phone").css("margin-left", "39px"),
                $("#stackView #entriesTemplateNarrative-phone").css("width", "72%")

            $("#session-entries-phone .massActionCheckbox").css("display", "block")
            $("#session-entries-phone .massActionCheckboxLabel").css("display", "block")

            $("#stackView #timeEntryAddDefaultDiv").hide()
            $("#stackView #massActionCancelTouch").show()

            $("#stackView #button-massSave-phone").show()
            $("#stackView #button-massRelease-phone").show()
            $("#stackView #button-massFlag-phone").show()
            $("#stackView #button-massRemove-phone").show()

            $("#stackView #button-massAction-phone").hide()
            $("#stackView #button-journal-phone").hide()
            $("#stackView #button-clear-phone").hide()



            // document.querySelectorAll("#session-entries-phone #divTouchEntryTemplate-phone").forEach(x => x.dataset.touchstart = 'swp.stackView.handleMassActionToggle')
            // document.querySelectorAll("#session-entries-phone #divTouchEntryTemplate-phone").forEach(x => x.dataset.touchstart = 'console.log("touchstartchange")')
        },

        massFlag_Phone: function (e) {
            let b = " entries flagged."
            swpSettings.massFlagCount = 0
            swpSettings.massSelected = 0

            $("#stackView #userFeedbackMessages .km-collapsible-content").children().hide()

            swpSettings.arrEntries.forEach(x => {
                if (x.includeInMassAction === true) {
                    swpSettings.activeEntryPosition = swpSettings.arrEntries.indexOf(x)

                    swpSettings.arrEntries[swpSettings.activeEntryPosition].flagged = true
                    swp.stackView.checkReturnedStatus(x)
                    swp.stackView.checkFlaggedStatus(x)
                    swpSettings.massFlagCount++
                    swpSettings.massSelected++
                }
            })

            setTimeout(() => {

                if (swpSettings.massFlagCount === 1) {
                    b = " entry flagged.";
                }

                $("#stackView #userFeedbackMessageContent").text(`${swpSettings.massFlagCount.toString()} of ${swpSettings.massSelected} ${b}`)
                $("#stackView #userFeedbackMessageContent").show()
                module.toggleUserFeedbackMessages(this)
                module.cancelMassAction()
            }, 500)

        },

        massRelease_Phone: function (e) {
            let b = " entries released.";
            swpSettings.supressError = !0
            swpSettings.massReleaseCount = 0
            swpSettings.massSelected = 0
            $("#stackView #userFeedbackMessages .km-collapsible-content").children().hide()



            swpSettings.arrEntries.forEach(x => {
                if (x.includeInMassAction === true) {

                    swpSettings.activeEntryPosition = swpSettings.arrEntries.indexOf(x)

                    swpSettings.arrEntries[swpSettings.activeEntryPosition].validate()

                    if (swpSettings.arrEntries[swpSettings.activeEntryPosition].status != "Released" && swpSettings.arrEntries[swpSettings.activeEntryPosition].status != "Posted") {
                        swpSettings.arrEntries[swpSettings.activeEntryPosition].releaseEntry();
                    }

                    swp.stackView.checkReturnedStatus(swpSettings.arrEntries[swpSettings.activeEntryPosition])
                    // swp.stackView.checkFlaggedStatus(swpSettings.arrEntries[swpSettings.activeEntryPosition])
                    swpSettings.massSelected++
                }
            })

            setTimeout(() => {
                swpSettings.arrEntries.forEach(x => {
                    swp.stackView.checkReturnedStatus(x)
                    // swp.stackView.checkFlaggedStatus(x)
                })

                // swp.stackView.checkEntryType()
                if (swpSettings.massReleaseCount === 1) {
                    b = " entry released.";
                }

                $("#stackView #userFeedbackMessageContent").text(`${swpSettings.massReleaseCount.toString()} of ${swpSettings.massSelected} ${b}`)
                $("#stackView #userFeedbackMessageContent").show()
                module.toggleUserFeedbackMessages(this)
                module.cancelMassAction()
                swp.stackView.checkIncludeToggles()
                // swp.stackView.checkEntryType()
            }, 300)
        },

        massRemove_Phone: function (e) {
            let b = " entries removed.";
            swpSettings.massRemoveCount = 0
            swpSettings.massSelected = 0
            $("#stackView #userFeedbackMessages .km-collapsible-content").children().hide()

            swpSettings.arrEntries.forEach(x => {
                if (x.includeInMassAction === true) {
                    swpSettings.activeEntryPosition = swpSettings.arrEntries.indexOf(x)

                    swp.timeentry.actions.removeActiveEntry()
                    swpSettings.massRemoveCount++
                    swpSettings.massSelected++
                }
            })

            setTimeout(() => {

                if (swpSettings.massRemoveCount === 1) {
                    b = " entry removed.";
                }

                $("#stackView #userFeedbackMessages #userFeedbackMessageContent").text(`${swpSettings.massRemoveCount.toString()} of ${swpSettings.massSelected} ${b}`)
                $("#stackView #userFeedbackMessages #userFeedbackMessageContent").show()
                module.toggleUserFeedbackMessages(this)
                module.cancelMassAction()
                swp.stackView.checkIncludeToggles()
                // swp.stackView.checkEntryType()
            }, 500)

        },

        massSave_Phone: function (e) {
            let b = " entries saved.";
            swpSettings.massSaveCount = 0
            swpSettings.massSelected = 0

            $("#stackView #userFeedbackMessages .km-collapsible-content").children().hide()

            swpSettings.arrEntries.forEach(x => {
                if (x.includeInMassAction === true) {
                    swpSettings.activeEntryPosition = swpSettings.arrEntries.indexOf(x)

                    swpSettings.offlineMode === false
                        ? swpSettings.arrEntries[swpSettings.activeEntryPosition].saveEntry(e)
                        : swpSettings.arrEntries[swpSettings.activeEntryPosition].saveLocal(e)

                    swp.stackView.checkReturnedStatus(x)
                    // swp.stackView.checkFlaggedStatus(x)
                    swpSettings.massSelected++
                }
            })

            module.cancelMassAction()

            setTimeout(() => {

                if (swpSettings.massSaveCount === 1) {
                    b = " entry saved.";
                }

                $("#stackView #userFeedbackMessages #userFeedbackMessageContent").text(`${swpSettings.massSaveCount.toString()} of ${swpSettings.massSelected} ${b}`)
                $("#stackView #userFeedbackMessages #userFeedbackMessageContent").show()
                module.toggleUserFeedbackMessages(this)
                swp.stackView.checkIncludeToggles()
                // swp.stackView.checkEntryType()
            }, 500)

        },

        messageHeaderCollapse: function () {
            $('.k-overlay').remove()
            swpSettings.set_ToggleActive = false
        },

        parseNumValue: function (aValue) {
            //KP 2018-9-94: updated to handle ","
            if (swpSettings.decimalComma === true) {
                aValue = aValue.toString().replace(",", ".");
            }

            if (aValue != undefined)
                if (aValue.toString().substring(0, 1) == ".")
                    aValue = "0" + aValue.toString();

            /* I'm wrapping the value in a parseFloat first because parseFloat can handle something
                that isNaN would still say are invalid (multiple dots for example) */
            if (isNaN(parseFloat(aValue)))
                return 0;
            else
                if (aValue.toString().indexOf(".") > 0)
                    return parseFloat(aValue);
                else
                    return parseInt(aValue);
        },

        refreshBillingGuidelines: function () {
            let activeEntry, guidelines;

            if (swpSettings.bTimers === true) {
                activeEntry = swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition]
            } else {
                activeEntry = swpSettings.arrEntries[swpSettings.activeEntryPosition]
            }

            if (!activeEntry) return
            if (activeEntry.client) {
                if (activeEntry.matter == "") activeEntry.matter = null
                $.ajax({
                    url: `${swpSettings.service_url}billingguideline/${swpSettings.objTook.token}/${activeEntry.client}/${activeEntry.matter}`,
                    type: "GET",
                    success: function (result) {
                        console.log("success:", result)
                        if (result.Description !== "") {
                            // let guidelines = JSON.parse
                            $("#userFeedbackMessages .km-collapsible-content #billingGuidelines").empty()
                            $("#userFeedbackMessages .km-collapsible-content #billingGuidelineErrors").empty()
                            swp.entry.setupBillDisplay(result, '#billingGuidelines')
                            $("#toggleBillingGuideInfo .km-warning-swp").css("color", "black")
                            let bg = document.querySelector("#timeEntryView #billingGuidelines")

                            bg
                                ? bg = bg.childElementCount
                                : bg = 0

                            bg > 0
                                ? ($("#toggleBillingGuideInfo").show(),
                                    $("#timeEntryView #bgHeaderContainer").show(),
                                    $("#timeEntryView #billingGuidelines").show())
                                : ($("#toggleBillingGuideInfo").hide(),
                                    $("#userFeedbackMessages .km-collapsible-content #billingGuidelines").empty(),
                                    $("#userFeedbackMessages .km-collapsible-content #billingGuidelineErrors").empty())
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#toggleBillingGuideInfo").hide()
                        $("#userFeedbackMessages .km-collapsible-content #billingGuidelines").empty()
                        $("#userFeedbackMessages .km-collapsible-content #billingGuidelineErrors").empty()
                        console.log("error", thrownError)
                    }
                })

                setTimeout(() => {
                    $("#timeEntryView #userFeedbackMessages p").text(guidelines)
                }, 300)
            } else {
                $("#toggleBillingGuideInfo").hide()
            }
        },

        refreshCustom: function (_ds, menu_num) {

            log.debug('Refreshing menu ' + menu_num.toString() + ' ');

            //if (swpSettings.offlineMode === false) {

            var client = $("#fieldClient_phone").val()
            var matter = $("#fieldMatter_phone").val()
            var custom1 = $("#fieldCustom1_phone").val()
            var custom2 = $("#fieldCustom2_phone").val()
            var custom3 = $("#fieldCustom3_phone").val()
            var custom4 = $("#fieldCustom4_phone").val()
            var custom5 = $("#fieldCustom5_phone").val()

            /*if (swpSettings.bTimers === true) {
                client = $("#fieldClient_timer").val().toString().split(" ")[0];
                matter = $("#fieldMatter_timer").val().toString().split(" ")[0];
                custom1 = $("#fieldCustom1_timer").val().toString().split(" ")[0];
                custom2 = $("#fieldCustom2_timer").val().toString().split(" ")[0];
                custom3 = $("#fieldCustom3_timer").val().toString().split(" ")[0];
            }*/

            if (client != null && !swpSettings.isTablet) {
                client = $("#fieldClient_phone").val().toString().split(" ")[0];
                matter = $("#fieldMatter_phone").val().toString().split(" ")[0];
                custom1 = $("#fieldCustom1_phone").val().toString().split(" ")[0];
                custom2 = $("#fieldCustom2_phone").val().toString().split(" ")[0];
                custom3 = $("#fieldCustom3_phone").val().toString().split(" ")[0];
                custom4 = $("#fieldCustom4_phone").val().toString().split(" ")[0];
                custom5 = $("#fieldCustom5_phone").val().toString().split(" ")[0];
            }

            if (_ds != null && matter != null) {

                var customUrl = swpSettings.service_url + "custom" + menu_num + "/?user=" + swpSettings.swp_username + "&client=" + client + "&matter=" + matter;
                var fullUrl;
                /* set the URL based on which menus is being updated */
                log.info(menu_num, "MENU NUMBER", customUrl, "Custom URL")
                if (menu_num == "2" || menu_num == "3" || menu_num == "4") {
                    customUrl = customUrl + "&custom1=" + custom1;

                    if (swpSettings.custom2.set_Custom2 == "")
                        customUrl = "";
                }

                if (menu_num == "3") {
                    customUrl = customUrl + "&custom2=" + custom2;

                    if (swpSettings.custom3.set_Custom3 === "")
                        customUrl = "";
                }

                if (menu_num == "4") {
                    customUrl = customUrl + "&custom2=" + custom2 + "&custom3=" + custom3;

                    if (swpSettings.custom4.set_Custom4 === "")
                        customUrl = "";
                }

                if (menu_num == "5") {
                    customUrl = customUrl + "&custom2=" + custom2 + "&custom3=" + custom3;

                    if (swpSettings.custom5.set_Custom5 === "")
                        customUrl = "";
                }

                fullUrl = customUrl + '&took=' + swpSettings.objTook.token;

                if (customUrl != "" && swpSettings.offlineMode === false) {
                    log.debug('URL: ' + fullUrl);
                    _ds.transport.options.read.url = fullUrl;
                    _ds.read();
                } else {
                    /* if offline always refresh */
                    _ds.read();
                }

                module.checkSingleCustomResult(_ds, menu_num)

                setTimeout(() => {
                    module.checkDefaultInCustomResult(_ds, menu_num)
                }, 200)
                
            } else {
                log.debug('_ds is null');
            }
        },

        refreshCustomMenus: function () {
            log.debug('refreshing customs, timers=' + swpSettings.bTimers);
            $(".lblfieldClient").text(swpSettings.clientLabel + ":")
            $(".lblfieldMatter").text(swpSettings.matterLabel + ":")

            swpSettings.bTimers === true 
            ? (entryArray = swpSettings.arrEntriesTimers,
            entryPosition = swpSettings.activeTimerPosition)
            : (entryArray = swpSettings.arrEntries, 
                entryPosition = swpSettings.activeEntryPosition)

            if (swpSettings.custom1.set_Custom1 != "") {
                swp.utilities.refreshCustom(swp.dataSources.custom1_dataSource, '1');
                $(".lblfieldCustom1").text(swpSettings.custom1.set_Custom1 + ":");
            }

            if (swpSettings.custom2.set_Custom2 != "") {
                swp.utilities.refreshCustom(swp.dataSources.custom2_dataSource, '2');
                $(".lblfieldCustom2").text(swpSettings.custom2.set_Custom2 + ":");
            }

            if (swpSettings.custom3.set_Custom3 != "") {
                swp.utilities.refreshCustom(swp.dataSources.custom3_dataSource, '3');
                $(".lblfieldCustom3").text(swpSettings.custom3.set_Custom3 + ":");
            }

            if (swpSettings.custom4.set_Custom4 != "") {
                swp.utilities.refreshCustom(swp.dataSources.custom4_dataSource, '4');
                $(".lblfieldCustom4").text(swpSettings.custom4.set_Custom4 + ":");
            }

            if (swpSettings.custom5.set_Custom5 !== "") {
                swp.utilities.refreshCustom(swp.dataSources.custom5_dataSource, '5');
                $(".lblfieldCustom5").text(swpSettings.custom5.set_Custom5 + ":");
            }
        },

        refreshMatters: function () {
            let matterDataSource = swp.dataSources.matterDataSource,
                a;
            try {
                swpSettings.bTimers === true
                    ? (entryArray = swpSettings.arrEntriesTimers,
                        entryPosition = swpSettings.activeTimerPosition)
                    : (entryArray = swpSettings.arrEntries,
                        entryPosition = swpSettings.activeEntryPosition)

                a = entryArray[entryPosition].client.toString().split(" ")[0]

                log.debug("Refreshing matter list for client: " + a)
                if (null != swp.dataSources.matterDataSource) {
                    if (swpSettings.searchParams.searchAllEntries == true) {
                        swp.dataSources.matterDataSource.transport.options.read.url = swpSettings.service_url + "matters/50/" + swpSettings.objTook.token + "/"
                    } else {
                        swp.dataSources.matterDataSource.transport.options.read.url = swpSettings.service_url + "matters/50/" + a + "/" + swpSettings.objTook.token + "/"
                    }
                    swp.dataSources.matterDataSource.read()
                    if ($("#matter-listview-phone").data("kendoMobileListView")) $("#matter-listview-phone").data("kendoMobileListView").refresh()
                }
                // module.checkDefaultInCustomResult()
                // module.checkSingleMatter()
                // swp.dataSources.matterDataSource.data()
            } catch (a) {
                console.log("Error refreshing matter list")
                log.debug("Error refreshing matter list")
            }
        },

        releasedPostedCheck: function (arr, arrPosition) {
            arr[arrPosition].status === "Released" || arr[arrPosition].status === "Posted"
                ? ($("#list-timeform-phone a").removeAttr("href"),
                    $("#timeEntryView #fieldNarrative-phone").attr("readonly", true))
                : ($("#liDate-phone").attr("href", "#datepicker-phone"),
                    $("#liClient-phone").attr("href", "#views/timeEntryViews/clientLookup.html"),
                    $("#liMatter-phone").attr("href", "#views/timeEntryViews/matterLookup.html"),
                    $("#liCustom1-phone").attr("href", "#views/timeEntryViews/custom1Lookup.html"),
                    $("#timeEntryView #fieldNarrative-phone").removeAttr("readonly"))
        },

        removeWSRest: function (url) {

            if (url.substring((url.length - 12), url.length) == '/wsRest.svc/') {
                url = url.substring(0, url.length - 12)
                swpSettings.desktop_service_url = url
            }

            return url
        },

        resetSearchParams: function () {
            swpSettings.searchParams.searchAllEntries = true
            swpSettings.searchParams.searchDateStart = "1/1/1990"
            swpSettings.searchParams.searchDateEnd = "12/31/2040"
            swpSettings.searchParams.searchEntriesClient = ""
            swpSettings.searchParams.searchEntriesMatter = ""
            searchStateArr = []
            swpSettings.searchParams.saved = 0
            swpSettings.searchParams.released = 0
            swpSettings.searchParams.posted = 0
            swpSettings.searchParams.returned = 0

            $("#searchStartDatePicker").data("kendoDateInput").value(swpSettings.searchParams.searchDateStart)
            $("#searchEndDatePicker").data("kendoDateInput").value(swpSettings.searchParams.searchDateEnd)

            $("#searchView #list-search-phone span input").val("")
            $("#searchView #list-search-phone a input").val("")
            $("#searchView #list-search-phone .round > input").prop("checked", false)
        },

        stringToBoolean: function (string) {

            if (typeof string == 'undefined')
                return "";

            if (typeof string == 'boolean')
                return string;

            switch (string.toLowerCase()) {
                case "true": case "yes": case "1": return true;
                case "false": case "no": case "0": case null: return false;
                default: return Boolean(string);
            }
        },

        setTimeToLocal: function (entry, stamp) {

            //The timestamp comes back to us as an epoch, the timezone is provided but the timezone does
            //not actually impact the epoch value, it just lets us know what the server tz is.  We need
            //to parse the timezone and then offset based on the local timezone.

            //KP 3/30/17-- There is an issue where the server does not always return the same offset because it
            //varies based on time of year (DST).  So right after a daylight savings change items from before
            //the change come back with the other offset and then the below adjustment breaks.  I fixed this in
            //the web services by making sure they are adjusted.

            var hrs = -stamp.substr(stamp.indexOf('-') + 1, 2) - swpSettings.localOffset;

            //could be positive hours
            if (stamp.indexOf('-') < 1) {
                hrs = stamp.substr(stamp.indexOf('+') + 1, 2) - swpSettings.localOffset;
            }

            entry.eventDT = moment(stamp).add(hrs, 'h').toDate(); //format('l').toString());
            entry.sortDate = moment(stamp);

            return entry;
        },

        toggleBillingGuideInfo: function (e) {
            $("#timeEntryView #userFeedbackMessages .km-collapsible-content").children().hide()

            if ($("#timeEntryView #billingGuidelines").children().length) {
                $("#timeEntryView #bgHeaderContainer").show()
                $("#timeEntryView #billingGuidelines").show()
                let errCount = document.querySelector("#timeEntryView #billingGuidelineErrors").childElementCount
                if (errCount > 0) {
                    $("#timeEntryView #bgViolationContainer").show()
                    $("#timeEntryView #billingGuidelineErrors").show()
                }
                module.toggleUserFeedbackMessages(this)
                $("#timeEntryView .km-collapsible-header .km-x-swp").css("top", "2.7em")
            }
        },

        toggleBGReleaseOverride: function (e) {
            $("#timeEntryView #userFeedbackMessages .km-collapsible-content").children().hide()
            $("#timeEntryView #bgHeaderContainer").show()
            $("#timeEntryView #billingGuidelines").show()
            let errCount = document.querySelector("#timeEntryView #billingGuidelineErrors").childElementCount
            if (errCount > 0) {
                $("#timeEntryView #bgViolationContainer").show()
                $("#timeEntryView #billingGuidelineErrors").show()
                swpSettings.set_IgnoreBillingGuidelines == "true" || swpSettings.set_IgnoreBillingGuidelines == "True" || swpSettings.set_IgnoreBillingGuidelines == true
                    ? $("#timeEntryView #bgReleaseOverrideButtons").show()
                    : $("#timeEntryView #bgReleaseOverrideButtons").hide()
            }
            module.toggleUserFeedbackMessages(this)
            $("#timeEntryView .km-collapsible-header .km-x-swp").css("top", "2.7em")
        },

        toggleEventTitleInfo: function (e) {
            $("#timeEntryView #userFeedbackMessages .km-collapsible-content").children().hide()
            if (swpSettings.arrEntries[swpSettings.activeEntryPosition].eventTitle == (null || "")) {
                swpSettings.arrEntries[swpSettings.activeEntryPosition].eventTitle = "Time Entry"
            }
            $("#timeEntryView #userFeedbackMessageContent").text(swpSettings.arrEntries[swpSettings.activeEntryPosition].eventTitle)
            $("#timeEntryView #userFeedbackMessageContent").show()
            module.toggleUserFeedbackMessages(this)
            $("#timeEntryView .km-collapsible-header .km-x-swp").css("top", "2.7em")
        },

        toggleNarrDescDrowpdown: function () {
            $("#timeEntryView #userFeedbackMessages .km-collapsible-content").children().hide()

            swpSettings.arrEntries[swpSettings.activeEntryPosition].returned === true
                ? swpSettings.arrEntries[swpSettings.activeEntryPosition].prebillNarrative.length
                    ? $("#timeEntryView #userFeedbackMessageContent").text(swpSettings.arrEntries[swpSettings.activeEntryPosition].prebillNarrative)
                    : $("#timeEntryView #userFeedbackMessageContent").text("Returned Entry")
                : $("#timeEntryView #userFeedbackMessageContent").text(swpSettings.arrEntries[swpSettings.activeEntryPosition].description)

            $("#timeEntryView #userFeedbackMessageContent").show()
            module.toggleUserFeedbackMessages(this)
        },

        toggleSortDrowpdown: function () {
            $("#sortByTimerClientRow > div > span").text(swpSettings.clientLabel)

            $("#timersView #userFeedbackMessages .km-collapsible-content").children().hide()
            $("#timersView #timersSortCollapsibleHeader").show()
            module.toggleUserFeedbackMessages(this)
            $(`#timersView #timersSortList input`).prop('checked', false)
            $(`#timersView #${swpSettings.timerSortOption}`).prop('checked', true)
            $("#timersView .km-collapsible-header .km-x-swp").css("top", "2.4em")
        },

        toggleSortDirection: function () {
            let sortOption = swpSettings.stackParams.stackSortOption.split("_")[1];

            swpSettings.stackParams.stackSortDirection == "asc"
            ? (swpSettings.stackParams.stackSortDirection = "desc",
                $("#sortDirectionToggle span").removeClass("k-flip-v"))
            : (swpSettings.stackParams.stackSortDirection = "asc",
                $("#sortDirectionToggle span").addClass("k-flip-v"))
            
            swp.stackView.updateSorting(sortOption, swpSettings.stackParams.stackSortDirection)
            swp.stackView.checkEntryType()

            $("#stackView #session-entries-phone").data("kendoMobileListView").scroller().reset()
        },

        toggleStackIncludeDrowpdown: function () {
            //dsiable functionality if mass action toggle has been clicked
            if (swpSettings.set_MassActionsEnabled === true) return

            $("#stackView #userFeedbackMessages .km-collapsible-content").children().hide()
            $("#stackView #stackIncShwCollapsibleHeader").show()
            swp.stackView.setupCaptureButtons()
            swp.stackView.checkIncludeToggles()
            swp.stackView.checkEntryType()
            module.toggleUserFeedbackMessages(this)
            $(`#stackView #userFeedbackMessages .km-collapsible-content`).height($(`#stackView #stackIncShwCollapsibleHeader`).height() + 15)
            // $(`#stackView #timersSortList input`).prop('checked', false)
            // $(`#stackView #${swpSettings.timerSortOption}`).prop('checked', true)
            // $("#stackView .km-collapsible-header .km-x-swp").css("top", "2.4em")
        },

        toggleStackSortDrowpdown: function () {
            //dsiable functionality if mass action toggle has been clicked
            if (swpSettings.set_MassActionsEnabled === true) return
            $("#sortByEntryClientRow > div > span").text(swpSettings.clientLabel)

            // let sortOption = swpSettings.stackParams.stackSortOption.split("_")[1]
            $("#stackView #userFeedbackMessages .km-collapsible-content").children().hide()
            $("#stackView #stackSortCollapsibleHeader").show()
            $("#stackView #timersSortList input").prop('checked', false)
            $(`#stackView #${swpSettings.stackParams.stackSortOption}`).prop('checked', true)
            module.toggleUserFeedbackMessages(this)
        },

        toggleUserFeedbackMessages: function (e) {
            let view = window.location.hash.split("/")[window.location.hash.split("/").length - 1].split(".")[0]
            if ($(`#${view} #userFeedbackMessages`).hasClass("km-collapsed") === true) {
                $(`#${view} #userFeedbackMessages`).data("kendoMobileCollapsible").expand()
                $("<div class='k-overlay' id='greyOverlay'></div>").appendTo($(`#${view} .km-content`))
                $("<div class='k-overlay' id='greyOverlay'></div>").appendTo($(`#${view} #divStackButtons-phone`))
                setTimeout(() => {
                    $("#greyOverlay").click(swp.utilities.toggleUserFeedbackMessages)
                    $("#greyOverlayFoot").click(swp.utilities.toggleUserFeedbackMessages)
                }, 300)
            } else {
                $(`#${view} #userFeedbackMessages`).data("kendoMobileCollapsible").collapse()
                $('.k-overlay').remove()
                swpSettings.timerDropDown.open = false
                swpSettings.timerDropDown.entryId = ""
            }
        },

    }

    return module;

}()
