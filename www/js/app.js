var Auth0 = require('auth0-js');
var Auth0Cordova = require('@auth0/cordova');
var _user;
var _pass;
var _ogPass = "";
var _userSub;
//if testing is needed ,htis is originial hard coded auth config var
// const AUTH_CONFIG = {
//   // Needed for Auth0 (capitalization: ID):
//   clientID: 'CbybGnmOWpOmxxfN80CfL0ZLqtmzD2ql',
//   // Needed for Auth0Cordova (capitalization: Id):
//   clientId: 'CbybGnmOWpOmxxfN80CfL0ZLqtmzD2ql',
//   domain: 'smarttime.auth0.com',
//   connection:   "smarttime-test",
//   packageIdentifier: 'com.smartwebparts.smarttime' // config.xml widget ID, e.g., com.auth0.ionic
// };
const AUTH_CONFIG = {
  // Needed for Auth0 (capitalization: ID):
  clientID: '',
  // Needed for Auth0Cordova (capitalization: Id):
  clientId: '',
  domain: 'smarttime.auth0.com',
  connection: "",
  packageIdentifier: 'com.smartwebparts.smarttime' // config.xml widget ID, e.g., com.auth0.ionic
};

log.info("app . js firing ")
function getAllBySelector(arg) {
  return document.querySelectorAll(arg);
}

function getBySelector(arg) {
  return document.querySelector(arg);
}

function getById(id) {
  return document.getElementById(id);
}

function getAllByClassName(className) {
  return document.getElementsByClassName(className);
}

function initializeAdminLogin(user, pass) {
  if (isTablet) {
    if (pass == 'admin') {
      function hideTabstrip() {
        /*grant access to settings screen only*/
        $("#splash-screen").hide();
        $("#splash-screen-vertical").hide();
        $("#popover-login").data("kendoMobileModalView").close();

        /*hide all fields but the URL */
        $("#liEmail_tab").hide();
        $("#listview-settings-timer").hide();
        $("#liAuth_tab").hide();
        $('#liOfflinePass_tab').hide();

        location.href = "#tabstrip-settings";

        $(".km-tabstrip").hide();

        $.when(this).done(function () {

          if ($(".km-tabstrip")[0].style.display !== "none") {
            setTimeout(function () {
              hideTabstrip()
            }, 500)
          }
        })
      }

      hideTabstrip()
    }
  } else {
    if (pass == "admin") {
      $("#liEmailSets-phone").hide()
      $("#liTimersSets-phone").hide()
      $("#liMultTimersSets-phone").hide()
      $("#btnLogout-phone").hide()
      location.href = "#tabstrip-phone-settings"
      $(".km-tabstrip").hide()

      $.when(this).done(function () {

        if ($(".km-tabstrip")[0].style.display !== "none") {
          setTimeout(function () {
            hideTabstrip()
          }, 500)
        }
      })
    }
  }
}

function App() {
  log.info("HERE IS AUTH0 FROM APP()")

  this.auth0 = new Auth0.WebAuth(AUTH_CONFIG);
  log.info("HERE IS AUTH0 FROM APP()")
  log.info(this.auth0.toString())
  this.login = this.login.bind(this);
  this.logout = this.logout.bind(this);
}

function setRemeberMe() {
  log.info("SET REMEMBER ME HIT. PLACING VALUES IN TEXTBOX")
  var phoneUserInput = $('#txtLoginUser-phone');
  var userInput = $('#txtLoginUser');
  var phonePassInput = $("#txtLoginPass-phone");
  var tabPass = $('#txtLoginPass');

  if (localStorage.getItem("swp_RememberUser") === 'true') {
    if (phoneUserInput.val() !== localStorage.getItem("swp_username")) {
      phoneUserInput.empty().val(localStorage.getItem("swp_username"));
    }
    if (userInput.val() !== localStorage.getItem("swp_username")) {
      userInput.empty().val(localStorage.getItem("swp_username"));
    }
    if (phonePassInput.val() !== localStorage.getItem("swp_password")) {
      phonePassInput.empty().val(localStorage.getItem("swp_password"));
    }
    if (tabPass.val() !== localStorage.getItem("swp_password")) {
      tabPass.empty().val(localStorage.getItem("swp_password"));
    }

    $("#checkRemember-phone").prop("checked", true);
  }
  else {
    $("#checkRemember-phone").prop("checked", false);
  }
}

function setRemeberMeLocalStorage(user, pass) {
  //alert("SET REMEMBER ME LOCAL STORAGE HIT. PLACING VALUES IN LOCAL STORAGE")
  //alert(user)
  //alert(pass)

  var loginButtonPhone = $('#btnLogin-phone');
  // var loginButtonTab = $('#btnLogin');
  var passButtonPhone = $("#txtLoginPass-phone");
  // var passButtonTab = $('#txtLoginPass');
  //check the switch to see if we should or should not remember
  if (localStorage.getItem("swp_RememberUser") !== $("#checkRemember-phone").prop("checked")) {
    localStorage.setItem("swp_RememberUser", $("#checkRemember-phone").prop("checked"))
  }

  //place user and pass if remeber me is set
  if (localStorage.getItem("swp_RememberUser") == 'true') {
    if (user !== "" && user !== localStorage.getItem("swp_username") ||
      pass !== "" && pass !== localStorage.getItem("swp_password")) {
      localStorage.setItem("swp_username", user);
      localStorage.setItem("swp_password", pass);
    }

  }
  else {
    localStorage.removeItem("swp_username");
    localStorage.removeItem("swp_password");
  }
}

App.prototype.state = {
  authenticated: false,
  accessToken: false,
  currentRoute: '/',
  routes: {
    '/': {
      id: 'splash',
      onMount: function (page) {
        log.info("loading from auth AUTHENTICATED = ", this.state.authenticated)
        // if (this.state.authenticated === true) {
        //   log.info("redirecting to the /home")
        //   return this.redirectTo('/home');

        // }
        // log.info("redirecting to the /login")
        // return this.redirectTo('/login');
      }
    },
    '/login': {
      id: 'login',
      onMount: function (page) {
        log.info("Login Route hit")
        var that = this;
        //if authenticated is true here it means auth 0 has ran and authenticated successfully. will now reroute to home and init app
        // if (this.state.authenticated === true) {
        //   return this.redirectTo('/home');
        // }

        // inject the phone app
        // alert("popover login" + $("#popover-login-phone"))

        // if ($("#popover-login-phone").data("kendoMobileModalView")) {
        //   $("#login").empty()
        //   $("#home").empty()
        //   $("#login").html($("body").html())
        // }

        if ($("#popover-login-phone").data()) {
          $("#login").empty()
          $("#home").empty()
          $("#login").html($("body").html())
        }

        index.onDeviceReady()
        // StatusBar.hide()

        //This is where we will check for auth0 service.
        //if the url is set adn the auth0 settings is true. We will bind the
        //the login button to the login below and handle accordingly
        //else: if either is false we should bind the login buttons to the
        //data-click events they were previously calling
        log.info("assigning login listeners")
        var loginButtonPhone = $('#btnLogin-phone'),
          passButtonPhone = $("#txtLoginPass-phone"),
          wsurl = localStorage.swp_wsURL,
          EnableAuth0 = localStorage.swp_EnableAuth0;

        console.log("WSURL FROM LOGIN BUTTON ASSIGNERS", wsurl)
        console.log("EnableAuth0 FROM LOGIN BUTTON ASSIGNERS", EnableAuth0)
        alert("wsurl", wsurl, "EnableAuth0", EnableAuth0)
        if (wsurl == "null" || wsurl == null || wsurl == "" || wsurl == "undefined" ||
          EnableAuth0 == "null" || EnableAuth0 == null || EnableAuth0 == "" || EnableAuth0 == "undefined" || EnableAuth0 == "false" || EnableAuth0 == "False") {
          //Initialize app without auth0 via assigning data-click event to normal loginclick for phone or tablet
          console.log("Assigning NON AUTH 0 LISTENERS")
          loginButtonPhone.attr('data-click', "swp.auth.loginClick_Phone");
          //set user and pass if needed
          // setRemeberMe();
        } else {
          //ifi both of these are set, we should call the web service via url to get auth config Settings
          //once settings are applied to object listeners for the buttons will be assigned
          var url = wsurl + "/wsRest.svc/" + "auth0";
          var that = this;
          log.info("TRYING TO PULL IN AUTH 0 CONFIG", url)
          $.ajax({
            url: url,
            type: "GET",
            success: function (result) {
              log.info("ASSIGNING RESULT TO AUTH CONFIG", result)
              AUTH_CONFIG.clientID = result.ClientId;
              AUTH_CONFIG.clientId = result.ClientId; //both are needed see readme
              AUTH_CONFIG.connection = result.Connection;

              log.info("AUTH_CONFIG VAR AUTH CONFIG", AUTH_CONFIG)

              //Listeners for auth 0 action
              console.log("Assigning AUTH0 Listeners", that.login)
              // loginButtonTab.unbind("click").bind('click', that.login);
              loginButtonPhone.unbind("click").bind('click', that.login);

              setRemeberMe();

            },
            error: function (err) {
              log.info("TRYING TO PULL IN AUTH 0 CONFIG ERRORS! assigning non auth0 listeners", err);

              //bind normal listeners here incase admin backdoor needs to be hit
              // loginButtonTab.bind('click', that.login);
              loginButtonPhone.bind('click', that.login);

              setRemeberMe();

            }
          })

        }
      }
    },
    '/home': {
      id: 'home',
      onMount: function (page) {
        log.info("!!!Successful Login Route hit, do app init things here access granted!!!!!!")

        // if (this.state.authenticated === false) {
        //   return this.redirectTo('/login');
        // }

        stMobile.onLoadReady();
        // if (typeof _ogPass !== "") {
        //   stMobile.appInit(_user, _ogPass, _userSub);
        // } else {
        //   stMobile.appInit(_user, _pass, _userSub);
        // }

        // StatusBar.hide()

        //app needs time to be modified by appinit before injecting into home template or ipad will break
        setTimeout(function () {
          //inject the phone app //no need to reinject app at this point, should injected from login, doing it twice will break the app
          if ($("#popover-login").data("kendoMobileModalView")) {

            $("#popover-login").data("kendoMobileModalView").close()

          }

          setTimeout(function () {
            if ($("#popover-login").data("kendoMobileModalView")) {
              $("#popover-login").data("kendoMobileModalView").close();
              location.href = "#tabstrip-calendar";
              // location.href = "#homeViews/homeView.html";
            }
          }, 300)
        }, 2500)
        //console.log("TRUING TO PASS user" + _user + " " + _pass)

        if ($("#popover-login").data("kendoMobileModalView")) {
          $("#popover-login").data("kendoMobileModalView").close();
        }

        console.log("SETTING AUTH ACCESS TOKEN" + authResult.accessToken)
        localStorage.setItem('access_token', authResult.accessToken);

        //from example
        var logoutButton = page.querySelector('.btn-logout');
        // var avatar = page.querySelector('#avatar');
        // var profileCodeContainer = page.querySelector('.profile-json');
        logoutButton.addEventListener('click', this.logout);
      }
    }
  }
};

App.prototype.run = function (id) {
  log.info("app.run function hit")
  alert("app.run function hit")
  function intentHandler(url) {
    log.info("intent handlesr hit  redirecting to:", url)
    Auth0Cordova.onRedirectUri(url);
  }

  window.handleOpenURL = intentHandler;

  this.container = getBySelector(id);
  this.resumeApp();
};

App.prototype.loadProfile = function (cb) {
  log.info("load PROFILE FUNCTION HIT")
  this.auth0.userInfo(this.state.accessToken, cb);
};

App.prototype.login = function (e) {
  //e.target.disabled = true;
  log.info("login function hit")
  log.info("lTesting auth0 . client inding !!==!!==!!", this)
  var self = this;
  var user = $("#txtLoginUser-phone").val();
  var pass = $("#txtLoginPass-phone").val();
  if (isTablet) {
    user = $("#txtLoginUser").val();
    pass = $("#txtLoginPass").val();
  }
  _user = user;
  _pass = pass;

  //catch and make changes for admin back door
  if (user === "" && pass.toLowerCase() === "admin") {
    log.info("initalizing admin!!")
    initializeAdminLogin(user, pass)
    return
  } else {
    //var callback = 'https://mobile.smarttimeapps.com/' + 'LoginCallback.ashx?remember=' + $("#checkRemember-phone").is(':checked');
    // var callback = 'https://mobile.smarttimeapps.com/callback';
    // log.info("preCLIENT:", Auth0Cordova);
    // log.info("CALLBACK:" + callback);
    //if this has made it this far but is undefined still the web service can not be reached
    if (AUTH_CONFIG.clientID === "") {
      alert('Unable to authenticate because the web service could not be reached. Please contact the helpdesk for further assistance. ');
    } else {
      var client = new Auth0Cordova(AUTH_CONFIG);
      log.info("CLIENT:", client);

      var options = {
        username: user,
        password: pass,
        responseType: "code",
        realm: AUTH_CONFIG.connection
      }

      log.info("OPTIONS:", options);
      client.client.login(options, function (err, authResult) {
        if (typeof err !== "undefined" && err !== null) {
          alert("Login failed.  Invalid username and/or password.");
          console.log("::::ERROR FROM CLINET.AUTH attempt::::: ", err);
          return (e.target.disabled = false);
        }

        if (authResult && typeof authResult != "undefined") {
          console.log("AUTH RESULT: " + authResult.toString());
          console.log("Successful login!! ", authResult.accessToken);
          setRemeberMeLocalStorage(user, pass)
          client.auth0.client.userInfo(authResult.accessToken, function (err, user) {
            // Now you have the user's information
            console.log("Successful UserInfo!! ", user);
            console.log("Successful UserInfo!! Here are your keys ....");
            var keys = Object.keys(user);
            var vals = Object.values(user);
            $.each(keys, function (i, item) {
              console.log(i + "This is a user info key" + item.toString())
            })
            //
            // $.each(vals, function(i, item){
            //   console.log(i + "This is a user info val" + item.toString())
            // })

            //set passweord to auth0 user id beforing trying to auth with web service
            //keep og password so the sub is not set as pass after appinit
            _ogPass = pass;
            _pass = user.sub;
            _userSub = user.sub;
            console.log("Successful UserInfo!! Settings Password to:", user.sub);
            self.state.authenticated = true;
            // self.redirectTo('/home');
            return e.target.disabled = false
          });
        }
      });
    }
  }

}

App.prototype.logout = function (e) {
  localStorage.removeItem('access_token');
  this.resumeApp();
};

App.prototype.redirectTo = function (route) {
  if (!this.state.routes[route]) {
    throw new Error('Unknown route ' + route + '.');
  }
  this.state.currentRoute = route;
  this.render();
};

App.prototype.resumeApp = function () {
  var accessToken = localStorage.getItem('access_token');
  log.info("Resume App Hit ACCESS TOKEN:")
  //console.log(accessToken)
  //this is where the succesfull login will init the rest of the app

  if (accessToken) {
    //if the accress token is set. set these settings and init app in regualar script js files
    this.state.authenticated = true;
    this.state.accessToken = accessToken;
  } else {
    this.state.authenticated = false;
    this.state.accessToken = null;
  }
  //render  when no access token so login template is displayed
  this.render();
};

App.prototype.render = function () {
  log.info("app.render function hit")
  var currRoute = this.state.routes[this.state.currentRoute];
  var currRouteEl = getById(currRoute.id);
  var element = document.importNode(currRouteEl.content, true);
  this.container.innerHTML = '';
  this.container.appendChild(element);
  currRoute.onMount.call(this, this.container);
};

module.exports = App;
