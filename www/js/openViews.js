//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.openViews) { swp.openViews = {} }

swp.openViews = function () {
    "use strict";
    let arrEntries = swpSettings.arrEntries,
        arrEntriesTimers = swpSettings.arrEntriesTimers,
        activeEntryPosition = swpSettings.activeEntryPosition,
        activeTimerPosition = swpSettings.activeTimerPosition,
        entryArray,
        entryPosition;

    var module = {

        openAboutView: function () {
            location.href = "#views/settingsViews/aboutView.html"
        },

        openCredentialsFromLogin: function () {
            location.href = "#views/settingsViews/credentialsView.html"
            $("#txtEmail-phone").val("")
            swpSettings.settingAdjust = true
        },

        openCredentialsView: function () {
            location.href = "#views/settingsViews/credentialsView.html"
        },

        openDebugView: function () {
            location.href = "#views/settingsViews/debugView.html"
        },

        openHomeView: function () {
            location.href = "#views/homeViews/homeView.html"
        },

        openPrivacyPolicyView: function () {
            location.href = "#views/settingsViews/privacyPolicyView.html"
        },

        openSettingsView: function () {
            location.href = "#views/settingsViews/settingsView.html"
        },

        openStackView: function () {
            location.href = "#views/stackView.html"
        },

        openTimeEntryView: function () {
            location.href = "#views/timeEntryViews/timeEntryView.html"
        },

        openTimersView: function () {
            location.href = "#views/timersView.html"
        },

        openTimerSettingsView: function () {
            location.href = "#views/settingsViews/timersSettings/timersSettingsView.html"
        },

        tabstripSelect_Phone: function (a) {

            //Home View navigation
            if (a.item[0].id === "tab-home-phone") {
                swp.openViews.openHomeView()
                // swp.homeView.viewShow()
                // setTimeout(() => {
                //     try {

                //         kendo.culture(swpSettings.set_Culture)
                //         let b = $("#scheduler-phone").data("kendoScheduler")
                //         b.dataSource.read()
                //         b.refresh()

                //     } catch (err) {
                //         log.debug(`Error refreshing HomeView Calendar: ${err}`)
                //     }
                // }, 300)
            }
            
            //Stack View navigation
            //a.item[0].id === "tab-stack-phone" && swp.stackView.loadTimeForm() //a.item.context.id == 

            //Time Entry View navigation
            //a.item[0].id === "tab-timeentry-phone" && swp.stackView.loadTimeForm()
        },

    }

    return module
}()


