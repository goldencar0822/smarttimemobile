//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.homeView) { swp.homeView = {} }

swp.homeView = function () {
    "use strict";
    let arrEntries = swpSettings.arrEntries,
        activeEntryPosition = swpSettings.activeEntryPosition,
        stackListView, entries, capture, flagged, saved, released, posted;

    var module = {

        viewInit: function (e) {
            log.debug("Home View - View Init Hit")
        },

        viewShow: function (e) {
            log.debug("Home View - View Show Hit")
            swpSettings.searchParams.searchAllEntries = false
            swpSettings.bTimers = false

            module.statHourFooterCheck()

            $("#calendarLegendContainer").css("margin-bottom", $("#homeView .km-footer").height())

            if (swpSettings.set_MassActionsEnabled == true) {
                swp.utilities.cancelMassAction()
                $("#massActionCancelTouch").hide()
            }

            /*Try Catch necessary since this runs before setup application on first load and will break app - 
             Could possible refactor setup application function to run on homeView viewInit function call */
            try {
                kendo.culture(swpSettings.set_Culture)
                let scheduler = $("#scheduler-phone").data("kendoScheduler")
                $("#scheduler-phone").data("kendoScheduler").dataSource.read()
                $("#scheduler-phone").data("kendoScheduler").refresh()

                // scheduler.dataSource.read();
                // scheduler.view(scheduler.view().name)

            } catch (err) {
                log.debug(`Error refreshing HomeView Calendar: ${err}`)
            }

            //swp.debugView.addDebugListener()
        },

        calendarHome_Phone: function () {
            location.href = "#views/homeViews/homeView.html"
        },

        calendarHours_Phone: function () {
            location.href = "#views/homeViews/hoursView.html"
        },

        calendarSearch_Phone: function () {
            location.href = "#views/homeViews/searchView.html"
        },

        calendarStatistics_Phone: function () {
            location.href = "#views/homeViews/statView.html"
        },

        calendarSwipe_Phone: function (a) {
            if (swpSettings.offlineMode === false) {
                let b = $("#scheduler-phone").data("kendoScheduler"),
                    c;

                if ("left" == a.direction) {
                    c = new Date(moment(b.date()).add("months", 1));
                    b.date(c)
                } else {
                    c = new Date(moment(b.date()).subtract("months", 1));
                    b.date(c)
                }

                let d = b.dataSource,
                    e = moment(c).format("YYYYM").toString();

                log.debug(e);
                let f = swpSettings.service_url + "/calendar/" + e + "/" + swpSettings.objTook.token;

                d.transport.options.read.url = f, 
                d.read()
            }
        },

        daySearch_Phone: function (a) {
            swpSettings.bTimers = false
            if (!a || a == null || a == "") {
                a = moment()
                swpSettings.defaultDate = moment()._d
            } else {
                swpSettings.defaultDate = a
            }

            var date = moment(a).format("YYYY-M-D").toString()
            var tmrw = moment(a).add(1, "d").format("YYYY-M-D").toString()

            setTimeout(() => {
                (async () => {
                    const res = await fetch(`${swpSettings.service_url}/fullsearch/${swpSettings.objTook.token}/${date}/${tmrw}/1/1/1/1`).catch((a, b) => {
                        console.log("Web service error " + b + a)
                        log.debug("Web service error " + b + a)
                        //swp.debugLogView.addToDebugLog("Web service error " + b + a)
                    })
                    // may need to check json here.
                    const json = await res.json().catch((a, b) => {
                        console.log("Web service error " + b + a)
                        log.debug("Web service error " + b + a)
                    })
                    

                    //swp.debugLogView.addToDebugLog("Web service error " + b + a)

                    module.daySearchResult(json)
                })()
            }, 100)
        },

        daySearchResult: function (arr) {
            swpSettings.arrEntries = []
            swpSettings.activeEntryPosition = 0
            swpSettings.activeTimerPosition = 0
            swpSettings.totalCurDayHrs = 0
            kendo.ui.progress($("#scheduler-phone"), false)

            //Need to adjust this check to make sure we are checking for empty results properly and acting accordingly. Still may need to adjust how we read the data for the calendar when navigating to homeView since it breaks if the active entry is a new/unsaved item created by blank daySearch.
            let savedEntries = arr.filter(x => (x.status == "New" && (x.type !== "Mobile" || x.type !== "Timer")))
            savedEntries += arr.filter(x => x.status != "New")
            //swpSettings.arrEntries.filter(x => x.status != "New" && (x.type == "Mobile" || x.type == "Timer"))

            if (arr && savedEntries.length < 1) { //&& swpSettings.set_ToggleActive === false - TA - took out as of 9/24/20 to try to fix emptyStackToHomeViewBug
                swp.entry.addNewTimeEntry()
                //swp.openViews.openTimeEntryView()
            } else {
                for (var e = 0; e < arr.length; e++) {
                    let unique = true;

                    if (arr[e].Timer !== "" && arr[e].Timer !== null) {
                        for (var j = 0; j < swpSettings.arrEntriesTimers.length; j++) {
                            if (swpSettings.arrEntriesTimers[j].id === arr[e].ID) {
                                unique = false
                            }
                        }
                    } else {
                        for (var g = 0; g < swpSettings.arrEntries.length; g++) {
                            if (swpSettings.arrEntries[g].id === arr[e].ID) {
                                unique = false
                            }
                        }
                    }

                    if (unique === true) {
                        var h = swp.entry.newEntry()

                        if (null != arr[e].DateToFrom) {
                            h = swp.utilities.setTimeToLocal(h, arr[e].DateToFrom)
                        }

                        h.id = arr[e].ID
                        h.origId = arr[e].ID
                        h.client = arr[e].ClientName
                        h.matter = arr[e].MatterName
                        h.custom1 = arr[e].Custom1
                        h.custom2 = arr[e].Custom2
                        h.custom3 = arr[e].Custom3
                        h.custom4 = arr[e].Custom4
                        h.custom5 = arr[e].Custom5
                        h.duration = arr[e].Hours
                        h.narrative = arr[e].TimeEntryDesc
                        h.timer = arr[e].Timer
                        h.notes = arr[e].Notes
                        h.color = arr[e].Color
                        h.status = arr[e].Status
                        h.statusImage = "img/" + arr[e].Status + ".png"
                        h.type = arr[e].Type
                        h.typeImage = arr[e].TypeImage
                        h.description = arr[e].Description
                        h.returned = arr[e].Returned
                        h.prebillNarrative = arr[e].PrebillNarrative

                        // Your moment
                        var mmt = moment();
                        // Your moment at midnight
                        var mmtMidnight = mmt.clone().startOf('day');
                        // var startTimeIsSameDay = moment(h.timerStart).isSame(mmtMidnight, "day") startTimeIsSameDay === false ||
                        var eventDateIsSameDay = moment(h.eventDate()).isSame(mmtMidnight, "day")

                        if (arr[e].Timer == null && arr[e].Timer == undefined) {
                            arr[e].Timer = ""
                        }

                        swpSettings.totalCurDayHrs = swpSettings.totalCurDayHrs + +arr[e].Hours

                        if (arr[e].Timer !== "") h.eventTitle = "Mobile Timer"
                        // TA:checking for color here since a released timer does not save color and we want any timer that has been released to be in the stack, since once released it is an entry (4/24/20)
                        arr[e].Timer !== "" && arr[e].Color !== "" && arr[e].Status !== "Released" && arr[e].Status !== "Posted" && eventDateIsSameDay === true
                            ? swpSettings.arrEntriesTimers.push(h)
                            : swpSettings.arrEntries.push(h)
                        // swpSettings.arrEntries.push(h)
                    }
                    
                    let capParam = arr[e].Type
                    if (Object.values(swpSettings.timeCaptureParams).map(x => typeof x).includes("boolean") == false || Object.keys(swpSettings.timeCaptureParams).includes(capParam) == false) {

                        swpSettings.timeCaptureParams[capParam] = true
                    }
                }

                swpSettings.arrEntries.length < 1 && swpSettings.set_ToggleActive === false
                    ? location.href = "#views/timersView.html"
                    : location.href = "#views/stackView.html"

                if ($("#session-entries-phone").data("kendoMobileListView")) {
                    $("#session-entries-phone").data("kendoMobileListView").setDataSource(swpSettings.arrEntries)
                } else {
                    setTimeout(() => {
                    if ($("#session-entries-phone").data("kendoMobileListView")) {
                        $("#session-entries-phone").data("kendoMobileListView").setDataSource(swpSettings.arrEntries)
                    }
                    }, 50)
                }
            }
        },

        eventEdit: function (e, cell) {
            log.debug("eventEdit() firing.")

            if (swpSettings.offlineMode === false) {
                /* find the parent so we can highlight it */
                var day = moment(e).format('D');

                if (day.length === 1) {
                    day = "0" + day;
                }
                var clickedCell = $('span:contains(' + day + ')').parent('td').not($('.k-other-month'))
                clickedCell.css('background-color', swpSettings.styles.entryHighlightColor)
                setTimeout(() => {
                    clickedCell.css('background-color', "white")
                }, 500)

                module.daySearch_Phone(e);

                setTimeout(() => location.href = "#views/stackView.html", 200)

            };
        },

        handleStatToggle: function() {
            let grid = $("#trendHoursGrid").data("kendoGrid")
            grid.dataSource.transport.options.read.url = `${swpSettings.service_url}trendhours/` +
                swpSettings.objTook.token +
                swpSettings.statParams.duration +
                swpSettings.statParams.billable + `/` +
                swpSettings.statParams.nonbillable + `/` +
                swpSettings.statParams.accountable

            grid.dataSource.read()
            module.statsToggle()
        },

        statHourFooterCheck: function() {
             let view = window.location.hash.split("/")[window.location.hash.split("/").length - 1].split(".")[0]
            //hide/show stats and hours pages based on user settings
            swpSettings.set_StatsHidden === true && swpSettings.set_HoursHidden === true ?
                $(`#${view} #divHomeButtons-phone`).hide():
                $(`#${view} #divHomeButtons-phone`).show()

            swpSettings.set_StatsHidden === true ?
                $(`#${view} #button-calendar-statistics-phone`).hide() :
                $(`#${view} #button-calendar-statistics-phone`).show()
                
            swpSettings.set_HoursHidden === true ?
                $(`#${view} #button-calendar-hours-phone`).hide() :
                $(`#${view} #button-calendar-hours-phone`).show()
        },

        statsToggle: () => {
            let grid = $("#trendHoursGrid").data("kendoGrid")

            //hide all by default
            grid.hideColumn("Actual")
            grid.hideColumn("Budget")
            grid.hideColumn("Target")
            grid.hideColumn("Variance")
            grid.hideColumn("CumActual")
            grid.hideColumn("CumBudget")
            grid.hideColumn("CumTarget")
            grid.hideColumn("CumVariance")

            // if ($("#chkHoursMonthly").is(':checked')) {
            if (swpSettings.statParams.duration === "/mo/") {
                $("#trendHoursGrid th[data-field=Month]").html("")

                grid.showColumn("Actual")

                if (swpSettings.statParams.showBudget === true) {
                    grid.showColumn("Budget")
                    grid.showColumn("Target")
                    grid.showColumn("Variance")
                }

            } else {
                $("#trendHoursGrid th[data-field=Month]").html("YTD")

                grid.showColumn("CumActual")

                if (swpSettings.statParams.showBudget === true) {
                    grid.showColumn("CumBudget")
                    grid.showColumn("CumTarget")
                    grid.showColumn("CumVariance")
                }

            }

        },
    }

    return module

}();

