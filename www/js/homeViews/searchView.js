//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.searchView) { swp.searchView = {} }

swp.searchView = function () {
    "use strict";
    let arrEntries = swpSettings.arrEntries,
        activeEntryPosition = swpSettings.activeEntryPosition,
        searchStateArr = [],
        searchEntryArr, startDate, endDate, saved, released, posted, capture = 0, searchURL, sortOption, sortDir;

    var module = {

        viewInit: function (e) {
            log.debug("Search View - View Init Hit")
            module.setupDatePickers()
            module.setupSearchPresets()
        },

        viewShow: function (e) {
            log.debug("Search View - View Show Hit")

            $("#searchClientRow > a > label").text(swpSettings.clientLabel + ":")
            $("#searchMatterRow > a > label").text(swpSettings.matterLabel + ":")

            $("#searchView #pageTitleTop").text("Search")
            $("#searchView #timerColorIndicatorTouch").hide()
            $("#searchView #backNavButton").hide()
            // $("#searchView #backNavButton").attr("href", "#views/homeViews/homeView.html")
            // $("#searchView #backNavButton").removeAttr("onclick")
            // $("#searchView #backNavButton").show()
            $("#searchView #timeEntryAddDefaultDiv").hide()

            $("#searchView #list-search-phone input").css('left')

            searchStateArr = []

            if (swpSettings.searchParams.searchAllEntries === false) {
                swp.utilities.resetSearchParams()
            } else {
                module.setupSearchParams()
            }

            module.resetURLFromSettings()
            swp.debugView.addDebugListener()
            $("#searchView .km-content").css("margin-bottom", $("#searchView .km-footer").height())
        },

        filterBillPreset: function (arr, btn) {
            if (arr.length !== 0) {
                swpSettings.arrEntries = arr.filter(x => x.billType.toLowerCase() == btn.toLowerCase())
            }
        },

        filterReturnedPreset: function (arr) {
            if (arr.length !== 0) {
                swpSettings.arrEntries = arr.filter(x => x.returned === true)
            }
        },

        getAllEntriesSearch: function () {
            $.ajax({
                type: "GET",
                traditional: true,
                url: searchURL,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (b) {
                    console.log('success', b)
                    searchEntryArr = []
                    swpSettings.arrEntries = []
                    swpSettings.activeEntryPosition = 0
                    swpSettings.activeTimerPosition = 0
                    // kendo.ui.progress($("#scheduler-phone"), false)
                    // kendo.ui.progress($("#session-entries-phone"), true)
                    kendo.ui.progress($("#stackView"), true)

                    for (var e = 0; e < b.length; e++) {
                        // var f = true;
                        // if (b[e].Timer !== "" && b[e].Timer !== null) {
                        //     for (var j = 0; j < swpSettings.arrEntriesTimers.length; j++) {
                        //         if (swpSettings.arrEntriesTimers[j].id === b[e].ID) {
                        //             f = false;
                        //         }
                        //     }
                        // } else {
                        //     for (var g = 0; g < swpSettings.arrEntries.length; g++) {
                        //         if (swpSettings.arrEntries[g].id === b[e].ID) {
                        //             f = false;
                        //         }
                        //     }
                        // }

                        // if (f === true) {
                            var h = swp.entry.newEntry();
                            if (null != b[e].DateToFrom) {

                                h = swp.utilities.setTimeToLocal(h, b[e].DateToFrom)
                            }

                            h.id = b[e].ID
                            h.origId = b[e].ID
                            h.client = b[e].ClientName
                            h.matter = b[e].MatterName
                            h.custom1 = b[e].Custom1
                            h.custom2 = b[e].Custom2
                            h.custom3 = b[e].Custom3
                            h.custom4 = b[e].Custom4
                            h.custom5 = b[e].Custom5
                            h.duration = b[e].Hours
                            h.narrative = b[e].TimeEntryDesc
                            h.timer = b[e].Timer
                            h.notes = b[e].Notes
                            h.color = b[e].Color
                            h.status = b[e].Status
                            h.statusImage = "img/" + b[e].Status + ".png"
                            h.type = b[e].Type
                            h.typeImage = b[e].TypeImage
                            h.description = b[e].Description
                            h.returned = b[e].Returned
                            h.prebillNarrative = b[e].PrebillNarrative

                            // Your moment
                            var mmt = moment();
                            // Your moment at midnight
                            var mmtMidnight = mmt.clone().startOf('day');
                            // var startTimeIsSameDay = moment(h.timerStart).isSame(mmtMidnight, "day") startTimeIsSameDay === false ||
                            var eventDateIsSameDay = moment(h.eventDate()).isSame(mmtMidnight, "day")

                            // if (b[e].Timer !== "") h.eventTitle = "Mobile Timer"

                            // b[e].Timer !== "" && eventDateIsSameDay === true && b[e].status !== "Released" && b[e].status !== "Posted"
                            //     ? swpSettings.arrEntriesTimers.push(h)
                            //     : swpSettings.arrEntries.push(h)
                            searchEntryArr.push(h)
                        // }
                    }

                    if (searchEntryArr.length !== 0) {
                        let finalArr = []

                        if (swpSettings.searchParams.searchEntriesClient !== "") {
                            searchEntryArr = searchEntryArr.filter((x) => x.client === swpSettings.searchParams.searchEntriesClient)
                        }

                        if (swpSettings.searchParams.searchEntriesMatter !== "") {
                            searchEntryArr = searchEntryArr.filter((x) => x.matter === swpSettings.searchParams.searchEntriesMatter)
                        }

                        for (let i = 0; i < searchStateArr.length; i++) {
                            finalArr.push(...searchEntryArr.filter((x) => x.status === searchStateArr[i]))
                        }

                        swpSettings.arrEntries = finalArr
                    }

                    // if ($("#session-entries-phone").data("kendoMobileListView")) {

                        $("#session-entries-phone").data("kendoMobileListView").setDataSource(swpSettings.arrEntries)

                        sortOption = swpSettings.stackParams.stackSortOption.split("_")[1]
                        sortDir = "asc"

                        $("#session-entries-phone").data("kendoMobileListView").dataSource.sort([
                            { field: `${sortOption}`, dir: `${sortDir}` }
                        ])
                    // }
                    location.href = "#views/stackView.html"
                },
                error: function (a, b, c) {
                    log.debug("Web service error " + b + a)
                }
            })
        },

        getPresetEntriesSearch: function (e) {
            let btn
            if (e) btn = e.id.split("Preset")[0]

            $.ajax({
                type: "GET",
                traditional: true,
                url: searchURL,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (b) {
                    console.log('success', b)
                    searchEntryArr = []
                    swpSettings.arrEntries = []
                    swpSettings.activeEntryPosition = 0
                    swpSettings.activeTimerPosition = 0
                    kendo.ui.progress($("#scheduler-phone"), false)
                    kendo.ui.progress($("#stackView"), true)

                    for (var e = 0; e < b.length; e++) {
                        var f = true;
                        if (b[e].Timer !== "" && b[e].Timer !== null) {
                            for (var j = 0; j < swpSettings.arrEntriesTimers.length; j++) {
                                if (swpSettings.arrEntriesTimers[j].id === b[e].ID) {
                                    f = false;
                                }
                            }
                        } else {
                            for (var g = 0; g < swpSettings.arrEntries.length; g++) {
                                if (swpSettings.arrEntries[g].id === b[e].ID) {
                                    f = false;
                                }
                            }
                        }

                        if (f === true) {
                            var h = swp.entry.newEntry();
                            if (null != b[e].DateToFrom) {
                                h = swp.utilities.setTimeToLocal(h, b[e].DateToFrom)
                            }

                            h.id = b[e].ID
                            h.origId = b[e].ID
                            h.client = b[e].ClientName
                            h.matter = b[e].MatterName
                            h.custom1 = b[e].Custom1
                            h.custom2 = b[e].Custom2
                            h.custom3 = b[e].Custom3
                            h.custom4 = b[e].Custom4
                            h.custom5 = b[e].Custom5
                            h.duration = b[e].Hours
                            h.narrative = b[e].TimeEntryDesc
                            h.timer = b[e].Timer
                            h.notes = b[e].Notes
                            h.color = b[e].Color
                            h.status = b[e].Status
                            h.statusImage = "img/" + b[e].Status + ".png"
                            h.type = b[e].Type
                            h.typeImage = b[e].TypeImage
                            h.description = b[e].Description
                            h.billType = b[e].BillType
                            h.returned = b[e].Returned
                            h.prebillNarrative = b[e].PrebillNarrative

                            // Your moment
                            var mmt = moment();
                            // Your moment at midnight
                            var mmtMidnight = mmt.clone().startOf('day');
                            // var startTimeIsSameDay = moment(h.timerStart).isSame(mmtMidnight, "day") startTimeIsSameDay === false ||
                            var eventDateIsSameDay = moment(h.eventDate()).isSame(mmtMidnight, "day")

                            // if (b[e].Timer !== "") h.eventTitle = "Mobile Timer"

                            // b[e].Timer !== "" && eventDateIsSameDay === true && b[e].status !== "Released" && b[e].status !== "Posted"
                            //     ? swpSettings.arrEntriesTimers.push(h)
                            //     : swpSettings.arrEntries.push(h)
                            searchEntryArr.push(h)
                        }
                    }

                    swpSettings.arrEntries = searchEntryArr

                    if (btn === "billable" || btn === "nonBillable") {
                        module.filterBillPreset(swpSettings.arrEntries, btn)
                    } else if (btn === "returned") {
                        module.filterReturnedPreset(swpSettings.arrEntries)
                    }


                    location.href = "#views/stackView.html"


                    if ($("#session-entries-phone").data("kendoMobileListView")) {
                        $("#session-entries-phone").data("kendoMobileListView").setDataSource(swpSettings.arrEntries)

                        $("#session-entries-phone").data("kendoMobileListView").dataSource.bind("change", (e) => {
                        })

                        // let sortOption = swpSettings.stackParams.stackSortOption.split("_")[1]
                        // if (sortOption === "sortDate") {
                        //     sortDir = "desc"
                        // } else {
                        //     sortDir = "asc"
                        // }

                        // $("#session-entries-phone").data("kendoMobileListView").dataSource.sort([
                        //     { field: `${sortOption}`, dir: `${sortDir}` }
                        // ])

                        // $("#session-entries-phone").data("kendoMobileListView").dataSource.sort([
                        //     { field: "eventDT", dir: "asc" },
                        //     { field: "client", dir: "asc" },
                        //     { field: "matter", dir: "asc" }
                        // ])
                    }
                },
                error: function (a, b, c) {
                    log.debug("Web service error " + b + a)
                }
            })
        },

        getReturnedEntries: function () {

            // set end date to tomorrow if it is the same as start date or empty
            if (($("#searchView #searchStartDatePicker").val().length > 1 && $("#searchView #searchEndDatePicker").val() === "") || ($("#searchView #searchStartDatePicker").val().length > 1 && swpSettings.searchParams.searchDateEnd.toString() == swpSettings.searchParams.searchDateStart.toString())) {
                swpSettings.searchParams.searchDateEnd = moment(swpSettings.searchParams.searchDateStart).add(1, "d").format("YYYY-M-D").toString()
                endDate = swpSettings.searchParams.searchDateEnd.toString()
            }

            $.ajax({
                type: "GET",
                traditional: true,
                url: `${swpSettings.service_url}/fullsearch/${swpSettings.objTook.token}/${startDate}/${endDate}/1/0/0/0`,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (b) {
                    console.log('success', b)
                    searchEntryArr = []
                    // swpSettings.arrEntries = []
                    // swpSettings.activeEntryPosition = 0
                    // swpSettings.activeTimerPosition = 0
                    // kendo.ui.progress($("#scheduler-phone"), false)

                    for (var e = 0; e < b.length; e++) {
                        var f = true;
                        if (b[e].Timer !== "" && b[e].Timer !== null) {
                            for (var j = 0; j < swpSettings.arrEntriesTimers.length; j++) {
                                if (swpSettings.arrEntriesTimers[j].id === b[e].ID) {
                                    f = false;
                                }
                            }
                        } else {
                            for (var g = 0; g < swpSettings.arrEntries.length; g++) {
                                if (swpSettings.arrEntries[g].id === b[e].ID) {
                                    f = false;
                                }
                            }
                        }

                        if (f === true) {
                            var h = swp.entry.newEntry();
                            if (null != b[e].DateToFrom) {
                                h = swp.utilities.setTimeToLocal(h, b[e].DateToFrom)
                            }

                            h.id = b[e].ID
                            h.origId = b[e].ID
                            h.client = b[e].ClientName
                            h.matter = b[e].MatterName
                            h.custom1 = b[e].Custom1
                            h.custom2 = b[e].Custom2
                            h.custom3 = b[e].Custom3
                            h.custom4 = b[e].Custom4
                            h.custom5 = b[e].Custom5
                            h.duration = b[e].Hours
                            h.narrative = b[e].TimeEntryDesc
                            h.timer = b[e].Timer
                            h.notes = b[e].Notes
                            h.color = b[e].Color
                            h.status = b[e].Status
                            h.statusImage = "img/" + b[e].Status + ".png"
                            h.type = b[e].Type
                            h.typeImage = b[e].TypeImage
                            h.description = b[e].Description
                            h.returned = b[e].Returned
                            h.prebillNarrative = b[e].PrebillNarrative

                            // Your moment
                            var mmt = moment();
                            // Your moment at midnight
                            var mmtMidnight = mmt.clone().startOf('day');
                            // var startTimeIsSameDay = moment(h.timerStart).isSame(mmtMidnight, "day") startTimeIsSameDay === false ||
                            var eventDateIsSameDay = moment(h.eventDate()).isSame(mmtMidnight, "day")

                            // if (b[e].Timer !== "") h.eventTitle = "Mobile Timer"

                            // b[e].Timer !== "" && eventDateIsSameDay === true && b[e].status !== "Released" && b[e].status !== "Posted"
                            //     ? swpSettings.arrEntriesTimers.push(h)
                            //     : swpSettings.arrEntries.push(h)
                            searchEntryArr.push(h)
                        }

                    }

                    searchEntryArr = searchEntryArr.filter(x => x.returned === true)
                    swpSettings.arrEntries = swpSettings.arrEntries.concat(searchEntryArr)

                    if ($("#session-entries-phone").data("kendoMobileListView")) {
                        $("#session-entries-phone").data("kendoMobileListView").setDataSource(swpSettings.arrEntries)

                        sortOption = swpSettings.stackParams.stackSortOption.split("_")[1]

                        sortDir = "asc"

                        $("#session-entries-phone").data("kendoMobileListView").dataSource.sort([
                            { field: `${sortOption}`, dir: `${sortDir}` }
                        ])

                        setTimeout(() => {
                            // swp.stackView.checkEntryType()
                        }, 300)
                        // swp.stackView.viewShow()
                    }
                },
                error: function (a, b, c) {
                    log.debug("Web service error " + b + a)
                }
            })
        },

        handleStateFilterToggle: function (cb) {
            let tState = cb.id.split("State")[1]
            $("#searchView #searchStatusNote").hide()
            swpSettings.searchParams[tState.toLowerCase()] = !swpSettings.searchParams[tState.toLowerCase()]
            if (cb.checked === true) {
                // swpSettings.searchParams[tState.toLowerCase()] = 1
                // if (tState === "Returned") tState = "Saved"
                swpSettings.searchParams[tState.toLowerCase()] = 1
                searchStateArr.push(tState)
            } else if (cb.checked === false) {
                // swpSettings.searchParams[tState.toLowerCase()] = 0
                // if (tState === "Returned") tState = "Saved"
                swpSettings.searchParams[tState.toLowerCase()] = 0
                searchStateArr = searchStateArr.filter((x) => x !== tState)
            }
        },

        presetSearchAllEntries: function () {
            startDate = moment().startOf('month').format('YYYY-M-D').toString()
            endDate = moment().endOf('month').format('YYYY-M-D').toString()
            searchURL = `${swpSettings.service_url}/fullsearch/${swpSettings.objTook.token}/${startDate}/${endDate}/1/1/1/0`

            module.getPresetEntriesSearch()
        },

        presetSearchAllSaved: function () {
            searchURL = `${swpSettings.service_url}/fullsearch/${swpSettings.objTook.token}/${startDate}/${endDate}/1/0/0/0`

            module.getPresetEntriesSearch()
        },

        presetSearchBillable: function (e) {
            startDate = moment().startOf('month').format('YYYY-M-D').toString()
            endDate = moment().endOf('month').format('YYYY-M-D').toString()
            searchURL = `${swpSettings.service_url}/fullsearch/${swpSettings.objTook.token}/${startDate}/${endDate}/1/1/1/0`

            module.getPresetEntriesSearch(e)
        },

        presetSearchNonBillable: function (e) {
            startDate = moment().startOf('month').format('YYYY-M-D').toString()
            endDate = moment().endOf('month').format('YYYY-M-D').toString()
            searchURL = `${swpSettings.service_url}/fullsearch/${swpSettings.objTook.token}/${startDate}/${endDate}/1/1/1/0`

            module.getPresetEntriesSearch(e)
        },

        presetSearchPosted: function () {
            startDate = moment().startOf('month').format('YYYY-M-D').toString()
            endDate = moment().endOf('month').format('YYYY-M-D').toString()
            searchURL = `${swpSettings.service_url}/fullsearch/${swpSettings.objTook.token}/${startDate}/${endDate}/0/0/1/0`

            module.getPresetEntriesSearch()
        },

        presetSearchReleased: function () {
            startDate = moment().startOf('month').format('YYYY-M-D').toString()
            endDate = moment().endOf('month').format('YYYY-M-D').toString()
            searchURL = `${swpSettings.service_url}/fullsearch/${swpSettings.objTook.token}/${startDate}/${endDate}/0/1/0/0`

            module.getPresetEntriesSearch()
        },

        presetSearchReturned: function (e) {
            searchURL = `${swpSettings.service_url}/fullsearch/${swpSettings.objTook.token}/${startDate}/${endDate}/1/1/1/0`

            module.getPresetEntriesSearch(e)
        },

        presetSearchSaved: function () {
            startDate = moment().startOf('month').format('YYYY-M-D').toString()
            endDate = moment().endOf('month').format('YYYY-M-D').toString()
            searchURL = `${swpSettings.service_url}/fullsearch/${swpSettings.objTook.token}/${startDate}/${endDate}/1/0/0/0`

            module.getPresetEntriesSearch()
        },

        // resetSearchParams: function () {
        //     swpSettings.searchParams.searchAllEntries = true
        //     swpSettings.searchParams.searchDateStart = "1/1/1990"
        //     swpSettings.searchParams.searchDateEnd = "12/31/2040"
        //     swpSettings.searchParams.searchEntriesClient = ""
        //     swpSettings.searchParams.searchEntriesMatter = ""
        //     searchStateArr = []

        //     $("#searchView #list-search-phone span input").val("")
        //     $("#searchView #list-search-phone a input").val("")
        //     $("#searchView #list-search-phone .round > input").prop("checked", false)
        // },

        resetURLFromSettings: function () {
            startDate = moment(swpSettings.searchParams.searchDateStart).format('YYYY-M-D').toString()
            endDate = moment(swpSettings.searchParams.searchDateEnd).format('YYYY-M-D').toString()
            // kendo.ui.progress($("#scheduler-phone"), true)

            // swpSettings.searchParams.includeTimeEntries === true
            //     ? (saved = swpSettings.searchParams.saved,
            //         released = swpSettings.searchParams.released,
            //         posted = swpSettings.searchParams.posted)
            //     : (saved = 0,
            //         released = 0,
            //         posted = 0)

            saved = swpSettings.searchParams.saved
            released = swpSettings.searchParams.released
            posted = swpSettings.searchParams.posted

            // swpSettings.searchParams.includeTimeCaptures === true
            //     ? capture = 1
            //     : capture = 0

            searchURL = `${swpSettings.service_url}/fullsearch/${swpSettings.objTook.token}/${startDate}/${endDate}/${saved}/${released}/${posted}/${capture}`
        },

        searchEntries: function () {
            let checkedNum = $("#searchStateFilterRow div.round > input:checked").length

            //check to make sure a state is included in the search parameters
            if (!checkedNum) {
                // $("#searchView #searchStatusNote").show()
                module.toggleSearchErrorMessage()
            } else {

                // check if there is a start and end date if released/posted entries are included in the search
                if ((swpSettings.searchParams.released === 1 && $("#searchView #searchStartDatePicker").val() === "") || (swpSettings.searchParams.posted === 1 && $("#searchView #searchStartDatePicker").val() === "")) {

                    module.toggleDateErrorMessage()

                } else {
                    // set end date to tomorrow if it is the same as start date or empty
                    if (($("#searchView #searchStartDatePicker").val().length > 1 && $("#searchView #searchEndDatePicker").val() === "") || ($("#searchView #searchStartDatePicker").val().length > 1 && swpSettings.searchParams.searchDateEnd.toString() == swpSettings.searchParams.searchDateStart.toString())) {
                        swpSettings.searchParams.searchDateEnd = moment(swpSettings.searchParams.searchDateStart).add(1, "d")
                    }

                    swp.utilities.closeUserFeedbackMessages()
                    // $("#searchView #searchStatusNote").hide()
                    swpSettings.searchParams.searchAllEntries = true
                    location.href = "#views/stackView.html"
                    module.resetURLFromSettings()
                    swpSettings.arrEntries = []

                    // kendo.ui.progress($("#stackView"), true)

                    //other potential spinner displayes that are not working below.
                    // kendo.ui.progress($("#session-entries-phone"), true)
                    // $(".km-loader").show(), $(".km-loader").children().hide(), $(".km-loader .km-spin").show()
                    // swpSettings.app.pane.loader.show()

                    if (swpSettings.searchParams.saved === 1 || swpSettings.searchParams.released === 1 || swpSettings.searchParams.posted === 1) {
                        module.getAllEntriesSearch()
                    }

                    if (swpSettings.searchParams.returned === 1) {
                        setTimeout(() => {
                            module.getReturnedEntries()
                        }, 100)
                    }
                }
            }
        },

        setupDatePickers: function () {
            $("#searchStartDatePicker").kendoDatePicker({
                dateInput: true,
                value: "",
                change: function (e) {
                    swpSettings.searchParams.searchDateStart = this.value()
                    $("#searchEndDatePicker").data("kendoDatePicker").setOptions({
                        min: new Date(e.sender._value)
                    })
                    $("#searchView #searchEndDatePicker").val("")
                },
                open: (e) => {
                    $("<div class='k-overlay' id='greyOverlay'></div>").appendTo($(document.body))
                    //bind to the navigation of the calendar
                    e.sender.dateView.calendar.bind("navigate", () => {
                        setTimeout(() => {
                            // remove k-state-focused class with jQuery
                            $("td.k-state-focused").removeClass("k-state-focused");
                        }, 1);
                    })
                },
                close: (e) => {
                    swpSettings.searchParams.searchDateStart = e.sender.value()
                    $('#greyOverlay').remove()
                },
            })

            $("#searchEndDatePicker").kendoDatePicker({
                dateInput: true,
                value: "",
                //month: {
                //    // template for dates in month view
                //    content: '# if (swp.timeEntryView.isInArray(data.date, data.dates)) { #' +
                //        '<div class="birthday"></div>' +
                //        '# } #' +
                //        '#= data.value #',
                //    weekNumber: '<a class="italic">#= data.weekNumber #</a>'
                //},
                change: function () {
                    swpSettings.searchParams.searchDateEnd = this.value()
                },
                open: (e) => {
                    $("<div class='k-overlay' id='greyOverlay'></div>").appendTo($(document.body))
                    //bind to the navigation of the calendar
                    e.sender.dateView.calendar.bind("navigate", () => {
                        setTimeout(() => {
                            // remove k-state-focused class with jQuery
                            $("td.k-state-focused").removeClass("k-state-focused");
                        }, 1);
                    })
                },
                close: (e) => {
                    swpSettings.searchParams.searchDateEnd = e.sender.value()
                    $('#greyOverlay').remove()
                },
            })

            $("#searchStartDatePickerRow input").prop("readonly", true)
            $("#searchEndDatePickerRow input").prop("readonly", true)
        },

        setupSearchParams: function () {
            $("#searchStartDatePicker").data("kendoDateInput").value(swpSettings.searchParams.searchDateStart)
            $("#searchEndDatePicker").data("kendoDateInput").value(swpSettings.searchParams.searchDateEnd)

            $("#searchView #searchStateSaved").prop("checked", swpSettings.searchParams.saved)
            $("#searchView #searchStateReleased").prop("checked", swpSettings.searchParams.released)
            $("#searchView #searchStatePosted").prop("checked", swpSettings.searchParams.posted)

            searchStateArr = []

            document.querySelectorAll("#searchView #searchStateFilterRow input").forEach(x => {
                if (x.checked === true) {
                    searchStateArr.push(x.id.split("State")[1])
                }
            })
        },

        setupSearchPresets: function () {
            $("#savedPresetButton").kendoButton({
                imageUrl: "img/Saved.png"
            })
            $("#releasedPresetButton").kendoButton({
                imageUrl: "img/Released.png"
            })
            $("#postedPresetButton").kendoButton({
                imageUrl: "img/Posted.png"
            })
            $("#allEntriesPresetButton").kendoButton()
            $("#billablePresetButton").kendoButton()
            $("#nonBillablePresetButton").kendoButton()
            $("#allSavedPresetButton").kendoButton({
                imageUrl: "img/Saved.png"
            })
            $("#returnedPresetButton").kendoButton({
                imageUrl: "img/red_star.png"
            })
        },

        toggleDatePicker: function (e) {
            console.log("toggleDatePicker" + e.sender)
            // e.preventDefault()
            $(`#searchView #${e.sender.element[0].id.split("Touch")[0]}`).data("kendoDatePicker").open()
            // setTimeout(() => {
            //     $(`#searchView #${e.sender.element[0].id.split("Touch")[0]}`).data("kendoDatePicker").open()
            // }, 300)
        },

        toggleSearchErrorMessage: function () {
            $("#searchView #userFeedbackMessages .km-collapsible-content").children().hide()
            $("#searchView #userFeedbackMessages #userFeedbackMessageContent").text("You must select one or more of the status options.")
            $("#searchView #userFeedbackMessages #userFeedbackMessageContent").show()
            swp.utilities.toggleUserFeedbackMessages()
        },

        toggleDateErrorMessage: function () {
            $("#searchView #userFeedbackMessages .km-collapsible-content").children().hide()
            $("#searchView #userFeedbackMessages #userFeedbackMessageContent").text("You must include a date range when searching for Posted and Released Entries.")
            $("#searchView #userFeedbackMessages #userFeedbackMessageContent").show()
            swp.utilities.toggleUserFeedbackMessages()
        }


    }

    return module

}();

