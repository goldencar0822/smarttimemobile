//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.statView) { swp.statView = {} }

swp.statView = function () {
    "use strict";
    let arrEntries = swpSettings.arrEntries,
        activeEntryPosition = swpSettings.activeEntryPosition;

    var module = {

        viewInit: function (e) {
            log.debug("Statistics View - View Init Hit")

            module.setupKendoGrids()
        },

        viewShow: function (e) {
            log.debug("Statistics View - View Show Hit")

            $("#statView #pageTitleTop").text("Statistics")
            $("#statView #timerColorIndicatorTouch").hide()
            $("#statView #backNavButton").hide()

            module.setupBroadcastDisplay()
            swp.setup.refreshStatsPhone()

            let monthGrid = $("#table-mtd-phone").data("kendoGrid")
            let ytdGrid = $("#table-ytd-phone").data("kendoGrid")
            //attempt to fix non-Budget Display
            if (swpSettings.statParams.showBudget === false) {

                monthGrid.hideColumn(2)
                monthGrid.hideColumn(3)
                ytdGrid.hideColumn(2)
                ytdGrid.hideColumn(3)

            } else {

                monthGrid.showColumn(2)
                monthGrid.showColumn(3)
                ytdGrid.showColumn(2)
                ytdGrid.showColumn(3)

            }

            $("#statView #BroadcastHome").css("margin-bottom", $("#statView .km-footer").height())

            //show or hide accountable rows depending on user server setting
            if (swpSettings.statParams.showAccountable === false) {
                monthGrid.tbody.find("tr:last").hide()
                ytdGrid.tbody.find("tr:last").hide()
            } else {
                monthGrid.tbody.find("tr:last").show()
                ytdGrid.tbody.find("tr:last").show()
            }

            if (swpSettings.statParams.showBudget === false) {
                $(".colBudget").hide()
            }

            swp.homeView.statHourFooterCheck()
            swp.debugView.addDebugListener()
        },

        setupBroadcastDisplay: function () {
            $("#BroadcastHome").kendoEditor({
                value: `<p>${swpSettings.set_Broadcast}</p>`,
            })
            $($("#BroadcastHome").data("kendoEditor").body).removeAttr("contenteditable").find("p")
            $("#BroadcastHome").height($("#table-ytd-phone").height())
        },

        setupKendoGrids: function () {
            $("#table-month-phone").kendoGrid()
            $("#table-mtd-phone").kendoGrid()
            $("#table-ytd-phone").kendoGrid()
        }

    }


    return module

}()

