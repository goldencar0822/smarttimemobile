//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.hoursView) { swp.hoursView = {} }

swp.hoursView = function () {
    "use strict";
    let arrEntries = swpSettings.arrEntries,
        activeEntryPosition = swpSettings.activeEntryPosition,
        bill = 1,
        nbill = 1,
        acc = 1,
        date, target, cumTarget,
        trendCols = [{
            field: "Month",
            title: "Month",
            template: (dataItem) => {
                date = dataItem.Month.split("-")[0]
                return date
            }
        }, {
            field: "Actual",
            title: "Actual",
        }, {
            field: "Budget",
            title: "Budget"
        }, {
            field: "Variance",
            title: "Var"
        }, {
            field: "Target",
            title: "Target",
            template: (dataItem) => {
                target = kendo.toString(parseFloat(dataItem.Target), "##.0 \\\%")
                return target
            }
        }, {
            field: "CumActual",
            title: "Actual"
        }, {
            field: "CumBudget",
            title: "Budget"
        }, {
            field: "CumVariance",
            title: "Var"
        }, {
            field: "CumTarget",
            title: "Target",
            template: (dataItem) => {
                cumTarget = kendo.toString(parseFloat(dataItem.CumTarget), "##.0 \\\%")
                return cumTarget
            }
        }];


    var module = {

        viewInit: (e) => {
            log.debug("Hours View - View Init Hit")
        },

        viewShow: (e) => {
            log.debug("Hours View - View Show Hit")

            $("#hoursView #pageTitleTop").text("Hours")

            /* Sets initial view to monthly view */
            swpSettings.statParams.duration === "/mo/"
                ? ($("#chkHoursMonthly").prop('checked', true),
                    $("#chkHoursYTD").prop('checked', false))
                : ($("#chkHoursMonthly").prop('checked', false),
                    $("#chkHoursYTD").prop('checked', true))

            $("#trendHoursGrid").kendoGrid({
                groupable: false,
                dataSource: swp.dataSources.gridHours_dataSource,
                columns: trendCols
            })

            swpSettings.statParams.showAccountable === false
                ? $("#rowAccountableToggle").css("display", "none")
                : ($("#rowAccountableToggle").css("display", "block"),
                    swpSettings.statParams.accountable === 1
                        ? $("#chkStatsAccountable").prop("checked", true)
                        : $("#chkStatsAccountable").prop("checked", false))
            module.handleBNACheck()
            module.handleComparisonCheck()
            module.showHoursInfo()
            swp.homeView.statHourFooterCheck()
            swp.debugView.addDebugListener()
        },

        handleBNACheck: () => {
            swpSettings.statParams.billable == 1
                ? $("#chkStatsBillable").prop('checked', true)
                : $("#chkStatsBillable").prop('checked', false)

            swpSettings.statParams.nonbillable == 1
                ? $("#chkStatsNonBillable").prop('checked', true)
                : $("#chkStatsNonBillable").prop('checked', false)

            swpSettings.statParams.accountable == 1
                ? $("#chkStatsAccountable").prop('checked', true)
                : $("#chkStatsAccountable").prop('checked', false)
        },

        handleBNACheckClick: (cb) => {
            let row = cb.id.split("Stats")[1]

            swpSettings.statParams[row.toLowerCase()] === 0
                ? swpSettings.statParams[row.toLowerCase()] = 1
                : swpSettings.statParams[row.toLowerCase()] = 0

            swp.homeView.handleStatToggle()
        },

        handleComparisonCheck: () => {
            if (swpSettings.statParams.compare === "ActVsBudg") {
                $("#chkCompareActVsBudg").prop('checked', true)
                $("#chkCompareActVsLastYr").prop('checked', false)
            } else if (swpSettings.statParams.compare === "ActVsLastYr") {
                $("#chkCompareActVsBudg").prop('checked', false)
                $("#chkCompareActVsLastYr").prop('checked', true)
            }

            // swp.homeView.handleStatToggle()
        },

        handleComparisonClick: (cb) => {
            let row = cb.id.split("Compare")[1]

            if (cb.id === "chkCompareActVsBudg") {
                $("#chkCompareActVsBudg").prop('checked', true)
                $("#chkCompareActVsLastYr").prop('checked', false)
                swpSettings.statParams.compare = "ActVsBudg"
                // swpSettings.statParams.showBudget === true
                // ? //logic here to display budget column and info budget info)
                // : // logic here to hide budget column)
                // $("#hoursView #trendHoursGrid tr > th:first").text("")
            } else if (cb.id === "chkCompareActVsLastYr") {
                $("#chkCompareActVsBudg").prop('checked', false)
                $("#chkCompareActVsLastYr").prop('checked', true)
                swpSettings.statParams.compare = "ActVsLastYr"
                //add logic here to display column with last years info (may want to create a new column for the grid with last years info that I hide and show)

                // $("#hoursView #trendHoursGrid tr > th:first").text("YTD")
            }
            // swpSettings.statParams[row.toLowerCase()] === 0
            //     ? swpSettings.statParams[row.toLowerCase()] = 1
            //     : swpSettings.statParams[row.toLowerCase()] = 0

            // swp.homeView.handleStatToggle()
        },

        handleCumulativeClick: (cb) => {
            if (cb.id === "chkHoursMonthly") {
                $("#chkHoursMonthly").prop('checked', true)
                $("#chkHoursYTD").prop('checked', false)
                $("#hoursView #trendHoursGrid tr > th:first").text("")
                swpSettings.statParams.duration = "/mo/"
            } else if (cb.id === "chkHoursYTD") {
                $("#chkHoursMonthly").prop('checked', false)
                $("#chkHoursYTD").prop('checked', true)
                $("#hoursView #trendHoursGrid tr > th:first").text("YTD")
                swpSettings.statParams.duration = "/ytd/"
            }

            swp.homeView.handleStatToggle()
        },

        handleHourHeaderCollapse: () => {
            $("#greyOverlay").remove()
        },

        handleHourHeaderExpand: () => {
            $("<div class='k-overlay' id='greyOverlay' onclick='swp.hoursView.toggleHoursHeader()'></div>").appendTo($("#hoursView .km-content"))
        },

        showHoursInfo: () => {
            let grid = $("#trendHoursGrid").data("kendoGrid"),
                statsUrl = `${swpSettings.service_url}trendhours/` +
                    swpSettings.objTook.token +
                    swpSettings.statParams.duration +
                    swpSettings.statParams.billable + `/` +
                    swpSettings.statParams.nonbillable + `/` +
                    swpSettings.statParams.accountable

            // fix for first cell of column "CumActual" throwing "cannot read style of undefined" @ grid.hideColumn("CumActual") on second viewshow
            grid.columns = trendCols;
            grid.dataSource.transport.options.read.url = statsUrl
            grid.dataSource.read()
            module.statsToggle()
        },

        statsToggle: () => {
            let grid = $("#trendHoursGrid").data("kendoGrid")

            //hide all by default
            grid.hideColumn("Actual")
            grid.hideColumn("Budget")
            grid.hideColumn("Target")
            grid.hideColumn("Variance")
            grid.hideColumn("CumActual")
            grid.hideColumn("CumBudget")
            grid.hideColumn("CumTarget")
            grid.hideColumn("CumVariance")

            // if ($("#chkHoursMonthly").is(':checked')) {
            if (swpSettings.statParams.duration === "/mo/") {
                $("#trendHoursGrid th[data-field=Month]").html("")

                grid.showColumn("Actual")

                if (swpSettings.statParams.showBudget === true) {
                    grid.showColumn("Budget")
                    grid.showColumn("Target")
                    grid.showColumn("Variance")
                }
            } else {
                $("#trendHoursGrid th[data-field=Month]").html("YTD")

                grid.showColumn("CumActual")

                if (swpSettings.statParams.showBudget === true) {
                    grid.showColumn("CumBudget")
                    grid.showColumn("CumTarget")
                    grid.showColumn("CumVariance")
                }
            }

        },

        toggleHoursHeader: (e) => {
            $("#hoursView #hourStatsDropdown").data("kendoMobileCollapsible").toggle()
        }
    }


    return module

}();

