//Copyright 2017 by Smart WebParts LLC
if (!swp) {
    var swp = {}
}
if (!swp.popups) {
    swp.popups = {}
}

swp.popups = function () {
    //private stuff
    let arrEntries = swpSettings.arrEntries,
        activeEntryPosition = swpSettings.activeEntryPosition,
        activeTimerPosition = swpSettings.activeTimerPosition,
        entryArray, entryPosition;

    //public interface
    var module = {

        cancelClearMessage: function () {
            swpSettings.bTimers === true ?
                (entryArray = swpSettings.arrEntriesTimers,
                    entryPosition = swpSettings.activeTimerPosition) :
                (entryArray = swpSettings.arrEntries,
                    entryPosition = swpSettings.activeEntryPosition)

            $("#session-entries-phone").css("pointer-events", "none");
            setTimeout(function () {
                $("#session-entries-phone").css("pointer-events", "auto");
            }, 700);
            $("#modalMessage").data("kendoMobileModalView").close();
        },

        cancelDeleteEntry: function () {
            $("#modalDelete").data("kendoMobileModalView").close();
        },

        ignoreBGAndRelease: function () {
            swpSettings.bTimers === true ?
                (entryArray = swpSettings.arrEntriesTimers,
                    entryPosition = swpSettings.activeTimerPosition) :
                (entryArray = swpSettings.arrEntries,
                    entryPosition = swpSettings.activeEntryPosition)

            $("#spanIgnore-phone").text().indexOf("Save") > 0 ?
                (entryArray[entryPosition].BillingGuidelineError = "IGNORE",
                    entryArray[entryPosition].saveEntry()) :
                (entryArray[entryPosition].narrative = "IGNOREBG" + entryArray[entryPosition].narrative,
                    entryArray[entryPosition].releaseEntry(),
                    entryArray[entryPosition].narrative = entryArray[entryPosition].narrative.substring(8))

            //swp.utilities.toggleUserFeedbackMessages(this)
        },

        okClearMessage: function () {
            
            swpSettings.bTimers === true ?
                (entryArray = swpSettings.arrEntriesTimers,
                    entryPosition = swpSettings.activeTimerPosition) :
                (entryArray = swpSettings.arrEntries,
                    entryPosition = swpSettings.activeEntryPosition)

            if (activeEntryPosition = 0, "true" === set_Timers || set_Timers === !0) {
                for (var a = entryArray.length; a--;) "" !== entryArray[a].get("timerId") && "Released" !== entryArray[a].get("status") && "Posted" !== entryArray[a].get("status") || entryArray.splice(a, 1);
            } else {
                entryArray = [];
            }
            $("#session-entries-phone").data("kendoMobileListView").setDataSource(entryArray);
            $("#session-entries-phone").data("kendoMobileListView").refresh();
            $("#modalMessage").data("kendoMobileModalView").close();
        },

        okDeleteEntry: function () {
            swp.timeentry.actions.deleteActiveEntry()
        },

        closeError: function () {
            $("#modalError").data("kendoMobileModalView").close()
            $("#timeEntryView #fieldNarrative-phone").attr('readonly', false)
            // swp.utilities.toggleUserFeedbackMessages(this)
        },

        openDeletePopup: function () {
            // $("#modalDelete").data("kendoMobileModalView").open()
            let deleteConfirm = confirm("Delete: Are you sure?")

            if (deleteConfirm == true) {
                swp.timeentry.actions.deleteActiveEntry()
            } 
        },


    }

    return module;

}()