//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.offline) { swp.offline = {} }

swp.offline = function () {
    "use strict";
    let arrEntries = swpSettings.arrEntries,
        arrEntriesTimers = swpSettings.arrEntriesTimers,
        activeEntryPosition = swpSettings.activeEntryPosition,
        activeTimerPosition = swpSettings.activeTimerPosition,
        c1 = swpSettings.custom1,
        c2 = swpSettings.custom2,
        c3 = swpSettings.custom3,
        c4 = swpSettings.custom4,
        c5 = swpSettings.custom5;

    var module = {

        cacheDataOffline: function () {

            log.debug("Caching offline data");

            swpSettings.cacheOffline = true;

            /* get recently used clients and matters */

            $.ajax({
                type: "GET",
                url: service_url + "clients/" + took + "/",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    localStorage.setItem("swp-clients", JSON.stringify(data));
                },
                failure: function (errMsg) {
                    alert("Error caching clients: " + errMsg);
                }
            });

            /* get ALL matters for MRU clients */
            $.ajax({
                type: "GET",
                url: service_url + "mrumatters/" + took + "/",
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    localStorage.setItem("swp-matters", JSON.stringify(data));
                },
                failure: function (errMsg) {
                    alert("Error caching matters: " + errMsg);
                }
            });


            /* get the custom menu data */

            $.ajax({
                type: "GET",
                url: service_url + "customall/" + took,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {

                    var custom1Items = [];
                    var custom2Items = [];
                    var custom3Items = [];
                    var custom4Items = [];


                    for (var i = 0; i < data.length; i++) {
                        if (data[i].number === 1) {
                            custom1Items.push(data[i]);
                        }
                        if (data[i].number === 2) {
                            custom2Items.push(data[i]);
                        }
                        if (data[i].number === 3) {
                            custom3Items.push(data[i]);
                        }
                        if (data[i].number === 4) {
                            custom4Items.push(data[i]);
                        }
                    }

                    localStorage.setItem("swp-custom1", JSON.stringify(custom1Items));
                    localStorage.setItem("swp-custom2", JSON.stringify(custom2Items));
                    localStorage.setItem("swp-custom3", JSON.stringify(custom3Items));
                    localStorage.setItem("swp-custom4", JSON.stringify(custom4Items));

                },
                failure: function (errMsg) {
                    alert("Error caching clients: " + errMsg);
                }
            });

            /* get the text shortcuts */
            $.ajax({
                type: "GET",
                url: service_url + "allshortcuts/?user=" + swp_username,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    localStorage.setItem("swp-shortcuts", JSON.stringify(data));
                },
                failure: function (errMsg) {
                    alert("Error caching shortcuts: " + errMsg);
                }
            });

            /* get phone stats*/
            if (!isTablet) { refreshStatsPhone(); }

        },

        createEntriesFromArray: function(arrEnts) {
    
             /* the methods and prototype info are lost during the serialization process so we need to create the objects */
            for (var i = 0; i < arrEnts.length; i++)
            {
                var entry = arrEnts[i];
                var entry2 = swp.entry.newEntry();  
        
                for (var p in entry2) 
                {
                    if (entry2.hasOwnProperty(p) && entry.hasOwnProperty(p))
                    {
                        entry2[p] = entry[p];
                    }                       
                }
        
                 /* if this entry is a timer, convert it */
                if (entry2.timerId !== '' && typeof entry.timerId !== 'undefined')
                {
                    var hrstime = roundTimer(entry2.get("timer"), entry2.get("matterIncrement"));
                    entry2.set("duration", hrstime);
                    entry2.timerId = '';
                }
        
                arrEnts[i] = entry2;
            }
    
            return arrEnts;
        },

        createApptsFromArray: function(arrEnts) {
    
             /* the methods and prototype info are lost during the serialization process so we need to create the objects */
            var data = [];
    
            var t = new kendo.data.SchedulerEvent({
                        id: 1,
                        title: "Event1",
                        start: new Date("2013/4/4 12:00"),
                        end: new Date("2013/4/4 14:00")
                    })
    
            data.push(t);
    
            for (var i = 0; i < arrEnts.length; i++) {

                var entry = arrEnts[i];
        
                log.debug(entry);
                log.debug(entry.description);
        
                var appt = new kendo.data.SchedulerEvent({
                    title: entry.title,
                    description: entry.description,
                    start: new Date(entry.start),
                    end: new Date(entry.end)
                });          
        
                data.push(appt);
            }
    
            return data;
        },

        customMessageBox: function(title, msg) {    
    
            if (swpSettings.isTablet) {                    
                setTimeout(function() {        
                    $("#modalview-login-fail").data("kendoMobileModalView").open();  
                    $("#spanLoginFailedTitle").text(title);
                    $("#pLoginFail").text(msg)
            
                    /* align text */
                    if (title === "Login Failed") {
                        $("#pLoginFail").css('margin-left', '16px')
                        $("#pLoginFail").css('margin-top', '15px')
                    }
                    else {
                        $("#pLoginFail").css('margin-left', '40px')
                        $("#pLoginFail").css('margin-top', '25px')
                    }            
                }, 1000);          
            } else {
                alert(msg);
            }
        },

        offlineLogin: function (user, pass) {

            /* authenticate user against the offline password */
            if (pass === localStorage.getItem("swp_OfflinePassword") && user === localStorage.getItem("swp_OfflineUser") && pass !== "") {
                log.debug("succesful offline login.");

                swpSettings.offlineMode = true;

                setupApplication(jQuery.parseJSON(localStorage.getItem("swp_settings")));

                swp_username = user;

                /* make sure we are not on stats */
                showCal();

                /* set datasource to offline mode */
                matterDataSource.online(false);
                custom1_dataSource.online(false);
                custom2_dataSource.online(false);
                custom3_dataSource.online(false);
                custom4_dataSource.online(false);

                /* turn off server side filtering */
                custom1_dataSource.options.serverFiltering = false;
                custom2_dataSource.options.serverFiltering = false;
                custom3_dataSource.options.serverFiltering = false;
                custom4_dataSource.options.serverFiltering = false;

                /* show offline icon */
                $(".divOfflineIcon").show();

                /* pull stats from cache */

                if (isTablet) {

                    $("#spanSavedMonthly").text(localStorage.getItem("swp-stat-saved"));
                    $("#spanReleasedMonthly").text(localStorage.getItem("swp-stat-released"));
                    $("#spanPostedMonthly").text(localStorage.getItem("swp-stat-posted"));
                    $("#spanTotalMonthly").text(localStorage.getItem("swp-stat-total"));

                    $("#liMassRelease").hide();
                    $("#liEmailReport").hide();
                    $("#liMassRelease_Timer").hide();
                    $("#liEmailReport_Timer").hide();
                    $("#session-totals-li-released").hide();
                    $("#session-totals-li-posted").hide();
                    $("#session-totals-li-total").hide();
                    $("#session-totals-li-released-timer").hide();
                    $("#session-totals-li-posted-timer").hide();

                    $("#btnRelease_tab").hide();
                    $("#btnRemove_tab").hide();
                    $("#divRelease_timer").hide();
                    $("#divRemove_timer").hide();

                    $("#divCalSearch").hide();

                    /* round bottom corners on saved li's */
                    $("#session-totals-li-saved").css("border-bottom-left-radius", "5px")
                        .css("border-bottom-right-radius", "5px")
                        .css("height", "42px")
                        .css("padding-bottom", "3px");

                    $("#session-totals-li-saved-timer").css("border-bottom-left-radius", "5px")
                        .css("border-bottom-right-radius", "5px")
                        .css("height", "42px")
                        .css("padding-bottom", "3px");

                    /* hide dashboard stuff */
                    $("#divLabelDashboard").hide();
                    $("#ulDashboard").hide();

                }
                else {
                    /* hide dupe icon */
                    $("#divDupe-phone").hide();

                    /* populate stats for phone */
                    $("#spanSavedMonthly-phone").text(localStorage.getItem("swp-stat-saved"));
                    $("#spanReleasedMonthly-phone").text(localStorage.getItem("swp-stat-released"));
                    $("#spanPostedMonthly-phone").text(localStorage.getItem("swp-stat-posted"));
                    $("#spanTotalMonthly-phone").text(localStorage.getItem("swp-stat-total"));

                    refreshStatsPhone();

                    /* hide all stack buttons */
                    $("#divStackButtons-phone").hide();

                    /* hide release/remove */
                    $("#btnRelease-phone").hide();
                    $("#btnRemove-phone").hide();

                    /* hide all saved icon  */
                    $("#divAllSaved-Phone").hide();

                    /* time entry buttons */
                    $("#btnSave-phone").css("width", "49%")
                    $("#btnDelete-phone").css("width", "49%")

                }

                /* retrieve all saved items from local storage */
                arrEntries = JSON.parse(localStorage.getItem("swp-entries"));

                if (arrEntries === null || typeof arrEntries === "undefined" || arrEntries.length === 0) {
                    arrEntries = [];
                    swp.entry.addNewEntry();
                }
                else {
                    /* the methods and prototype info are lost during the serialization process so we need to create the objects */
                    arrEntries = createEntriesFromArray(arrEntries);

                    activeEntryPosition = 0;
                    kendo.bind($("#viewTimeEntryTablet"), arrEntries[activeEntryPosition]);
                }

                if (typeof $("#session-entries-list").data("kendoMobileListView") !== "undefined") {
                    $("#session-entries-list").data("kendoMobileListView").setDataSource(arrEntries);
                    $("#session-entries-list").data("kendoMobileListView").refresh();
                }

            }
            else {
                customMessageBox("Login Failed", "The offline password is wrong.  Please try again.");
            }

        },

        resetControls: function () {

            /* online login, set everything back to normal */
            swpSettings.offlineMode = false;

            /* set datasource to offline mode */
            matterDataSource.online(true);
            custom1_dataSource.online(true);
            custom2_dataSource.online(true);
            custom3_dataSource.online(true);
            custom4_dataSource.online(true);

            /* turn off server side filtering */
            matterDataSource.options.serverFiltering = true;
            custom1_dataSource.options.serverFiltering = true;
            custom2_dataSource.options.serverFiltering = true;
            custom3_dataSource.options.serverFiltering = true;
            custom4_dataSource.options.serverFiltering = true;

            /* show offline icon */
            $(".divOfflineIcon").hide();

            if (isTablet) {

                $("#liMassRelease").show();
                $("#liEmailReport").show();
                $("#liMassRelease_Timer").show();
                $("#liEmailReport_Timer").show();
                $("#session-totals-li-released").show();
                $("#session-totals-li-posted").show();
                $("#session-totals-li-total").show();
                $("#session-totals-li-released-timer").show();
                $("#session-totals-li-posted-timer").show();

                $("#btnRelease_tab").show();
                $("#btnRemove_tab").show();
                $("#divRelease_timer").show();
                $("#divRemove_timer").show();

                $("#divCalSearch").show();

                /* round bottom corners on saved li's */
                $("#session-totals-li-saved").css("height", "29px")
                    .css("padding-bottom", "0px");

                $("#session-totals-li-saved-timer").css("height", "29px")
                    .css("padding-bottom", "0px");


                /* hide dashboard stuff */
                $("#divLabelDashboard").show();
                $("#ulDashboard").show();

            }
            else {
                refreshStatsPhone();

                /* show dupe icon */
                $("#divDupe-phone").show();

                /* hide all stack buttons */
                $("#divStackButtons-phone").show();

                /* hide release/remove */
                $("#btnRelease-phone").show();
                $("#btnRemove-phone").show();

                /* hide all saved icon  */
                $("#divAllSaved-Phone").show();

                /* time entry buttons */
                $("#btnSave-phone").css("width", "24%")
                $("#btnDelete-phone").css("width", "24%")

            }

        },

        syncOfflineData: function () {

            var offlineEntries = JSON.parse(localStorage.getItem("swp-entries"));

            log.debug("Looking for offline entries");

            if (offlineEntries !== null && typeof offlineEntries !== "undefined") {
                /* we have entries */
                var count = offlineEntries.length;

                log.debug("syncing " + count + " entries");

                offlineEntries = swp.offline.createEntriesFromArray(offlineEntries);

                for (var i = 0; i < count; i++) { offlineEntries[i].saveEntry(); }

                offlineEntries = [];

                localStorage.setItem("swp-entries", JSON.stringify(offlineEntries));

                if (count > 0) {
                    if (count === 1) {
                        swp.offline.customMessageBox("Sync Result", "   " + count.toString() + " entry synced online.");
                    }
                    else {
                        swp.offline.customMessageBox("Sync Result", "   " + count.toString() + " entries synced online.");
                    }
                }

            }

        },
    }
    return module
}()