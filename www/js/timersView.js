//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.timersView) { swp.timersView = {} }

swp.timersView = function () {
    "use strict";
    let arrEntries = swpSettings.arrEntries,
        arrEntriesTimers = swpSettings.arrEntriesTimers,
        activeEntryPosition = swpSettings.activeEntryPosition,
        activeTimerPosition = swpSettings.activeTimerPosition,
        sortOption, sortDir;

    var module = {
        viewInit: function(e) {
            log.debug("Timers View - View Init Hit")

            module.setupKendoElements()
        },

        viewShow: function(e) {
            log.debug("Timers View - View Show Hit")
            swpSettings.bTimers = true;

            arrEntriesTimers = swpSettings.arrEntriesTimers,
            activeTimerPosition = swpSettings.activeTimerPosition;

            if (swpSettings.set_MassActionsEnabled == true) {
                swp.utilities.cancelMassAction()
                $("#timersView #massActionCancelTouch").hide()
            }

            $("#timersView #timeEntryDescToggle").hide()
            //$("#timersView #divTouchTimerTemplate-phone").kendoTouch({
            //    enableSwipe: true, swipe: function (e) {
            //        if (e.direction === "left") {
            //            console.log(e);
            //        }
            //    }
            //})

            //$("#timersView  #timerTemplateTimerValue").kendoTouch({ enableSwipe: true, swipe: function (e) { if (e.direction === "left") { console.log(e); } } })
            //logic for first timersView visit
            if (swpSettings.first_timers_visit == true) {
                //set first view visit toggle to false
                swpSettings.first_timers_visit = false
                //check if load previous session setting is selected, otherwise query user

                if (swpSettings.timerSettings.LoadRadio === 4) {
                    swp.timer.actions.loadPreviousSession()
                } else {
                    setTimeout(() => {
                        swp.timer.actions.togglePreviousSession()
                    }, 300)
                }
            } else if (swpSettings.timerDropDown.open === true) {
                //logic for client/matter lookup from timersView dropdown
                swpSettings.activeTimerPosition = swpSettings.arrEntriesTimers.indexOf(swpSettings.arrEntriesTimers.find(x => x.id === swpSettings.timerDropDown.entryId))
                kendo.bind($("#timersView #timerCollapsibleHeader"), swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition])
                $(`#timersView #userFeedbackMessages .km-collapsible-content`).children().hide()
                $(`#timersView #timerCollapsibleHeader`).show()
                $("<div class='k-overlay' id='greyOverlay'></div>").appendTo($(`#timersView .km-content`))
                $("<div class='k-overlay' id='greyOverlay'></div>").appendTo($(`#timersView #divStackButtons-phone`))
                setTimeout(() => {
                    $(`#timersView #userFeedbackMessages`).data("kendoMobileCollapsible").expand()
                    $("#timersView #greyOverlay").click(swp.utilities.closeTimerDropDown)
                    $("#timersView #greyOverlayFoot").click(swp.utilities.closeTimerDropDown)
                }, 100) 
            } else {
                // swp.utilities.closeTimerDropDown()
                // same as above function but not working properly due to unwanted redirect. 
                swpSettings.timerDropDown.open = false
                swpSettings.timerDropDown.entryId = ""
                $(`#timersView #userFeedbackMessages`).data("kendoMobileCollapsible").collapse()
                $('.k-overlay').remove()
            }

            if (arrEntriesTimers.length === 0)
                swp.entry.addNewEntry()

            module.showTimers()
            swp.debugView.addDebugListener()
            //reset scroller so we start at top of list
            $("#session-entries-list-timer").data("kendoMobileListView").scroller().reset()
        },

        clearAllClick: function () {
            let id = this.element.context.id.split("-")[0]
            swp.timer.actions.selectTimer(id)

            //clear all necessary fields
            swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].timer = "00:00:00"
            swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].notes = ""
            swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].color = "white"
            swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].client = ""
            swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].matter = ""
            swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].custom1 = ""
            swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].custom2 = ""
            swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].custom3 = ""
            swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].custom4 = ""
            swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].custom5 = ""
            swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].narrative = ""

            module.reloadTimerStack()
            // location.href = "#views/timersView.html"
        },

        clearClockClick: function () {
            let id = this.element.context.id.split("-")[0]
            console.log(`clearClockClick function hit id: ${id}`)
            swp.timer.actions.selectTimer(id)
            swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].timer = "00:00:00"
            module.reloadTimerStack()
            // location.href = "#views/timersView.html"
        },

        closeTimerSidebar: function (e) {
            let id = e.sender.element.parent()[0].id
            console.log("closeTimerSidebar", e)
            $(`#timersView #${id}.sidebar`).css("width", "0px")
        },

        deleteTimerClick: function () {
            let id = this.element.context.id.split("-")[0]
            swp.timer.actions.selectTimer(id)
            console.log(`clearAllClick function hit id: ${id}`)
            swp.timeentry.actions.deleteActiveEntry()
            module.reloadTimerStack()
        },

        handleSortTimersToggle: function (cb) {
            sortOption = cb.id.split("_")[1];

            console.log("handleSortTimersToggle - sortOption: ", sortOption)

            $("#timersView #timersSortList input").prop('checked', false)
            $(`#timersView #sortByTimer_${sortOption}`).prop('checked', true)

            if (sortOption === "sortDate") {
                sortDir = "desc"
            } else {
                sortDir = "asc"
            }

            $("#session-entries-list-timer").data("kendoMobileListView").dataSource.sort([
                { field: `${sortOption}`, dir: `${sortDir}` },
            ])

            swpSettings.timerSortOption = `sortByTimer_${sortOption}`
        },

        releaseTimerButton: function (e) {
            console.log("releaseTimerButton function hit - e:", e)
            arrEntriesTimers = swpSettings.arrEntriesTimers
            activeTimerPosition = swpSettings.activeTimerPosition

            if (arrEntriesTimers[activeTimerPosition].status !== "Posted" && arrEntriesTimers[activeTimerPosition].status !== "Released") {
                arrEntriesTimers[activeTimerPosition].validate();
                arrEntriesTimers[activeTimerPosition].releaseEntry(e)

            } else if (arrEntriesTimers[activeTimerPosition].status === "Released" && swpSettings.unrelease === true) {
                arrEntriesTimers[activeTimerPosition].unrelease(e)
            }
        },

        reloadTimerStack: function () {
            $("#session-entries-list-timer").data("kendoMobileListView").setDataSource(swpSettings.arrEntriesTimers)
            $("#session-entries-list-timer").data("kendoMobileListView").dataSource.sort([
                { field: `${sortOption}`, dir: `${sortDir}` },
            ])
        },

        removeTimerClick: function () {
            let id = this.element.context.id.split("-")[0]
            swp.timer.actions.selectTimer(id)
            swp.timeentry.actions.removeActiveEntry()
            console.log(`removeTimerClick function hit id: ${id}`)
            module.reloadTimerStack()
        },

        saveTimerButton: function () {
            swpSettings.offlineMode === false
                ? swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].saveEntry()
                : swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].saveLocal()

            swpSettings.set_ValSave == "false" || swpSettings.set_ValSave == "False"
                swp.timeentry.actions.clearValidation()
        },

        setupKendoElements: function () {

            $("#session-entries-list-timer").kendoMobileListView({
                dataSource: new kendo.data.DataSource(arrEntriesTimers),
                scroller: (e) => {
                    console.log("scroller" + e)
                },
                template: $("#myTimersTemplate-phone").text(),
                dataBound: (e) => {
                    console.log("Timers dataBound event", e)

                    if (e.sender.dataSource.options.data !== null) {
                        e.sender.dataSource.options.data.forEach(x => {
                            //apply red overlay to timers that cannot be started and custom coloring to active timers
                            if (moment(x.eventDT).isSame(moment().startOf("day"), "day") === false) {
                                $(`#timersView #${x.id} .timerValueBorder`).css("background-color", "var(--customGrey)")
                                $(`#timersView #${x.id} #timerTemplateTimerValue`).css("font-weight", "700")
                                $(`.km-on-android #timersView #${x.id} #timerTemplateTimerValue`).css("padding", "1.25em .2em 1em .9em")
                                //$(`#timersView #${x.id} #timerTemplateTimerValue`).css("opacity", "0.25")
                            } else if (x.timerRunning === true) {
                                $(`#timersView #${x.id} .timerValueBorder`).css("background-color", "var(--customRed)")
                                $(`#timersView #${x.id} #timerTemplateTimerValue`).css("color", "white")
                                $(`#timersView #${x.id} #timerTemplateTimerValue`).css("font-weight", "700")
                                $(`.km-on-android #timersView #${x.id} #timerTemplateTimerValue`).css("padding", "1.25em .2em 1em .9em")
                            } else if (x.timer === "00:00:00") {
                                $(`#timersView #${x.id} .timerValueBorder`).css("background-color", "inherit")
                                $(`#timersView #${x.id} #timerTemplateTimerValue`).css("color", "black")
                                $(`#timersView #${x.id} #timerTemplateTimerValue`).css("font-weight", "100")
                                $(`.km-on-android #timersView #${x.id} #timerTemplateTimerValue`).css("padding", "1.25em .2em 1em 1.1em")
                            } else {
                                $(`#timersView #${x.id} .timerValueBorder`).css("background-color", "inherit")
                                $(`#timersView #${x.id} #timerTemplateTimerValue`).css("color", "black")
                                $(`#timersView #${x.id} #timerTemplateTimerValue`).css("font-weight", "700")
                                $(`.km-on-android #timersView #${x.id} #timerTemplateTimerValue`).css("padding", "1.25em .2em 1em .9em")
                            }

                            // //apply swipe action to each timer
                            // $("#timersView #divTouchTimerTemplate-phone").kendoTouch({
                            //     enableSwipe: true, swipe: function (e) {
                            //         if (e.direction === "right") {
                            //             console.log(e.direction)
                            //             module.swipeTimer(this)
                            //         }
                            //     }
                            // })
                        })
                    }
                }
            })
        },

        showTimers: function () {
            arrEntriesTimers = swpSettings.arrEntriesTimers

            setTimeout(() => {
                //swp.utilities.generalViewShow()
                $("#timersView #timerColorIndicatorTouch").hide()
                $("#timersView #timeEntryNarDone").hide()
                $("#timersView #timeEntryKeyboardClose").hide()
                $("#timersView #backNavButton").hide()
                swpSettings.searchParams.searchAllEntries = false
                $("#timersView #pageTitleTop").text("Timers")
                $("#session-entries-list-timer").data("kendoMobileListView").setDataSource(swpSettings.arrEntriesTimers)

                sortOption = swpSettings.timerSortOption.split("_")[1]

                if (sortOption === "sortDate") {
                    sortDir = "desc"
                } else {
                    sortDir = "asc"
                }

                $("#session-entries-list-timer").data("kendoMobileListView").dataSource.sort([
                    { field: `${sortOption}`, dir: `${sortDir}` },
                ])

                $("#timersView #session-entries-list-timer").css("margin-bottom", $("#timersView .km-footer").height())

            }, 200)
        },

        swipeTimer: function (timer) {
            console.log(timer);
            let id = timer.element.context.children[0].id
            //$(`#timersView #${id}.sidebar`).css("width", "100px")

            var drawerInstance = $(`#timersView #${id}.drawer`).data().kendoDrawer;
            var drawerContainer = drawerInstance.drawerContainer;

            if (drawerContainer.hasClass("k-drawer-expanded")) {
                drawerInstance.hide()
            } else {
                drawerInstance.show()
            }
        },

    }

    return module

}()
