//Copyright 2017 by Smart WebParts LLC
if (!swp) {
    var swp = {}
}
if (!swp.stackView) {
    swp.stackView = {}
}

swp.stackView = function () {
    "use strict";
    let arrEntries = swpSettings.arrEntries,
        activeEntryPosition = swpSettings.activeEntryPosition,
        entryHighlightColor = swpSettings.styles.entryHighlightColor,
        entryFlaggedColor = swpSettings.styles.entryFlaggedColor,
        disabledPhoneButtonColor = swpSettings.styles.disabledPhoneButtonColor,
        stackListView, hrs, min, sec, tempTime, includeOption, showOption, dayArr, sortOption, sortDir;
    var checkEntryCount = 0,
        ordering = ["New", "Saved", "Released", "Posted"];

    var module = {

        viewShow: (e) => {
            log.debug("Stack View - View Show Hit")
            swpSettings.bTimers = false
            activeEntryPosition = swpSettings.activeEntryPosition
            module.adjustFieldLength()
            dayArr = swpSettings.arrEntries
            kendo.ui.progress($("#stackView"), true)

            $("#stackView #timeEntryDescToggle").hide()
            swp.utilities.cancelMassAction()
            $("#stackView #massActionCancelTouch").hide()
            // removed first stackview logic to fix empty daysearch freezing app on stack -> homeview navigation. 
            // if (swpSettings.first_stack_visit === true && swpSettings.arrEntries.length === 0 && swpSettings.searchParams.searchAllEntries === false) {

            //     swp.homeView.daySearch_Phone(swpSettings.defaultDate)
            //     // if (swpSettings.stackParams.includeTimeCaptures === true) {
            //     //     module.checkCaptureFilters(swpSettings.arrEntries)
            //     // }
            // }
            // else {
            //     // if (swpSettings.stackParams.includeTimeCaptures === true) {
            //     //     module.checkCaptureFilters(swpSettings.arrEntries)
            //     // }

            //     // $("#session-entries-phone").data("kendoMobileListView").setDataSource(swpSettings.arrEntries)
            // }

            //module.loadTimeForm(swpSettings.defaultDate)
            /*Timeout to Fix Kendo.all.js line 208735 _mouseup function overwriting backNavButton href */
            setTimeout(() => {

                $(`#stackView #timerColorIndicatorTouch`).hide()
                $("#stackView #timeEntryNarDone").hide()
                $("#stackView #timeEntryKeyboardClose").hide()
                $("#stackView #backNavButton").hide()
                if ($(`#stackView #userFeedbackMessages`).data("kendoMobileCollapsible")) $(`#stackView #userFeedbackMessages`).data("kendoMobileCollapsible").collapse()
                $('.k-overlay').remove()
                swpSettings.searchParams.searchAllEntries = false
                $("#stackView #pageTitleTop").text("Stack")
                $("#stackView #divStackButtons-phone").show()
                $("#stackView #timeEntryAddDefaultDiv").show()

                $("#stackView #timeEntryAddDefaultDiv").kendoTouch({
                    doubletap: (e) => {
                        console.log("doubletap" + e)
                        return
                    }
                })

                sortOption = swpSettings.stackParams.stackSortOption.split("_")[1]
                sortDir = swpSettings.stackSortDirection

                module.updateSorting(sortOption, sortDir)
                module.checkSortDir()

                module.checkIncludeToggles()

                $("#session-entries-phone").data("kendoMobileListView").dataSource.data().forEach(x => {
                    swp.stackView.checkReturnedStatus(x)
                    module.checkNarrativeContent(x)

                    // //apply swipe action to each timer
                    // $("#stackView #divTouchEntryTemplate-phone").kendoTouch({
                    //     enableSwipe: true,
                    //     swipe: (e) => {
                    //         if (e.direction === "right") {
                    //             console.log(e.direction)
                    //             // module.flagEntry(e)
                    //         } else if (e.direction === "left") {
                    //             // module.removeEntry(e)
                    //         }
                    //     }
                    // })
                })

                //fix for Timer or Time Entry Icon display bug
                module.checkEntryType()

                module.setupCaptureButtons()
                swp.debugView.addDebugListener()

                $("#stackView #session-entries-phone").data("kendoMobileListView").scroller().reset()
                // $("#stackView .km-scroll-container").css("transform", "translate3d(0px, 0px, 0px)")
                $("#stackView #session-entries-phone").css("margin-bottom", $("#stackView .km-footer").height())
                swpSettings.first_stack_visit = false
            }, 300)
        },

        viewInit: (e) => {
            log.debug("Stack View - View Init Hit")
            // var ordering = [{ status: "New", idx: 1 }, { status: "Saved", idx: 2 }, { status: "Released", idx: 3 }, { status: "Posted", idx: 4 }]
            // sortOrder = ['New', 'Saved', 'Released', 'Posted']

            let pageSize = 30,
                date = moment(swpSettings.defaultDate).format("YYYY-M-D").toString(),
                tmrw = moment(swpSettings.defaultDate).add(1, "d").format("YYYY-M-D").toString();

            var stackData = new kendo.data.DataSource({
                data: [],
                change: (e) => {
                    console.log(e);
                    $(".km-scroller-refresh").hide()
                },
                requestEnd: function (e) {
                    console.log("requestEnd" + e)
                    $(".km-scroller-refresh").hide()
                },
            })

            $("#session-entries-phone").kendoMobileListView({
                dataSource: stackData,
                template: $("#myEntriesTemplate-phone").text(),
                filterable: {
                    field: "matter",
                    ignoreCase: true,
                },
                refresh: (e) => {
                    console.log(e)
                },
                dataBound: (e) => {
                    if (e.sender.dataSource.options.data !== null) {
                        e.sender.dataSource.options.data.forEach(x => {
                            // module.checkFlaggedStatus(x)
                            module.checkNarrativeContent(x)
                            module.checkReturnedStatus(x)
                        })

                        $(".km-loader").hide()
                    }
                    kendo.ui.progress($("#stackView"), false)
                    $(".km-scroller-refresh").hide()
                },
                change: function (e) {
                    // get ListView selection
                },
                sort: {
                    field: "status",
                    dir: "asc",
                    compare: (a, b) => {
                        // return (ordering[a].idx - ordering[b].idx);
                        return ordering.indexOf(a) - ordering.indexOf(b)
                    }
                }
            })

            $("#stackView li#removeEvent a").kendoButton({
                icon: "x-swp"
            })
            //commenting this out to bring back clear button. (unsure what this was for)
            // $('#divStackScroll-Phone form input').off();
            $('#divStackScroll-Phone form input').keyup(() => {
                let val = $('#divStackScroll-Phone form input')[0].value

                if (val === "") {
                    $('#session-entries-phone').data("kendoMobileListView").dataSource.filter({})
                }

                if (val !== "Search" && val !== "") {
                    $('#session-entries-phone').data("kendoMobileListView").dataSource.filter({
                        logic: "or",
                        filters: [{
                                field: "matter",
                                operator: "contains",
                                value: val
                            },
                            {
                                field: "client",
                                operator: "contains",
                                value: val
                            },
                            {
                                field: "narrative",
                                operator: "contains",
                                value: val
                            },
                            {
                                field: "narrative",
                                operator: "isempty",
                                filters: [{
                                        field: "matter",
                                        operator: "contains",
                                        value: val
                                    },
                                    {
                                        field: "client",
                                        operator: "contains",
                                        value: val
                                    },
                                    {
                                        field: "description",
                                        operator: "contains",
                                        value: val
                                    }
                                ]
                            }
                        ]
                    })

                    //make sure filters are being highlighted when active
                    $("#gridTimeEntry > div.k-grid-header > div > table > thead > tr > th[data-field=Client] > a.k-grid-filter").addClass("k-state-active")
                    $("#gridTimeEntry > div.k-grid-header > div > table > thead > tr > th[data-field=Matter] > a.k-grid-filter").addClass("k-state-active")


                }

                setTimeout(() => {
                    module.checkEntryType()
                }, 200)
                // e.view.element.find("#session-entries-phone").data("kendoMobileListView").setDataSource(swpSettings.arrEntries)
            })
            // module.setupCaptureButtons()
            // setTimeout(() => {
            //     module.setupCaptureButtons()
            // }, 300)

        },

        stackRefresh: function (e) {
            console.log("stackRefresh" + e)
            $(".km-scroller-refresh").hide()
        },

        adjustFieldLength: function () {
            let entries = $("#session-entries-phone").children()
            for (var i = 0; i < entries.length; i++) {
                $("#entriesTemplateClient-phone")
            }
        },

        bindEntry: function (id, sender) {
            let entryArray, entryPosition;

            /* this runs when an entry in the stack is clicked */
            log.debug('Binding: ' + id)
            swp.timeentry.actions.clearValidation()
            //arrEntries = swpSettings.arrEntries

            swpSettings.bTimers === true ?
                entryArray = swpSettings.arrEntriesTimers :
                entryArray = swpSettings.arrEntries

            for (var i = 0; i < entryArray.length; i++) {

                if (entryArray[i].id === id) {
                    location.href = "#views/timeEntryViews/timeEntryView.html";

                    entryPosition = i

                    swpSettings.bTimers === true ?
                        swpSettings.activeTimerPosition = entryPosition :
                        swpSettings.activeEntryPosition = entryPosition

                    console.log('Stack View', 'entryArray[i]', entryArray[i], 'entryPosition', entryPosition,
                        'swpSettings.activeEntryPosition', swpSettings.activeEntryPosition,
                        'swpSettings.activeTimerPosition', swpSettings.activeTimerPosition)

                    setTimeout(() => {

                        swpSettings.bTimers === true ?
                            kendo.bind($("#timeEntryView"), swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition]) :
                            kendo.bind($("#timeEntryView"), swpSettings.arrEntries[swpSettings.activeEntryPosition])


                        //check to display and update Billing Guidelines if they exist for current entry
                        if (entryArray[entryPosition].client) {
                            swp.utilities.refreshBillingGuidelines()
                        }

                        /* setup form */
                        // swp.utilities.refreshCustomMenus()
                        module.loadTimeForm()
                    }, 100)
                }
            }
        },

        checkCaptureFilters: function (data) {
            let tempArr = [],
                capArr = Object.keys(swpSettings.timeCaptureParams).filter(x => swpSettings.timeCaptureParams[x] === true),
                capFilters = [];

            if (swpSettings.stackParams.includeTimeCaptures === true) {
                capArr.forEach(x => {
                    capFilters.push({
                        field: "type",
                        operator: "contains",
                        value: x
                    })
                })
            }

            if (swpSettings.stackParams.includeTimeEntries === true) {
                capFilters.push({
                        field: "type",
                        operator: "contains",
                        value: "Mobile"
                    }),
                    capFilters.push({
                        field: "type",
                        operator: "contains",
                        value: "Mobile Entry"
                    }),
                    capFilters.push({
                        field: "type",
                        operator: "contains",
                        value: "Timer"
                    }),
                    capFilters.push({
                        field: "type",
                        operator: "contains",
                        value: "Entry"
                    })
            }

            if (swpSettings.stackParams.stackSortOption === "sortByEntry_returned" === true)
                capFilters = [{
                    field: "returned",
                    operator: "eq",
                    value: true
                }]

            if (!capFilters.length) capFilters = [{
                field: "id",
                operator: "isempty"
            }]

            if ($("#session-entries-phone").data("kendoMobileListView")) {
                $("#session-entries-phone").data("kendoMobileListView").dataSource.filter({
                    logic: "or",
                    filters: capFilters
                })
            }
        },

        checkEntryType: function () {
            let data = $('#session-entries-phone').data("kendoMobileListView").dataSource.data(),
                recurr = false;

            if (data !== null) {
                data.forEach(x => {
                    //apply red overlay to timers that cannot be started and custom coloring to active timers
                    if (x.type == "Timer" || x.timerEntry == true) {
                        $(`#stackView #${x.id} #stackTimerIdentifier`).css("display", "inline-flex")
                        $(`#stackView #${x.id} #stackTimeEntryIdentifier`).css("display", "none")
                        $(`#stackView #${x.id} #stackTimeCaptureIdentifier`).css("display", "none")
                    } else if (x.type == "Mobile") {
                        $(`#stackView #${x.id} #stackTimerIdentifier`).css("display", "none")
                        $(`#stackView #${x.id} #stackTimeEntryIdentifier`).css("display", "inline-flex")
                        $(`#stackView #${x.id} #stackTimeCaptureIdentifier`).css("display", "none")
                    } else {
                        $(`#stackView #${x.id} #stackTimerIdentifier`).css("display", "none")
                        $(`#stackView #${x.id} #stackTimeEntryIdentifier`).css("display", "none")
                        $(`#stackView #${x.id} #stackTimeCaptureIdentifier`).css("display", "inline-flex")
                    }
                })
                kendo.ui.progress($("#stackView"), false)
                $(".km-scroller-refresh").hide()
            }

            console.log("this", this)

            // $.when(swp.stackView.checkEntryType).done(() => {
            setTimeout(() => {

                $("#stackView #session-entries-phone li div.spanEntries-phone").each((i, item) => {

                    // console.log($(item).children("#stackTimerIdentifier")[0].style.display !== $(item).children("#stackTimeEntryIdentifier")[0].style.display)

                    if ($(item).children("#stackTimerIdentifier")[0].style.display === $(item).children("#stackTimeEntryIdentifier")[0].style.display) {
                        recurr = true
                    }
                })

                console.log("checkEntryCount", checkEntryCount)
                if (checkEntryCount < 5 && recurr === true) {
                    checkEntryCount = checkEntryCount + 1

                    kendo.ui.progress($("#stackView"), false)
                    $(".km-scroller-refresh").hide()

                    setTimeout(() => {
                        console.log("checkEntryType Recurring")
                        $(".km-scroller-refresh").hide()
                        module.checkEntryType()
                    }, 200)

                } else {
                    $(".km-scroller-refresh").hide()
                    console.log("checkEntryType Exiting")
                    checkEntryCount = 0
                    recurr = false
                }

            }, 200)
        },

        checkFlaggedStatus: function (x) {
            //apply Star for flagged entries
            if (x.flagged === true) {
                // $(`#stackView #${x.id}`).parent().parent().css("background-color", entryFlaggedColor)
                $(`#stackView #${x.id} #stackFlaggedIdentifier`).css("display", "inline-block")
            } else {
                // $(`#stackView #${x.id}`).parent().parent().css("background-color", "white")
                $(`#stackView #${x.id} #stackFlaggedIdentifier`).css("display", "none")
            }
        },

        checkIncludeToggles: function () {
            swpSettings.stackParams.includeTimeCaptures === true 
            ? ($("#stackView #stack_includeTimeCaptures").prop("checked", true),
                    swpSettings.stackParams.includeTimeEntries === true 
                    ? ($("#stackView #stack_includeTimeEntries").prop("checked", true),
                        module.checkCaptureFilters()) 
                    : ($("#stackView #stack_includeTimeEntries").prop("checked", false),
                        swpSettings.timeCaptureParams.Entry = false,
                        swpSettings.timeCaptureParams["Mobile Entry"] = false,
                        swpSettings.timeCaptureParams.Timer = false,
                        module.checkCaptureFilters()),
                    $("#stackView #stack_includeTimeCaptures").prop("checked", true)) 
            : ($("#stackView #stack_includeTimeCaptures").prop("checked", false),
                swpSettings.stackParams.includeTimeEntries === true 
                ? ($("#stackView #stack_includeTimeEntries").prop("checked", true),
                    $("#session-entries-phone").data("kendoMobileListView").dataSource.filter({
                        logic: "or",
                        filters: [{
                                field: "type",
                                operator: "contains",
                                value: "Mobile"
                            },
                            {
                                field: "type",
                                operator: "contains",
                                value: "Timer"
                            },
                            {
                                field: "type",
                                operator: "contains",
                                value: "Entry"
                            },
                            {
                                field: "type",
                                operator: "contains",
                                value: "Mobile Entry"
                            }
                        ]})) 
                    : ($("#stackView #stack_includeTimeEntries").prop("checked", false),
                        module.toggleAllOff()))


            if (swpSettings.stackParams.stackSortOption === "sortByEntry_returned" === true)
                $("#session-entries-phone").data("kendoMobileListView").dataSource.filter({
                    field: "returned",
                    operator: "eq",
                    value: true
                })
        },

        checkSortDir: function () {
            swpSettings.stackParams.stackSortDirection == "asc" 
            ? $("#sortDirectionToggle span").addClass("k-flip-v")
            : $("#sortDirectionToggle span").removeClass("k-flip-v")
        },

        checkNarrativeContent: function (x) {
            if (x.type === "Mobile" || x.type === "Timer" || x.status !== "New") {
                // $(`#stackView #${x.id}`).parent().parent().css("background-color", entryFlaggedColor)
                $(`#stackView #${x.id} #entriesTemplateNarrative-phone`).text(x.narrative)
            } else {
                // $(`#stackView #${x.id}`).parent().parent().css("background-color", "white")
                $(`#stackView #${x.id} #entriesTemplateNarrative-phone`).text(x.description)
            }
        },

        checkReturnedStatus: function (x) {
            //app red star for returned entries
            if (x.returned === true && x.status !== "Released" && x.status !== "Posted") {
                // $(`#stackView #${x.id}`).parent().parent().css("background-color", entryFlaggedColor)
                $(`#stackView #${x.id} #returnedEntryIndicator`).css("display", "block")
            } else {
                // $(`#stackView #${x.id}`).parent().parent().css("background-color", "white")
                $(`#stackView #${x.id} #returnedEntryIndicator`).css("display", "none")
            }
        },

        flagEntry: function (e) {
            let id = e.sender.element.children()[0].id
            var drawerInstance = $(`#stackView #${id}.flagDrawer`).data().kendoDrawer;

            $(`#stackView #${id} #stackFlaggedIdentifier`).css("display", "inline-block")
            // var drawerContainer = drawerInstance.drawerContainer;
            drawerInstance.show()

            //find & set activeEntryPosition based on id
            swpSettings.activeEntryPosition = swpSettings.arrEntries.indexOf(swpSettings.arrEntries.find(x => x.id === id))

            swpSettings.arrEntries[swpSettings.activeEntryPosition].flagged === true ?
                swpSettings.arrEntries[swpSettings.activeEntryPosition].flagged = false :
                swpSettings.arrEntries[swpSettings.activeEntryPosition].flagged = true

            // swp.stackView.checkEntryType()

            setTimeout(() => {
                swp.stackView.checkFlaggedStatus(swpSettings.arrEntries[swpSettings.activeEntryPosition])
                // swp.stackView.flagEntry(id)
                drawerInstance.hide()
            }, 500)
        },

        handleFlaggedEntriesToggle: function (cb) {
            sortOption = cb.id.split("_")[1]
            swpSettings.stackParams.stackSortOption = `sortByEntry_${sortOption}`

            console.log("handleSortTimersToggle - sortOption: ", sortOption)

            //set checkboxes in header to proper state
            $("#stackView #stackSortList input").prop('checked', false)
            $(`#stackView #sortByEntry_${sortOption}`).prop('checked', true)

            //filter stack for entries with flagged state true and then sort by date
            $("#session-entries-phone").data("kendoMobileListView").dataSource.filter({
                field: "flagged",
                operator: "eq",
                value: true
            })

            $("#session-entries-phone").data("kendoMobileListView").dataSource.sort([{
                field: `sortDate`,
                dir: `desc`
            }])

            //fix to show proper icon for each entry on stack
            // module.checkEntryType()
        },

        handleIncludeEntriesToggle: function (cb) {

            swpSettings.set_ToggleActive = true
            includeOption = cb.id.split("_")[1]
            swpSettings.stackParams[includeOption] = $(cb).prop("checked")

            swpSettings.stackParams[includeOption] === true 
            ? ($(`#stackView #stack_${includeOption}`).prop("checked", true),
                localStorage[includeOption] = true)
            : ($(`#stackView #stack_${includeOption}`).prop("checked", false),
                localStorage[includeOption] = false)

            module.checkCaptureFilters(swpSettings.arrEntries)

            setTimeout(() => {
                $("#stackView #userFeedbackMessages .km-collapsible-content").height($("#stackView #userFeedbackMessages .km-collapsible-content #stackIncShwCollapsibleHeader").height() + 15)
            }, 500)

            $("#session-entries-phone").data("kendoMobileListView").dataSource.sort([{
                field: `${sortOption}`,
                dir: `${sortDir}`
            }])

            module.checkIncludeToggles()
            module.checkEntryType()
            $(".km-scroll-container").css("transform", "translate3d(0px, 0px, 0px)")
        },

        handleMassActionToggle: function (item) {
            console.log(item)
            let checked = item.sender.element[0].children[0].checked
            swpSettings.activeEntryPosition = swpSettings.arrEntries.indexOf(swpSettings.arrEntries.find(x => x.id === item.sender.element[0].id))

            checked === true ?
                (swpSettings.arrEntries[swpSettings.activeEntryPosition].includeInMassAction = false) :
                (swpSettings.arrEntries[swpSettings.activeEntryPosition].includeInMassAction = true)

            // swpSettings.arrEntries.indexOf(swpSettings.arrEntries.find(x => x.id === item.sender.element[0].children[0].id.split("-")[0]))
        },

        handleMassActionTouch: function (item) {
            console.log(item)
            let checked = item.sender.element.siblings()[0].children[0].checked
            let id = item.sender.element[0].children[0].id
            swpSettings.activeEntryPosition = swpSettings.arrEntries.indexOf(swpSettings.arrEntries.find(x => x.id === id))

            checked === true ?
                (swpSettings.arrEntries[swpSettings.activeEntryPosition].includeInMassAction = false,
                    item.sender.element.siblings()[0].children[0].checked = false) :
                (swpSettings.arrEntries[swpSettings.activeEntryPosition].includeInMassAction = true,
                    item.sender.element.siblings()[0].children[0].checked = true)
            // swpSettings.arrEntries.indexOf(swpSettings.arrEntries.find(x => x.id === item.sender.element[0].children[0].id.split("-")[0]))
        },

        handleReturnedEntriesToggle: function (cb) {
            sortOption = cb.id.split("_")[1]
            swpSettings.stackParams.stackSortOption = `sortByEntry_${sortOption}`

            console.log("handleSortTimersToggle - sortOption: ", sortOption)

            // $("#stackView #stackSortList input").prop('checked', false)
            // $(`#stackView #sortByEntry_${sortOption}`).prop('checked', true)
            $("#stackView #stackSortList input").prop('checked', false)
            $(`#stackView #sortByEntry_${sortOption}`).prop('checked', true)

            // $("#stackView #sortByEntry_returned").prop("checked") === true
            //     ? dayArr = swpSettings.arrEntries.filter(x => x.returned === true)
            //     : dayArr = swpSettings.arrEntries

            // $("#session-entries-phone").data("kendoMobileListView").setDataSource(dayArr)
            $("#session-entries-phone").data("kendoMobileListView").dataSource.filter({
                field: "returned",
                operator: "eq",
                value: true
            })

            $("#session-entries-phone").data("kendoMobileListView").dataSource.sort([{
                field: `sortDate`,
                dir: `desc`
            }])

            // $("#session-entries-phone").data("kendoMobileListView").dataSource.sort(
            //     { field: "returned", dir: "desc" }
            // )


            // module.checkEntryType()
        },

        handleSortEntriesToggle: function (cb) {
            sortOption = cb.id.split("_")[1];
            swpSettings.stackParams.stackSortOption = `sortByEntry_${sortOption}`

            module.checkCaptureFilters()

            $("#stackView #stackSortList input").prop('checked', false)
            $(`#stackView #sortByEntry_${sortOption}`).prop('checked', true)

            $("#session-entries-phone").data("kendoMobileListView").dataSource.sort([{
                field: `${sortOption}`,
                dir: `${sortDir}`
            }, ])

            if (sortOption === "status") {
                $("#session-entries-phone").data("kendoMobileListView").dataSource.sort({
                    field: "status",
                    dir: "asc",
                    compare: (a, b) => {
                        // return (ordering[a].idx - ordering[b].idx);
                        return ordering.indexOf(a.status) - ordering.indexOf(b.status)
                    }
                })
            }

            module.checkEntryType()
            $(".km-scroll-container").css("transform", "translate3d(0px, 0px, 0px)")
        },

        highlightItem_Phone: function (a) {
            a.sender.element.parent("li").css("background-color", entryHighlightColor)

            if (swpSettings.set_MassActionsEnabled === true) {
                a.preventDefault()
                module.handleMassActionTouch(a)
            }

            setTimeout(() => {
                a.sender.element.parent("li").css("background-color", "white")
                // swpSettings.arrEntries[swpSettings.activeEntryPosition].flagged === true
                //     ? a.sender.element.parent("li").css("background-color", entryFlaggedColor)
                //     : a.sender.element.parent("li").css("background-color", "white")
            }, 500)
        },

        loadTimeForm: function (data) {
            console.log("loadTimeForm Function Hit")
            let d, entryArray, entryPosition;

            swpSettings.bTimers === true 
            ? (entryArray = swpSettings.arrEntriesTimers,
                entryPosition = swpSettings.activeTimerPosition,
                swp.timer.actions.timerNameColorUpdate()) 
            : (entryArray = swpSettings.arrEntries,
                entryPosition = swpSettings.activeEntryPosition,
                $("#lblHours-phone").text("Hours:"))

            //create a new entry/timer if none exist for that day
            if (entryArray.length === 0) {
                let b = swp.entry.newEntry();

                //if a date is passed in, use for new entry, else date is today - may want to use swpSettings.defaultDate
                data
                ? (b.eventDT = moment(data).format(swpSettings.dateFormat),
                    b.sortDate = moment(data)) 
                : swpSettings.defaultDate 
                    ? (b.eventDT = moment(swpSettings.defaultDate).format(),
                        b.sortDate = moment(swpSettings.defaultDate)) 
                    : (b.eventDT = moment().format(),
                        b.sortDate = moment())

                if (swpSettings.selectTime == "true" || swpSettings.selectTime == "True") {
                    var c = moment(b.eventDT).add((new Date).getMinutes(), "m").add((new Date).getHours(), "h").toDate();
                    b.set("eventDT", c)
                }

                swpSettings.bTimers === true 
                ? (swpSettings.arrEntriesTimers.push(b),
                    entryArray = swpSettings.arrEntriesTimers,
                    swpSettings.activeTimerPosition = 0)
                : (swpSettings.arrEntries.push(b),
                    entryArray = swpSettings.arrEntries,
                    swpSettings.activeEntryPosition = 0)
            }
debugger
            swp.timeentry.actions.clearValidation()

            if (entryArray.length === 1) {
                entryPosition = 0
            }
            console.log(`swpSettings.arrEntries ${swpSettings.arrEntries}, swpSettings.activeEntryPosition ${swpSettings.activeEntryPosition}, entryArray ${entryArray}, entryPosition ${entryPosition}`)

            swpSettings.bTimers === true 
            ? kendo.bind($("#list-timeform-phone"), swpSettings.arrEntriesTimers[entryPosition]) 
            : kendo.bind($("#list-timeform-phone"), swpSettings.arrEntries[entryPosition])

            // Your moment
            var mmt = moment();
            // Your moment at midnight
            var mmtMidnight = mmt.clone().startOf('day');
            var eventDateIsSameDay = moment(entryArray[entryPosition].eventDT).isSame(mmtMidnight, "day");

            //TA: .color check for timers that have been released previously, so they are now time entries without a timer note/color, may want to switch this to a setting a some point if we allow the user to reapply a timer note/color (4.28.20)
            //Toggle which time format to display
            if ((swpSettings.set_DurationInUnits === true && entryArray[entryPosition].timer === "") || (swpSettings.set_DurationInUnits === true && entryArray[entryPosition].timer === null) || (swpSettings.set_DurationInUnits === true && entryArray[entryPosition].timer !== "" && entryArray[entryPosition].color === "") || entryArray[entryPosition].status === "Released" || entryArray[entryPosition].status === "Posted") {
                $("#fieldDurationTime-phone").hide()
                $("#fieldTimer-phone").hide()
                $("#fieldDurationUnits-phone").show()

            } else {
                $("#fieldDurationTime-phone").show()
                $("#fieldDurationUnits-phone").hide()
                $("#timeEntryView #btnTimerToggle-phone").hide()
                $("#timeEntryView #list-timeform-phone a.clearEntryField").hide()


                if (entryArray[entryPosition].timer === "" || entryArray[entryPosition].timer === null || (entryArray[entryPosition].timer !== "" && entryArray[entryPosition].color === "")) {

                    tempTime = entryArray[entryPosition].duration.split(".")
                    hrs = tempTime[0]

                    if (hrs.length !== 0 && hrs.length < 2) {
                        hrs = "0" + hrs
                    }

                    min = swp.utilities.checkTimeFormat(Math.ceil(tempTime[1] * .6))

                    swpSettings.arrEntries[swpSettings.activeEntryPosition].durationTimeDisplay = `${hrs}:${min}`
                    $("#fieldDurationTime-phone").val(`${hrs}:${min}`)
                    $("#fieldTimer-phone").hide()

                } else {
                    //logic here for timer display options
                    $("#fieldDurationTime-phone").hide()
                    $("#fieldTimer-phone").show()
                    $("#lblHours-phone").text("Timer:")
                    //swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].timer = `${hrs}:${min}:${sec}`
                }
            }

            // dateField.addEventListener("click", this.openDatePicker, true)
            let dateField = document.querySelector("#timeEntryView #timeEntryDatePicker-phone-row .phoneTimeInput")

            /* disable fields and buttons for released/posted entry */
            entryArray[entryPosition].status === "Released" || entryArray[entryPosition].status === "Posted" ?
                ($("#timeEntryView #timeEntryDateInput").data("kendoDatePicker").enable(false),
                    dateField.removeEventListener("click", swp.timeEntryView.openDatePicker),
                    $("#timeEntryView #list-timeform-phone a.clearEntryField").hide(),
                    $("#list-timeform-phone a").removeAttr("href"),
                    $("#timeEntryView #fieldNarrative-phone").attr("readonly", true),
                    $("#button-save-phone").css("color", disabledPhoneButtonColor),
                    $("#button-Delete-phone").css("color", disabledPhoneButtonColor),
                    $("#btnRemove-phone").css("color", "black"),
                    $("#timeEntryView #btnTimerToggle-phone").hide(), /* hide timer toggle */
                    swpSettings.unrelease === false && $("#btnRelease-phone").css("color", disabledPhoneButtonColor)) :
                ($("#timeEntryView #timeEntryDateInput").data("kendoDatePicker") &&
                    $("#timeEntryView #timeEntryDateInput").data("kendoDatePicker").enable(true),
                    // $("#timeEntryView #list-timeform-phone a.clearEntryField").show(),
                    $("#timeEntryView #list-timeform-phone a.clearEntryField").css("display", "block"),
                    $(".te-button-phone").css("color", "black"),
                    swp.entry.updateReferences(),
                    $("#divStackButtons-phone a").css("color", "black"),
                    $("#timeEntryView #fieldNarrative-phone").removeAttr("readonly"))

            entryArray[entryPosition].status === "Posted" ?
                $("#button-ReleaseUnrelease-phone").css("color", disabledPhoneButtonColor) :
                $("#button-ReleaseUnrelease-phone").css("color", "black")

            //    ("" === d || null === d || 0 === parseFloat(d)) && entryArray[entryPosition].timer === "" && entryArray[entryPosition].status === "New" && entryArray[entryPosition].status !== "Posted" && entryArray[entryPosition].status !== "Released"
            // entryArray[entryPosition].status === "New" && entryArray[entryPosition].status !== "Posted" && entryArray[entryPosition].status !== "Released"
            //     ? $("#timeEntryView #btnTimerToggle-phone").show()
            //     : $("#timeEntryView #btnTimerToggle-phone").hide()
            // entryArray[entryPosition].status === "New" && entryArray[entryPosition].status !== "Posted" && entryArray[entryPosition].status !== "Released" && (entryArray[entryPosition].type === "Mobile" || entryArray[entryPosition].type === "Timer")
            //     ? entryArray[entryPosition].eventTitle === "Mobile Timer" && entryArray[entryPosition].status !== "Posted" && entryArray[entryPosition].status !== "Released" && eventDateIsSameDay !== false

            //Logic here to diplay or hide btnTimerToggle
            d = entryArray[entryPosition].duration;
            (entryArray[entryPosition].status === "New" && entryArray[entryPosition].type === "Mobile" && ("" === d || null === d || 0 === parseFloat(d))) ?
            ($("#timeEntryView #btnTimerToggle-phone").show(),
                $("#spanTimerPhone").hide()) :
            (entryArray[entryPosition].type === "Timer" || entryArray[entryPosition].eventTitle === "Mobile Timer") && entryArray[entryPosition].status !== "Posted" &&
                entryArray[entryPosition].status !== "Released" && entryArray[entryPosition].color !== "" && eventDateIsSameDay !== false ?
                ($("#timeEntryView #btnTimerToggle-phone").hide(),
                    $("#spanTimerPhone").show(),
                    $("#timeEntryView #timeEntryDateInput").data("kendoDatePicker") && $("#timeEntryView #timeEntryDateInput").data("kendoDatePicker").enable(false),
                    entryArray[entryPosition].timerRunning === true ?
                    ($("#btnStartTimer-phone").css("display", "none"),
                        $("#btnPauseTimer-phone").css("display", "inline-block")) :
                    ($("#btnStartTimer-phone").css("display", "inline-block"),
                        $("#btnPauseTimer-phone").css("display", "none"))) :
                ($("#timeEntryView #btnTimerToggle-phone").hide(),
                    $("#spanTimerPhone").hide())


            // ((entryArray[entryPosition].status === "New" || entryArray[entryPosition].status === "Saved") && (entryArray[entryPosition].type === "Mobile" || entryArray[entryPosition].type === "Timer") && ("" === d || null === d || 0 === parseFloat(d)))
            //     ? entryArray[entryPosition].eventTitle === "Mobile Timer" && entryArray[entryPosition].status !== "Posted" && entryArray[entryPosition].status !== "Released" && eventDateIsSameDay !== false
            //         ? ($("#timeEntryView #btnTimerToggle-phone").hide(),
            //             $("#spanTimerPhone").show(),
            //             $("#timeEntryView #timeEntryDateInput").data("kendoDatePicker") && $("#timeEntryView #timeEntryDateInput").data("kendoDatePicker").enable(false),
            //             entryArray[entryPosition].timerRunning === true
            //                 ? ($("#btnStartTimer-phone").css("display", "none"),
            //                     $("#btnPauseTimer-phone").css("display", "inline-block"))
            //                 : ($("#btnStartTimer-phone").css("display", "inline-block"),
            //                     $("#btnPauseTimer-phone").css("display", "none")))
            //         : ($("#timeEntryView #btnTimerToggle-phone").show(),
            //             $("#spanTimerPhone").hide())
            //     : ($("#timeEntryView #btnTimerToggle-phone").hide(),
            //         $("#spanTimerPhone").hide())

            //Always Run this logic
            $("#timeEntryView #timeEntryDateInput").attr("readonly", true)
            $("#timeEntryView #timeEntryDateInput").css("color", "black")

            entryArray[entryPosition].type !== "Mobile" && entryArray[entryPosition].type !== "Timer" && entryArray[entryPosition].type !== "" && entryArray[entryPosition].type !== null && !entryArray[entryPosition].narrative.length ?
                (entryArray[entryPosition].description.length >= swpSettings.set_MaxNarrative ?
                    $("#narrativeCharCount").css("font-weight", "700") :
                    $("#narrativeCharCount").css("font-weight", "400"),
                    $("#narrativeCharCount").text(`${entryArray[entryPosition].description.length} of ${swpSettings.set_MaxNarrative}`),
                    $("#fieldNarrative-phone").val(entryArray[entryPosition].description),
                    $("#fieldNarrative-phone").css("background-color", "lightgrey"),
                    $("#timeEntryView #fieldNarrative-phone").css("border-bottom", `solid white 14em`),
                    $("#timeEntryView #fieldNarrative-phone")[0].scrollHeight > 75 && $("#fieldNarrative-phone").css("border-bottom", "solid white 13em"),
                    $("#timeEntryView #fieldNarrative-phone")[0].scrollHeight > 95 && $("#fieldNarrative-phone").css("border-bottom", "solid white 12em"),
                    $("#timeEntryView #fieldNarrative-phone")[0].scrollHeight > 120 && $("#fieldNarrative-phone").css("border-bottom", "solid white 10.5em"),
                    $("#timeEntryView #fieldNarrative-phone")[0].scrollHeight > 145 && $("#fieldNarrative-phone").css("border-bottom", "solid white 9.5em")) :
                (entryArray[entryPosition].narrative.length >= swpSettings.set_MaxNarrative ?
                    $("#narrativeCharCount").css("font-weight", "700") :
                    $("#narrativeCharCount").css("font-weight", "400"),
                    $("#narrativeCharCount").text(`${entryArray[entryPosition].narrative.length} of ${swpSettings.set_MaxNarrative}`),
                    $("#timeEntryView #fieldNarrative-phone").css("height", "18em"),
                    $("#timeEntryView #fieldNarrative-phone").css("background-color", "white"),
                    $("#fieldNarrative-phone").css("border-bottom", "solid white 0em"))

            //height check for view
            var e = $("body").height()
            e = parseInt(e) - 125
            $("#divTimeForm-Phone .km-listview-wrapper").height(e)

            setTimeout(() => {
                swp.utilities.refreshMatters()
                swp.utilities.refreshCustomMenus()
                swp.entry.entryType(entryArray[entryPosition])
            }, 300)
        },

        removeEntry: function (e) {
            let id = e.sender.element.children()[0].id
            var drawerInstance = $(`#stackView #${id}.removeDrawer`).data().kendoDrawer;

            drawerInstance.show()
            swpSettings.activeEntryPosition = swpSettings.arrEntries.indexOf(swpSettings.arrEntries.find(x => x.id === id))

            // swp.stackView.checkEntryType()

            setTimeout(() => {
                swp.timeentry.actions.removeActiveEntry()
                drawerInstance.hide()
            }, 500)
        },

        setupCaptureButtons: function () {
            document.getElementById("showCaptureToggleContainer").innerHTML = ""

            let typeArr = swpSettings.arrEntries.map(x => x.type)
            let filterArr = Object.keys(swpSettings.timeCaptureParams).filter(x => typeArr.includes(x)) /*Object.keys(swpSettings.timeCaptureParams).filter(x => typeArr.includes(x)).map(x => x.type) x[0].toUpperCase() + x.slice(1)*/

            filterArr.forEach(x => {

                if (x !== "_events" && x !== "_handlers" && x !== "uid" && x !== "parent" && x !== "Entry"
                    && x !== "Mobile" && x !== "Timer" && x !== "Mobile Entry") {
                    document.getElementById("showCaptureToggleContainer").innerHTML += `<button kendoButton onClick='swp.stackView.toggleCaptureShow(this)' id=${x} >${x}</button>`
                }

                swpSettings.timeCaptureParams[x] === true ?
                    $(`#stackView #showCaptureToggleContainer #${x}`).addClass("showCaptureOption") :
                    $(`#stackView #showCaptureToggleContainer #${x}`).removeClass("showCaptureOption")
            })
        },

        toggleAllOff: function () {
            let tempArr = [],
                capArr = Object.keys(swpSettings.timeCaptureParams).filter(x => swpSettings.timeCaptureParams[x] === true),
                capFilters = [];

            capArr.forEach(x => {
                capFilters.push({
                    field: "type",
                    operator: "doesnotcontain",
                    value: x
                })
            })

            capFilters.push({
                    field: "type",
                    operator: "doesnotcontain",
                    value: "Mobile"
                }),
                capFilters.push({
                    field: "type",
                    operator: "doesnotcontain",
                    value: "Timer"
                }),
                capFilters.push({
                    field: "type",
                    operator: "doesnotcontain",
                    value: "Entry"
                });

            if (swpSettings.stackParams.stackSortOption === "sortByEntry_returned" === true)
                capFilters = [{
                    field: "returned",
                    operator: "eq",
                    value: true
                }]

            if (!capFilters.length) capFilters = [{
                field: "id",
                operator: "isempty"
            }]

            if ($("#session-entries-phone").data("kendoMobileListView")) {
                $("#session-entries-phone").data("kendoMobileListView").dataSource.filter({
                    logic: "and",
                    filters: capFilters
                })
            }
        },

        toggleAllCaptureParams: function () {
            let btn = $("#stackView #toggleAllCaptureParamsButton")
            btn.text() === "Turn All Buttons On" ?
                (btn.text("Turn All Buttons Off")) :
                (btn.text("Turn All Buttons On"))

            Object.keys(swpSettings.timeCaptureParams).forEach(x => {
                if (x !== "_events" && x !== "_handlers" && x !== "uid" && x !== "parent" && x !== "Mobile" && x !== "Timer")
                    btn.text() === "Turn All Buttons On" ?
                    swpSettings.timeCaptureParams[x] = false :
                    swpSettings.timeCaptureParams[x] = true

                swpSettings.timeCaptureParams[x] === true ?
                    $(`#stackView #showCaptureToggleContainer #${x}`).addClass("showCaptureOption") :
                    $(`#stackView #showCaptureToggleContainer #${x}`).removeClass("showCaptureOption")
            })

            module.checkCaptureFilters(swpSettings.arrEntries)
            localStorage.setItem("timeCaptureParams", JSON.stringify(swpSettings.timeCaptureParams))

            $("#session-entries-phone").data("kendoMobileListView").dataSource.sort([{
                field: `${sortOption}`,
                dir: `${sortDir}`
            }])

            module.checkEntryType()
            $(".km-scroll-container").css("transform", "translate3d(0px, 0px, 0px)")
        },

        toggleCaptureShow: function (e) {
            $(e).hasClass("showCaptureOption") ?
                ($(e).removeClass("showCaptureOption"),
                    swpSettings.timeCaptureParams[e.textContent] = false) :
                ($(e).addClass("showCaptureOption"),
                    swpSettings.timeCaptureParams[e.textContent] = true)

            localStorage.setItem("timeCaptureParams", JSON.stringify(swpSettings.timeCaptureParams))
            module.checkCaptureFilters(swpSettings.arrEntries)

            $("#session-entries-phone").data("kendoMobileListView").dataSource.sort([{
                field: `${sortOption}`,
                dir: `${sortDir}`
            }])

            module.checkEntryType()
            $(".km-scroll-container").css("transform", "translate3d(0px, 0px, 0px)")
        },

        updateSorting: function (sortOption, sortDir) {
            if (sortOption === "status") {
                $("#session-entries-phone").data("kendoMobileListView").dataSource.sort({
                    field: "status",
                    dir: `${sortDir}`,
                    compare: (a, b) => {
                        return ordering.indexOf(a) - ordering.indexOf(b)
                    }
                })
            } else {
                $("#session-entries-phone").data("kendoMobileListView").dataSource.sort([{
                    field: `${sortOption}`,
                    dir: `${sortDir}`
                }])
            }
        },
    }

    return module

}()