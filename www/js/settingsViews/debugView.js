//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.debugView) { swp.debugView = {} }

swp.debugView = function () {
    "use strict";

    var module = {

        viewInit: (e) => {
            log.debug("Debug View - View Init Hit")
           
        },

        viewShow: (e) => {
            log.debug("Debug View - View Show Hit")
            module.addDebugListener()
            $("#debugView #backNavButton").attr("href", "#views/settingsViews/settingsView.html")
            $("#debugView #pageTitleTop").text("Debug")
            $("#debugView #timeEntryAddDefaultDiv").hide()
        },

        addDebugListener: () => {
            // document.body.addEventListener("click", (ev) => {
            if (!document.body.onclick) {
                document.body.onclick = (ev) => {
                    console.log(ev.target.id)
                    module.addToDebug(ev.target.id, ev.type, ev.target.className, ev.view.location.href)
                    //still need to find correct way of deleting an item, may need to adjust how I am adding items to the listview.
                    // if (swpSettings.debugData.data().length > 29) {
                    //     // $("#list-debug-phone").data("kendoMobileListView").remove(swpSettings.debugData.data()[19])
                    //     let dataItem = swpSettings.debugData.at(29)
                    //     swpSettings.debugData.remove(dataItem)
                    // }
                }
            }
        },

        addToDebug: (id, type, className, location) => {
            swpSettings.debugData.add({
                data: {
                    timeStamp: moment().format('YYYY-MM-DDTHH:mm:ss:SSS'),
                    id: id,
                    type: type,
                    className: className,
                    location: location,
                    indx: swpSettings.debugData.data().length,
                }
            })
        },

        clearDebugData: (e) => {
            e.preventDefault()
            swpSettings.debugData.data([])
        },

        debugDataTimeCheck: () => {
            // Your moment
            let mmt = moment()
            // Your moment at midnight
            let mmtMidnight = mmt.clone().startOf('day');
            // Difference in minutes
            let diffMinutes = mmt.diff(mmtMidnight, 'minutes');

            // let startTimeIsSameDay = moment(this.timerStart).isSame(mmtMidnight, "day");
            // let eventDateIsSameDay = moment(this.eventDate()).isSame(mmtMidnight, "day");

            let data = swpSettings.debugData.data()
            data.forEach(x => {
                // moment(x.timeStamp).isSame(mmtMidnight, "day") === true
                let duration = Math.abs(moment.duration(mmt.diff(x.timeStamp)).asHours())

                if (duration > swpSettings.set_DebugDurationLimit) {
                    let dataItem = swpSettings.debugData.at(x.indx)
                    swpSettings.debugData.remove(dataItem);
                }
            })
        },

        emailAllDebugData: (e) => {
            e.preventDefault()

            let a = [],
                d = [],
                dataArr = swpSettings.debugData.data();

            for (let b = 0; b < dataArr.length; b++) {

                let c = {
                    timeStamp: dataArr[b].data.timeStamp,
                    id: dataArr[b].data.id,
                    type: dataArr[b].data.type,
                    className: dataArr[b].data.className,
                    location: dataArr[b].data.location
                }

                a.push(c)
            }

            dataArr = swpSettings.debugLogData.data();

            for (let e = 0; e < dataArr.length; e++) {
                let f = {
                    timeStamp: dataArr[e].data.timeStamp,
                    log: dataArr[e].data.log,
                }
                d.push(f);
            }

            a = JSON.stringify(a)
            d = JSON.stringify(d)
            window.open(`mailto:${swpSettings.swp_supportEmail}?subject=Smart Time Mobile Debug Logg for ${swpSettings.swp_username}&body=Actions: ${a} Log: ${d}`);
        },

        emailDebugData: (e) => {
            e.preventDefault()

            let a = [],
                dataArr = swpSettings.debugData.data();

            for (let b = 0; b < dataArr.length; b++) {

                let c = {
                    timeStamp: dataArr[b].data.timeStamp,
                    id: dataArr[b].data.id,
                    type: dataArr[b].data.type,
                    className: dataArr[b].data.className,
                    location: dataArr[b].data.location
                }

                a.push(c)
            }
            a = JSON.stringify(a)
            window.open(`mailto:${swpSettings.swp_supportEmail}?subject=Smart Time Mobile Debug Logg for ${swpSettings.swp_username}&body=${a}`);
        },

        showDebugDisplay: (e) => {
            console.log("showDebugDisplay e" + e)
            // swp.openViews.openDebugView()
            location.href = "#popover-debug-phone"

        },

        toggleDebugBtn: (e) => {
            console.log("toggleDebugBtn e" + e)
            swpSettings.debugCount++

            if (swpSettings.debugCount == 1) {
                setTimeout(() => {
                    swpSettings.debugCount = 0
                    $("#settingsView #debugButton").hide()
                }, 5000)
            }

            if (swpSettings.debugCount >= 3) {
                $("#settingsView #debugButton").show()
            }
        },
    }

    return module
}()
