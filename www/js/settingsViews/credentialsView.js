//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.credentialsView) { swp.credentialsView = {} }

swp.credentialsView = function () {
    "use strict";
    let activeEntryPosition = swpSettings.activeEntryPosition,
        arrEntries = swpSettings.arrEntries,
        arrEntriesTimers = swpSettings.arrEntriesTimers;

    function localStoragePhone() { }

    localStoragePhone.prototype = {
        run: function () {
            var a = this,
                b = localStorage.getItem("swp_wsURL"),
                webservice;

            swpSettings.enableAuth0 = localStorage.getItem("swp_EnableAuth0")

            swpSettings.set_Timers = localStorage.getItem("swp_phoneTimers")
            if (b == "undefined" || b == "null") {
                b = ""
            }
            //"undefined" == b && (b = "")
            //"null" == b && (b = "")
            null == swpSettings.set_Timers && (swpSettings.set_Timers = !0)

            if (swpSettings.enableAuth0 == null)
                swpSettings.enableAuth0 = "false"

            webservice = document.getElementById("txtURL-phone")

            if (webservice) webservice.value = swp.utilities.removeWSRest(b)

            webservice = document.getElementById("txtEmail-phone")
            if (webservice) webservice.value = swpSettings.swp_emailaddr

            //$("#offline-pass-phone").val(localStorage.getItem("swp_OfflinePassword"))
            //"false" == swpSettings.set_Timers ? $("#timers-switch-phone").data("kendoMobileSwitch").check(!1) : $("#timers-switch-phone").data("kendoMobileSwitch").check(!0)
            //"false" == swpSettings.set_MultiTimers ? $("#multi-switch-phone").data("kendoMobileSwitch").check(!1) : $("#multi-switch-phone").data("kendoMobileSwitch").check(!0)
            if ($("#auth0CloudSwitch").data("kendoSwitch")) "false" == swpSettings.enableAuth0 ? $("#auth0CloudSwitch").data("kendoSwitch").check(!1) : $("#auth0CloudSwitch").data("kendoSwitch").check(!0)
            $("btnSaveSettings-phone").unbind()
            document.getElementById("btnSaveSettings-phone").addEventListener("touchstart", function () {
                a._SaveSettings.apply(a, arguments)
            })
        },
        _SaveSettings: function () {

            let valueInput = document.getElementById("txtURL-phone")
            valueInput.value != ""
                ? (localStorage.swp_wsURL = swp.utilities.cleanupURL(valueInput.value),
                    swpSettings.service_url = swp.utilities.cleanupURL(valueInput.value))
                : (localStorage.swp_wsURL = valueInput.value,
                    swpSettings.service_url = "https://mobile.smarttimeapps.com/wsRest.svc/")

            //swpSettings.set_Timers = $("#timers-switch-phone").data("kendoMobileSwitch").check()
            localStorage.swp_phoneTimers = swpSettings.set_Timers
            valueInput = document.getElementById("txtEmail-phone")
            swpSettings.swp_emailaddr = valueInput.value
            $("#imgSaveCredentials-phone").show()
            $("#imgSaveCredentials-phone").delay(1e3).fadeOut("slow")

            //var b = $("#multi-switch-phone").data("kendoMobileSwitch").check()
            //swpSettings.set_MultiTimers = b.toString().trim();

            var c = new kendo.data.ObservableObject({
                Username: swpSettings.swp_username,
                EmailAddr: swpSettings.swp_emailaddr,
                MultiTimers: swpSettings.set_MultiTimers
            })

            $.ajax({
                type: "POST",
                url: swpSettings.service_url + "savesettings",
                data: JSON.stringify(c),
                contentType: "application/json",
                dataType: "json",
                success: function (a) {
                    alert(a)
                },
                failure: function (a) {
                    alert(a)
                }
            })

            if ("" === swpSettings.swp_username && "admin" === $("#txtLoginPass-phone").val()) {
                alert("Notice: The application must be closed and restarted for the URL change to take effect. ")
            }

            //localStorage.setItem("swp_OfflinePassword", $("#offline-pass-phone").val())
            localStorage.setItem("swp_OfflineUser", swpSettings.swp_username)
            // localStorage.setItem("swp_EnableAuth0", $("#auth0CloudSwitch").data("kendoSwitch").value())
        },
        _clearLocalStorage: function () {
            localStorage.clear()
        }
    }

    var module = {

        viewInit: (e) => {
            log.debug("Credentials View - View Init Hit")

            localStoragePhone = new localStoragePhone, localStoragePhone.run()
        },

        viewShow: (e) => {
            log.debug("Credentials View - View Show Hit")

            $("#credentialsView #pageTitleTop").text("Credentials")
            $("#credentialsView #timerColorIndicatorTouch").hide()

            //check if navigating from admin login or within application
            if (!swpSettings.swp_username || swpSettings.settingAdjust == true) {
                // $('.k-overlay').remove()
                $("#txtEmail-phone").val("")
                $("#credentialsView #backNavButton").attr("href", "#popover-login-phone")
                $("#credentialsView #backNavButton").show()
                $("<div class='k-overlay' id='greyOverlay'></div>").appendTo($("#credentialsView .km-footer"))
                $(".km-footer").hide()
                $(".km-tabstrip").hide()
            } else {
                $("#credentialsView #backNavButton").attr("href", "#views/settingsViews/settingsView.html")
                $("#credentialsView #backNavButton").show()
                $("#txtEmail-phone").val(swpSettings.swp_emailaddr)

                if ($(`#credentialsView #userFeedbackMessages`).data("kendoMobileCollapsible")) {
                    $(`#credentialsView #userFeedbackMessages`).data("kendoMobileCollapsible").collapse()
                    // $('.k-overlay').remove()
                }
            }

            setTimeout(()=> {
                $('.k-overlay').remove()
            }, 300)

            swp.debugView.addDebugListener()
            swpSettings.settingAdjust = false
        },

        fetchFirmCode: function () {
            log.debug("Credentials View - fetchFirmCode function Hit")
            console.log("Credentials View - fetchFirmCode function Hit")

            $("#invalidFirmCodeFetch").hide()
            swpSettings.swp_firmCode = $("#txtFirmCode-phone").val()

            if (swpSettings.swp_firmCode !== "") {

                swp.auth.setWSUrlFromFirmCode(swpSettings.swp_firmCode)
            } else {
                $("#invalidFirmCodeFetch").show()
            }
        },

        saveCredentials: function () {
            $("#liSettingsSaveAlert-phone").text("Please close application and restart to enable new credentials.")

            /* track service URL to see if it changed */
            let old_url = swpSettings.service_url,

            valueInput = document.getElementById("txtURL-phone").value
            localStorage.setItem("swp_wsURL", valueInput)

            if (!valueInput) {
                valueInput = "http://www.stdemo.smarttimeapps.com:8080";
                // valueInput ="http://www.st5pro.smarttimeapps.com:8089"
            }

            valueInput = swp.utilities.cleanupURL(valueInput)
            localStorage.setItem("swp_wsURL", valueInput);
            swpSettings.service_url = valueInput;

            valueInput = document.getElementById("txtEmail-phone").value;

            if (valueInput) {
                localStorage.setItem("swp_emailaddr", valueInput);
                swpSettings.swp_emailaddr = valueInput;
            }

            /* valueInput = document.getElementById("auth-source");
		        localStorage.setItem("swp_AuthSource", valueInput.value); */


            /* var timer_check = $("#timers-switch").data("kendoMobileSwitch").check();
            localStorage.setItem("swp_multitimers", timer_check.toString().trim()); 
            swpSettings.set_MultiTimers = timer_check.toString().trim(); */

            /* email and timer settings need to go up to the server */

            swpSettings.objText = new kendo.data.ObservableObject({
                Username: swpSettings.swp_username,
                EmailAddr: swpSettings.swp_emailaddr,
                MultiTimers: swpSettings.set_MultiTimers
            })

            $.ajax({
                type: "POST",
                url: swpSettings.service_url + "savesettings",
                data: JSON.stringify(swpSettings.objText),
                contentType: "application/json",
                dataType: "json",
                success: (data) => {

                },
                failure: (errMsg) => {
                    alert(errMsg)
                }
            });

            $("#imgSaveCredentials-phone").show()
            $("#imgSaveCredentials-phone").delay(1000).fadeOut('slow')

            if (swpSettings.swp_username === "" && $("#txtLoginPass").val() === "admin")
                alert("Notice: The application must be closed and restarted for the URL change to take effect. ")

            /* save offline password on the device only -- also save user so the pass can only be used wtih that user */
            localStorage.setItem("swp_OfflinePassword", $('#offline-pass').val())
            localStorage.setItem("swp_OfflineUser", swpSettings.swp_username)
            localStorage.setItem("swp_EnableAuth0", swpSettings.enableAuth0)
        },

        textAreaAdjust: (field) => {
            field.style.height = "1px"
            field.style.height = `${field.scrollHeight}px`
        }

    }

    return module
}()
