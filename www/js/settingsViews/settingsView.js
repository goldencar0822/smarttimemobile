//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.settingsView) { swp.settingsView = {} }

swp.settingsView = function () {
    "use strict";

    function setupSwitches() {
        $("#settingsTimeToggle-phone").kendoSwitch({
            checked: !swpSettings.set_DurationInUnits,
            messages: {
                checked: "on",
                unchecked: "off"
            },
            change: swp.settingsView.timeUnitToggle,
        })

        $("#auth0CloudSwitch").kendoSwitch({
            checked: swpSettings.enableAuth0 == "true",
            messages: {
                checked: "on",
                unchecked: "off"
            },
            change: swp.settingsView.auth0CloudToggle,
        })

        $("#offline-mode-phone").kendoSwitch({
            checked: swpSettings.offlineMode,
            messages: {
                checked: "on",
                unchecked: "off"
            }
        })
    }

    var module = {

        viewInit: (e) => {
            log.debug("Settings View - View Init Hit")
            kendo.bind($("settingsTimeToggle-phone"), swpSettings)
            setupSwitches()
        },

        viewShow: (e) => {
            log.debug("Settings View - View Show Hit")
            $("#settingsView #divStackButtons-phone").hide()

            if (swpSettings.set_MassActionsEnabled == true) {
                swp.utilities.cancelMassAction()
                $("#settingsView #massActionCancelTouch").hide()
            }

            $("#settingsView #timeEntryDescToggle").hide()

            $("#imgSaveCheck").hide()
            swp.debugView.addDebugListener()
        },

        auth0CloudToggle: function (e) {
            console.log(e.checked)
            swpSettings.enableAuth0 = e.checked
            localStorage.setItem("swp_EnableAuth0", swpSettings.enableAuth0)
        },

        timeUnitToggle: function (e) {
            console.log(e.checked)
            swpSettings.set_DurationInUnits = !e.checked
            localStorage.userDisplaySimpleTimeInScheduler = !swpSettings.set_DurationInUnits
        },

        OfflineModeToggle: function (e) {
            console.log(e.checked)
            swpSettings.offlineMode = e.checked
        },

    }

    return module
}()
