//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.privacyPolicyView) {
    swp.privacyPolicyView = {}
}

swp.privacyPolicyView = function () {
    "use strict";

    var module = {

        viewInit: (e) => {
            log.debug("Privacy Policy View - View Init Hit")

        },

        viewShow: (e) => {
            log.debug("Privacy Policy View - View Show Hit")

            swp.debugView.addDebugListener()
            $("#privacyPolicyView #pageTitleTop").text("Privacy Policy")
            $("#privacyPolicyView #backNavButton").attr("href", "#views/settingsViews/settingsView.html")
            $("#privacyPolicyView #backNavButton").show()
            // $("#privacyPolicyView .km-content").css("margin-bottom", $("#privacyPolicyView .km-footer").height() + 10)
            $("#privacyPolicyView #timerColorIndicatorTouch").hide()
            if ($(`#privacyPolicyView #userFeedbackMessages`).data("kendoMobileCollapsible")) {
                $(`#privacyPolicyView #userFeedbackMessages`).data("kendoMobileCollapsible").collapse()
                $('.k-overlay').remove()
            }
        },

    }

    return module
}()
