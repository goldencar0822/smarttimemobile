//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.lastPostedCMView) { swp.lastPostedCMView = {} }

swp.lastPostedCMView = function () {
    "use strict";

    let activeEntryPosition = swpSettings.activeEntryPosition,
        arrEntries = swpSettings.arrEntries,
        arrEntriesTimers = swpSettings.arrEntriesTimers;

    var module = {
        viewInit: function(e) {
            log.debug("Timers Last Posted CM View - View Init Hit")
        },

        viewShow: function(e) {
            log.debug("Timers Last Posted CM View - View Show Hit")

            $("#lastPostedCMView #pageTitleTop").text("Last Posted Numbers")
            $("#lastPostedCMView #backNavButton").removeAttr("href")
            $("#lastPostedCMView #backNavButton").attr("onclick", "swp.openViews.openTimerSettingsView()")
            // $("#lastPostedCMView #backNavButton").attr("href", "#views/settingsViews/timersSettings/timersSettingsView.html")
            $("#lastPostedCMView #backNavButton").show()
            $("#lastPostedCMView #divStackButtons-phone").hide()
            $("#lastPostedCMView #timerColorIndicatorTouch").hide()
            swpSettings.timerSettings.LoadRadio = 2
            module.updateCurrentSelection()
            swp.debugView.addDebugListener()
        },

        handleLastPostedClientMattersToggle: function (cb) {
            let num = cb.id.split("M")[1]

            if (cb.id === `LPCM${num}`) {
                $("#lastPostedCMView #list-last-posted-cm-phone li input").prop('checked', false)
                $(`#LPCM${num}`).prop('checked', true)
                swpSettings.timerSettings.LastPostedClientMatterNumbers = Number(num)
            }

            swp.timer.actions.updateTimerSettings()
            window.history.go(-1)
        },

        updateCurrentSelection: function () {
            let tempLPCM = swpSettings.timerSettings.LastPostedClientMatterNumbers
            $("#lastPostedCMView #list-last-posted-cm-phone li input").prop('checked', false)
            $(`#LPCM${tempLPCM}`).prop('checked', true)
        },
    }    

    return module

}()
