//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.timersSettingsView) { swp.timersSettingsView = {} }


swp.timersSettingsView = function () {
    "use strict";

    let activeEntryPosition = swpSettings.activeEntryPosition,
        arrEntries = swpSettings.arrEntries,
        arrEntriesTimers = swpSettings.arrEntriesTimers,
        sortOption, sortDir;

        
    var module = {
        viewInit: function(e) {
            log.debug("Timers Settings View - View Init Hit")

            module.setupDrowpdownLists()
        },

        viewShow: function(e) {
            log.debug("Timers Settings View - View Show Hit")

            $("#timersSettingsView #pageTitleTop").text("Timers")
            $("#timersSettingsView #backNavButton").removeAttr("href")
            $("#timersSettingsView #backNavButton").attr("onclick", "swp.openViews.openSettingsView()")
            $("#timersSettingsView #backNavButton").show()
            $("#timersSettingsView #divStackButtons-phone").hide()
            $("#timersSettingsView #timerColorIndicatorTouch").hide()
            setTimeout(module.timerSettingsDisplay(), 500)
            swp.debugView.addDebugListener()
        },

        handleOpenTimersToggle: function(cb) {
            if (cb.id === "blankRowsTimersToggle") {
                $("#blankRowsTimersToggle").prop('checked', true)
                $("#lastPostedCMTimersToggle").prop('checked', false)
                $("#presetsTimersToggle").prop('checked', false)
                $("#previousSessionTimersToggle").prop('checked', false)
                swpSettings.timerSettings.LoadRadio = 1
            } else if (cb.id === "lastPostedCMTimersToggle") {
                $("#blankRowsTimersToggle").prop('checked', false)
                $("#lastPostedCMTimersToggle").prop('checked', true)
                $("#presetsTimersToggle").prop('checked', false)
                $("#previousSessionTimersToggle").prop('checked', false)
                swpSettings.timerSettings.LoadRadio = 2
            } else if (cb.id === "presetsTimersToggle") {
                $("#blankRowsTimersToggle").prop('checked', false)
                $("#lastPostedCMTimersToggle").prop('checked', false)
                $("#presetsTimersToggle").prop('checked', true)
                $("#previousSessionTimersToggle").prop('checked', false)
                swpSettings.timerSettings.LoadRadio = 3
            } else if (cb.id === "previousSessionTimersToggle") {
                $("#blankRowsTimersToggle").prop('checked', false)
                $("#lastPostedCMTimersToggle").prop('checked', false)
                $("#presetsTimersToggle").prop('checked', false)
                $("#previousSessionTimersToggle").prop('checked', true)
                swpSettings.timerSettings.LoadRadio = 4
            }

            swp.timer.actions.updateTimerSettings()
        },

        setupDrowpdownLists: function() {

            $("#timerBlankRows").kendoButtonGroup({
                index: 0
            });

            $("#timerLastPostedClientMatters").kendoSlider({
                increaseButtonTitle: "Right",
                decreaseButtonTitle: "Left",
                min: -10,
                max: 10,
                smallStep: 2,
                largeStep: 1
            }).data("kendoSlider");

            $("#presetsDropdownList").kendoDropDownList({
                dataSource: ["Presets: 1", "Presets: 2", "Presets: 3"]
            });

        },

        timerSettingsDisplay: function () {
            $("#list-timers-settings-phone input").prop("checked", false)
            if (swpSettings.timerSettings.LoadRadio == 1) {
                $("#blankRowsTimersToggle").prop('checked', true)
            } else if (swpSettings.timerSettings.LoadRadio == 2) {
                $("#lastPostedCMTimersToggle").prop('checked', true)
            } else if (swpSettings.timerSettings.LoadRadio == 3) {
                $("#presetsTimersToggle").prop('checked', true)
            } else if (swpSettings.timerSettings.LoadRadio == 4) {
                $("#previousSessionTimersToggle").prop('checked', true)
            }

            $("#timersSettingsView #openBlankTimersView").text(`Blank Timers: (${swpSettings.timerSettings.BlankRows})`)
            $("#timersSettingsView #openLastPostedCMView").text(`Last Posted Client Matters: (${swpSettings.timerSettings.LastPostedClientMatterNumbers})`)
        },
    }

    return module

}()
