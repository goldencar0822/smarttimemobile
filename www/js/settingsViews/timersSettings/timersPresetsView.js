//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.timersPresetsView) { swp.timersPresetsView = {} }

swp.timersPresetsView = function () {
    "use strict";

    let activeEntryPosition = swpSettings.activeEntryPosition,
        arrEntries = swpSettings.arrEntries,
        arrEntriesTimers = swpSettings.arrEntriesTimers;

    var module = {
        viewInit: function(e) {
            log.debug("Timers Presets View - View Init Hit")
        },

        viewShow: function(e) {
            log.debug("Timers Presets View - View Show Hit")
            $("#timersPresetsView #pageTitleTop").text("Presets")
            $("#timersPresetsView #backNavButton").removeAttr("href")
            $("#timersPresetsView #backNavButton").attr("onclick", "swp.openViews.openTimerSettingsView()")
            // $("#timersPresetsView #backNavButton").attr("href", "#views/settingsViews/timersSettings/timersSettingsView.html")
            $("#timersPresetsView #backNavButton").show()
            $("#timersPresetsView #divStackButtons-phone").hide()
            $("#timersPresetsView #timerColorIndicatorTouch").hide()
            swpSettings.timerSettings.LoadRadio = 3
            module.getUserPresets()
            setTimeout(() => {
                module.updateCurrentSelection()
            }, 300)
            swp.debugView.addDebugListener()
        },

        getUserPresets: function () {
            var timerPresets_dataSource = new kendo.data.DataSource({
                transport: {
                    read: (options) => {
                        $.ajax({
                            url: `${swpSettings.service_url}/getpresets/${swpSettings.objTook.token}`,
                            dataType: "json",
                            success: function (data) {

                                let presetArr = []

                                for (let i = 0; i < data.length; i++) {
                                    let presetObj = {}
                                    presetObj.id = i
                                    presetObj.preset = data[i]
                                    presetArr.push(presetObj)
                                }

                                data = presetArr
                                swpSettings.timerPresetOptions = data
                                console.log("preset data", data)
                                options.success(data);
                            }
                        })
                    }
                },
                error: function (e) {
                    // Handle error.
                    alert("Status: " + e.status + "; Error message: " + e.errorThrown);
                }
            })

            $("#timersPresetsView #list-timers-presets-phone").kendoMobileListView({
                dataSource: timerPresets_dataSource,
                template: kendo.template($("#timerPresetsTemplate").html())
            })
        },

        handlePresetsToggle: function (cb) {
            let preset = cb.children[0].innerText
            let id = cb.children[1].id

            console.log("timer preset:" + preset)
            swpSettings.timerSettings.PresetOption = preset
            $(`#timersPresetsView #list-timers-presets-phone li input`).prop("checked", false)
            $(`#timersPresetsView #${id}`).prop("checked", true)
            
            swp.timer.actions.updateTimerSettings()
        },

        presetLoadButton: function () {
            if (swpSettings.loadTimerPreset == true) {
                setTimeout(() => {
                    location.href = "#views/timersView.html";
                    swp.timer.actions.loadTimerPreset()
                    swpSettings.loadTimerPreset = false
                }, 150)
            } else {
                location.href = "#views/settingsViews/timersSettings/timersSettingsView.html";
            }
        },

        updateCurrentSelection: function () {
            let tempPreset = swpSettings.timerPresetOptions.find(x => x.preset === swpSettings.timerSettings.PresetOption)
            let tempId = tempPreset.id

            $(`#timersPresetsView #list-timers-presets-phone li input`).prop("checked", false)
            $(`#timersPresetsView #${tempId}`).prop("checked", true)
        },
    }    
    

    return module

}()