//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.blankTimersView) { swp.blankTimersView = {} }

swp.blankTimersView = function () {
    "use strict";

    let activeEntryPosition = swpSettings.activeEntryPosition,
        arrEntries = swpSettings.arrEntries,
        arrEntriesTimers = swpSettings.arrEntriesTimers;

    var module = {

        viewInit: function (e) {
            log.debug("Timers Settings Blank Timers View - View Init Hit")
        },

        viewShow: function (e) {
            log.debug("Timers Settings Blank TImers View - View Show Hit")

            $("#blankTimersView #pageTitleTop").text("Blank Timers")
            $("#blankTimersView #backNavButton").removeAttr("href")
            $("#blankTimersView #backNavButton").attr("onclick", "swp.openViews.openTimerSettingsView()")
            // $("#blankTimersView #backNavButton").attr("href", "#views/settingsViews/timersSettings/timersSettingsView.html")
            $("#blankTimersView #backNavButton").show()
            $("#blankTimersView #divStackButtons-phone").hide()
            $("#blankTimersView #timerColorIndicatorTouch").hide()
            swpSettings.timerSettings.LoadRadio = 1
            module.updateCurrentSelection()
            swp.debugView.addDebugListener()
        },

        handleBlankTimersToggle: function (cb) {
            let num = cb.id.split("Timer")[1]

            if (cb.id === `BlankTimer${num}`) {
                $("#blankTimersView #list-blank-timers-phone li input").prop('checked', false)
                $(`#BlankTimer${num}`).prop('checked', true)
                swpSettings.timerSettings.BlankRows = Number(num)
            }
            
            swp.timer.actions.updateTimerSettings()
            window.history.go(-1)
        },

        updateCurrentSelection: function () {
            let tempLPCM = swpSettings.timerSettings.BlankRows
            $("#blankTimersView #list-blank-timers-phone li input").prop('checked', false)
            $(`#BlankTimer${tempLPCM}`).prop('checked', true)
        },
    }    
    

    return module

}();
