//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.debugLogView) { swp.debugLogView = {} }

swp.debugLogView = function () {
    "use strict";

    var module = {

        viewInit: (e) => {
            log.debug("Debug Log View - View Init Hit")
           
        },

        viewShow: (e) => {
            log.debug("Debug Log View - View Show Hit")
            module.addDebugListener()
            $("#debugLogView #backNavButton").attr("href", "#views/settingsViews/settingsView.html")
            $("#debugLogView #pageTitleTop").text("Debug")
            $("#debugLogView #timeEntryAddDefaultDiv").hide()
        },

        addDebugLogListener: () => {
            // if (!document.body.onclick) {
            //     document.body.onclick = (ev) => {
            //         console.log(ev.target.id)
            //         module.addToDebug(ev.target.id, ev.type, ev.target.className, ev.view.location.href)
            //         //still need to find correct way of deleting an item, may need to adjust how I am adding items to the listview.
            //         if (swpSettings.debugData.data().length > 29) {
            //             // $("#list-debug-phone").data("kendoMobileListView").remove(swpSettings.debugData.data()[19])
            //             let dataItem = swpSettings.debugData.at(29)
            //             swpSettings.debugData.remove(dataItem)
            //         }
            //     }
            // }
        },
        
        addToDebugLog: (log) => {
            swpSettings.debugLogData.add({
                data: {
                    timeStamp: moment().format('YYYY-MM-DDTHH:mm:ss:SSS'),
                    log: log,
                    indx: swpSettings.debugLogData.data().length,
                }
            })
        },

        clearDebugLogData: (e) => {
            e.preventDefault()
            swpSettings.debugLogData.data([])
        },

        debugLogTimeCheck: () => {
            // Your moment
            let mmt = moment()
            // // Your moment at midnight
            // let mmtMidnight = mmt.clone().startOf('day');
            // // Difference in minutes
            // let diffMinutes = mmt.diff(mmtMidnight, 'minutes');
            // let startTimeIsSameDay = moment(this.timerStart).isSame(mmtMidnight, "day");
            // let eventDateIsSameDay = moment(this.eventDate()).isSame(mmtMidnight, "day");

            let data = swpSettings.debugLogData.data()
            data.forEach(x => {
                // moment(x.timeStamp).isSame(mmtMidnight, "day") === true
                let duration = Math.abs(moment.duration(mmt.diff(x.timeStamp)).asHours())
            
                if (duration > swpSettings.set_DebugDurationLimit) {
                    let dataItem = swpSettings.debugLogData.at(x.indx)
                    swpSettings.debugLogData.remove(dataItem);
                }
            })
        },

        emailDebugLogData: (e) => {
            e.preventDefault()

            let a = [],
                dataArr = swpSettings.debugLogData.data();

            for (let b = 0; b < dataArr.length; b++) {
                let c = {
                    timeStamp: dataArr[b].data.timeStamp,
                    log: dataArr[b].data.log,
                }
                a.push(c);
            }
            a = JSON.stringify(a)
            window.open(`mailto:${swpSettings.swp_supportEmail}?subject=Smart Time Mobile Debug Logg for ${swpSettings.swp_username}&body=${a}`);
        },

        showDebugLogDisplay: (e) => {
            console.log("showDebugDisplay e" + e)
            // swp.openViews.openDebugView()
            location.href = "#popover-debug-log-phone"

        },
    }

    return module
}()
