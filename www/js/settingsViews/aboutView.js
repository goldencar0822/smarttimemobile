//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.aboutView) { swp.aboutView = {} }

swp.aboutView = function () {
    "use strict";

    // var Plugins = require("@capacitor/core")
    // const { Browser } = Plugins

    // var openURL = require("../browserPlugin.js")

    var module = {

        viewInit: (e) => {
            log.debug("About View - View Init Hit")

        },

        viewShow: (e) => {
            log.debug("About View - View Show Hit")

            swp.debugView.addDebugListener()
            $("#aboutView #pageTitleTop").text("About")
            $("#aboutView #backNavButton").attr("href", "#views/settingsViews/settingsView.html")
            $("#aboutView #backNavButton").show()
            $("#aboutView #timerColorIndicatorTouch").hide()
            $("#aboutView #aboutVersionNumber").text(swpSettings.version)
            $("#aboutView #timerColorIndicatorTouch").hide()
            if ($(`#aboutView #userFeedbackMessages`).data("kendoMobileCollapsible")) {
                $(`#aboutView #userFeedbackMessages`).data("kendoMobileCollapsible").collapse()
                $('.k-overlay').remove()
            }
        },

        launchKnowledgebase: function() {
            // window.open("https://desk.zoho.com/portal/smarttimeapps/kb/smart-time-users")
            // cordova.exec(null, null, "InAppBrowser", "open", ["https://desk.zoho.com/portal/smarttimeapps/kb/smart-time-users", "_system"]);
            // openURL("https://desk.zoho.com/portal/smarttimeapps/kb/smart-time-users")
            console.log("launchKnowledgebase function hit")
            swpPlugins.launchKnowledgebase()

        },

        launchSmartTimeApps: function() {
            // window.open("http://www.smarttimeapps.com")
            // cordova.exec(null, null, "InAppBrowser", "open", ["http://www.smarttimeapps.com", "_system"]);
            // openURL("http://www.smarttimeapps.com")
            swpPlugins.launchSmartTimeApps()
        },

    }

    return module
}()
