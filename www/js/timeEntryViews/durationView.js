//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.durationView) { swp.durationView = {} }
//this will keep track of the order of functions being fired


swp.durationView = function () {
    "use strict";
    let activeEntryPosition = swpSettings.activeEntryPosition,
        activeTimerPosition = swpSettings.activeTimerPosition,
        arrEntries = swpSettings.arrEntries,
        arrEntriesTimers = swpSettings.arrEntriesTimers,
        entryArray,
        entryPosition,
        increment,
        splitTime,
        num,
        hrs, mins, secs,
        lowToHigh,
        newTotal,
        interval;

    function clearInvalidDurationField() {
        $("#durationView #divDurationTotalTime").css("margin-top", "0.16em")
        $("#durationView #durationViewErrorMessageDisplay").hide()
        $("#divDurationTotal").removeClass("invalidDurationField")
        $("#divDurationTotalTime #hours").removeClass("invalidDurationField")
    }
    
    function durationUnitCheck() {
        swpSettings.set_DurationInUnits === true && entryArray[entryPosition] && (entryArray[entryPosition].timer === "" || entryArray[entryPosition].timer === null || (entryArray[entryPosition].timer !== "" && entryArray[entryPosition].color === "")) && swpSettings.bTimers === false
            ? ($("#durationView #divDurationTotal").show(),
                $("#durationView #divDurationTotalTime").hide(),
                $("#durationView #pageTitleTop").text("Units"),
                unitDurationValueCheck(),
                module.commaCheck())
            : ($("#durationView #divDurationTotalTime").show(),
                $("#durationView #divDurationTotal").hide(),
                entryArray[entryPosition] && entryArray[entryPosition].timer !== "" && entryArray[entryPosition].timer !== null && entryArray[entryPosition].color !== "" || swpSettings.bTimers === true 
                    ? ($("#durationView #pageTitleTop").text("HH : MM : SS"),
                        timeDurationValueCheck(),
                        $("#divDurationTotalTime #hours").css("background-color", "black"),
                        $("#durationView #minutes").focus(),
                        $("#divDurationTotalTime #minutes").css("background-color", "rgb(128, 128, 128)"),
                        $("#divDurationTotalTime #seconds").css("background-color", "black"),
                        $("#divDurationTotalTime #seconds").show(),
                        $("#divDurationTotalTime #minSecSeparator").show(),
                        $("#button-duration-dot").text(":").css("padding-top", ".45em"))
                    : ($("#durationView #pageTitleTop").text("Hours : Minutes"),
                        timeDurationValueCheck(),
                        $("#divDurationTotalTime #hours").css("background-color", "black"),
                        $("#durationView #minutes").focus(),
                        $("#divDurationTotalTime #minutes").css("background-color", "rgb(128, 128, 128)"),
                        $("#divDurationTotalTime #seconds").hide(),
                        $("#divDurationTotalTime #minSecSeparator").hide(),
                        $("#button-duration-dot").text(":").css("padding-top", ".45em")))
    }

    function timeDurationValueCheck() {

        swpSettings.bTimers === true
            ? (entryArray = swpSettings.arrEntriesTimers,
                entryPosition = swpSettings.activeTimerPosition)
            : (entryArray = swpSettings.arrEntries,
                entryPosition = swpSettings.activeEntryPosition)

        if (entryArray[entryPosition]) {
            if (entryArray[entryPosition].timer !== "" && entryArray[entryPosition].timer !== null && entryArray[entryPosition].color !== "") {
                splitTime = entryArray[entryPosition].timer.split(":")
                $("#divDurationTotalTime #hours").text(splitTime[0])
                $("#divDurationTotalTime #minutes").text(splitTime[1])
                $("#divDurationTotalTime #seconds").text(splitTime[2])
            } else if (entryArray[entryPosition].durationTimeDisplay) {
                splitTime = entryArray[entryPosition].durationTimeDisplay.split(":")
                $("#divDurationTotalTime #hours").text(splitTime[0])
                $("#divDurationTotalTime #minutes").text(splitTime[1])
                //$("#divDurationTotalTime #seconds").text(splitTime[2])
            } else {
                $("#divDurationTotalTime #hours").text("00")
                $("#divDurationTotalTime #minutes").text("00")
            }
        }
    }

    function unitDurationValueCheck() {
        arrEntries = swpSettings.arrEntries;
        activeEntryPosition = swpSettings.activeEntryPosition;

        (arrEntries[activeEntryPosition] && arrEntries[activeEntryPosition].duration && arrEntries[activeEntryPosition].duration != "0.00")
            ? swpSettings.decimalComma == true
                ? $("#divDurationTotal").text(arrEntries[activeEntryPosition].duration.replace(".", ","))
                : $("#divDurationTotal").text(arrEntries[activeEntryPosition].duration)
            : $("#divDurationTotal").text("")

    }

    var module = {

        viewShow: function (e) {
            log.debug("Duration View - View Show Hit")
            arrEntries = swpSettings.arrEntries
            arrEntriesTimers = swpSettings.arrEntriesTimers
            activeEntryPosition = swpSettings.activeEntryPosition
            activeTimerPosition = swpSettings.activeTimerPosition

            swpSettings.bTimers === true
                ? (entryArray = swpSettings.arrEntriesTimers,
                    entryPosition = swpSettings.activeTimerPosition)
                : (entryArray = swpSettings.arrEntries,
                    entryPosition = swpSettings.activeEntryPosition)

            durationUnitCheck()
            swp.debugView.addDebugListener()

            // if (swpSettings.decimalComma === true) {
            //     $("#button-duration-dot").text(",")
            // }

            $("#durationView #divStackButtons-phone").hide()
            $("#durationView #backNavButton").show()
            $("#durationView #backNavButton").attr("href", "#views/timeEntryViews/timeEntryView.html")
            $("#durationView #backNavButton").removeAttr("onclick")
            $("#durationView #timerColorIndicatorTouch").hide()
        },

        viewInit: function (e) {
            log.debug("Duration View - View Init Hit")
            module.setUpHold()
        },

        backClick: function () {
            let myStr = $("#divDurationTotal").text();

            clearInvalidDurationField()

            if (myStr != "") {
                myStr = myStr.slice(0, -1);
            };
            if (myStr.length === 0) {
                myStr = ""
            }

            $("#divDurationTotal").text(myStr);

            if ($("#divDurationTotalTime #hours").css("background-color") === "rgb(128, 128, 128)") {
                hrs = $("#divDurationTotalTime #hours").text().toString();

                if (hrs != "") {
                    hrs = "0" + hrs.slice(0, -1);
                };

                $("#divDurationTotalTime #hours").text(hrs);

            } else if ($("#divDurationTotalTime #minutes").css("background-color") === "rgb(128, 128, 128)") {
                mins = $("#divDurationTotalTime #minutes").text().toString();

                if (mins != "") {
                    mins = "0" + mins.slice(0, -1);
                };

                $("#divDurationTotalTime #minutes").text(mins);
            } else if ($("#divDurationTotalTime #seconds").css("background-color") === "rgb(128, 128, 128)") {
                secs = $("#divDurationTotalTime #seconds").text().toString();

                if (secs != "") {
                    secs = "0" + secs.slice(0, -1);
                };

                $("#divDurationTotalTime #seconds").text(secs);
            }
        },

        changeInputFocus: function () {

            swpSettings.bTimers === true
                ? (entryArray = swpSettings.arrEntriesTimers,
                    entryPosition = swpSettings.activeTimerPosition)
                : (entryArray = swpSettings.arrEntries,
                    entryPosition = swpSettings.activeEntryPosition)

            if ($("#divDurationTotalTime #hours").css("background-color") === "rgb(128, 128, 128)") {
                $("#divDurationTotalTime #hours").css("background-color", "black")
                $("#divDurationTotalTime #minutes").css("background-color", "rgb(128, 128, 128)")
                $("#divDurationTotalTime #minutes").focus()
            } else if (entryArray[entryPosition].timer !== "" && entryArray[entryPosition].timer !== null) {
                if ($("#divDurationTotalTime #minutes").css("background-color") === "rgb(128, 128, 128)") {
                    $("#divDurationTotalTime #minutes").css("background-color", "black")
                    $("#divDurationTotalTime #seconds").css("background-color", "rgb(128, 128, 128)")
                    $("#divDurationTotalTime #seconds").focus()
                } else if ($("#divDurationTotalTime #seconds").css("background-color") === "rgb(128, 128, 128)") {
                    $("#divDurationTotalTime #seconds").css("background-color", "black")
                    $("#divDurationTotalTime #hours").css("background-color", "rgb(128, 128, 128)")
                    $("#divDurationTotalTime #hours").focus()
                }
            } else if (entryArray[entryPosition].timer === "" || entryArray[entryPosition].timer === null) {
                if ($("#divDurationTotalTime #minutes").css("background-color") === "rgb(128, 128, 128)") {
                    $("#divDurationTotalTime #minutes").css("background-color", "black")
                    $("#divDurationTotalTime #hours").css("background-color", "rgb(128, 128, 128)")
                    $("#divDurationTotalTime #hours").focus()
                }
            }
        },

        clearClick: function () {
            clearInvalidDurationField()

            if (swpSettings.set_DurationInUnits === true && swpSettings.bTimers === false) {
                $("#divDurationTotal").text("");
            } else {
                if ($("#divDurationTotalTime #hours").css("background-color") === "rgb(128, 128, 128)") {
                    $("#divDurationTotalTime #hours").text("00");
                } else if ($("#divDurationTotalTime #minutes").css("background-color") === "rgb(128, 128, 128)") {
                    $("#divDurationTotalTime #minutes").text("00");
                } else {
                    $("#divDurationTotalTime #seconds").text("00");
                }
            }
        },

        commaCheck: function () {

            swpSettings.decimalComma == true
                ? $("#button-duration-dot").text(",").css("padding-top", ".4em")
                : $("#button-duration-dot").text(".").css("padding-top", ".4em")
        },

        convertTime: function (num) {
            num = Math.round(((num % 60) + Number.EPSILON) * 100) / 100
            num < 10
                ? num = "0" + num.toString()
                : num.toString();

            return num
        },

        focusInput: function (e) {
            $("#divDurationTotalTime p").css("backgroundColor", "black")
            e.style.backgroundColor = "rgb(128, 128, 128)"
        },

        setUpHold: function () {
            $("#button-add-arrow").kendoTouch({
                multiTouch: true,
                hold: (e) => {
                    console.log(e)
                    interval = setInterval(module.touchAdd, 100)
                },
                tap: () => {
                    console.log("touchEnd")
                    // module.touchAdd()
                    clearInterval(interval)
                }
            })

            $("#button-minus-arrow").kendoTouch({
                multiTouch: true,
                hold: (e) => {
                    console.log(e)
                    interval = setInterval(module.touchMinus, 100)
                },
                tap: () => {
                    console.log("touchEnd")
                    // module.touchMinus()
                    clearInterval(interval)
                }
            })
        },

        submitClick: function () {

            swpSettings.bTimers === true
                ? (entryArray = swpSettings.arrEntriesTimers,
                    entryPosition = swpSettings.activeTimerPosition)
                : (entryArray = swpSettings.arrEntries,
                    entryPosition = swpSettings.activeEntryPosition)

            let maxHrs = Number(swpSettings.set_MaxEntryHours),
                time, units;
            if (((swpSettings.set_DurationInUnits === true && entryArray[entryPosition].timer === "") || (swpSettings.set_DurationInUnits === true && entryArray[entryPosition].timer !== "" && entryArray[entryPosition].color === "")) && swpSettings.bTimers === false) {
                
                time = $("#divDurationTotal").text()
                
                if (swpSettings.decimalComma === true) {
                    time = time.toString().replace(",", ".");
                }

                hrs = parseInt(swp.utilities.parseNumValue(time))
                
                //add preceeding zero if 1-9
                mins = time.split('.')[1]
                
                mins
                ? mins.length < 2 
                    ? mins = mins.toString() + "0" 
                    : mins.toString()
                : mins = "00"
                // mins = swp.utilities.parseNumValue(time.split('.')[1])
                // mins < 10 ? mins = "0" + mins.toString() : mins.toString()
                
                units = hrs.toString() + '.' + mins
                if (hrs < maxHrs || (hrs === maxHrs && mins == "00")) {
                    arrEntries[activeEntryPosition].duration = units
                    $("#divDurationTotal").removeClass("invalidDurationField")
                    arrEntries[activeEntryPosition].roundEntry()
                    $("#fieldDurationTime-phone").val(entryArray[entryPosition].duration)
                    location.href = "#views/timeEntryViews/timeEntryView.html"
                } else {
                    $("#durationView #divDurationTotal").css("margin-top", "7px")
                    $("#divDurationTotal").addClass("invalidDurationField")
                    $("#durationView #durationViewErrorMessage").text(`Maximum hours per entry limited to ${swpSettings.set_MaxEntryHours} hours`)
                    $("#durationView #durationViewErrorMessageDisplay").show()
                }
            } else {

                hrs = parseFloat($("#divDurationTotalTime #hours").text())
                mins = parseFloat($("#divDurationTotalTime #minutes").text())

                if (entryArray[entryPosition].timer !== "" || swpSettings.bTimers === true) {
                    secs = parseFloat($("#divDurationTotalTime #seconds").text())
                    if (secs > 60) {
                        secs = module.convertTime(secs)
                        mins++
                    }
                    if (Number(secs) < 10) {
                        secs = "0" + secs
                    }

                    ////may want to remove this rounding up to nearest minute for timers if any seconds are present, depending on feedback from todd
                    //if (Number(secs) > 1) {
                    //    secs = "00"
                    //    mins += 1
                    //}
                } else {
                    secs = "00"
                }

                if (mins >= 60) {
                    mins = module.convertTime(mins)
                    hrs++
                } else if (mins < 10) {
                    mins = "0" + mins
                }

                if (hrs < 10) { hrs = "0" + hrs }
                //if (mins < 10) { mins = "0" + mins }
                //if (secs < 10) { secs = "0" + secs }

                time = hrs + ":" + mins

                //swp.utilities.roundTime(time)
                if (hrs < maxHrs || (hrs === maxHrs && mins == "00" && secs == "00")) {

                    entryArray[entryPosition].set('durationTimeDisplay', time);
                    mins = (mins / 60).toFixed(2).split(".")[1]
                    units = `${hrs}.${mins}`;
                    entryArray[entryPosition].set('duration', units);
                    entryArray[entryPosition].roundEntry()
                    $("#divDurationTotalTime #hours").removeClass("invalidDurationField")
                    $("#fieldDurationTime-phone").val(entryArray[entryPosition].durationTimeDisplay)
                    //if (swpSettings.bTimers == true && $("#divDurationTotal").text() != '') {
                    if (entryArray[entryPosition].timer !== "") {
                        time = time + ":" + secs
                        entryArray[entryPosition].set("timer", time)
                    }
                    location.href = "#views/timeEntryViews/timeEntryView.html"
                } else {
                    $("#durationView #divDurationTotalTime").css("margin-top", ".12em")
                    $("#divDurationTotalTime #hours").addClass("invalidDurationField")
                    $("#durationView #durationViewErrorMessage").text(`Maximum hours per entry limited to ${swpSettings.set_MaxEntryHours} hours`)
                    $("#durationView #durationViewErrorMessageDisplay").show()
                }
            };
        },

        touchDot: function (e) {
            clearInvalidDurationField()

            swpSettings.bTimers === true
                ? (entryArray = swpSettings.arrEntriesTimers,
                    entryPosition = swpSettings.activeTimerPosition)
                : (entryArray = swpSettings.arrEntries,
                    entryPosition = swpSettings.activeEntryPosition)

            if (((swpSettings.set_DurationInUnits === true && entryArray[entryPosition].timer === "") || (swpSettings.set_DurationInUnits === true && entryArray[entryPosition].timer !== "" && entryArray[entryPosition].color === "")) && swpSettings.bTimers === false) {
                /* only do it if there is not already a dot */
                if ($("#divDurationTotal").text().indexOf('.') === -1 && $("#divDurationTotal").text().indexOf(',') === -1) {
                    var hours = $("#divDurationTotal").text() + ".";

                    if (swpSettings.decimalComma === true) {
                        hours = hours.toString().replace(".", ",");
                    }

                    $("#divDurationTotal").text(hours);
                    $("#divDurationTotal-phone").text(hours);
                }
            } else {
                swp.durationView.changeInputFocus()
            }

            $(e.sender.element).css("color", "white");

            setTimeout(function () {
                $(e.sender.element).css("color", "black");
            }, 300);
        },

        touchAdd: function (e) {

            clearInvalidDurationField()

            // swpSettings.bTimers === true
            //     ? (entryArray = swpSettings.arrEntriesTimers,
            //         entryPosition = swpSettings.activeTimerPosition)
            //     : (entryArray = swpSettings.arrEntries,
            //         entryPosition = swpSettings.activeEntryPosition)

            // entryArray[0].matterIncrement !== null && entryArray[0].matterIncrement !== ""
            //     ? increment = entryArray[0].matterIncrement
            //     : increment = swpSettings.set_DefaultIncrement
            increment = .01

            if (((swpSettings.set_DurationInUnits === true && entryArray[entryPosition].timer === "") || (swpSettings.set_DurationInUnits === true && entryArray[entryPosition].timer !== "" && entryArray[entryPosition].color === "")) && swpSettings.bTimers === false) {
                newTotal = parseFloat($("#divDurationTotal").text())
                if (!newTotal) newTotal = 0
                newTotal = (newTotal + parseFloat(increment)).toFixed(2)
                if (newTotal < 24.00) {
                    newTotal.toString()
                    $("#divDurationTotal").text(newTotal)
                }
            } else {
                if ($("#divDurationTotalTime #hours").css("background-color") === "rgb(128, 128, 128)") {
                    hrs = parseFloat($("#divDurationTotalTime #hours").text())
                    if (hrs < 24) {
                        hrs += 1
                        hrs < 10
                            ? hrs = "0" + hrs.toString()
                            : hrs.toString()
                        $("#divDurationTotalTime #hours").text(hrs)
                    }
                } else if ($("#divDurationTotalTime #minutes").css("background-color") === "rgb(128, 128, 128)") {
                    // increment = parseFloat(increment) * 60
                    // if (increment < 1) { increment = 1 }
                    increment = 1
                    mins = parseFloat($("#divDurationTotalTime #minutes").text()) + increment
                    hrs = parseFloat($("#divDurationTotalTime #hours").text())
                    secs = parseFloat($("#divDurationTotalTime #seconds").text())

                    if (hrs < 24) {
                        if (mins >= 60) {
                            mins -= 60
                            hrs += 1

                            if (hrs == 24) {
                                mins = 0
                                $("#divDurationTotalTime #seconds").text("00")
                            }

                            hrs < 10
                                ? hrs = "0" + hrs.toString()
                                : hrs.toString()
                            $("#divDurationTotalTime #hours").text(hrs)
                        }

                        mins < 10
                            ? mins = "0" + Math.ceil(mins).toString()
                            : Math.ceil(mins).toString();
                        $("#divDurationTotalTime #minutes").text(mins)
                    }
                } else if ($("#divDurationTotalTime #seconds").css("background-color") === "rgb(128, 128, 128)") {
                    secs = parseFloat($("#divDurationTotalTime #seconds").text()) + 1
                    hrs = parseFloat($("#divDurationTotalTime #hours").text())
                    mins = parseFloat($("#divDurationTotalTime #minutes").text())

                    if (hrs < 24) {
                        if (secs >= 60) {
                            secs -= 60
                            mins += 1

                            if (mins === 60) {
                                hrs += 1
                                mins = 0
                                secs = 0
                            }

                            hrs < 10
                                ? hrs = "0" + hrs.toString()
                                : hrs.toString()

                            mins < 10
                                ? mins = "0" + mins.toString()
                                : mins.toString()

                            $("#divDurationTotalTime #hours").text(hrs)
                            $("#divDurationTotalTime #minutes").text(mins)
                        }
                        secs < 10
                            ? secs = "0" + secs.toString()
                            : secs.toString()
                        $("#divDurationTotalTime #seconds").text(secs)
                    }
                }
            }
        },

        touchMinus: function (e) {

            clearInvalidDurationField()

            // swpSettings.bTimers === true
            //     ? (entryArray = swpSettings.arrEntriesTimers,
            //         entryPosition = swpSettings.activeTimerPosition)
            //     : (entryArray = swpSettings.arrEntries,
            //         entryPosition = swpSettings.activeEntryPosition)

            // entryArray[0].matterIncrement !== null && entryArray[0].matterIncrement !== ""
            //     ? increment = entryArray[0].matterIncrement
            //     : increment = swpSettings.set_DefaultIncrement
            increment = .01

            if (((swpSettings.set_DurationInUnits === true && entryArray[entryPosition].timer === "") || (swpSettings.set_DurationInUnits === true && entryArray[entryPosition].timer !== "" && entryArray[entryPosition].color === "")) && swpSettings.bTimers === false) {
                newTotal = parseFloat($("#divDurationTotal").text()) - parseFloat(increment)

                if (newTotal < 0) {
                    newTotal = 0
                }

                $("#divDurationTotal").text(newTotal.toFixed(2))

            } else if ($("#divDurationTotalTime #hours").css("background-color") === "rgb(128, 128, 128)") {
                hrs = parseFloat($("#divDurationTotalTime #hours").text()) - 1
                if (hrs < 0) {
                    hrs = 0;
                };
                hrs < 10
                    ? hrs = "0" + hrs.toString()
                    : hrs.toString();
                $("#divDurationTotalTime #hours").text(hrs)
            } else if ($("#divDurationTotalTime #minutes").css("background-color") === "rgb(128, 128, 128)") {
                // increment = parseFloat(increment) * 60
                // if (increment < 1) { increment = 1 }
                increment = 1
                mins = parseFloat($("#divDurationTotalTime #minutes").text()) - increment;
                if (mins < 0) {
                    hrs = parseFloat($("#divDurationTotalTime #hours").text())
                    hrs > 0
                        ? (hrs -= 1,
                            hrs < 10
                                ? hrs = "0" + hrs.toString()
                                : hrs.toString(),
                            $("#divDurationTotalTime #hours").text(hrs),
                            mins += 60)
                        : mins = 0
                }
                mins < 10
                    ? mins = "0" + Math.ceil(mins).toString()
                    : Math.ceil(mins).toString();
                $("#divDurationTotalTime #minutes").text(mins)
            } else if ($("#divDurationTotalTime #seconds").css("background-color") === "rgb(128, 128, 128)") {
                secs = parseFloat($("#divDurationTotalTime #seconds").text()) - 1
                if (secs < 0) {
                    hrs = parseFloat($("#divDurationTotalTime #hours").text())
                    mins = parseFloat($("#divDurationTotalTime #minutes").text())

                    mins > 0
                        ? (mins -= 1,
                            secs += 60)
                        : hrs > 0
                            ? (hrs -= 1,
                                mins += 59,
                                secs += 60)
                            : secs = 0

                    hrs < 10
                        ? hrs = "0" + hrs.toString()
                        : hrs.toString()
                    $("#divDurationTotalTime #hours").text(hrs)

                    mins < 10
                        ? mins = "0" + mins.toString()
                        : mins.toString()
                    $("#divDurationTotalTime #minutes").text(mins)
                }

                secs < 10
                    ? secs = "0" + secs.toString()
                    : secs.toString()
                $("#divDurationTotalTime #seconds").text(secs)
            }
        },

        touchNum: function (e) {
            clearInvalidDurationField()
            num = e.target.text()
            newTotal = $("#divDurationTotal").text()

            if (((swpSettings.set_DurationInUnits === true && entryArray[entryPosition].timer === "") || (swpSettings.set_DurationInUnits === true && entryArray[entryPosition].timer !== "" && entryArray[entryPosition].color === "")) && swpSettings.bTimers === false) {

                newTotal = newTotal + num

                swpSettings.decimalComma == true
                    ? (mins = newTotal.split(",")[1],
                        hrs = newTotal.split(",")[0])
                    : (mins = newTotal.split(".")[1],
                        hrs = newTotal.split(".")[0])
                

                //this checks if the length of each entry is over 2 units only if that timeplace exists
                if (!!mins && mins.length > 2) return
                if (!!hrs && hrs.length > 2) return

                $("#divDurationTotal").text(newTotal);
                $("#divDurationTotal-phone").text(newTotal);

            } else if ($("#divDurationTotalTime #hours").css("background-color") === "rgb(128, 128, 128)") {
                hrs = $("#divDurationTotalTime #hours").text() + num;

                if (hrs.length > 2) {
                    hrs = hrs.slice(hrs.length - 2, hrs.length)
                };

                $("#divDurationTotalTime #hours").text(hrs);

            } else if ($("#divDurationTotalTime #minutes").css("background-color") === "rgb(128, 128, 128)") {
                mins = $("#divDurationTotalTime #minutes").text() + num;

                if (mins.length > 2) {
                    mins = mins.slice(mins.length - 2, mins.length)
                };

                $("#divDurationTotalTime #minutes").text(mins);

            } else if ($("#divDurationTotalTime #seconds").css("background-color") === "rgb(128, 128, 128)") {
                secs = $("#divDurationTotalTime #seconds").text() + num;

                if (secs.length > 2) {
                    secs = secs.slice(secs.length - 2, secs.length)
                };

                $("#divDurationTotalTime #seconds").text(secs);
            }
        },
    }

    return module

}()
