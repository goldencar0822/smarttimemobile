//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.custom2Lookup) { swp.custom2Lookup = {} }

swp.custom2Lookup = function () {
    "use strict";
    let c2 = swpSettings.custom2;

    var module = {

        viewShow: function (e) {
            log.debug("Custom 2 View - View Show Hit")

            swp.debugView.addDebugListener()
            $("#custom2LookupView #pageTitleTop").text(c2.set_Custom2)
            $("#custom2LookupView #backNavButton").removeAttr("onclick")
            $("#custom2LookupView #backNavButton").attr("href", "#views/timeEntryViews/timeEntryView.html")
            $("#custom2LookupView #backNavButton").show()
            $("#custom2LookupView #timerColorIndicatorTouch").hide()
            $(".km-scroll-container").css("transform", "translate3d(0px, 0px, 0px)")
        },

        viewInit: function (e) {
            log.debug("Custom 2 View - View Init Hit")

            module.custom2Lookup_phone()
        },

        custom2Lookup_phone: function () {
            log.debug("custom2Lookup_phone function hit")

            $("#custom2-listview-phone").kendoMobileListView({
                dataSource: swp.dataSources.custom2_dataSource,
                template: $("#matter-listview-template").text(),
                filterable: {
                    field: "text",
                    operator: "contains"
                },
                click: (a) => {
                    let arrEntries = swpSettings.arrEntries,
                        activeEntryPosition = swpSettings.activeEntryPosition,
                        arrEntriesTimers = swpSettings.arrEntriesTimers,
                        activeTimerPosition = swpSettings.activeTimerPosition;

                    document.activeElement.blur()
                    $("input").blur()
                    $('[type="search"]').val("")
                    $("#custom2-listview-phone").data("kendoMobileListView").dataSource.filter({})

                    swpSettings.bTimers === true
                        ? arrEntriesTimers[activeTimerPosition].custom2 = a.dataItem.text
                        : arrEntries[activeEntryPosition].custom2 = a.dataItem.text

                    location.href = "#views/timeEntryViews/timeEntryView.html"
                    swp.utilities.refreshCustomMenus()

                    if (c2.set_Custom2Sticky == true && c2.set_Custom2Search == true) {
                        c2.set_Custom2Default = a.dataItem.text
                    }

                    if (typeof c2.CopyCustom2 !== "undefined" && (c2.CopyCustom2 == "true" ||  c2.CopyCustom2 == "True")) {
                        swp.utilities.copyMenuToNarrative("#custom2-listview-phone", a.dataItem.text)
                    }
                }
            });
            swpSettings.offlineMode === !0 && $('[type="search"]').on("keyup", () => {
                swp.utilities.addItemToCustomMenu(c2.custom2_dataSource, $(this).val())
            });
            //var a = $("body").height() - 103;
            //$("#divCustom2Scroll-Phone").height(a)
        },
    }

    return module

}()