//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.clientLookup) { swp.clientLookup = {} }

swp.clientLookup = function () {
    "use strict";
    let arrEntries = swpSettings.arrEntries,
        arrEntriesTimers = swpSettings.arrEntriesTimers,
        activeEntryPosition = swpSettings.activeEntryPosition,
        activeTimerPosition = swpSettings.activeTimerPosition,
        client,
        entryArray,
        entryPosition;

    var module = {

        viewInit: function (e) {
            log.debug("Client Lookup View - View Init Hit")

            module.clientLookup_phone()
        },

        viewShow: function (e) {
            log.debug("Client Lookup View - View Show Hit")
            try {
                $('[type="search"]').val("")
                $("#client-listview-phone").data("kendoMobileListView").dataSource.filter({})
                $("#client-listview-phone").data("kendoMobileListView").refresh()
            } catch (a) {
                alert(a)
            }

            swp.debugView.addDebugListener()
            $("#userFeedbackMessages .km-collapsible-content").hide()
            $("#clientLookupView #pageTitleTop").text(swpSettings.clientLabel)
            $("#clientLookupView #backNavButton").removeAttr("href")
            $("#clientLookupView #backNavButton").attr("onclick", "window.history.go(-1)")
            $("#clientLookupView #backNavButton").show()
            $("#clientLookupView #timerColorIndicatorTouch").hide()
            $(".km-scroll-container").css("transform", "translate3d(0px, 0px, 0px)")
        },

        checkForMatter: function () {
            if ($("#clientViewMatterName").text() === null || $("#clientViewMatterName").text() === "") $("#clientViewClientName").css("margin-top", "10px")
        },

        clientLookup_phone: function () {
            log.debug("setting up client listview", swpSettings.service_url);
            //data source for client view
            // var clientDataSource = new kendo.data.DataSource({
            //     offlineStorage: {
            //         getItem: function () {
            //             return JSON.parse(localStorage.getItem("swp-clients"))
            //         },
            //         setItem: function (a) { }
            //     },
            //     transport: {
            //         read: {
            //             url: swpSettings.service_url + "clients/" + swpSettings.objTook.token + "/",
            //             dataType: "json"
            //         }
            //     },
            //     serverPaging: !1,
            //     serverFiltering: !swpSettings.offlineMode
            // })

            swp.dataSources.clientDataSource.options.transport.read.url = swpSettings.service_url + "clients/" + swpSettings.objTook.token + "/"
            swp.dataSources.clientDataSource.online(!swpSettings.offlineMode)
            $("#client-listview-phone").kendoMobileListView({
                dataBound: module.onDataBound,
                dataSource: swp.dataSources.clientDataSource,
                template: $("#tablet-mru-template").text(),
                filterable: {
                    field: "ClientName",
                    operator: "contains"
                },
                click: (ev) => {
                    swpSettings.bTimers === true
                        ? (entryArray = swpSettings.arrEntriesTimers,
                            entryPosition = swpSettings.activeTimerPosition)
                        : (entryArray = swpSettings.arrEntries,
                            entryPosition = swpSettings.activeEntryPosition)

                    $('[type="search"]').val().substring(0, 1) === "/" ?
                        $.ajax({
                            type: "GET",
                            url: service_url + "shortcut/" + ev.dataItem.ClientName + "/" + swpSettings.objTook.token,
                            contentType: "application/json",
                            dataType: "json",
                            success: (e) => {
                                entryArray[entryPosition].client = e.ClientName
                                entryArray[entryPosition].matter = e.MatterName

                                swpSettings.searchParams.searchEntriesClient = e.ClientName
                                swpSettings.searchParams.searchEntriesMatter = e.MatterName
                            },
                            failure: (err) => {
                                alert(err)
                            }
                        })
                        : (swpSettings.searchParams.searchAllEntries === true
                            ? (swpSettings.searchParams.searchEntriesClient = ev.dataItem.ClientName,
                                swpSettings.searchParams.searchEntriesMatter = ev.dataItem.MatterName,
                                $("#searchView #searchClient").val(swpSettings.searchParams.searchEntriesClient),
                                $("#searchView #searchMatter").val(swpSettings.searchParams.searchEntriesMatter))
                            : (entryArray[entryPosition].client = ev.dataItem.ClientName,
                                entryArray[entryPosition].matter = ev.dataItem.MatterName,
                                $("#fieldClient_phone").siblings(".invalid-icon").hide(),
                                $("#fieldMatter_phone").siblings(".invalid-icon").hide(),
                                swp.timeentry.actions.clearCustomMenuValues(),
                                swp.utilities.refreshMatters(),
                                swp.utilities.refreshBillingGuidelines(),
                                setTimeout(() => { 
                                    swp.utilities.checkSingleMatter(),
                                    // swp.utilities.refreshCustomMenus(),
                                    swp.utilities.refreshBillingGuidelines()
                                }, 150),
                                swpSettings.offlineMode === true && (
                                    entryArray[entryPosition].plan1 = ev.dataItem.Plan1,
                                    entryArray[entryPosition].plan2 = ev.dataItem.Plan2,
                                    entryArray[entryPosition].plan3 = ev.dataItem.Plan3,
                                    entryArray[entryPosition].plan4 = ev.dataItem.Plan4)),
                            window.history.go(-1))

                    setTimeout(() => {
                        swp.entry.checkWorkingAtty(entryArray[entryPosition], ev, entryArray[entryPosition], entryArray[entryPosition].status.toLowerCase())
                        swp.utilities.refreshCustomMenus()
                    }, 350)
                    
                    // swp.entry.checkWorkingAtty()
                    document.activeElement.blur()
                    $("input").blur()
                }
            })

            swpSettings.offlineMode === !0 && $('[type="search"]').on("keyup", () => {
                var c, d, b = $(this).val(),
                    e = swp.dataSources.clientDataSource.data();
                for (d = e.length - 1; d >= 0; d--) {
                    c = e[d];
                    c.MatterName === "";
                    swp.dataSources.clientDataSource.remove(c);
                    swp.dataSources.clientDataSource.insert(0, {
                        ClientName: b,
                        MatterName: ""
                    })

                }
            })

            //var b = $("body").height() - 103;
            //$("#divClientScroll-Phone").height(b)
        },

        onDataBound: function () {
            if ($("#clientViewMatterName").text() === "") {
                $(".cvClientName").css("margin-top", "0px")
                $("#clientLookupView #client-listview-phone li").css("height", "45px")
            }
        },

    }

    return module

}()
