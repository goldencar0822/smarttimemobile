//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.matterLookup) { swp.matterLookup = {} }

swp.matterLookup = function () {
    "use strict";
    let activeEntryPosition = swpSettings.activeEntryPosition,
        arrEntries = swpSettings.arrEntries,        
        arrEntriesTimers = swpSettings.arrEntriesTimers;

    var module = {

        viewInit: function (e) {
            log.debug("Matter Lookup View - View Init Hit")

            module.matterLookup_phone()
        },

        viewShow: function (e) {
            log.debug("Matter Lookup View - View Show Hit")
            swp.utilities.refreshMatters()
            swp.debugView.addDebugListener()
            $("#userFeedbackMessages .km-collapsible-content").hide()
            $("#matterLookupView #pageTitleTop").text(swpSettings.matterLabel)
            $("#matterLookupView #backNavButton").removeAttr("href")
            $("#matterLookupView #backNavButton").attr("onclick", "window.history.go(-1)")
            $("#matterLookupView #backNavButton").show()
            $("#matterLookupView #timerColorIndicatorTouch").hide()
            $(".km-scroll-container").css("transform", "translate3d(0px, 0px, 0px)")
        },

        matterLookup_phone: function () {

            $("#matter-listview-phone").kendoMobileListView({
                dataSource: swp.dataSources.matterDataSource,
                template: $("#matter-listview-template").text(),
                filterable: {
                    field: "text",
                    operator: "contains"
                },
                click: function (ev) {
                    let entryArray, entryPosition;

                    swpSettings.bTimers === true
                        ? (entryArray = swpSettings.arrEntriesTimers,
                            entryPosition = swpSettings.activeTimerPosition)
                        : (entryArray = swpSettings.arrEntries,                  
                            entryPosition = swpSettings.activeEntryPosition)

                    swpSettings.searchParams.searchAllEntries === true
                        ? (swpSettings.searchParams.searchEntriesMatter = ev.dataItem.text,
                            $("#searchView #searchMatter").val(swpSettings.searchParams.searchEntriesMatter))
                        : (entryArray[entryPosition].set("matter", ev.dataItem.text),
                            "" === entryArray[entryPosition].client && (entryArray[entryPosition].set("client", ev.dataItem.client),
                            swpSettings.searchParams.searchEntriesClient = ev.dataItem.client,
                            $("#fieldClient_phone").siblings(".invalid-icon").hide()),
                            log.debug("Calling RefreshCustomMenus() from matter select"))
                            // swp.utilities.refreshCustomMenus())

                    swp.utilities.refreshBillingGuidelines()
                    $("#matter-listview-phone").data("kendoMobileListView").dataSource.filter({})
                    $('[type="search"]').val("")
                    //location.href = "#views/timeEntryViews/timeEntryView.html",
                    window.history.go(-1)
                    document.activeElement.blur()
                    $("input").blur()

                    setTimeout(() => {
                        swp.entry.checkWorkingAtty(entryArray[entryPosition], ev, entryArray[entryPosition], entryArray[entryPosition].status.toLowerCase())
                    }, 300)
//potential place to fix activity field update/clear
                    swp.timeentry.actions.clearCustomMenuValues()
                    swp.utilities.refreshCustomMenus()
                    // swp.utilities.checkEmptyCustom()
                }
            })

            swpSettings.offlineMode === true && $('[type="search"]').on("keyup", () => {
                var b, c, a = $(this).val(),
                    d = matterDataSource.data();
                for (c = d.length - 1; c >= 0; c--) b = d[c], b.flag === !0 && matterDataSource.remove(b);
                swp.matterLookup.matterDataSource.insert(0, {
                    text: a,
                    flag: !0
                })
            })
            //var a = $("body").height() - 103;
            //$("#divMatterScroll-Phone").height(a)
            //$("#matter-listview-phone").data("kendoMobileListView").refresh()    
        },

    }

    return module
}()
