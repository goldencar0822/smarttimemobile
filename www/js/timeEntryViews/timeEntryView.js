//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.timeEntryView) { swp.timeEntryView = {} }

swp.timeEntryView = function () {
    "use strict";
    let entryArray, entryPosition,
        arrEntries = swpSettings.arrEntries,
        arrEntriesTimers = swpSettings.arrEntriesTimers,
        activeEntryPosition = swpSettings.activeEntryPosition,
        activeTimerPosition = swpSettings.activeTimerPosition,
        today = new Date(),
        footerHeight;


        function updateActiveItem() {
            swpSettings.bTimers === true ?
                (entryArray = swpSettings.arrEntriesTimers,
                    entryPosition = swpSettings.activeTimerPosition) :
                (entryArray = swpSettings.arrEntries,
                    entryPosition = swpSettings.activeEntryPosition)
        }

    var module = {

        viewInit: function (e) {
            log.debug(`Time Entry View - View Init Hit. Time Entry Event: ${e}`)

            // Setup Main Content - List View
            $("#list-timeform-phone").kendoMobileListView()

            $("#list-timeform-phone").kendoTouch({
                enableSwipe: true,
                swipe: (a) => {
                    console.log("User swiped the element");
                    if (swpSettings.set_narFullScreen === true) return
                    swpSettings.bTimers === true
                        ? "left" === a.direction && (location.href = "#views/timersView.html")
                        : "left" === a.direction
                            ? parseInt(swpSettings.activeEntryPosition) < swpSettings.arrEntries.length - 1 &&
                            (swpSettings.activeEntryPosition += 1,
                                swp.stackView.bindEntry(swpSettings.arrEntries[swpSettings.activeEntryPosition].id),
                                swp.stackView.loadTimeForm())
                            : parseInt(swpSettings.activeEntryPosition) > 0 &&
                            (swpSettings.activeEntryPosition -= 1,
                                swp.stackView.bindEntry(swpSettings.arrEntries[swpSettings.activeEntryPosition].id),
                                swp.stackView.loadTimeForm())
                }
            })

            // Setup date Picker
            $("#timeEntryDateInput").kendoDatePicker({
                dateInput: true,
                culture: swpSettings.set_Culture,
                value: swpSettings.shortDate,
                min: moment(swpSettings.set_OldestValidDate).format(),
                max: moment(new Date()).add(swpSettings.set_futureDateNonBill, "days").format(),
                month: {
                    // template for dates in month view
                    content: '# if (swp.timeEntryView.isInArray(data.date, data.dates)) { #' +
                        '<div class="birthday"></div>' +
                        '# } #' +
                        '#= data.value #',
                    weekNumber: '<a class="italic">#= data.weekNumber #</a>'
                },
                change: function () {

                    swpSettings.bTimers === true
                        ? (entryArray = swpSettings.arrEntriesTimers,
                            entryPosition = swpSettings.activeTimerPosition)
                        : (entryArray = swpSettings.arrEntries,
                            entryPosition = swpSettings.activeEntryPosition)

                    let value = this.value()
                    entryArray[entryPosition].eventDT = moment(value).format()
                    entryArray[entryPosition].sortDate = moment(value)
                    entryArray[entryPosition].status = "New" // may want to set status to modified here depending on logic on the backend.
                    swpSettings.defaultDate = moment(value)._d

                    // need to add logic here to not clear swpSettings.defaultDate, although that seems to be linked to the entry sortDate change event on entry.js(1231)
                },
                open: (e) => {
                    $("<div class='k-overlay' id='greyOverlay'></div>").appendTo($(document.body))
                    //bind to the navigation of the calendar
                    e.sender.dateView.calendar.bind("navigate", () => {
                        setTimeout(() => {
                            // remove k-state-focused class with jQuery
                            $("td.k-state-focused").removeClass("k-state-focused");
                        }, 1);
                    })
                    $('#fieldNarrative-phone').attr('disabled', 'true')
                },
                close: (e) => {
                    $('#greyOverlay').remove()
                    setTimeout(() => {
                        $('#fieldNarrative-phone').removeAttr('disabled')
                    }, 500)
                },
            })

            let dateField = document.querySelector("#timeEntryView #timeEntryDatePicker-phone-row .phoneTimeInput")
            dateField.addEventListener("click", swp.timeEntryView.openDatePicker)

            window.addEventListener('keyboardWillShow', (info) => {
                console.log("keyboardWillShow Event Triggered", info)
                $("#timeEntryView #divStackButtons-phone").css("bottom", swpSettings.set_keyboardHeight)
                console.log("keyboardWillShow" + info)
                swpSettings.set_keyboardHeight = info.keyboardHeight
                footerHeight = $("#timeEntryView .km-footer").height() + swpSettings.set_keyboardHeight
                $("#timeEntryView #divTimeEntryScroll-Phone").css("margin-bottom", footerHeight)
                $("#timeEntryView #divStackButtons-phone").css("bottom", swpSettings.set_keyboardHeight)
            })

            window.addEventListener('keyboardWillHide', (info) => {
                console.log("keyboardWillhide Event Triggered", info)
                $("#timeEntryView #tabstrip-phone").show()

                swpSettings.set_narFullScreen === true
                    ? ($("#timeEntryView #fieldNarrative-phone").css("height", "36em"),
                        $("#timeEntryView #liNarrative-phone").css("height", "42em"))
                    : ($("#timeEntryView #fieldNarrative-phone").css("height", "18em"),
                        $("#timeEntryView #liNarrative-phone").css("height", "19em"))

                $("#timeEntryView #divStackButtons-phone").css("bottom", "0px")
                footerHeight = $("#timeEntryView .km-footer").height()
                $("#timeEntryView #divTimeEntryScroll-Phone").css("margin-bottom", footerHeight)
            })

            /* setup max narrative length */
            $('#fieldNarrative-phone').attr("maxLength", swpSettings.set_MaxNarrative)

            /* setup error message for all text areas */
            $('#fieldNarrative-phone').bind('input propertychange', (e) => {
                $("#narrativeCharCount").text(`${e.currentTarget.value.length} of ${swpSettings.set_MaxNarrative}`)
                e.currentTarget.value.length >= swpSettings.set_MaxNarrative
                    ? $("#narrativeCharCount").css("font-weight", "700")
                    : $("#narrativeCharCount").css("font-weight", "400")

                if (e.target.value.slice(0, -1) === entryArray[entryPosition].description)
                    e.target.value = e.target.value.slice(-1)
            })

            module.narrativeHeightCheck()
        },

        viewShow: function (e) {
            log.debug("Time Entry View - View Show Hit.")
            
            if (swpSettings.set_narFullScreen == true) {
                module.narClose_Phone()
                $("#timeEntryView #timeEntryDescToggle").hide()
            }

            if (swpSettings.set_MassActionsEnabled == true) {
                swp.utilities.cancelMassAction()
                $("#timeEntryView #massActionCancelTouch").hide()
            }

            swp.timeentry.actions.clearValidation()

            //View show does not get fired on back click from the client/matter/custom/duration views
            swpSettings.bTimers === true
                ? (entryArray = swpSettings.arrEntriesTimers,
                    entryPosition = swpSettings.activeTimerPosition)
                : (entryArray = swpSettings.arrEntries,
                    entryPosition = swpSettings.activeEntryPosition)

            // reformatting items in the header, may be able to template this but unsure how.
            setTimeout(() => {
                $("#timeEntryView #pageTitleTop").text("Time Entry")
                $("#timeEntryView #timeEntryAddDefaultDiv").show()

                swpSettings.bTimers === true
                    ? ($(`#timeEntryView #backNavButton`).hide(),
                        $(`#timeEntryView #timerColorIndicatorTouch`).show())
                    : ($(`#timeEntryView #backNavButton`).show(),
                        $("#timeEntryView #backNavButton").attr("href", "#views/stackView.html"),
                        $("#timeEntryView #backNavButton").removeAttr("onclick"),
                        $(`#timeEntryView #timerColorIndicatorTouch`).hide())

                swpSettings.searchParams.searchAllEntries = false
                swp.stackView.loadTimeForm()
                // module.timeSelectorFlag()
                module.setTimeEntryLabels()
                swp.setup.hideEmptyCustomFields()

                swpSettings.bTimers === true
                    ? (entryArray = swpSettings.arrEntriesTimers,
                        entryPosition = swpSettings.activeTimerPosition)
                    : (entryArray = swpSettings.arrEntries,
                        entryPosition = swpSettings.activeEntryPosition)

                if ($(`#timeEntryView #userFeedbackMessages`).data("kendoMobileCollapsible")) {
                    if (entryArray[entryPosition] && entryArray[entryPosition].bValidAtty === true) {
                        $(`#timeEntryView #userFeedbackMessages`).data("kendoMobileCollapsible").collapse()
                        $('.k-overlay').remove()
                    }
                }

                // if (typeof $("#list-timeform-phone").data("kendoMobileListView") === "undefined") {
                //     /* kind of resolves the issue but sometimes you have to wait a half sec --ghetto but working*/
                //     e.view.element.find("#list-timeform-phone").data("kendoMobileListView").refresh();
                //     setTimeout(function () { e.view.element.find("#list-timeform-phone").data("kendoMobileListView").refresh(); }, 500);
                //     setTimeout(function () { e.view.element.find("#list-timeform-phone").data("kendoMobileListView").refresh(); }, 1000);
                //     setTimeout(function () { e.view.element.find("#list-timeform-phone").data("kendoMobileListView").refresh(); }, 1500);
                // }

                //fix for release not displaying on new entry from empty calendar day selection
                if ($("#timeEntryView #button-ReleaseUnrelease-phone").text() === "") {
                    $("#timeEntryView #button-ReleaseUnrelease-phone span").text(entryArray[0].releaseButtonCaption())
                }
                //$("#timeEntryView #button-ReleaseUnrelease-phone").text(swpSettings.arrEntries[0].releaseButtonCaption())
                module.narrativeHeightCheck()
                $("#timeEntryView #divTimeEntryScroll-Phone").css("margin-bottom", $("#timeEntryView .km-footer").height())
                
                swp.debugView.addDebugListener()
                //prevent carriage return and emojis in textarea 
                $('#timeEntryView #fieldNarrative-phone').keypress((e) => {
                    // Check the keyCode and if the user pressed Enter (code = 13) disable it
                    if (e.keyCode == 13) {
                        e.preventDefault()
                    }

                    console.log("removing emoji")
                    let newValue = swp.timeEntryView.removeEmojis(`${$('#timeEntryView #fieldNarrative-phone')[0].value}`)
                    $('#timeEntryView #fieldNarrative-phone')[0].value = newValue
                    entryArray[entryPosition].narrative = newValue
                })

                $('#timeEntryView #fieldNarrative-phone').keyup((e) => {
                    let newValue = swp.timeEntryView.removeEmojis($('#timeEntryView #fieldNarrative-phone')[0].value)
                    $('#timeEntryView #fieldNarrative-phone')[0].value = newValue
                    entryArray[entryPosition].narrative = newValue
                })

                $('#timeEntryView #fieldNarrative-phone').focus((e) => {
                    // toggle into narrative full screen if user focused the narrative
                    if (window.innerWidth < 500) {
                        swp.timeEntryView.narOpen_Phone()
                        window.addEventListener("native.keyboardhide", () => {
                            log.info("KEYBOARD HAS CLOSED - trying to close full screen toggle")
                            // swp.timeEntryView.narFullScreenToggle()
                            swp.timeEntryView.narClose_Phone()
                        }, false)
                    } else {
                        $("#timeEntryView #fieldNarrative-phone").val(entryArray[entryPosition].narrative)
                        $("#timeEntryView #fieldNarrative-phone").css("height", "18em")
                        $("#timeEntryView #fieldNarrative-phone").css("background-color", "white")
                    }
                })

                module.iPadAdjustments()
            }, 300)
        },

        deleteActiveEntry_Phone: function () {
            if (log.debug("Deleting active entry"), "Released" !== arrEntries[activeEntryPosition].status && "Posted" !== arrEntries[activeEntryPosition].status) {

                var a = arrEntries[activeEntryPosition].get("id");
                log.debug("delete entry: " + a)

                $.ajax({
                    type: "POST",
                    url: service_url + "delete/" + swp_username + "/" + a,
                    data: "",
                    contentType: "application/json",
                    dataType: "json",
                    timeout: 4000,
                    success: function (a) { },
                    failure: function (a) {
                        alert(a)
                    },
                    error: function (result) {
                        if (result.status != 200) {
                            //set item back to new
                            alert("Delete failed, you may be experiencing poor network connectivity. Item removed from session only.");
                        }
                    }

                })

                module.removeActiveEntry()
            }
        },

        duplicateActiveEntry_Phone: function () {
            log.debug('duplicating  entry')

            updateActiveItem()
            // swpSettings.bTimers === true
            //     ? (entryArray = swpSettings.arrEntriesTimers,
            //         entryPosition = swpSettings.activeTimerPosition)
            //     : (entryArray = swpSettings.arrEntries,
            //         entryPosition = swpSettings.activeEntryPosition)

            var entry1 = swp.entry.newEntry()

            /* if the entry is a timer, round it  */
            if (entryArray[entryPosition].timer !== "") {
                log.debug("settings duration")
                entry1.duration = swp.timer.actions.roundTimer(entryArray[entryPosition].timer, entryArray[entryPosition].matterIncrement)
            }
            else {
                entry1.duration = entryArray[entryPosition].duration
            }

            // entry1.id = entryArray[entryPosition].id + "-copy"
            entry1.eventDT = entryArray[entryPosition].eventDT

            entryArray[entryPosition].client
                ? entry1.client = entryArray[entryPosition].client
                : entry1.client = ""

            entryArray[entryPosition].matter
                ? entry1.matter = entryArray[entryPosition].matter
                : entry1.matter = ""

            entryArray[entryPosition].custom1
                ? entry1.custom1 = entryArray[entryPosition].custom1
                : entry1.custom1 = ""

            entryArray[entryPosition].custom2
                ? entry1.custom2 = entryArray[entryPosition].custom2
                : entry1.custom2 = ""

            entryArray[entryPosition].custom3
                ? entry1.custom3 = entryArray[entryPosition].custom3
                : entry1.custom3 = ""

            entryArray[entryPosition].custom4
                ? entry1.custom4 = entryArray[entryPosition].custom4
                : entry1.custom4 = ""

            entryArray[entryPosition].custom5
                ? entry1.custom5 = entryArray[entryPosition].custom5
                : entry1.custom5 = ""

            entry1.narrative = entryArray[entryPosition].narrative

            entryArray[entryPosition].type
                ? entry1.type = entryArray[entryPosition].type
                : entry1.type = "Mobile"

            entry1.typeImage = `${entry1.type}.png`
            entryArray.push(entry1)
            entryPosition = entryArray.length - 1

            swpSettings.bTimers === true
                ? (swpSettings.arrEntriesTimers = entryArray,
                    swpSettings.activeTimersPosition = entryPosition)
                : (swpSettings.arrEntries = entryArray,
                    swpSettings.activeEntryPosition = entryPosition)

                    if ($("#session-entries-phone").data("kendoMobileListView")) {
                        $("#session-entries-phone").data("kendoMobileListView").setDataSource(entryArray)
                        $("#session-entries-phone").data("kendoMobileListView").refresh()
                    }
            // kendo.bind($("#viewTimeEntryTablet"), entryArray[entryPosition])
            swp.stackView.bindEntry(entryArray[entryPosition].id)
            swp.stackView.loadTimeForm()

            // $("#session-entries-list").data("kendoMobileListView").refresh()
            // UpdateNavTitle();
        },

        duplicateButton_Phone: function (e) {

            module.duplicateActiveEntry_Phone()

            setTimeout(() => {
                $("#timeEntryView #userFeedbackMessages .km-collapsible-content").children().hide()
                // $("#timeEntryView #bgHeaderContainer").empty()

                $("#timeEntryView #userFeedbackMessageContent").text("Time Entry Duplicated");
                $("#timeEntryView #userFeedbackMessageContent").show()
                swp.utilities.toggleUserFeedbackMessages(this)
            }, 300) 
        },

        iPadAdjustments: function () {
            //adjustments for all iPads
            if (window.innerWidth > 500) {
                $("#timeEntryView #timeEntryDescToggle").show();
                // setTimeout(() => {
                //     module.narrativeIPadHeight()
                // }, 300)
            }
            
        },

        isInArray: function (date, dates) {

            for (var idx = 0, length = dates.length; idx < length; idx++) {
                var d = dates[idx];
                if (date.getFullYear() == d.getFullYear() &&
                    date.getMonth() == d.getMonth() &&
                    date.getDate() == d.getDate()) {
                    return true;
                }
            }

            return false;
        },

        narBlur_Phone: function () {
            /* Track Active Entry */
            updateActiveItem()

            let activeEntryId = entryArray[entryPosition].id, 
                oldNarr = entryArray[entryPosition].narrative;

            //reset scroller to top of page
            $("#timeEntryView .km-scroll-wrapper").data("kendoMobileScroller").reset();

            if (swpSettings.offlineMode === false) {
                try {
                    var b = new kendo.data.ObservableObject({
                        TimeEntryDesc: oldNarr,
                        Username: swpSettings.swp_username
                    })
                    $.ajax({
                        type: "POST",
                        url: swpSettings.service_url + "narrative",
                        data: JSON.stringify(b),
                        contentType: "application/json",
                        dataType: "json",
                        success: function (a) {
                            swp.debugLogView.addToDebugLog("successful retrieval of text shortcuts: " + a)
                            for (var b = 0; b < entryArray.length; b++) {
                                if (entryArray[b].id === activeEntryId) {
                                    $('#timeEntryView #fieldNarrative-phone')[0].value = a
                                    entryArray[b].narrative = a
                                }
                            }
                        },
                        failure: function (a) {
                            log.debug("Failed to retrieve text shortcuts: " + a)
                            swp.debugLogView.addToDebugLog("Failed to retrieve text shortcuts: " + a)
                            $('#timeEntryView #fieldNarrative-phone')[0].value = oldNarr
                            entryArray[b].narrative = oldNarr
                        }
                    })
                } catch (a) {
                    log.debug("Error expanding text shortcuts: " + a)
                    swp.debugLogView.addToDebugLog("Error expanding text shortcuts: " + a)
                    $('#timeEntryView #fieldNarrative-phone')[0].value = oldNarr
                    entryArray[b].narrative = oldNarr
                }

            } else {
                var shortcuts = jQuery.parseJSON(localStorage.getItem("swp-shortcuts"))

                for (var j = 0; j < shortcuts.length; j++) {
                    //log.debug('Shortcut: ' + shortcuts[i].code);

                    var tx = swpSettings.textShortcutChar;
                    var short = shortcuts[j];
                    var codelower1 = tx + short.code.substr(0, 1).toLowerCase() + short.code.substr(1).toLowerCase();
                    var codelower2 = tx + short.code.substr(0, 1).toLowerCase() + short.code.substr(1).toUpperCase();
                    var textlower = short.text.substr(0, 1).toLowerCase() + short.text.substr(1);
                    var codeupper1 = tx + short.code.substr(0, 1).toUpperCase() + short.code.substr(1).toLowerCase();
                    var codeupper2 = tx + short.code.substr(0, 1).toUpperCase() + short.code.substr(1).toUpperCase();
                    var textupper = short.text.substr(0, 1).toUpperCase() + short.text.substr(1);

                    /* handle the various cases */
                    text = text.replace(codelower1, textlower);
                    text = text.replace(codelower2, textlower);
                    text = text.replace(codeupper1, textupper);
                    text = text.replace(codeupper2, textupper);

                    /* last do a case insentive "catch all" */
                    var regex = new RegExp('(' + RegExp.escape("/" + short.code) + ')', 'gi');
                    text = text.replace(regex, short.text);

                }

                for (var i = 0; i < entryArray.length; i++) {
                    if(entryArray[i].id === activeEntryId) {
                        entryArray[i].narrative = text;
                    }
                }
            }
        },

        narClose_Phone: function () {
            updateActiveItem()

            $("#narrativeFullScreenToggle").attr("data-icon", "image-resize-swp")
            $("#narrativeFullScreenToggle span").removeClass("km-x-swp")
            $("#narrativeFullScreenToggle span").addClass("km-image-resize-swp")
            $("#timeEntryView #timeEntryDescToggle").css("display", "none")

            $("#timeEntryView #button-Close-phone").hide()
            $("#timeEntryView #narrativeCountContainer").hide()
            $("#timeEntryView #button-Delete-phone").show()
            $("#timeEntryView #button-Remove-phone").show()
            $("#timeEntryView #button-duplicate-phone").show()
            $("#timeEntryView #tabstrip-phone").show()
            $("#timeEntryView #divStackButtons-phone").css("bottom", "0px")

            entryArray[entryPosition].type !== "Mobile" && entryArray[entryPosition].type !== "Timer" && entryArray[entryPosition].type !== "" && entryArray[entryPosition].type !== null && !entryArray[entryPosition].narrative.length
                ? ($("#timeEntryView #fieldNarrative-phone").val(entryArray[entryPosition].description),
                    $("#timeEntryView #fieldNarrative-phone").css("background-color", "lightgrey"),
                    $("#timeEntryView #fieldNarrative-phone").css("border-bottom", `solid white 14em`),

                    setTimeout(() => {
                        $("#timeEntryView #fieldNarrative-phone")[0].scrollHeight > 75 && $("#fieldNarrative-phone").css("border-bottom", "solid white 13em"),
                            $("#timeEntryView #fieldNarrative-phone")[0].scrollHeight > 95 && $("#fieldNarrative-phone").css("border-bottom", "solid white 12em"),
                            $("#timeEntryView #fieldNarrative-phone")[0].scrollHeight > 120 && $("#fieldNarrative-phone").css("border-bottom", "solid white 10.5em"),
                            $("#timeEntryView #fieldNarrative-phone")[0].scrollHeight > 145 && $("#fieldNarrative-phone").css("border-bottom", "solid white 9.5em")
                    }, 300)
                )
                : ($("#timeEntryView #fieldNarrative-phone").val(entryArray[entryPosition].narrative),
                    // $("#timeEntryView #fieldNarrative-phone").css("height", "18em"),
                    // $("#timeEntryView #liNarrative-phone").css("height", "19em"),
                    $("#timeEntryView #fieldNarrative-phone").css("background-color", "white"),
                    $("#fieldNarrative-phone").css("border-bottom", "solid white 0em"))

            $("#timeEntryView #fieldNarrative-phone").css("height", "18em")
            $("#timeEntryView #liNarrative-phone").css("height", "19em")

            //Header adjustments
            $("#timeEntryView #timeEntryAddDefaultDiv").show()
            
            swpSettings.bTimers === true
                ? ($(`#timeEntryView #backNavButton`).hide(),
                    $(`#timeEntryView #timerColorIndicatorTouch`).show(),
                    $("#timeEntryView #pageTitleTop").text(entryArray[entryPosition].notes))
                : ($(`#timeEntryView #backNavButton`).show(),
                    $("#timeEntryView #backNavButton").attr("href", "#views/stackView.html"),
                    $("#timeEntryView #backNavButton").removeAttr("onclick"),
                    $(`#timeEntryView #timerColorIndicatorTouch`).hide(),
                    $("#timeEntryView #pageTitleTop").text("Time Entry"))

            $("#timeEntryView #timeEntryNarDone").hide()
            $("#timeEntryView #timeEntryKeyboardClose").hide()
            $(`#timeEntryView #userFeedbackMessages`).data("kendoMobileCollapsible").collapse()
            $('.k-overlay').remove()

            swpSettings.set_narFullScreen = false
            $("#timeEntryDatePicker-phone-row").show()
            $("#client-timeentry-row").show() //fieldClient-phone-row
            $("#matter-timeentry-row").show() //fieldMatter-phone-row
            $("#custom1-timeentry-row").show() //liCustom1-phone-row
            $("#custom2-timeentry-row").show() //liCustom2-phone-row
            $("#custom3-timeentry-row").show() //liCustom3-phone-row
            $("#custom4-timeentry-row").show() //liCustom4-phone-row
            $("#custom5-timeentry-row").show() //liCustom5-phone-row
            $("#duration-timeentry-row").show() //liDurationParent-phone
            swp.setup.hideEmptyCustomFields()

            setTimeout(() => {
                $("#divTimeForm-Phone").height(window.innerHeight - $("#defaultHeaderContainer").height() - $("#timeEntryView .km-footer").height())
                // $("#timeEntryView #fieldNarrative-phone").height($("#divTimeForm-Phone").height() - $("#liNarrative-phone")[0].offsetTop)
                $("#touchTimeEntry-phone").height($("#list-timeform-phone").height())
            }, 200)
            // module.narrativeHeightCheck()
            module.narBlur_Phone()
        },

        narFocus_Phone: function () {
            if (swpSettings.set_narFullScreen == true) return

            let a = $("#timeEntryView .km-scroll-wrapper")
            a.scrollTop(500)
        },

        narOpen_Phone: function () {
            updateActiveItem()
            
            $("#narrativeFullScreenToggle").attr("data-icon", "x-swp")
            $("#narrativeFullScreenToggle span").removeClass("km-image-resize-swp")
            $("#narrativeFullScreenToggle span").addClass("km-x-swp")
            $("#timeEntryView #timeEntryDescToggle").css("display", "block")
            $("#timeEntryView #narrativeCountContainer").show()
            $("#timeEntryView #button-Close-phone").show()
            $("#timeEntryView #button-Delete-phone").hide()
            $("#timeEntryView #button-Remove-phone").hide()
            $("#timeEntryView #button-duplicate-phone").hide()
            $("#timeEntryView #divStackButtons-phone").css("position", "relative")
            $("#timeEntryView #fieldNarrative-phone").val(entryArray[entryPosition].narrative)
            $("#timeEntryView #fieldNarrative-phone").css("height", "18em")
            $("#timeEntryView #fieldNarrative-phone").css("background-color", "white")

            entryArray[entryPosition].narrative.length >= swpSettings.set_MaxNarrative
                ? $("#narrativeCharCount").css("font-weight", "700")
                : $("#narrativeCharCount").css("font-weight", "400")

            entryArray[entryPosition].status === "Posted" || entryArray[entryPosition].status === "Released"
                ? ($("#timeEntryView #tabstrip-phone").show(),
                    $("#timeEntryView #liNarrative-phone").css("height", "42em"),
                    $("#timeEntryView #fieldNarrative-phone").css("height", "36em"),
                    $("#timeEntryView #divStackButtons-phone").css("bottom", "0px"))
                : ($("#timeEntryView #tabstrip-phone").hide(),
                    $("#timeEntryView #liNarrative-phone").css("height", "19em"),
                    $("#timeEntryView #fieldNarrative-phone").css("height", "18em"),
                    document.body.classList.contains("km-on-android")
                        ? $("#timeEntryView #divStackButtons-phone").css("bottom", "0px")
                        : $("#timeEntryView #divStackButtons-phone").css("bottom", swpSettings.set_keyboardHeight))        

            $("#timeEntryView .km-scroll-container").css("transform", "translate3d(0px, 0px, 0px)")   
            $("#timeEntryView #timeEntryKeyboardClose").attr("data-touchstart", "swpPlugins.hideKeyboard")
            $("#timeEntryView #timeEntryAddDefaultDiv").hide()
            $("#timeEntryView #backNavButton").hide()
            $(`#timeEntryView #timerColorIndicatorTouch`).hide()
            $("#timeEntryView #pageTitleTop").text("Narrative")
            $("#timeEntryView #timeEntryNarDone").show()
            $("#timeEntryView #timeEntryKeyboardClose").show()

            swpSettings.set_narFullScreen = true
            $("#timeEntryDatePicker-phone-row").hide()
            $("#client-timeentry-row").hide()
            $("#matter-timeentry-row").hide()
            $("#custom1-timeentry-row").hide()
            $("#custom2-timeentry-row").hide()
            $("#custom3-timeentry-row").hide()
            $("#custom4-timeentry-row").hide()
            $("#custom5-timeentry-row").hide()
            $("#duration-timeentry-row").hide()

            setTimeout(() => {
                let element = document.querySelector("#timeEntryView #fieldNarrative-phone")
                element.setSelectionRange(element.value.length, element.value.length);
            }, 100)

            setTimeout(() => {
                $("#divTimeForm-Phone").height(window.innerHeight - $("#defaultHeaderContainer").height() - $("#timeEntryView .km-footer").height())
                $("#timeEntryView #touchTimeEntry-phone").height($("#timeEntryView #fieldNarrative-phone").height())

                document.body.classList.contains("km-on-android") || entryArray[entryPosition].status === "Posted" || entryArray[entryPosition].status === "Released"
                    ? $("#timeEntryView #divStackButtons-phone").css("bottom", "0px")
                    : $("#timeEntryView #divStackButtons-phone").css("bottom", swpSettings.set_keyboardHeight)
            }, 200)

            setTimeout(() => {
                document.body.classList.contains("km-on-android") || entryArray[entryPosition].status === "Posted" || entryArray[entryPosition].status === "Released"
                    ? $("#timeEntryView #divStackButtons-phone").css("bottom", "0px")
                    : $("#timeEntryView #divStackButtons-phone").css("bottom", swpSettings.set_keyboardHeight)
            }, 300)

            module.narBlur_Phone()
        },

        narrativeHeightCheck: function () {
            setTimeout(() => {
                $("#divTimeForm-Phone").height(window.innerHeight - $("#defaultHeaderContainer").height() - $("#timeEntryView .km-footer").height())
                // $("#timeEntryView #fieldNarrative-phone").height($("#divTimeForm-Phone").height() - $("#liNarrative-phone")[0].offsetTop - (2 * parseFloat($("#timeEntryView #fieldNarrative-phone").css("padding")) + parseFloat($("#timeEntryView #liNarrative-phone").css("padding"))))
                $("#touchTimeEntry-phone").height($("#list-timeform-phone").height())
            }, 300)
        },

        narrativeIPadHeight: function () {
            let otherCells = window.innerHeight - ($("#defaultHeaderContainer").height() +
                $("#timeEntryView .km-footer").height() +
                ($("#list-timeform-phone .inputField").height() * $("#list-timeform-phone .inputField").length) + 20)
            $("#liNarrative-phone").height(otherCells)
            $("#fieldNarrative-phone").height(otherCells)
        },

        openDatePicker: function () {
            $("#timeEntryView #timeEntryDateInput").data("kendoDatePicker").open()
        },

        openDurationView: function (e) {
            let hrs, mins, secs, tempTime;
            
            swpSettings.bTimers === true
                ? (entryArray = swpSettings.arrEntriesTimers,
                    entryPosition = swpSettings.activeTimerPosition)
                : (entryArray = swpSettings.arrEntries,
                    entryPosition = swpSettings.activeEntryPosition)

            //deactivate for released and posted entries
            if (entryArray[entryPosition].status === "Released" || entryArray[entryPosition].status === "Posted") return

            if (swpSettings.bTimers === false) {
                if (swpSettings.set_DurationInUnits === true) {

                    $("#fieldDurationTime-phone").hide()
                    $("#fieldDurationUnits-phone").show()
                    $("#divDurationTotal").text(entryArray[entryPosition].duration)
                    $("#divDurationTotal-phone").text(entryArray[entryPosition].duration)
                    tempTime = ""

                } else {

                    $("#fieldDurationTime-phone").show()
                    $("#fieldTimer-phone").hide()
                    $("#fieldDurationUnits-phone").hide()
                    tempTime = entryArray[entryPosition].durationTimeDisplay.split(":")
                }
            } else {
                $("#fieldTimer-phone").show()
                $("#fieldDurationTime-phone").hide()
                $("#fieldDurationUnits-phone").hide()
                $("#divDurationTotal").text()
                tempTime = entryArray[entryPosition].timer.split(":")
            }

            hrs = Math.ceil(tempTime[0])
            mins = Math.ceil(tempTime[1])
            secs = Math.ceil(tempTime[2])

            hrs = swp.utilities.checkTimeFormat(hrs)
            mins = swp.utilities.checkTimeFormat(mins)
            secs = swp.utilities.checkTimeFormat(secs)

            if (swpSettings.bTimers === true) {
                //do nothing if timer is running
                if (entryArray[entryPosition].timerRunning === true) return

                swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].durationTimeDisplay = `${hrs}:${mins}:${secs}`
                $("#fieldDurationTime-phone").val(`${hrs}:${mins}:${secs}`)
            } else {
                swpSettings.arrEntries[swpSettings.activeEntryPosition].durationTimeDisplay = `${hrs}:${mins}`
                $("#fieldDurationTime-phone").val(`${hrs}:${mins}`)
            }

            location.href = "#views/timeEntryViews/durationView.html"
        },

        phoneSwipe: function (a) {
            swpSettings.bTimers === true
                ? "left" === a.direction && (location.href = "#views/timersView.html")
                : "left" === a.direction
                    ? parseInt(swpSettings.activeEntryPosition) < swpSettings.arrEntries.length - 1 &&
                    (swpSettings.activeEntryPosition += 1,
                        swp.stackView.bindEntry(swpSettings.arrEntries[swpSettings.activeEntryPosition].id),
                        swp.stackView.loadTimeForm())
                    : parseInt(swpSettings.activeEntryPosition) > 0 &&
                    (swpSettings.activeEntryPosition -= 1,
                        swp.stackView.bindEntry(swpSettings.arrEntries[swpSettings.activeEntryPosition].id),
                        swp.stackView.loadTimeForm())
        },

        releaseUnreleaseButton_Phone: function (e) {
            console.log('releaseUnreleaseButton_Phone Clicked')
            swpSettings.bTimers === true
                ? (entryArray = swpSettings.arrEntriesTimers,
                    entryPosition = swpSettings.activeTimerPosition)
                : (entryArray = swpSettings.arrEntries,
                    entryPosition = swpSettings.activeEntryPosition)

            if (entryArray[entryPosition].status == "Posted") return

            if (swpSettings.set_narFullScreen === true) {
                swp.timeEntryView.narClose_Phone()
            }
            //scroll to top
            if ($("#tabstrip-phone-timeentry > div.km-content").length > 0) {
                $("#tabstrip-phone-timeentry > div.km-content").animate({ scrollTop: "0" })
                $("#divTimeForm-Phone").animate({ scrollTop: "0" })
            }


            if (entryArray[entryPosition].status !== "Posted" && entryArray[entryPosition].status !== "Released") {

                if (entryArray[entryPosition].validate() === true) {

                    if (entryArray[entryPosition].timerRunning === true) {
                        swp.timer.actions.stopTimer_Phone()
                        $("#session-entries-list-timer").data("kendoMobileListView").refresh()
                    }

                    entryArray[entryPosition].color = ""
                    entryArray[entryPosition].returned = false
                    entryArray[entryPosition].releaseEntry()

                    $("#lblHours-phone").html("Hours:")
                    // $("#fieldDurationUnits-phone").show()
                    $("#fieldTimestamp-phone").hide()
                }

                // $("#lblHours-phone").html("Hours:")
                // $("#fieldDurationUnits-phone").show()
                // $("#fieldTimestamp-phone").hide()

            } else if (entryArray[entryPosition].status === "Released" && swpSettings.unrelease === true) {
                entryArray[entryPosition].unrelease()
            }
        },

        removeActiveEntry: function (e) {

            console.log('removeActiveEntry Clicked')

            if (swpSettings.set_narFullScreen === true) {
                swp.timeEntryView.narClose_Phone()
            }

            swpSettings.bTimers === true
                ? (entryArray = swpSettings.arrEntriesTimers,
                    entryPosition = swpSettings.activeTimerPosition)
                : (entryArray = swpSettings.arrEntries,
                    entryPosition = swpSettings.activeEntryPosition)

            log.debug("Remove entry: " + entryPosition)
            swp.timeentry.actions.clearValidation()

            entryArray.splice(entryPosition, 1)
            0 === entryArray.length || (entryPosition >= entryArray.length && (
                entryPosition = entryArray.length - 1),
                entryArray[entryPosition].status === "Released" || entryArray[entryPosition].status === "Posted"
                    ? ($("#list-timeform-phone a").removeAttr("href"),
                        $("#timeEntryView #timeEntryDateInput").attr("readonly", true),
                        $("#timeEntryView #fieldNarrative-phone").attr("readonly", true))
                    : ($("#liDate-phone").attr("href", "#datepicker-phone"),
                        $("#liClient-phone").attr("href", "#views/timeEntryViews/clientLookup.html"),
                        $("#liMatter-phone").attr("href", "#views/timeEntryViews/matterLookup.html"),
                        $("#liCustom1-phone").attr("href", "#views/timeEntryViews/custom1Lookup.html"),
                        $("#timeEntryView #fieldNarrative-phone").removeAttr("readonly"))
            )

            swpSettings.bTimers === true
                ? (location.href = "#views/timersView.html",
                    setTimeout(() => {
                        swpSettings.activeTimerPosition = entryPosition
                        // $("#session-entries-list-timer").data("kendoMobileListView").setDataSource(entryArray)
                        // $("#session-entries-list-timer").data("kendoMobileListView").refresh()
                    }, 300))
                : (location.href = "#views/stackView.html",
                    setTimeout(() => {
                        swpSettings.activeEntryPosition = entryPosition
                        $("#session-entries-phone").data("kendoMobileListView").setDataSource(entryArray),
                            $("#session-entries-phone").data("kendoMobileListView").refresh()
                    }, 300))
        },

        removeEmojis: function (input) {
            var result = '';
            if (input.length == 0)
                return input;
            for (var indexOfInput = 0, lengthOfInput = input.length; indexOfInput < lengthOfInput; indexOfInput++) {
                var charAtSpecificIndex = input[indexOfInput].charCodeAt(0);
                if ((32 <= charAtSpecificIndex) && (charAtSpecificIndex <= 126)) {
                    result += input[indexOfInput];
                }
            }

            return result;
        },

        saveButton_Phone: function (e) {
            swpSettings.bTimers === true
                ? (entryArray = swpSettings.arrEntriesTimers,
                    entryPosition = swpSettings.activeTimerPosition)
                : (entryArray = swpSettings.arrEntries,
                    entryPosition = swpSettings.activeEntryPosition)

            if (entryArray[entryPosition].status == "Posted" || entryArray[entryPosition].status == "Released") return
            console.log('saveButton_Phone Clicked - past posted/released check')
            if (swpSettings.set_narFullScreen === true) {
                swp.timeEntryView.narClose_Phone()
            }

            swpSettings.offlineMode === false
                ? entryArray[entryPosition].saveEntry(e)
                : entryArray[entryPosition].saveLocal()

            // swpSettings.set_ValSave == "false"
            // swp.timeentry.actions.clearValidation()
        },

        setTimeEntryLabels: function () {

        },

        // timeSelectorFlag: function () {
        //     if (swpSettings.selectTime == "true" || swpSettings.selectTime == "True") {
        //         /* phone date */
        //         $('#mobi_datetime').mobiscroll('option', 'controls', ['calendar', 'time']);
        //         $('#mobi_datetime').mobiscroll('option', 'buttons', ['set']);
        //         $('#mobi_datetime').mobiscroll('option', 'closeOnSelect', false);
        //     }

        // },
    }

    return module

}()
