//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.custom1Lookup) { swp.custom1Lookup = {} }

swp.custom1Lookup = function () {
    "use strict";
    let c1 = swpSettings.custom1;

    var module = {

        viewShow: function(e) {
            log.debug("Custom 1 View - View Show Hit")

            swp.debugView.addDebugListener()
            $("#custom1LookupView #pageTitleTop").text(c1.set_Custom1)
            $("#custom1LookupView #backNavButton").removeAttr("onclick")
            $("#custom1LookupView #backNavButton").attr("href", "#views/timeEntryViews/timeEntryView.html")
            $("#custom1LookupView #backNavButton").show()
            $("#custom1LookupView #timerColorIndicatorTouch").hide()
            $(".km-scroll-container").css("transform", "translate3d(0px, 0px, 0px)")
        },

        viewInit: function(e) {
            log.debug("Custom 1 View - View Init Hit")

            module.custom1Lookup_phone()
        },


        custom1Lookup_phone: function() {
            log.debug("custom1Lookup_phone function hit");

            $("#custom1-listview-phone").kendoMobileListView({
                dataSource: swp.dataSources.custom1_dataSource,
                template: $("#matter-listview-template").text(),
                filterable: {
                    field: "text",
                    operator: "contains"
                },
                click: (a) => {
                    let entryArray, entryPosition;

                    swpSettings.bTimers === true
                        ? (entryArray = swpSettings.arrEntriesTimers,
                            entryPosition = swpSettings.activeTimerPosition)
                        : (entryArray = swpSettings.arrEntries,
                            entryPosition = swpSettings.activeEntryPosition)

                    document.activeElement.blur()
                    $("input").blur()
                    $('[type="search"]').val("")
                    $("#custom1-listview-phone").data("kendoMobileListView").dataSource.filter({})
                    entryArray[entryPosition].set("custom1", a.dataItem.text)
                    location.href = "#views/timeEntryViews/timeEntryView.html"
                    swp.utilities.refreshCustomMenus()

                    if (c1.set_Custom1Sticky == true && c1.set_Custom1Search == true) {
                            c1.set_Custom1Default = a.dataItem.text
                    }

                    if ("" !== c1.custom1Shortcut && a.dataItem.text.toLowerCase().indexOf("default") < 1 && "" === entryArray[entryPosition].narrative) {
                        entryArray[entryPosition].narrative = c1.custom1Shortcut.substring(0, parseInt(swpSettings.set_MaxNarrative))
                        $("#timeEntryView #fieldNarrative-phone").siblings(".invalid-icon").hide()
                    }

                    if (typeof c1.CopyCustom1 !== "undefined" && (c1.CopyCustom1 == "true" || c1.CopyCustom1 == "True")) {
                        swp.utilities.copyMenuToNarrative("#custom1-listview-phone", a.dataItem.text)
                    }
                }
            }),
                swpSettings.offlineMode === !0 && $('[type="search"]').on("keyup", () => {
                    swp.utilities.addItemToCustomMenu(c1.custom1_dataSource, $(this).val())
                });
            //var a = $("body").height() - 103;
            //$("#divCustom1Scroll-Phone").height(a)
        },
    }

    return module

}()
