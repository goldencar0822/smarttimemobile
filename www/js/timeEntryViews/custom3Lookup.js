//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.custom3Lookup) { swp.custom3Lookup = {} }

swp.custom3Lookup = function () {
    "use strict";
    let c3 = swpSettings.custom3;

    var module = {

        viewShow: function (e) {
            log.debug("Custom 3 View - View Show Hit")

            swp.debugView.addDebugListener()
            $("#custom3LookupView #pageTitleTop").text(c3.set_Custom3)
            $("#custom3LookupView #backNavButton").removeAttr("onclick")
            $("#custom3LookupView #backNavButton").attr("href", "#views/timeEntryViews/timeEntryView.html")
            $("#custom3LookupView #backNavButton").show()
            $("#custom3LookupView #timerColorIndicatorTouch").hide()
            $(".km-scroll-container").css("transform", "translate3d(0px, 0px, 0px)")
        },

        viewInit: function (e) {
            log.debug("Custom 3 View - View Init Hit")

            module.custom3Lookup_phone()
        },

        custom3Lookup_phone: function () {
            log.debug("custom3Lookup_phone function hit")

            $("#custom3-listview-phone").kendoMobileListView({
                dataSource: swp.dataSources.custom3_dataSource,
                template: $("#matter-listview-template").text(),
                filterable: {
                    field: "text",
                    operator: "contains"
                },
                click: (a) => {
                    let arrEntries = swpSettings.arrEntries,
                        activeEntryPosition = swpSettings.activeEntryPosition,
                        arrEntriesTimers = swpSettings.arrEntriesTimers,
                        activeTimerPosition = swpSettings.activeTimerPosition;

                    document.activeElement.blur()
                    $("input").blur()
                    $('[type="search"]').val("")
                    $("#custom3-listview-phone").data("kendoMobileListView").dataSource.filter({})

                    swpSettings.bTimers === true
                        ? arrEntriesTimers[activeTimerPosition].custom3 = a.dataItem.text
                        : arrEntries[activeEntryPosition].custom3 = a.dataItem.text
                        
                    location.href = "#views/timeEntryViews/timeEntryView.html"

                    if (c3.set_Custom3Sticky == true && c3.set_Custom3Search == true) {
                        c3.set_Custom3Default = a.dataItem.text
                    }

                    if (typeof c3.CopyCustom3 !== 'undefined' && (c3.CopyCustom3 == "true" || c3.CopyCustom3 == "True")) {
                        swp.utilities.copyMenuToNarrative("#custom3-listview-phone", a.dataItem.text)
                    }
                }
            });
            swpSettings.offlineMode === !0 && $('[type="search"]').on("keyup", () => {
                swp.utilities.addItemToCustomMenu(c3.custom3_dataSource, $(this).val())
            });
            //var a = $("body").height() - 103;
            //$("#divCustom3Scroll-Phone").height(a)
        }

    }

    return module

}()