//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.custom4Lookup) { swp.custom4Lookup = {} }

swp.custom4Lookup = function () {
    "use strict";
    let c4 = swpSettings.custom4;

    var module = {

        viewShow: function(e) {
            log.debug("Custom 4 View - View Show Hit")

            swp.debugView.addDebugListener()
            $("#custom4LookupView #pageTitleTop").text(c4.set_Custom4)
            $("#custom4LookupView #backNavButton").removeAttr("onclick")
            $("#custom4LookupView #backNavButton").attr("href", "#views/timeEntryViews/timeEntryView.html")
            $("#custom4LookupView #backNavButton").show()
            $("#custom4LookupView #timerColorIndicatorTouch").hide()
            $(".km-scroll-container").css("transform", "translate3d(0px, 0px, 0px)")
        },

        viewInit: function(e) {
            log.debug("Custom 4 View - View Init Hit")

            module.custom4Lookup_phone()
        },

        custom4Lookup_phone: function() {
            log.debug("openCustom4_Phone function hit")
            
            $("#custom4-listview-phone").kendoMobileListView({
                dataSource: swp.dataSources.custom4_dataSource,
                template: $("#matter-listview-template").text(),
                filterable: {
                    field: "text",
                    operator: "contains"
                },
                click: (a) => {
                    let arrEntries = swpSettings.arrEntries,
                        activeEntryPosition = swpSettings.activeEntryPosition,
                        arrEntriesTimers = swpSettings.arrEntriesTimers,
                        activeTimerPosition = swpSettings.activeTimerPosition;

                    document.activeElement.blur()
                    $("input").blur()
                    $('[type="search"]').val("")
                    $("#custom4-listview-phone").data("kendoMobileListView").dataSource.filter({})
                    swpSettings.bTimers === true
                        ? arrEntriesTimers[activeTimerPosition].custom4 = a.dataItem.text
                        : arrEntries[activeEntryPosition].custom4 = a.dataItem.text
                    location.href = "#views/timeEntryViews/timeEntryView.html"

                    if (c4.set_Custom4Sticky == true && c4.set_Custom4Search == true) {
                        c4.set_Custom4Default = a.dataItem.text
                    }

                    if (typeof c4.CopyCustom4 !== 'undefined' && (c4.CopyCustom4 == "true" || c4.CopyCustom4 == "True")) {
                        swp.utilities.copyMenuToNarrative("#custom4-listview-phone", a.dataItem.text)
                    }
                }
            });
            swpSettings.offlineMode === !0 && $('[type="search"]').on("keyup", () => {
                swp.utilities.addItemToCustomMenu(c4.custom4_dataSource, $(this).val())
            });
            //var a = $("body").height() - 103;
            //$("#divCustom4Scroll-Phone").height(a);
        },
    }
    

    return module

}();