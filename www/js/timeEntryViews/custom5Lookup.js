//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.custom5Lookup) { swp.custom5Lookup = {} }

swp.custom5Lookup = function () {
    "use strict";
    let c5 = swpSettings.custom5;

    var module = {

        viewShow: function (e) {
            log.debug("Custom 5 View - View Show Hit")

            swp.debugView.addDebugListener()
            $("#custom5LookupView #pageTitleTop").text(c5.set_Custom5)
            $("#custom5LookupView #backNavButton").removeAttr("onclick")
            $("#custom5LookupView #backNavButton").attr("href", "#views/timeEntryViews/timeEntryView.html")
            $("#custom5LookupView #backNavButton").show()
            $("#custom5LookupView #timerColorIndicatorTouch").hide()
            $(".km-scroll-container").css("transform", "translate3d(0px, 0px, 0px)")
        },

        viewInit: function (e) {
            log.debug("Custom 5 View - View Init Hit")

            module.custom5Lookup_phone()
        },

        custom5Lookup_phone: function () {
            log.debug("custom5Lookup_phone function hit")

            $("#custom5-listview-phone").kendoMobileListView({
                dataSource: swp.dataSources.custom5_dataSource,
                template: $("#matter-listview-template").text(),
                filterable: {
                    field: "text",
                    operator: "contains"
                },
                click: (a) => {
                    let arrEntries = swpSettings.arrEntries,
                        activeEntryPosition = swpSettings.activeEntryPosition,
                        arrEntriesTimers = swpSettings.arrEntriesTimers,
                        activeTimerPosition = swpSettings.activeTimerPosition;

                    document.activeElement.blur()
                    $("input").blur()
                    $('[type="search"]').val("")
                    $("#custom5-listview-phone").data("kendoMobileListView").dataSource.filter({})
                    swpSettings.bTimers === true
                        ? arrEntriesTimers[activeTimerPosition].custom5 = a.dataItem.text
                        : arrEntries[activeEntryPosition].custom5 = a.dataItem.text
                    location.href = "#views/timeEntryViews/timeEntryView.html"

                    if (c5.set_Custom5Sticky == true && c5.set_Custom5Search == true) {
                        c5.set_Custom5Default = a.dataItem.text
                    }
                    
                    if (typeof c5.CopyCustom5 !== 'undefined' && (c5.CopyCustom5 == "true" || c5.CopyCustom5 == "True")) {
                        swp.utilities.copyMenuToNarrative("#custom5-listview-phone", a.dataItem.text)
                    }
                }
            });
            swpSettings.offlineMode === !0 && $('[type="search"]').on("keyup", function () {
                swp.utilities.addItemToCustomMenu(c5.custom5_dataSource, $(this).val())
            });
            //var a = $("body").height() - 103;
            //$("#divCustom5Scroll-Phone").height(a);
        },
    }
    

    return module

}()