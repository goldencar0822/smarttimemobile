// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in cordova-simulate or on Android devices/emulators: launch your app, set breakpoints,
// and then run "window.location.reload()" in the JavaScript Console.

// const { default: swpPlugins } = require("./pluginModules");

/* All Settings (ideally)*/
var swpSettings = new kendo.observable({
    activeEntryPosition: 0,
    activeTimerPosition: 0,
    app: null,
    auth: null,
    appElement: null,
    appLayout: null,
    arrEntries: [],
    arrEntriesTimers: [],
    billingGuidelines: false,
    bInitComplete: false,
    bTimers: false,
    cacheOffline: false,
    clientLabel: "",
    custom1: {
        CopyCustom1: "",
        custom1_dataSource: null,
        custom1Shortcut: "", //GD thing where action code triggers a shortcut to appear in narrative
        set_Custom1: true,
        set_Custom1Default: "",
        set_Custom1OriginalDefault: "",
        set_Custom1Reqs: false,
        set_Custom1Search: "",
        set_Custom1Sticky: "",
    },
    custom2: {
        CopyCustom2: "",
        custom2_dataSource: null,
        custom2Shortcut: "", //GD thing where action code triggers a shortcut to appear in narrative
        set_Custom2: true,
        set_Custom2Default: "",
        set_Custom2OriginalDefault: "",
        set_Custom2Reqs: false,
        set_Custom2Search: "",
        set_Custom2Sticky: "",
    },
    custom3: {
        CopyCustom3: "",
        custom3_dataSource: null,
        custom3Shortcut: "", //GD thing where action code triggers a shortcut to appear in narrative
        set_Custom3: true,
        set_Custom3Default: "",
        set_Custom3OriginalDefault: "",
        set_Custom3Reqs: false,
        set_Custom3Search: "",
        set_Custom3Sticky: "",
    },
    custom4: {
        CopyCustom4: "",
        custom4_dataSource: null,
        custom4Shortcut: "", //GD thing where action code triggers a shortcut to appear in narrative
        set_Custom4: true,
        set_Custom4Default: "",
        set_Custom4OriginalDefault: "",
        set_Custom4Reqs: false,
        set_Custom4Search: "",
        set_Custom4Sticky: "",
    },
    custom5: {
        CopyCustom5: "",
        custom5_dataSource: null,
        custom5Shortcut: "", //GD thing where action code triggers a shortcut to appear in narrative
        set_Custom5: true,
        set_Custom5Default: "",
        set_Custom5OriginalDefault: "",
        set_Custom5Reqs: false,
        set_Custom5Search: "",
        set_Custom5Sticky: "",
    },
    dateFormat: "MMMM D, YYYY", //format for date functions //"M/D/YY" old format
    debugCount: 0,
    debugData: new kendo.data.DataSource({
        data: [],
        sort: { field: "timeStamp", dir: "desc" }
    }),
    debugLogData: new kendo.data.DataSource({ 
        data: [],
        sort: { field: "timeStamp", dir: "desc" }
    }),
    decimalComma: false, //new value to specify that we should use comma delimiter for decimal
    defaultDate: "",
    enableAuth0: false,
    error_BlockBilling: "",
    error_EthicalWall: "",
    error_WorkingAtty: "",
    first_stack_visit: true,
    first_timers_visit: true,
    hourURLTemplate: "",
    idleTime: new Date(),
    idleTimeout: 0,
    isTablet: false, //isTablet: kendo.support.mobileOS && kendo.support.mobileOS.tablet, //may be overriden in (ready)
    keyboardHeight: "20px",
    localOffset: -(new Date().getTimezoneOffset() / 60), //track local timezone offset
    massSelected: 0,
    massFlagCount: 0,
    massReleaseCount: 0, //track this in the releaseentry method so we aren't waiting on the web service when dealing with BG
    massRemoveCount: 0,
    massSaveCount: 0,
    matterLabel: "",
    maxNarrServer: null,
    offlineMode: false,
    searchParams: new kendo.data.ObservableObject({
        searchAllEntries: false,
        searchDateStart: "1/1/1990",
        searchDateEnd: "12/31/2040",
        searchEntriesClient: "",
        searchEntriesMatter: "",
        includeTimeEntries: true,
        includeTimeCaptures: true,
        includeFlagged: true,
        saved: 0,
        released: 0,
        posted: 0,
        returned: 0,
    }),
    selectTime: false,
    service_url: localStorage.getItem("swp_wsURL"),
    service_url_display: localStorage.getItem("swp_wsURL"),
    desktop_service_url: null,
    set_AllowOver24Hrs: null,
    set__BackedupWithinTime: false,
    set_BackupInterval: null,
    set_Broadcast: null,
    set_CachePass: false,
    set_CurrentMo: null,
    set_Culture: "en-US",
    set_futureDateBill: null,
    set_DebugDurationLimit: 48,
    set_DurationInUnits: false,
    set_DefaultIncrement: "0.3",
    set_DefaultRounding: null,
    set_futureDateNonBill: null,
    set_HoursHidden: false,
    set_IgnoreBillingGuidelines: true,
    set_keyboardHeight: "20px",
    set_MassActionsEnabled: false,
    set_MatterIncrement: null,
    set_MaxDayHours: null,
    set_MaxDaysOld: null,
    set_MaxEntryHours: null,
    set_MaxNarrative: "10000",
    set_MaxNarrOverride: "True",
    set_MultiTimers: null,
    set_narFullScreen: false,
    set_OldestValidDate: null,
    set_PostEmpty: null,
    set_PostZero: null,
    set_SchedulerStartDay: 0,
    set_StatsHidden: false,
    set_TimerAutoSave: true,
    set_Timers: true,
    set_ToggleActive: false,
    set_TouchLocked: false,
    set_ValSave: false,
    settingAdjust: false,
    sched_dataSource: null,
    stackParams: {
        stackSortOption: "",
        stackSortDirection: "asc",
        includeTimeEntries: true,
        includeTimeCaptures: false,
        includeFlagged: true,
        saved: 1,
        released: 1,
        posted: 1,
        flagged: 1,
    },
    statParams: {
        duration: "/mo/",
        accountable: 0,
        billable: 1,
        nonbillable: 0,
        showAccountable: true,
        showAccountableStats: false,
        showBudget: true,
        compare: "ActVsBudg",
    },
    styles: {
        customBlue: "#0381DD",
        disabledPhoneButtonColor: "gray",
        entryFlaggedColor: "lightyellow",
        entryHighlightColor: "#E6E6E6",
        invalidFieldColor: "#FA5858",
    },
    _successsfullySetURL: false,
    supressError: false,
    swp_emailaddr: "",
    swp_firmname: "",
    swp_firmCode: "",
    swp_username: "",
    swp_variable: "No@30S9vj0@354;fsdwe[]||jvn(__sdfuhawn",
    swp_supportEmail: "support@smarttimeapps.com",
    textShortcutChar: "/",
    timeCaptureParams: {},
    timerDrawerHide: false,
    timerDropDown: {
        open: false,
        entryId: "",
    },
    timerPresetOptions: {},
    timerLoadPreset: false,
    timerSortOption: "",
    timerSettings: {
        BlankRows: null,
        Id: 0,
        LastPostedClientMatterNumbers: null,
        LoadRadio: null,
        PresetOption: null,
        TileSize: null,
        TimekeeperUser: null,
    },
    totalCurDayHrs: 0,
    objTook: new kendo.data.ObservableObject(),
    objText: null,
    TimeoutIntervalTimer: null,
    unrelease: true, //used to disable unrelease feature (brownstein),
    version: "1.2.61",
})

var index = (function () {
    "use strict";
    let service_url = swpSettings.service_url
    // document.addEventListener('deviceready' -> throwing errors due to prevent default inside passive event listener. Switching to $(document).ready(onDeviceReady) for now, even though .addEvenetListener recommended to prevent unexpected behavior.
    //document.addEventListener('deviceready', onDeviceReady, false);
    $(document).ready(onDeviceReady.bind(this));

    function addSplashScreenHideListener() {
        swpPlugins.default.addSplashScreenHideListener()
    }

    function doOnOrientationChange() {
        /* if any popover is open, close it and reopen it so it can reorient */
        /* http://www.telerik.com/forums/popover-on-mobile----orientation-change-bug */

        ///* for now only implement this on the login screen */
        //if (swpSettings.swp_username === "") {
        //    if (typeof $("#popover-login").data("kendoMobileModalView") !== "undefined") {
        //        $("#popover-login").data("kendoMobileModalView").close();
        //        setTimeout(function () { $("#popover-login").data("kendoMobileModalView").open(); }, 500);
        //    }
        //}

        /* leaving this here just for informational */
        switch (window.orientation) {

            case -90:
            case 90:
                {
                    console.log('landscape');
                    break;
                }
            default:
                {
                    console.log('port')
                    break;
                }
        }
    }

    function onDeviceReady() {
        document.addEventListener("online", function () {
            // YES! We have a connection....or at least the device thinks we do
        })

        document.addEventListener("offline", function () {
            // O NO! No connection....
        })

        addSplashScreenHideListener()
        
        // if (device.model.toString().indexOf("iPad") > -1) { isTablet = true; }
        //read config file to apply ws url if applied by mdm and is not already set
        //swp.debugLogView.addToDebugLog("pre setupServiceURL")
        swp.setup.setupServiceURL()
        //swp.debugLogView.addToDebugLog("post setupServiceURL")

        // swp.debugLogView.addToDebugLog("pre checkConnection")
        // Removing to clean up loading calls per Steve's request 8/10/20
        swp.timeentry.actions.checkConnection()
        // swp.debugLogView.addToDebugLog("post onLoadReady")

        //swp.debugLogView.addToDebugLog("pre onLoadReady")
        onLoadReady()
        //swp.debugLogView.addToDebugLog("post onLoadReady")
    }

    function onLoadReady() {
        console.log("Index.js - On Load READY HIT.")
        var landHeight = 0;
        var portHeight = 0;

        //debugging for MDM
        //swp.debugLogView.addToDebugLog("pre body Show - test")
        $('body').show()
        //swp.debugLogView.addToDebugLog("post body Show - test")
        
        //swp.debugLogView.addToDebugLog("pre Culture Set")
        kendo.culture("@System.Globalization.CultureInfo.CurrentCulture.Name")
        //swp.debugLogView.addToDebugLog("post Culture Set")

        //swp.debugLogView.addToDebugLog("pre doOnOrientationChange Set")
        window.onorientationchange = () => { setTimeout(doOnOrientationChange, 500); };
        setTimeout(doOnOrientationChange, 500);
        //swp.debugLogView.addToDebugLog("post doOnOrientationChange Set")
        ///* logging level - 0 = most logging */
        //log.setLevel(0);
        // let appVersion = Bundle.main.object(forInfoDictionaryKey, "CFBundleVersion")
        // console.log("appVersion", appVersion)
        ///* add 'endsWidth' method to string */
        //if (typeof String.prototype.endsWith !== 'function') {
        //    String.prototype.endsWith = function (suffix) {
        //        return this.indexOf(suffix, this.length - suffix.length) !== -1;
        //    };
        //}
 
        /* Get the correct app HTML container based on tablet status */
        swpSettings.appElement = (swpSettings.isTablet) ? $("#tabletApp") : $("#phoneApp")
        swpSettings.appLayout = (swpSettings.isTablet) ? null : ""

        var timestamp = new Date();
        log.debug('start loading app (' + timestamp.toString() + ')')

        $(".checkboxTheme").kendoSwitch({
            messages: {
                checked: "YES",
                unchecked: "NO"
            }
        })

        //swp.debugLogView.addToDebugLog("pre platform check and app creation")
        window.Capacitor.platform == "android"
            ? swpSettings.app = new kendo.mobile.Application($(document.body), {
                useNativeScrolling: false,
                platform: {
                    device: "android",       /* Mobile device, can be "ipad", "iphone", "android", "fire", "blackberry", "wp", "meego"*/
                },
                loading: false,
            })
            : swpSettings.app = new kendo.mobile.Application(document.body, {
                useNativeScrolling: false,
            })
        //swp.debugLogView.addToDebugLog("post platform check and app creation")

        setRemeberMe()
        //swp.debugLogView.addToDebugLog("post setRemeberMe")

        window.addEventListener("native.keyboardhide", function () {
            log.info("TRYING TO HIDE STATUS BAR!! KEYBOARD HAS CLOSE")
            swp.utilities.fixAndroidStatusBar()
        }, false)

debugger

        location.href = "#popover-debug-phone"
        //swp.debugLogView.addToDebugLog("post navigate to debugg")

        setTimeout(() => {
            // this .href is the first page/view that will load
debugger
            location.href = "#popover-login-phone"
            //swp.debugLogView.addToDebugLog("post navigate to login")

            swp.setup.hideSplashScreen()
            //swp.debugLogView.addToDebugLog("post hideSplashScreen")
            
            $("#faceID").kendoButton({
                imageUrl: "img/faceID.png"
            })

            $("#imageButton").kendoButton({
                imageUrl: "img/faceID.png"
            });
debugger
            //swp.debugLogView.addToDebugLog("pre setupFaceID")
            swp.auth.setupFaceID()
            //swp.debugLogView.addToDebugLog("post setupFaceID")

            setTimeout(() => {
debugger
                $("#popover-login-phone div.km-scroll-container").height($(".km-content").height())
            }, 300)
            //show body now that doc is ready
            $('body').show()
            //swp.debugLogView.addToDebugLog("post body Show actual")
        }, 1000);
    }

    function setRemeberMe() {
        log.info("SET REMEMBER ME HIT. PLACING VALUES IN TEXTBOX")
        var phoneUserInput = $('#txtLoginUser-phone')
        var phonePassInput = $("#txtLoginPass-phone")
        // var userInput = $('#txtLoginUser')

        if (localStorage.swp_RememberUser == "true" || localStorage.swp_RememberUser === "True") {
            log.info("SETTING VALUE OF PASS / USER FROM ALL.JS")
            log.info(localStorage.swp_username)
            log.info(localStorage.swp_password)

            if (phoneUserInput.val() !== localStorage.swp_username) {
                phoneUserInput.empty().val(localStorage.swp_username);
            }

            if (phonePassInput.val() !== localStorage.swp_password) {
                phonePassInput.empty().val(localStorage.swp_password)
            }

            $("#checkRemember-phone").data("kendoSwitch").check(true)
        } else {
            $("#checkRemember-phone").data("kendoSwitch").check(false)
        }
    }

    return {
        onLoadReady,
        onDeviceReady
    }
})()
