//Copyright 2019 by Smart WebParts LLC

//define application namespace
if (typeof swp == "undefined") {
    swp = {};
}
if (typeof swp.shared == "undefined") {
    swp.shared = {};
}

swp.shared.Rounding = function () {

    function Hundreth(hours) {
        return parseFloat(hours).toFixed(2);
    }

    function Twentieth(hours) {
        //WHAT CLIENTS ACTUALLY WANT IS JUST A .05 MIN BUT THEN .1 ROUNDING
        if (hours <= .05) {
            return "0.05";
        }
        else {
            return Tenth(hours);
        }  
    }

    function Tenth(hours) {
        if ((hours * 100) % 10 === 0) {
            return hours.toFixed(2);
        } else {
            return (Math.ceil(hours * 10) / 10).toFixed(2);
        }
    }

    function Eight(hours) {
        if ((hours * 100) % 125 === 0) {
            return parseFloat(hours).toFixed(3);
        } else {
            return (Math.ceil(hours * 8) / 8).toFixed(3);
        }
    }

    function Quarter(hours) {
        if ((hours * 10) % 25 === 0) {
            return parseFloat(hours).toFixed(2);
        } else {
            return (Math.ceil(hours * 4) / 4).toFixed(2);
        }
    }

    function QuarterAndTenth(hours) {
        /* .3 is actually not a real increment, instead it's a placeholder
         *  for the LawTime 'B' option which means tenths AND quarters */

        /* if it's already .25 or .75, leave it alone.  Otherwise round to 10th */
        if (hours.toString().endsWith('.25') || hours.toString().endsWith('.75')) {
            return hours.toString();
        }
        else {
            return Tenth(hours);
        }

    }

    //public interface
    var module = {

        onLoad: function (e) {
            //nothing for now
        },

        roundHours: function (hours, increment) {

            //parse if , is . for da-DK
            //if (kendo.cultures.current.name === "da-DK" || kendo.cultures.current.name == "no") {
            //    hours = hours.replace(",", ".")
            //}

            //convert from string
            hours = parseFloat(hours);
            increment = parseFloat(increment);

            var isNeg = 0;

            if (parseFloat(hours) < 0) {
                isNeg = 1;
                hours = Math.abs(hours);
            }

            switch (increment) {
                case .3:
                    hours = QuarterAndTenth(hours);
                    break;
                case .25:
                    hours = Quarter(hours);
                    break;
                case .125:
                    hours = Eight(hours);
                    break;
                case .1:
                    hours = Tenth(hours);
                    break;
                case .05:
                    hours = Twentieth(hours);
                    break;
                case .01:
                    hours = Hundreth(hours);
                    break;
            }

            if (isNeg == 1) {
                if (increment !== .125) {
                    hours = (-hours).toFixed(2);
                }
                else {
                    hours = (-hours).toFixed(3);
                }
            }

            //parse back da-DK
            //if (kendo.cultures.current.name === "da-DK" || kendo.cultures.current.name == "no") {
            //    newVal = newVal.replace(".", ",")
            //}

            return hours;
        },

        //Note: this function was initially copied from mobile code
        roundTimer: function (timer, increment) {

            var hrs = this.hoursFromTimer(timer);
            hrs = this.roundHours(hrs, increment);
            return hrs;

        },

        hoursFromTimer: function (timer) {

            if (timer === '' || timer === "__:__:__" || timer == null)
                return '0.00';

            var hrs = 0, mins = 0, secs = 0;

            /* secs only */
            if (timer && timer.length < 4) {
                secs = timer.substring(1);
            }

            /* mins and seconds */
            if (timer.length > 3 && timer.length < 6) {
                mins = timer.toString().split(':')[0];
                secs = timer.toString().split(':')[1];
            }

            /* all three */
            if (timer.length > 5) {
                hrs = timer.toString().split(':')[0];
                mins = timer.toString().split(':')[1];
                secs = timer.toString().split(':')[2];
            }

            var total = ((this.parseNumValue(hrs) * 3600) + (this.parseNumValue(mins) * 60) + this.parseNumValue(secs)) / 3600;

            return total;
        },

        msFromTimer: function (timer) {

            var hrs = 0, mins = 0, secs = 0;
            hrs = timer.toString().split(':')[0];
            mins = timer.toString().split(':')[1];
            secs = timer.toString().split(':')[2];

            var total = ((this.parseNumValue(hrs) * 3600) + (this.parseNumValue(mins) * 60) + this.parseNumValue(secs)) * 1000;

            return total;

        },

        parseNumValue: function (aValue) {

            if (aValue != undefined)
                if (aValue.toString().substring(0, 1) == ".")
                    aValue = "0" + aValue.toString();

            /* I'm wrapping the value in a parseFloat first because parseFloat can handle something 
                that isNaN would still say are invalid (multiple dots for example) */
            if (isNaN(parseFloat(aValue)))
                return 0;
            else
                if (aValue.toString().indexOf(".") > 0)
                    return parseFloat(aValue);
                else
                    return parseInt(aValue);

        }


    }

    return module;

}();