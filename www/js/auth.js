//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.auth) { swp.auth = {} }

swp.auth = function () {
    "use strict";
    let activeEntryPosition = swpSettings.activeEntryPosition;
    let arrEntries = swpSettings.arrEntries
    let arrEntriesTimers = swpSettings.arrEntriesTimers
    let c1 = swpSettings.custom1
    let c2 = swpSettings.custom2
    let c3 = swpSettings.custom3
    let c4 = swpSettings.custom4
    let c5 = swpSettings.custom5


    var module = {

        appInit: function (user, pass, userSub) {
            //this will now first check for a url via email then make the auth calls
            //it will technically run after auth0 stuff, but that shouldn't matter for that setup
            //the email query will return "" and the app will init normally
            console.log("Init url: " + swpSettings.service_url + "init")
            log.debug("Init url: " + swpSettings.service_url + "init")
            swp.debugLogView.addToDebugLog("Init url: " + swpSettings.service_url + "init")

            if (pass) {
                $.when(this.setWSUrlFromEmail(user)).done(() => {
                    //needs a little more time, adding timeout
                    // setTimeout(() => {
                    if (swpSettings._successsfullySetURL == false) {
                        module.makeAuthCallAndInit(user, pass, userSub)
                    }
                    // }, 1500)
                })
            }
        },

        backup: function () {
            if (swpSettings.set__BackedupWithinTime === false) {

                swpSettings.set__BackedupWithinTime = true

                setTimeout(() => {
                    swpSettings.set__BackedupWithinTime = false; //fail safe for multi backup bug where we were backup up the same entries twice
                }, 1000)
                
                let timers = JSON.parse(JSON.stringify(swpSettings.arrEntriesTimers))

                timers.forEach(x => {
                    x.duration = moment.duration(x.timer).asMinutes()
                })

                if (timers.length > 0) {
                    $.ajax({
                        traditional: true,
                        url: swpSettings.service_url + "saveautosession",
                        type: "POST",
                        data: JSON.stringify(timers),
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: (result) => {
                            //this is going on in the background so no feedback required

                        },
                        error: (xhr, ajaxOptions, thrownError) => {
                        }
                    })
                } else {

                }
            } else {

            }
        },

        faceIdLoginCheck: function (faceIDUser, faceIDPass, swp_token) {
            setTimeout(() => {

                faceIDUser
                ? $("#checkOffline-phone").prop("checked") === true 
                    ? swp.offline.offlineLogin(faceIDUser, faceIDPass) 
                    : (localStorage.swp_RememberUser = $("#checkRemember-phone").prop("checked"),
                    localStorage.swp_EnableAuth0 == "true" || localStorage.swp_EnableAuth0 == "True" 
                        ? $("#txtLoginUser-phone").val().includes("@") === true 
                            ? log.debug("Please use your desktop login to sign in, not your email address.") // alert("Please use your desktop login to sign in, not your email address.")):
                            : (swp.auth.appInit(faceIDUser, faceIDPass)) 
                        : swp.auth.appInit(faceIDUser, faceIDPass)) 
                : "admin" == $("#txtLoginPass-phone").val() 
                    ? (swpSettings.swp_username = "",
                        setTimeout(() => {
                            location.href = "#views/settingsViews/credentialsView.html"
                        }, 200)) 
                    : (console.log("Please Enter Valid Username and Password"), 
                    log.debug("Please Enter Valid Username and Password")) //alert("Please Enter Valid Username and Password")
            }, 400)
        },

        loginClick_Phone: function (e) {
            log.info("loginClick_Phone function hit")
            $("#txtLoginUser-phone").val() !== ""
                ? $("#checkOffline-phone").prop("checked") === true
                    ? swp.offline.offlineLogin($("#txtLoginUser-phone").val(), $("#txtLoginPass-phone").val())
                    : (e.preventDefault(),
                        localStorage.swp_RememberUser = $("#checkRemember-phone").prop("checked"),
                        localStorage.swp_EnableAuth0 == "true" || localStorage.swp_EnableAuth0 == "True"
                            ? $("#txtLoginUser-phone").val().includes("@") === true
                                ? (e.preventDefault(),
                                    alert("Please use your desktop login to sign in, not your email address."))
                                : (module.appInit($("#txtLoginUser-phone").val(), $("#txtLoginPass-phone").val(), localStorage.swp_token),
                                    e.preventDefault())
                            : (module.appInit($("#txtLoginUser-phone").val(), $("#txtLoginPass-phone").val()),
                                e.preventDefault()))
                : "admin" == $("#txtLoginPass-phone").val()
                    ? (swpSettings.swp_username = "",
                        setTimeout(() => {
                            location.href = "#views/settingsViews/credentialsView.html"
                        }, 200))
                    : (e.preventDefault(),
                        alert("Please Enter Valid Username and Password"))
        },

        logout_Phone: function (a) {
            log.info("logout_Phone function hit")

            if ("undefined" == typeof a) a = "no"
            module.backup()
            swpSettings.arrEntries = []
            swpSettings.arrEntriesTimers = []
            swpSettings.activeEntryPosition = 0
            swpSettings.activeTimerPosition = 0

            if ("tabstrip-phone-settings" != window.location.hash.substr(1) && "timeout" != a) {
                clearTimeout(swpSettings.TimeoutIntervalTimer)
                swpSettings.idleTime = new Date
            }

            if ($("#checkRemember-phone").prop("checked") != true) {
                swpSettings.swp_username = ""
                $("#txtLoginUser-phone").val("")
                $("#txtLoginPass-phone").val("")
            }

            if ($("#session-entries-phone").data("kendoMobileListView")) {
                $("#session-entries-phone").data("kendoMobileListView").setDataSource(swpSettings.arrEntries)
                $("#session-entries-phone").data("kendoMobileListView").refresh()
            }

            if ($("#session-entries-list-timer").data("kendoMobileListView")) {
                $("#session-entries-list-timer").data("kendoMobileListView").setDataSource(swpSettings.arrEntriesTimers)
                $("#session-entries-list-timer").data("kendoMobileListView").refresh()
            }

            if (swpSettings.set_CachePass === false) $("#txtLoginPass-phone").val("")

            swp.timer.actions.stopAllTimers_Phone()
            swp.debugView.debugDataTimeCheck()
            swp.debugLogView.debugLogTimeCheck()
            location.href = "#popover-login-phone"
            // $("#popover-login-phone div.km-scroll-container").height($(".km-content").height())
            $(".km-tabstrip").hide()
        },

        makeAuthCallAndInit: function (user, pass, token, userSub) {
            var objLogin,
                initTryCount = 0,
                initRetryLimit = 3;

            token !== "" && token !== undefined && token !== null
                ? objLogin = new kendo.data.ObservableObject({
                    Username: user,
                    Password: pass,
                    Token: token,
                    DeviceType: "mobile",
                    Key: swpSettings.swp_variable
                })
                : objLogin = new kendo.data.ObservableObject({
                    Username: user,
                    Password: pass,
                    DeviceType: "mobile",
                    Key: swpSettings.swp_variable
                })

            $.ajax({
                type: "POST",
                data: JSON.stringify(objLogin),
                url: swpSettings.service_url + "init",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",/* -- if we set this to "json" then we don't get error messages but we might still need it to work */
                timeout: 2000,
                tryCount: 0,
                retryLimit: 3,
                success: (data) => {
                    if (data === "") {
                        // $("#loginFailureMessage").text("Login failed.  Invalid username and/or password.")
                        // $("#loginFailureMessage").show()

                        alert("Login failed.  Invalid username and/or password.")

                        setTimeout(() => {
                            location.href = "#popover-login-phone"
                            $("#popover-login-phone div.km-scroll-container").height($(".km-content").height())
                        }, 300)
                        //swp.offline.customMessageBox("Login Failed", "Login failed.  Invalid username and/or password.")
                    } else {
                        var obj = jQuery.parseJSON(data)
                        
                        if (obj.enableSTPro === false || obj.enableSTPro == "false" || obj.enableSTPro == "False") {
                            alert("Smart Time Pro is not available for your site.")
                        } else {
                            // $("#loginFailureMessage").hide()
                            location.href = "#views/homeViews/homeView.html"
                            $("#popover-login-phone div.km-scroll-container").height($(".km-content").height())
                            /* store with domain */
                            localStorage.swp_username = user
                            if (!localStorage.userDisplaySimpleTimeInScheduler) localStorage.userDisplaySimpleTimeInScheduler = obj.displaySimpleTimeInScheduler

                            setTimeout(() => {
                                $("#timeEntryView #fieldNarrative-phone").removeAttr("readonly")
                                document.activeElement.blur()
                            }, 500)

                            /* parse out the username if there is a domain */
                            if (user.indexOf("\\") !== -1) user = user.substring(user.indexOf("\\") + 1, 99)

                            swpSettings.swp_username = user

                            /* store entire settings object locally for offline mode */
                            localStorage.swp_settings = JSON.stringify(obj)
                            swp.setup.setupApplication(obj);

                            /* 2017-4-7: start timer to monitor connectivity */
                            //for the moment just do this on the actions (save/release)
                            //setInterval(testConnectivity, 10000); //10 sec

                            /* start idle timer */
                            if (typeof obj.IdleTimeout !== undefined) {

                                swpSettings.idleTimeout = parseInt(obj.IdleTimeout);

                                if (swpSettings.idleTimeout > 0) {
                                    swpSettings.idleTime = new Date(); //restart the timer
                                    swpSettings.TimeoutIntervalTimer = setInterval(timerIncrement, 30000); // 30 sec

                                    //Zero the idle timer on mouse movement.
                                    $(document).bind("touchmove", function (e) {
                                        swpSettings.idleTime = new Date();
                                    });
                                    $(document).keypress(function (e) {
                                        swpSettings.idleTime = new Date();
                                    });
                                }
                            }

                            /* cache pass */
                            // make sure password is not the same as user sub,
                            if (typeof userSub === "undefined" || typeof userSub !== "undefined" && userSub !== pass) {
                                if (typeof obj.CachePass !== "undefined") { swpSettings.set_CachePass = obj.CachePass; }
                                if (swpSettings.set_CachePass === true && $("#checkRemember-phone").prop("checked") === true) {
                                    localStorage.swp_password = pass,
                                        localStorage.swp_token = obj.Token
                                } else if (obj.CachePass == false || obj.CachePass == "false") {
                                    localStorage.swp_password = "",
                                        localStorage.swp_token = "";
                                }
                            }

                            //for face ID, will need to encrypt with swp_password when we setup auth0
                            localStorage.faceIDUser = user,
                            localStorage.faceIDPass = pass,
                            localStorage.swp_token = obj.Token

                            /* check for offline entries to sync */
                            swp.offline.syncOfflineData()
                        }
                    }
                },
                error: function (x, textStatus, error) {

                    log.debug("init x: " + JSON.stringify(x))
                    log.debug("init textStatus: " + textStatus)
                    log.debug("init error: " + error)
                    console.log("init x: " + JSON.stringify(x))
                    console.log("textStatus: " + textStatus)
                    console.log("init error: " + error)
                    swp.debugLogView.addToDebugLog("init x: " + JSON.stringify(x))
                    swp.debugLogView.addToDebugLog("init textStatus:  " + textStatus)
                    swp.debugLogView.addToDebugLog("init error: " + error)
                    // $("#loginFailureMessage").text("Unable to authenticate because the web service could not be reached. Please contact the helpdesk for further assistance. ");
                    // if (textStatus == "timeout" || error == "timeout") {
                    this.tryCount++;

                    if (this.tryCount <= this.retryLimit) {
                        //try again
                        $.ajax(this);
                        // return;
                    }

                    swp.debugLogView.addToDebugLog("!!!Init Error Out!!!")
                    //alert("Invalid Web URL or Can't Reach the Server.")
                    // return;

                    // } else {
                    //     swp.debugLogView.addToDebugLog("!!!Init Error Out!!!")
                    //     alert("Invalid Web URL or Can't Reach the Server.")
                    // }
                }
            })
            // .fail(()=> {
            //     initTryCount++
            //     if (initTryCount <= initRetryLimit) {
            //         $.ajax(this)
            //         return
            //     } else {
            //         initTryCount
            //         swp.debugLogView.addToDebugLog("!!!Init Failed!!!")
            //         alert("Invalid Web URL or Can't Reach the Server.")
            //     }

            // })
        },

        // setRemeberMeLocalStorage: function (user, pass) {
        //     //check the switch to see if we should or should not remember
        //     if (localStorage.swp_RememberUser !== $("#checkRemember-phone").prop("checked")) {
        //         localStorage.swp_RememberUser = $("#checkRemember-phone").prop("checked")
        //     }
        //     //place user and pass if remeber me is set
        //     if (localStorage.swp_RememberUser == "true") {
        //         if (user !== "" && user !== localStorage.swp_username ||
        //             pass !== "" && pass !== localStorage.swp_password) {
        //             localStorage.swp_username = user
        //             localStorage.swp_password = pass
        //         }
        //     } else {
        //         localStorage.removeItem("swp_username")
        //         localStorage.removeItem("swp_password")
        //     }
        // },

        readPList: function() {
            swpPlugins.default.readPList()
        },
        
        setupFaceID: function() {
            swpPlugins.default.checkFaceID()
            if (window.Capacitor.platform === "android") {
                swpPlugins.default.checkBiometrics()
            }
        },

        setWSUrlFromFirmCode: function (firmCode) {
            log.info("setWSUrlFromFirmCode FUNCTION HIT, readin guser loggin credentials and applying url")
            swp.debugLogView.addToDebugLog("setWSUrlFromFirmCode FUNCTION HIT, readin guser loggin credentials and applying url")

            // http://www.stdemo.smarttimeapps.com:8080/wsRest.svc/GetUrl/2222
            //"https://mconfig.smarttimeapps.com/wsRest.svc/getUrl/" + firmCode

            var getFirmURL = $.ajax({
                type: "GET",
                url: "https://mconfig.smarttimeapps.com/wsRest.svc/getUrl/" + firmCode,
                contentType: "application/json",
                dataType: "json",
                timeout: 3000,
                success: function (data) {
                    //if "" is passed back, there is no url associated with the email.
                    if (data !== "") {
                        log.info("SETTTINGS LOCAL STORAGE ITEM TO WEB SERVICE URL PASSED HERE")
                        log.info(data.toString())

                        swpSettings.service_url = data;
                        $("#txtURL-phone").val(data);
                        localStorage.setItem("swp_wsURL", data);
                        swp.debugLogView.addToDebugLog("WebService is set, please relaunch and login");
                        swpSettings._successsfullySetURL = true
                    } else {
                        log.info("No web service being passed by Firm Code")
                        swp.debugLogView.addToDebugLog("No web service being passed by Firm Code")
                        $("#invalidFirmCodeFetch").show()
                    }
                },
                error: function (errMsg) {

                    log.info("Could not pull in the url from the user email, error:", errMsg)
                    swp.debugLogView.addToDebugLog("Could not pull in the url from the user email, error:", errMsg)
                    localStorage.setItem("swp_EnableAuth0", false)
                    $("#invalidFirmCodeFetch").show()
                    swpSettings._successsfullySetURL = false
                    return false
                }
            });

            $.when(getFirmURL).done(function () {
                return _successsfullySetURL;
            })
        },

        setWSUrlFromEmail: function (user) {
            log.info("setWSUrlFromEmail FUNCTION HIT, readin guser loggin credentials and applying url")
            swp.debugLogView.addToDebugLog("setWSUrlFromEmail FUNCTION HIT, readin guser loggin credentials and applying url")
            //hard coding for now
            var successsfullySet = false
            var currentUrl = localStorage.getItem("swp_wsURL")

            if (currentUrl === "" || currentUrl === "undefined" || currentUrl === null) {
                var makeTheCall = $.ajax({
                    type: "GET",
                    url: "https://mconfig.smarttimeapps.com/wsRest.svc/getUrl/" + user,
                    contentType: "application/json",
                    dataType: "json",
                    timeout: 1000,
                    success: function (data) {
                        //if "" is passed back, there is no url associated with the email.
                        if (data !== "") {
                            log.info("SETTTINGS LOCAL STORAGE ITEM TO WEB SERVICE URL PASSED HERE")
                            log.info(data.toString())
                            swpSettings.service_url = data;
                            localStorage.setItem("swp_wsURL", data);
                            alert("WebService is set, please relaunch and login")
                            swpSettings._successsfullySetURL = true
                        } else {
                            localStorage.setItem("swp_wsURL", "");
                            localStorage.setItem("swp_EnableAuth0", false)
                            log.info("No web service being passed by email && none in the cookie setting to guest system")
                            swpSettings._successsfullySetURL = false

                        }
                    },
                    error: function (errMsg) {
                        log.info("Could not pull in the url from the user email, error:", errMsg)
                        // alert("Mobile config service was unable to connect")
                        localStorage.setItem("swp_EnableAuth0", false)
                        // alert(errMsg)
                        swpSettings._successsfullySetURL = false
                        return false
                    }
                });

                $.when(makeTheCall).done(function () {
                    return swpSettings._successsfullySetURL;
                })
            } else {
                swpSettings._successsfullySetURL = false; //setting to false so the auth call will be made, eventhough the service is already set
                log.info("URL Already set, continue to login")
                return swpSettings._successsfullySetURL;
            }
        },

    }
    return module
}()







