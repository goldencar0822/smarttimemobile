//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.entry) { swp.entry = {} }

swp.entry = function () {
    "use strict";
    let service_url = swpSettings.service_url,
        arrEntriesTimers,
        activeTimerPosition,
        oldStatus;

    var module = {

        addNewCal: function() {
            console.log('addNewTimeEntry function hit')
            //swpSettings.defaultDate = ""
            swpSettings.defaultDate = moment()._d;
            module.addNewTimeEntry()
        },

        addNewTimeEntry: function() {
            console.log('addNewTimeEntry function hit')
            //location.href = "#views/timeEntryViews/timeEntryView.html"
            $("#divAddNewStackPhone").css("opacity", ".5")
            setTimeout(() => {
                module.addNewEntry()
            }, 500)
            setTimeout(() => {
                $("#divAddNewStackPhone").css("opacity", "1")
            }, 1500)
        },

        addEntry: function() {
            let defaultDate = swpSettings.defaultDate,
                entry1 = module.newEntry();

            if (defaultDate === "") {
                defaultDate = moment()._d
            }

            swpSettings.arrEntries.push(entry1)
            swpSettings.activeEntryPosition = swpSettings.arrEntries.length - 1

            if (defaultDate !== "") {
                log.debug("Adding new entry for date " + moment(defaultDate).toString())
                var shortdate = moment(defaultDate).format("l").toString()

                if(swpSettings.selectTime == "True") {
                    shortdate = moment(shortdate).add((new Date).getMinutes(), "m").add((new Date).getHours(), "h").toDate()
                }
                
                //entry1.set("eventDT", shortdate) <- from old code, may need to convert back to this format here but this is causing stack to not sort properly, altering to (Tue Dec 24 2019 04:00:00 GMT-0700 (Mountain Standard Time)) format
                entry1.set("eventDT", shortdate)
                entry1.set("sortDate", moment(defaultDate))
            }

            log.debug("activeEntryPosition: " + swpSettings.activeEntryPosition)
            kendo.bind($("#timeEntryView"), swpSettings.arrEntries[swpSettings.activeEntryPosition])
            $(".te-button-phone").css("color", "black")
            module.updateReferences()
            $("#fieldNarrative-phone").removeAttr("disabled")

            if (swpSettings.set_DurationInUnits = true) {
                $("#fieldDurationUnits-phone").show()
            } else {
                $("#fieldDurationTime-phone").show()
            }

            $("#toggleBillingGuideInfo").hide()

            $("#fieldTimestamp-phone").hide()
            $("#lblHours-phone").text("Hours:")

            log.debug("set_Timers = " + swpSettings.set_Timers.toString())
            ////toggle Timer show-hide check
            //if ("true" !== swpSettings.set_Timers && swpSettings.set_Timers !== true) {
            //    log.debug("Exposing timer button")
            //    $("#timeEntryView #btnTimerToggle-phone").show()
            //}
            if (!swpSettings.arrEntries[swpSettings.activeEntryPosition].durationTimeDisplay && !swpSettings.arrEntries[swpSettings.activeEntryPosition].duration) {
                $("#timeEntryView #btnTimerToggle-phone").show()
            }

            $("#spanTimerPhone").hide()
            //trying to set a timeout here to avoid setDatasource error (entry.js line 82 $("#session-entries-phone").data("kendoMobileListView").setDataSource(swpSettings.arrEntries)) on initial selection of date with no entries in it, may actually have to alter the sequence of calls so the kendo widget is available
            setTimeout(() => {
                $("#session-entries-phone").data("kendoMobileListView").setDataSource(swpSettings.arrEntries)
                $("#session-entries-phone").data("kendoMobileListView").refresh()
                $("#divTimeForm-Phone textarea").blur()
            }, 500)
        },

        addNewEntry: function() {

            log.debug('adding new entry')

            swp.timeentry.actions.clearValidation()

            if (swpSettings.bTimers === true)
                swp.timer.actions.addTimer()
            else
                module.addEntry()

            //UpdateNavTitle();
            swp.utilities.refreshMatters()
            swp.utilities.refreshCustomMenus()

        },

        //allSaved_Phone
        getAllSavedEntries: function() {
            $.ajax({
                type: 'POST',
                traditional: true,
                url: swpSettings.service_url + "allsaved",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(swpSettings.objTook),
                dataType: "json",
                success: function (data) {
                    console.log('success allSaved Call', data)
                    log.debug('Received ' + data.length.toString() + ' saved items.')

                    let arrEntriesTemp = [],
                        arrTimersTemp = [];

                    //arrEntriesTemp = data.filter(x => x.Timer === "" || x.Timer === null)
                    //arrTimersTemp = data.filter(x => x.Timer !== "")

                    //swpSettings.arrEntries = data
                    //swpSettings.activeEntryPosition = 0

                    //var b = !1;
                    //if ("true" == set_Timers) {
                    //    for (var c = arrEntries.length; c--;) "" === arrEntries[c].get("timerId") && arrEntries.splice(c, 1);
                    //    arrEntries.length > 0 && (b = !0)
                    //} else

                    for (var i = 0; i < data.length; i++) {
                        var entry = module.newEntry(),
                            entryDTF = data[i].DateToFrom;

                        if (entryDTF != null) {
                            entry = swp.utilities.setTimeToLocal(entry, entryDTF);
                            entry.set("eventDT", moment(entryDTF).format("l").toString());
                            entry.set("sortDate", moment(entryDTF));
                        }

                        entry.set("id", data[i].ID);
                        entry.set("origId", data[i].ID);
                        entry.set("client", data[i].ClientName);
                        entry.set("matter", data[i].MatterName);
                        entry.set("custom1", data[i].Custom1);
                        entry.set("custom2", data[i].Custom2);
                        entry.set("custom3", data[i].Custom3);
                        entry.set("custom4", data[i].Custom4);
                        entry.set("custom5", data[i].Custom5);
                        entry.set("duration", data[i].Hours);
                        entry.set("narrative", data[i].TimeEntryDesc);
                        entry.set("timer", data[i].Timer);
                        entry.set("color", data[i].Color);
                        entry.set("notes", data[i].Notes);

                        entry.set("status", data[i].Status);
                        entry.set("statusImage", "img/" + data[i].Status + ".png");

                        entry = swp.utilities.setTimeToLocal(entry, data[i].DateToFrom)

                        /* Replaced with buttonReleaseClass()/buttonsSaveDeleteClass() functions
                        if (entry.get("status") == "Posted" || entry.get("status") == "Released"){
                        entry.set("buttonClass", "button-div disabled-button");
                        entry.set("buttonClassTimer", "button-timer button-div disabled-button");}
                        */

                        if (entry.timer === "" || entry.timer === null) {
                            arrEntriesTemp.push(entry);
                        } else {
                            arrTimersTemp.push(entry);
                        }

                        //arrEntriesTemp.push(entry);

                        swpSettings.arrEntries = arrEntriesTemp
                        swpSettings.arrEntriesTimers = arrTimersTemp

                        //fix for stackView loading all entries
                        swpSettings.defaultDate = moment()._d;
                    }

                    //if (arrEntriesTemp.length === 0) {
                    //    var entry1 = module.newEntry();
                    //    entry1.set("eventDT", new Date());
                    //    entry1.set("sortDate", new Date());
                    //    arrEntriesTemp.push(entry1);
                    //    swpSettings.arrEntries = arrEntriesTemp
                    //}

                    if (typeof $("#session-entries-phone").data("kendoMobileListView") !== "undefined") {
                        $("#session-entries-phone").data("kendoMobileListView").setDataSource(swpSettings.arrEntries)
                        //swpSettings.arrEntries = []
                    }

                    if (typeof $("#session-entries-list-timer").data("kendoMobileListView") !== "undefined") {
                        $("#session-entries-list-timer").data("kendoMobileListView").setDataSource(swpSettings.arrEntriesTimers)
                    }

                    //var buttongroup = $("#select-entries-sort").data("kendoMobileButtonGroup");
                    //buttongroup.select(0); /* default to date */
                },
                error: function (x, textStatus, e) {
                    kendo.ui.progress($('#scheduler'), false);
                }
            })
        },

        setupBillDisplay: function(array, list) {
            let bgArr = array.split("<br />"),
                bgList = $(`#timeEntryView ${list}`)

            $("#timeEntryView #userFeedbackMessages .km-collapsible-content").children().hide()
            bgArr = bgArr.filter((el) => {
                return el != "";
            })

            for (let i = 0; i < bgArr.length; i++) {
                let li = document.createElement("li")
                li.innerText = bgArr[i]
                bgList.append(li)
            }

            $("#timeEntryView #billingGuidelineErrors").show()
            $("#timeEntryView #billingGuidelines").show()
            $("#timeEntryView #bgHeaderContainer").show()

        },

        newEntry: function() {

            var dt = new Date()
            var _id = dt.getTime();
            let c1 = swpSettings.custom1
            let c2 = swpSettings.custom2
            let c3 = swpSettings.custom3
            let c4 = swpSettings.custom4
            let c5 = swpSettings.custom5

            var viewModel = new kendo.data.ObservableObject({
                badWords: "",
                billType: "",
                blockBill: "", /* if this is anything but blank it means we need to enforce block billing */
                cleanId: function () { return this.id.replace(/\+/g, "").replace(/=/g, "").replace(/</g, "").replace(/>/g, "") }, //clean id which can be used in html
                client: "",
                clientName: function () {
                    if (this.client.toString().indexOf("|") != -1)
                        return this.client.toString().split(" | ")[1];
                    else
                        return this.client;  /* may not have | if user slugged it in (offline mode) */
                },
                clientNum: function () {
                    if (this.client.toString().indexOf("|") != -1)
                        return this.client.toString().split(" | ")[0] + " |";
                    else
                        return "";
                },
                createDate: dt,
                custom1: c1.set_Custom1Default,
                custom2: c2.set_Custom2Default,
                custom3: c3.set_Custom3Default,
                custom4: c4.set_Custom4Default,
                custom5: c5.set_Custom5Default,
                dateTime: function () { /* date and time (if time flag is set), otherwise just returns long date */
                    if (this.eventDT != null && this.eventDT != "" && typeof this.eventDT != "undefined") {
                        if (swpSettings.selectTime == "True") {
                            /* don't parse time if it's 0:00*/
                            if (moment(this.eventDT).format('H') !== "0" || moment(this.eventDT).format('m') !== "0") {
                                return moment(this.eventDT).format('MMMM D, YYYY  h:mm A').toString();
                            }
                            else {
                                return this.eventDate();
                            }

                        }
                        else {
                            return this.eventDate();
                        }
                    }
                    else {
                        return "";
                    }
                },
                duration: "",
                durationUnitDisplay: "",
                durationTimeDisplay: "",
                durationFont: "",
                durationSize: "",
                eventDate: function () {
                    if (this.eventDT != null && this.eventDT != "" && typeof this.eventDT != "undefined") {
                        return moment(this.eventDT).format('MMMM D, YYYY').toString();
                    }
                    else {
                        return "";
                    }
                },
                eventDT: "",
                eventTitle: "",
                id: _id + swpSettings.swp_username.replace('@', '').replace('.', ''),
                matter: "",
                matterIncrement: "",
                matterName: function () {
                    if (this.matter != null)
                        if (this.matter.toString().indexOf("|") != -1)
                            return this.matter.toString().split(" | ")[1];
                        else
                            return this.matter;
                },
                matterNum: function () {
                    if (this.matter != null)
                        if (this.matter.toString().indexOf("|") != -1)
                            return this.matter.toString().split(" | ")[0];
                        else
                            return "";
                },
                narrative: "",
                origId: "",
                plan1: "",
                plan2: "",
                plan3: "",
                plan4: "",
                shortDate: function () { /* short date, for phone time entry */
                    if (this.eventDT != null && this.eventDT != "" && typeof this.eventDT != "undefined") {
                        if (swpSettings.selectTime == "True") {
                            /* don't parse time if it's 0:00*/
                            if (moment(this.eventDT).format('H') !== "0" || moment(this.eventDT).format('m') !== "0") {
                                return moment(this.eventDT).format(swpSettings.dateFormat + '  h:mm A').toString();
                            }
                            else {
                                return moment(this.eventDT).format(swpSettings.dateFormat).toString();
                            }

                        }
                        else {
                            return moment(this.eventDT).format(swpSettings.dateFormat).toString();
                        }
                    }
                    else {
                        return "";
                    }
                },
                sortDate: 0,
                status: "New",
                statusImage: "img/New.png",
                timer: "",
                color: "",
                timerEntry: false,
                timerId: "",
                notes: "",
                timerRunning: false,
                timeStamp: "",
                timerStart: "",
                username: swpSettings.swp_username,

                /* show timestamp or duration, depending on status  */
                bEthicalWall: true,
                bValidAtty: true,
                timerHours: function () {
                    /* we have to make sure we "get" the status here so that we trigger the change and the viewmodel updates */
                    if (this.get("status") === 'Posted' || this.get("status") === 'Released')
                        return this.get("duration");
                    else
                        return ''; //this.get("timeStamp"); -- we aren't doing timestamp here anymore
                },
                timerStartButton: "",
                timerStopButton: "",
                unsavedFlag: false,
                unsavedFlagClass: "hidden-image",

                /* duration\timer value functions */
                durationValue: function (e) {
                    /* 6/30/2014 -- show timer even if it is not running */
                    if ((this.timerId === '') || this.status === 'Posted' || this.status === 'Released')
                        return this.duration;
                    else
                        return '';
                },
                roundEntry: function (e) {
                    let inc = swpSettings.set_DefaultIncrement,
                        duration = this.duration,
                        tempTime, hr, min;

                    /* get duration for timer */
                    if (this.get("timer") != '') {
                        var hrs = swp.timer.actions.roundTimer(this.get("timer"), this.get("matterIncrement"));
                        this.set("duration", hrs);
                    }

                    if (swpSettings.decimalComma === true) {
                        duration = this.duration.toString().replace(",", ".");
                    }

                if (this.matterIncrement !== '' && this.matterIncrement !== undefined)
                    inc = this.matterIncrement;               

                if (duration !== "") {

                    var hrs = swp.shared.Rounding.roundHours(duration, inc);

                    log.debug("Setting hours to " + hrs);
                    tempTime = hrs.split(".")
                    hr = tempTime[0]
                    min = (Number(tempTime[1]) / 100) * 60
                    if (hr.length === 1) { hr = "0" + hr }
                    if (min < 10) { min = "0" + min }
                    tempTime = `${hr}:${min}:00`
                    this.set("duration", hrs); //change event handles putting back the comma if applicable
                    this.set("durationTimeDisplay", tempTime)
                }
                else {
                    //KP 2018-9-26: blank for running timer on save, default to just an increment
                    if (inc == .3) { inc = ".10"; }
                    this.set("duration", inc);
                }
            },
                saveEntry: function (overrideValidation) {
                    //adjustment for ovverride when save button is pressed, may want to alter this to be identical to autosave call though.
                    if (overrideValidation != true) {
                        if (overrideValidation.target[0].id == "button-save-phone") overrideValidation = true
                    }

                    if ((overrideValidation == true || swpSettings.set_ValSave == false || this.validate() === true || swpSettings.set_ValSave == 'prompt') && (this.status !== "Released" && this.status !== "Posted")) {

                        /* round duration */
                        this.roundEntry();

                        var ev = new module.newEventObject()

                        /* parse date */
                        var tempDate = moment(this.eventDT).format();
                        //var tempDate = new Date()

                        /* trim timezone, could be a + or  - */
                        if (tempDate.indexOf('+') > 1) {
                            tempDate = tempDate.substring(0, tempDate.lastIndexOf('+'));
                        }
                        else {
                            tempDate = tempDate.substring(0, tempDate.lastIndexOf('-'));
                        }

                        if (this.eventDT != null) { ev.set("eventDT", tempDate); }

                        ev.client = this.client;
                        ev.matter = this.matter;
                        ev.custom1 = this.custom1;
                        ev.custom2 = this.custom2;
                        ev.custom3 = this.custom3;
                        ev.custom4 = this.custom4;
                        ev.custom5 = this.custom5;
                        ev.duration = this.duration;
                        ev.timer = this.timer;
                        ev.color = this.color;
                        ev.notes = this.notes;
                        ev.username = this.username;
                        ev.narrative = this.narrative;
                        ev.id = this.id

                        this.unsavedFlag = false;

                        var that = this;

                        // if we have billing guidelines we need two factor validation
                        // web service returns bool but somehow this becomes a string
                        //if (swpSettings.billingGuidelines.toString().toLowerCase() === "true" && this.BillingGuidelineError !== "IGNORE") {
                        if (overrideValidation !== true || swpSettings.set_ValSave == 'true') {

                            $.ajax({
                                type: "POST",
                                url: swpSettings.service_url + "validateevent",
                                data: JSON.stringify(ev),
                                contentType: "application/json",
                                dataType: "json",
                                success: function (data) {
                                    let objResult = data,
                                        invalidFieldColor = swpSettings.styles.invalidFieldColor,
                                        errArr,
                                        errDisplay;

                                    log.debug("validateevent result: " + data)

                                    if (objResult.EnableExport === false) {
                                        /* at this point billing guidelines should error */
                                        /* if we start getting more errors here we should add a function to handle all the crap release validation does */
                                        if (objResult.BillingGuidelineError !== "") {
                                            //$("#stackView #userFeedbackMessages .km-collapsible-content").children().hide()
                                            //$("#userFeedbackMessages .km-collapsible-content p").append("<p></p>")
                                            if (swpSettings.isTablet) {
                                                if (swpSettings.bTimers) {
                                                    $("#divValidation_timer").text("Billing Guidelines Errors")
                                                    $("#divValidation_timer").show()
                                                }
                                                else {
                                                    $("#divValidation").text("Billing Guidelines Errors")
                                                    $("#divValidation").show()
                                                }
                                                //$("#timeEntryView #userFeedbackMessages p").text((objResult.BillingGuidelineError.replace("<br />", ".")))
                                                //errArr = objResult.BillingGuidelineError.split("<br />")
                                                module.setupBillDisplay(objResult.BillingGuidelineError, "#billingGuidelineErrors")
                                                $("#timeEntryView #bgViolationContainer").show()
                                                $("#toggleBillingGuideInfo").show()

                                                //$("#timeEntryView #userFeedbackMessages p").text((objResult.BillingGuidelineError.replace(new RegExp(String.fromCharCode(13), 'g'), "<br/>")))
                                                $("#spanIgnore-tab").text("Ignore & Save")
                                                //bg errors on save will now still save but show the error, no need for ignore and save button
                                                $("#spanIgnore-tab").hide()
                                                innerSave()
                                            }
                                            else {

                                                log.debug(objResult.BillingGuidelineError)
                                                $("#toggleBillingGuideInfo .km-warning-swp").css("color", "red")
                                                $("#timeEntryView #billingGuidelineErrors").empty()
                                                module.setupBillDisplay(objResult.BillingGuidelineError, "#billingGuidelineErrors")
                                                $("#toggleBillingGuideInfo").show()
                                                $("#timeEntryView #bgViolationContainer").show()
                                                innerSave()
                                            }

                                        } else {
                                            /* client, matter and/or custom field(s) invalid, figure out which and mark it accordingly */
                                            if (objResult.DateColor === "Red") {
                                                $("#timeEntryDatePicker-phone-row").addClass('invalidField')

                                                $("#fieldDate_timer").css("background-color", invalidFieldColor)
                                                $("#fieldDate_timer").css("border", "1px solid red")
                                            }

                                            if (objResult.ClientColor === "Red") {
                                                $("#client-timeentry-row").addClass('invalidField')

                                                $("#fieldClient_timer").css("background-color", invalidFieldColor)
                                                $("#fieldClient_timer").css("border", "1px solid red")
                                            }

                                            if (objResult.MatterColor === "Red") {
                                                $("#matter-timeentry-row").addClass('invalidField')

                                                $("#fieldMatter_timer").css("background-color", invalidFieldColor)
                                                $("#fieldMatter_timer").css("border", "1px solid red")

                                            }

                                            if (objResult.Custom1Color === "Red") {
                                                $("#custom1-timeentry-row").addClass('invalidField') //liCustom1-phone-row

                                                $("#fieldCustom1_timer").css("background-color", invalidFieldColor)
                                                $("#fieldCustom1_timer").css("border", "1px solid red")
                                            }

                                            if (objResult.Custom2Color === "Red") {
                                                $("#custom2-timeentry-row").addClass('invalidField')

                                                $("#fieldCustom2_timer").css("background-color", invalidFieldColor)
                                                $("#fieldCustom2_timer").css("border", "1px solid red")
                                            }

                                            if (objResult.Custom3Color === "Red") {
                                                $("#custom3-timeentry-row").addClass('invalidField')

                                                $("#fieldCustom3_timer").css("background-color", invalidFieldColor)
                                                $("#fieldCustom3_timer").css("border", "1px solid red")
                                            }

                                            if (objResult.Custom4Color === "Red") {
                                                $("#custom4-timeentry-row").addClass('invalidField')

                                                $("#fieldCustom4_timer").css("background-color", invalidFieldColor)
                                                $("#fieldCustom4_timer").css("border", "1px solid red")
                                            }

                                            if (objResult.Custom5Color === "Red") {
                                                $("#custom5-timeentry-row").addClass('invalidField')

                                                $("#fieldCustom5_timer").css("background-color", invalidFieldColor)
                                                $("#fieldCustom5_timer").css("border", "1px solid red")
                                            }

                                            if (objResult.HoursColor === "Red") {
                                                $("#duration-timeentry-row").addClass('invalidField')

                                                $("#fieldDuration_timer").css("background-color", invalidFieldColor)
                                                $("#fieldDuration_timer").css("border", "1px solid red")
                                            }
                                        }
                                    }
                                    else {

                                        $("#toggleBillingGuideInfo .km-warning-swp").css("color", "black")
                                        $("#timeEntryView #billingGuidelineErrors").empty()
                                        $("#timeEntryView #bgViolationContainer").hide()
                                        innerSave()
                                    }
                                },
                                failure: function (errMsg) {
                                    alert(errMsg)
                                }
                            })

                        }
                        else {
                            log.debug('skipping web service validation')
                            innerSave()
                        }

                        /* just calls the service, creating this function so I dont have to duplicate this code twice above */
                        //var that = this;

                        function innerSave() {

                            log.debug('saving item')

                            //for foley only, call delayed release
                            if (swpSettings.service_url.toLowerCase().indexOf("foley") > -1) {
                                swp.timeentry.actions.saveEntryDelayed(that)
                            }
                            else {
                                swp.timeentry.actions.saveEntry(that)
                            }


                        }
                    }
                },
                saveLocal: function () {

                    log.debug("Saving locally")

                    this.roundEntry()
                    this.unsavedFlag = false
                    this.set("status", "Saved")
                    this.set("statusImage", "img/Saved.png")
                    this.set("unsavedFlagClass", "hidden-image")
                    $('.imgSave').css("visibility", "visible")

                    setTimeout(() => {
                        $('.imgSave').css("visibility", "hidden")
                    }, 1000)

                    var entries = JSON.parse(localStorage.getItem("swp-entries"))

                    if (entries === null || typeof entries === "undefined") {
                        /* no existing data so we just create a new array */
                        log.debug("Creating new entries array")
                        entries = []
                    }
                    else {

                        /* check to see if this entry already exists, remove it */
                        for (var i = 0; i < entries.length; i++) {
                            log.debug(entries[i].id)
                            if (entries[i].id === this.id) {
                                log.debug("removing entry from array")
                                entries.splice(i, 1)
                            }
                        }
                    }

                    /* add the entry */
                    entries.push(this)
                    localStorage.setItem("swp-entries", JSON.stringify(entries))

                    $("#imgStatus-phone").css("visibility", "hidden")
                    $("#save-confirm-phone").css("visibility", "visible")
                    setTimeout(() => {
                        $("#save-confirm-phone").css("visibility", "hidden")
                    }, 1000)
                

                    //if (isTablet) {
                    //    /* show save flag in stack */
                    //    $('#' + this.cleanId() + 'timer' + ' img.imgSaveStack').css("display", "inline-block");
                    //    $('#' + this.cleanId() + 'timer' + ' img.imgSaveStack').delay(1000).fadeOut(700);
                    //    $('#' + this.cleanId() + ' img.imgSaveStack').css("display", "inline-block");
                    //    $('#' + this.cleanId() + ' img.imgSaveStack').delay(1000).fadeOut(700);
                    //}

                    setTimeout(() => { 
                        $("#imgStatus-phone").css("visibility", "visible") 
                    }, 2000)
                },
                tickTimer: function () {
                    // Your moment
                    var mmt = moment();
                    // Your moment at midnight
                    var mmtMidnight = mmt.clone().startOf('day');
                    // Difference in minutes
                    var diffMinutes = mmt.diff(mmtMidnight, 'minutes');

                    var startTimeIsSameDay = moment(this.timerStart).isSame(mmtMidnight, "day");
                    var eventDateIsSameDay = moment(this.eventDate()).isSame(mmtMidnight, "day");

                    if (this.timerRunning == true) {
                        var time = new Date().getTime() - this.timerStart;
                        if (startTimeIsSameDay === false || eventDateIsSameDay === false) { //remove any time after midnight
                            time = moment(new Date().getTime() - this.timerStart, "x").subtract(diffMinutes.valueOf(), "minutes").format("x")
                        }

                        var hrs = 0;
                        var mins = 0;
                        var secs = 0;

                        secs = Math.floor(time / 1000)

                        if (secs > 3600) {
                            hrs = Math.floor(secs / 3600);
                            secs = secs % 3600;
                        }

                        if (secs > 60) {
                            mins = Math.floor(secs / 60);
                            secs = secs % 60;
                        }

                        hrs = hrs.toString()
                        mins = mins.toString()
                        secs = secs.toString()

                        if (hrs.length === 1) {
                            hrs = "0" + hrs
                        }

                        if (mins.length === 1)
                            mins = "0" + mins;

                        if (secs.length === 1)
                            secs = "0" + secs;

                        this.set("timer", `${hrs}:${mins}:${secs}`);

                    }

                    $(`#timersView #${this.id}.timerToggleTouch`).css("background-color", "var(--customBlue)")
                    $(`#timersView #${this.id}.timerToggleTouch`).css("color", "white")
                    $(`#timersView #${this.id}.timerToggleTouch`).css("font-weight", "700")

                    if (startTimeIsSameDay === false || eventDateIsSameDay === false) {
                        $(`#timersView #${this.id}.timerToggleTouch`).css("background-color", "inherit")
                        $(`#timersView #${this.id}.timerToggleTouch`).css("color", "black")
                        $(`#timersView #${this.id}.timerToggleTouch`).css("font-weight", "100")
                        //swp.timer.actions.stopAllTimers();
                        swp.timer.actions.stopAllTimers_Phone();
                    }

                },
                timerValue: function () {
                    /* 6/30/2014 -- show timer even if it is not running */
                    if ((this.timerId !== '') && (this.status === 'New' || this.status === 'Saved'))
                        return this.timer;
                    else
                        return '';
                },
                timerValueTab: function () {
                    if (this.status === "Released")
                        return this.duration;
                    else
                        return this.timer;
                },
                validate: function (e) {
                    ///* clear everything first */
                    swp.timeentry.actions.clearValidation()

                    let invalidField = swpSettings.styles.invalidFieldColor

                    log.debug('Validating event');

                    /* get duration for timer */
                    if (this.get("timer") != '') {
                        var hrs = swp.timer.actions.roundTimer(this.get("timer"), this.get("matterIncrement"));
                        this.set("duration", hrs);

                    }

                    var result = true;

                    /* bad words */
                    var errorWords = "";
                    if (this.badWords !== "") {

                        /* don't apply the error yet because we may combine it with the BB error */
                        var words = this.badWords.split(";");

                        for (var i = 0, l = words.length; i < l; i++) {
                            if (words[i] !== "" && this.narrative.toLowerCase().indexOf(words[i]) !== -1) {

                                result = false;

                                if (errorWords === "")
                                    errorWords = words[i];
                                else
                                    errorWords = errorWords + ", " + words[i];
                            }
                        }

                    }

                    /* check block billing */
                    if (this.blockBill !== '' && this.narrative.indexOf(';') !== -1) {
                        result = false;

                        /* combine with bad words error if appropriate */
                        var errorBB = error_BlockBilling;

                        if (errorWords !== "")
                            errorBB = "Block Billing and Stop Word Error: " + errorWords;

                        if (!isTablet) {
                            $('#divValidation').text(errorBB);
                            $('#divValidation_timer').text(errorBB);
                        } else {
                            if (window.location.hash === "#tabstrip-phone-timeentry") {
                                $("#modalErrorText-phone").text(errorBB);
                                $("#modal-error-phone").data("kendoMobileModalView").open();
                                $("#fieldNarrative-phone").attr("disabled", true);
                                $('#duration-timeentry-row').hide();
                            }
                        }
                    }
                    else {
                        /* apply just bad words error if needed */
                        if (errorWords !== "") {
                            errorWords = "Stop Word Error: " + errorWords;

                            if (!isTablet) {
                                $('#divValidation').text(errorWords);
                                $('#divValidation_timer').text(errorWords);
                            } else {
                                if (window.location.hash === "#tabstrip-phone-timeentry") {
                                    $("#modalErrorText-phone").text(errorWords);
                                    $("#modal-error-phone").data("kendoMobileModalView").open();
                                    $("#fieldNarrative-phone").attr("disabled", true);
                                    $('#duration-timeentry-row').hide();
                                }
                            }
                        }
                    }


                    /* check working atty flag which is set in the change event */
                    if (this.bValidAtty === false) {

                        result = false;

                        log.debug('Invalid working atty');

                        /* 10/11/16: The flag check was backwards so message never shown */
                        //if (!isTablet)  {
                        if (swpSettings.isTablet) {
                            $('#divValidation').text(swpSettings.error_WorkingAtty);
                            $('#divValidation_timer').text(swpSettings.error_WorkingAtty);
                        } else {
                            $("#modalErrorText-phone").text(swpSettings.error_WorkingAtty);
                            $("#modal-error-phone").data("kendoMobileModalView").open();
                        }
                    }

                    if (this.bEthicalWall === false) {

                        result = false;

                        if (swpSettings.isTablet) {
                            $('#divValidation').text(swpSettings.error_EthicalWall);
                            $('#divValidation_timer').text(swpSettings.error_EthicalWall);
                        } else {
                            $("#modalErrorText-phone").text(swpSettings.error_EthicalWall);
                            $("#modal-error-phone").data("kendoMobileModalView").open();
                        }
                    }



                    /* date validation */
                    var validDate = true;

                    /* make sure entry is not too old */
                    if ((moment(this.eventDT) < moment(swpSettings.set_OldestValidDate)) || (this.eventDT == ""))
                        validDate = false;
                    else {

                        /* KP 2019-1-22: check this against current date not including timestamp, otherwise next day stuff can pass validation */
                        var dtToCompare = moment().startOf('day');

                        /* check for future date -- only check this if date is still valid after above check */
                        if (this.billType === "billable" || this.billType === "" || typeof this.billType == "undefined") {
                            if (moment(this.eventDT).diff(dtToCompare, "Days") > swpSettings.set_futureDateBill) {
                                validDate = false;
                            }
                        }
                        else {
                            if (moment(this.eventDT).diff(dtToCompare, "Days") > swpSettings.set_futureDateNonBill) {
                                validDate = false;
                            }
                        }
                    }


                    if (validDate === false) {

                        result = false;
                        $("#timeEntryDatePicker-phone-row").addClass('invalidField');
                        //$("#fieldDate_timer").css("background-color", swpSettings.styles.invalidFieldColor);
                        //$("#fieldDate_timer").css("border", "1px solid red");
                    }

                    if (this.client == "") {
                        result = false;

                        log.debug('invalid client');
                        $("#client-timeentry-row").addClass('invalidField');

                        //$("#fieldClient_timer").css("background-color", invalidField);
                        //$("#fieldClient_timer").css("border", "1px solid red");


                    }


                    if (this.matter == "") {
                        result = false;

                        log.debug('invalid matter');
                        $("#matter-timeentry-row").addClass('invalidField')
                   
                        //$("#fieldMatter_timer").css("background-color", invalidField);
                        //$("#fieldMatter_timer").css("border", "1px solid red");

                    }

                    for (const property in this) {
                        if (property.includes("custom")) {
                            let num = property.slice(-1)
                            if ((this[property] == "" || this[property] == undefined) && `c${num}.set_Custom${num}Reqs.toString().toLowerCase()` == 'true' && `c${num}.custom${num}_dataSource._data.length` > 0) {
                                result = false
                                log.debug(`set_Custom${num}Reqs`)
                                $(`#custom${num}-timeentry-row`).addClass('invalidField')
                                //$(`#liCustom${num}-phone-row`).addClass('invalidField')
                                //console.log(`${property}: ${this[property]}`)
                            }
                        
                        }
                    }

                    //if ((this.custom1 == "" || this.custom1 == undefined) && c1.set_Custom1Reqs.toString().toLowerCase() == 'true' && c1.custom1_dataSource._data.length > 0) {
                    //    result = false;

                    //    log.debug(set_Custom1Reqs);
                    //    $("#custom1-timeentry-row").addClass('invalidField')

                    //    //$("#fieldCustom1_timer").css("background-color", invalidField);
                    //    //$("#fieldCustom1_timer").css("border", "1px solid red");

                    //}

                    //if ((this.custom2 == "" || this.custom2 == undefined) && c2.set_Custom2Reqs.toString().toLowerCase() == 'true' && c2.custom2_dataSource._data.length > 0) {

                    //    log.debug('invalid custom2');
                    //    $("#liCustom2-phone-row").addClass('invalidField')

                    //    result = false;

                    //    //$("#fieldCustom2_timer").css("background-color", invalidField);
                    //    //$("#fieldCustom2_timer").css("border", "1px solid red");
                    //}

                    //if ((this.custom3 == "" || this.custom3 == undefined) && c3.set_Custom3Reqs.toString().toLowerCase() == 'true' && c3.custom3_dataSource._data.length > 0) {
                    //    result = false;

                    //    $("#liCustom3-phone-row").addClass('invalidField')

                    //    //$("#fieldCustom3_timer").css("background-color", invalidField);
                    //    //$("#fieldCustom3_timer").css("border", "1px solid red");
                    //}

                    //if ((this.custom4 == "" || this.custom4 == undefined) && c4.set_Custom4Reqs.toString().toLowerCase() == 'true' && c4.custom4_dataSource._data.length > 0) {
                    //    result = false;

                    //    $("#liCustom4-phone-row").addClass('invalidField')

                    //    //$("#fieldCustom4_timer").css("background-color", invalidField);
                    //    //$("#fieldCustom4_timer").css("border", "1px solid red");
                    //}

                    //if ((this.custom5 == "" || this.custom5 == undefined) && c5.set_Custom5Reqs.toString().toLowerCase() == 'true' && c5.custom5_dataSource._data.length > 0) {
                    //    result = false;

                    //    $("#liCustom5-phone-row").addClass('invalidField')

                    //    //$("#fieldCustom5_timer").css("background-color", invalidField);
                    //    //$("#fieldCustom5_timer").css("border", "1px solid red");
                    //}

                    /* for purpose of this validation, put decimal back as normal */
                    let durationToValidate = this.duration.toString().replace(",", ".");

                    if ((durationToValidate == "" || durationToValidate == "0" || isNaN(durationToValidate) == true || durationToValidate == "0.00")
                        && (swpSettings.set_PostZero == 'false')) {
                        result = false;

                        log.debug('invalid duration');

                        /* tablet validation */
                        $("#duration-timeentry-row").addClass('invalidField')

                        //$("#fieldDuration_timer").css("background-color", invalidField);
                        //$("#fieldDuration_timer").css("border", "1px solid red");
                    }

                    /* check for > 24 hr duration */
                    if (swpSettings.set_Allow24Hrs === false) {
                        if (parseFloat(this.duration) > 24) {

                            result = false;

                            /* tablet validation */
                            $("#duration-timeentry-row").addClass('invalidField');

                            //$("#fieldDuration_timer").css("background-color", invalidField);
                            //$("#fieldDuration_timer").css("border", "1px solid red");
                        }
                    }

                    if (this.narrative == "" && swpSettings.set_PostEmpty == 'false') {

                        log.debug('invalid narrative');

                        result = false;

                        $("#liNarrative-phone").addClass('invalidField')

                        //$("#fieldNarrative_timer").css("background-color", invalidField);
                        //$("#fieldNarrative_timer").css("border", "1px solid red");
                    }

                    log.debug('Returning: ' + result.toString());

                    // return result // correct return statement
                    return true;  // using this since I have not setup the matter or custom lookup pages yet
                },
                buttonReleaseClass: function (e) {
                    if (this.get("status") === "Posted" || (this.get("status") === "Released" && swpSettings.unrelease === false))
                        return "button-div disabled-button";
                    else
                        return "button div";
                },
                buttonsSaveDeleteClass: function (e) {
                    if (this.get("status") === "Released" || this.get("status") === "Posted")
                        return "button-div disabled-button";
                    else
                        return "button div";
                },
                buttonTimerClass: function (e) {
                    if (this.get("status") === "Posted" || (this.get("status") === "Released" && swpSettings.unrelease === false))
                        return "button-div disabled-button";
                    else
                        return "button div";
                },
                releaseButtonCaption: function (e) {
                    if (this.get("status") === "Released" && swpSettings.unrelease === true)
                        return "Unrelease";
                    else
                        return "Release";
                },
                releaseEntry: function (e) {

                    //track old status in case we need to revert
                    oldStatus = this.status;

                    /*
                       There are two stages of validation -- client side and server
                       server side is only required if offline mode is available and it causes a slight delay
                       so if we don't have offline we will trust the client side validation and mark the item
                       as released right away
    
                        2015-4-8: BG validation also in second stage
                    */
                    //bg flag is comping across as a string
                    if (swpSettings.cacheOffline === false && swpSettings.billingGuidelines == false) {
                        let disabledPhoneButtonColor = swpSettings.styles.disabledPhoneButtonColor

                        if (swpSettings.bTimers === true) {
                            $('#fieldTimestamp_timer').hide();
                            $('#divTimerButtons').hide();
                            $("#fieldDuration_timer").show();
                            $('#lblHours-phone').text('Hours:');
                        }

                        /* show phone confirmation flag */
                        $("#save-confirm-phone").css("visibility", "hidden") /* force save check to hide in case user is going quickly from save to release */
                        $("#imgStatus-phone").css("visibility", "hidden")
                        $('.imgRelease').css("visibility", "visible")
                    
                        setTimeout(function () {
                            $('.imgRelease').css("visibility", "hidden")
                            $("#imgStatus-phone").css("visibility", "visible") 
                        }, 2000);

                        /* disable fields */
                        $('#list-timeform-phone a').removeAttr('href');
                        $('#fieldNarrative-phone').attr('disabled', 'true');
                        $('#btnSave-phone').css('color', disabledPhoneButtonColor);
                        $('#btnDelete-phone').css('color', disabledPhoneButtonColor);
                        $('#btnTimerToggle-phone').hide();  /* hide timer toggle */

                        if (swpSettings.unrelease === false) { $('#btnRelease-phone').css('color', disabledPhoneButtonColor); }

                        /* update status  -- I THINK THIS SECITON IS SUPERFLOUS*/
                        this.set("status", "Released");
                        this.set("statusImage", "img/Released.png");
                        this.set("unsavedFlagClass", "hidden-image");
                        $('#spanTimerPhone').hide();
                        //massReleaseCount++;
                    }

                    /* default username to logged in user */
                    if (this.username === "")
                        this.username = swpSettings.swp_username;

                    /* stop running timer -- tablet does this in the event handler for the release button */
                    if (this.timerRunning === true && !swpSettings.isTablet)
                        swp.timer.actions.stopTimer_PhoneTemplate(this.id);

                    this.roundEntry();

                    var ev = new module.newEventObject();

                    /* parse date */
                    var tempDate = moment(this.eventDT).format();

                    /* trim timezone, could be a + or  - */
                    if (tempDate.indexOf('+') > 1) {
                        tempDate = tempDate.substring(0, tempDate.lastIndexOf('+'));
                    }
                    else {
                        tempDate = tempDate.substring(0, tempDate.lastIndexOf('-'));
                    }

                    if (this.eventDT != null) { ev.set("eventDT", tempDate); }

                    ev.client = this.client;
                    ev.matter = this.matter;
                    ev.custom1 = this.custom1;
                    ev.custom2 = this.custom2;
                    ev.custom3 = this.custom3;
                    ev.custom4 = this.custom4;
                    ev.custom5 = this.custom5;
                    ev.duration = this.duration;
                    ev.username = this.username;
                    ev.narrative = this.narrative;
                    ev.id = this.id;

                    var that = this;

                    log.debug('making call to release event.');

                    //for foley only, call delayed release
                    if (swpSettings.service_url.toLowerCase().indexOf("foley") > -1) {
                        swp.timeentry.actions.releaseEntryDelayed(that, ev);
                    }
                    else {
                        swp.timeentry.actions.releaseEntry(that, ev);
                    }
                },
                unrelease: function (e) {
                    /* {key}/{user}/{token} */
                    var url = swpSettings.service_url + "unrelease/" + this.id + "/" + swpSettings.swp_username + "/" + swpSettings.objTook.token;
                    var that = this;

                    log.debug(url);

                    $.ajax({
                        type: "GET",
                        url: url,
                        contentType: "application/json",
                        dataType: "json",
                        timeout: 4000,
                        success: function (data) {
                            if (data.toString().toLowerCase() == 'true') {
                                that.set("status", "New"); //status set properly in save -- needs to be something other than release/posted

                                that.BillingGuidelineError = "IGNORE"
                                that.saveEntry()
                                swp.stackView.LoadTimeForm()
                                $('.te-button-phone').css('color', 'black')

                                /* enable fields */
                                $('#liClient-phone').attr('href', '#views/timeEntryViews/clientLookup.html');
                                $('#liMatter-phone').attr('href', '#views/timeEntryViews/matterLookup.html');
                                $('#liCustom1-phone').attr('href', '#views/timeEntryViews/custom1Lookup.html');
                                $('#fieldNarrative-phone').removeAttr('disabled');

                                // altering for timer toggle
                                if (swpSettings.arrEntries[swpSettings.activeEntryPosition].timer !== "") {
                                //if (swpSettings.arrEntries[swpSettings.activeEntryPosition].timerEntry === true) {
                                    $('#fieldTimer-phone').show()
                                    $('#spanTimerPhone').show()
                                    $('#lblHours-phone').text('Timer:')
                                    $("#fieldDurationUnits-phone").hide()
                                    $("#fieldDurationTime-phone").hide()
                                } else {
                                    $('#fieldTimer-phone').hide()
                                    $('#spanTimerPhone').hide()
                                    $('#lblHours-phone').text('Hours:')

                                    if (swpSettings.set_DurationInUnits === true) {
                                        $("#fieldDurationTime-phone").hide()
                                        $("#fieldDurationUnits-phone").show()
                                    } else {
                                        $("#fieldDurationUnits-phone").hide()
                                        $("#fieldDurationTime-phone").show()
                                    }
                                }
                            }
                            else {
                                /* mark as posted */
                                that.set("status", "Posted");
                                that.set("statusImage", "img/Posted.png");
                            }
                        },
                        error: function (result) {
                            if (result.status != 200) {
                                that.set("status", "Released");
                                that.set("statusImage", "img/Released.png");
                                alert("Un-release failed, you may be experiencing poor network connectivity. ");
                            }
                        },
                        failure: function (errMsg) {
                            alert(errMsg);
                        }
                    });

                },

                /* try to make narrative read-only for posted/released */
                narReadOnly: function (e) {
                    if (this.status === "Posted" || this.status === "Released") {
                        return true;
                    }
                    else {
                        return false;
                    }
                }

            })

            viewModel.bind("change", function (e) {


                if (e.field != "status" && e.field != "statusImage" && e.field != "timerStartButton"
                    && e.field != "timerStopButton" && e.field != "timerId" && e.field != "timerRunning"
                    && this.unsavedFlagClass == "hidden-image" && e.field != "unsavedFlagClass") {
                    this.unsavedFlag = true;
                }

                if (e.field === "timer" && this.timer !== ':00') {
                    this.set("unsavedFlagClass", "");
                    if (this.timerRunning === true) {
                        $(`#timersView #${this.id}.timerToggleTouch`).css("background-color", "var(--customBlue)")
                        $(`#timersView #${this.id}.timerToggleTouch`).css("color", "white")
                        $(`#timersView #${this.id}.timerToggleTouch`).css("font-weight", "700")
                    } else {
                        $(`#timersView #${this.id}.timerToggleTouch`).css("background-color", "inherit")
                        $(`#timersView #${this.id}.timerToggleTouch`).css("color", "black")
                        $(`#timersView #${this.id}.timerToggleTouch`).css("font-weight", "100")
                    }
                    
                }

                //removing this to fix error of clearing the defaultDate on an entry date change, may have to reimplement and refactor instead.
                //if (e.field == "sortDate" && swpSettings.defaultDate != "" && this.sortDate != 0 && this.sortDate != "" && this.sortDate != null)
                //    if (this.sortDate.format('YYYY-M-D').toString() != moment(swpSettings.defaultDate).format('YYYY-M-D').toString())
                //        swpSettings.defaultDate = ""

                if (e.field == "duration" || e.field == "status") {

                    swp.entry.updateTotals();

                    //if this site has a comma separator, make sure we don't have a decimal shown
                    if (swpSettings.decimalComma === true) {
                        if (this.duration.toString().indexOf(".") >= 0) {
                            this.set("duration", this.duration.toString().replace(".", ","));
                        }
                    }
                }

                if (e.field === "client") {
                    /* refresh matters list */
                    //if (swpSettings.isTablet) {
                    //    refreshMatters_tab()
                    //} else {
                        // add logic here to set billing guidelines to collapsable header BGCH
                        // $("#timeEntryView #userFeedbackMessages p").text("Updated Billing Guidelines")
                        swp.utilities.refreshBillingGuidelines()
                        swp.utilities.refreshMatters()
                    //}
                }

                if (e.field == "matter" && swpSettings.offlineMode === false) {

                    log.debug("GETTING MATTER INFO (INCREAMENT, STOPWORDS, ETC");

                    var source = e.sender.source;

                    /* reset increment & BB*/
                    this.matterIncrement = ''
                    this.blockBill = ''

                    if (source == undefined) source = e.sender

                    var status = source.get('status').toLowerCase()

                    log.debug('checking working atty: ' + status)

                    if (status === "new" || status === "saved") {
                        var objWA = new kendo.data.ObservableObject({
                            Client: this.client,
                            Matter: this.matter,
                            Username: swpSettings.swp_username
                        });

                        log.debug("URL: " + swpSettings.service_url + "workingatty")

                        if (this.matter === "") {
                            this.matter = null
                        } else {
                            $.ajax({
                                type: "POST",
                                url: swpSettings.service_url + "workingatty",
                                data: JSON.stringify(objWA),
                                contentType: "application/json",
                                dataType: "json",
                                success: function (data) {
                                    console.log("data", data)
                                    var result = data; /* used to be a bool */
                                    if (!data) return

                                    var obj = jQuery.parseJSON(data)
                                    

                                    log.debug(obj);

                                    source.set('matterIncrement', obj.Increment.toString().replace(",", "."));
                                    result = obj.WorkingAtty;

                                    source.set('blockBill', obj.BlockBill);
                                    source.set('billType', obj.BillType);

                                    if (typeof obj.EthicalWall !== 'undefined') {
                                        source.set('bEthicalWall', obj.EthicalWall);
                                    }

                                    if (typeof obj.BadWords !== 'undefined')
                                        source.set('badWords', obj.BadWords.toString().toLowerCase());


                                    /* I think this just preserves old behavior */
                                    if (result === false) {

                                        source.set('bValidAtty', false);

                                        if (!swpSettings.isTablet && this.matter !== '') {
                                            $('#divValidation').text(swpSettings.error_WorkingAtty);
                                            $('#divValidation_timer').text(swpSettings.error_WorkingAtty);
                                        } else {
                                            if (location.href.toString().indexOf("tabstrip-phone-timeentry") > 0) {
                                                /* only show message if we are on the time entry form */
                                                $("#modalErrorText-phone").text(swpSettings.error_WorkingAtty);
                                                $("#modal-error-phone").data("kendoMobileModalView").open();
                                                $("#fieldNarrative-phone").attr("disabled", true);
                                            }
                                        }

                                    }
                                    else {

                                        source.set('bValidAtty', true);

                                        /* clear the error */
                                        $('#divValidation').text('');
                                        $('#divValidation_timer').text('');
                                    }

                                },
                                failure: function (errMsg) {
                                    alert(errMsg);
                                }
                            })
                        }
                    }
                }

                if (e.field === "notes") {
                    arrEntriesTimers = swpSettings.arrEntriesTimers
                    activeTimerPosition = swpSettings.activeTimerPosition
                    let view = 

                    if (arrEntriesTimers.length) {
                        if (arrEntriesTimers[activeTimerPosition].notes == "") {
                            $("#timeEntryView #pageTitleTop").text("Timer")
                        } else {
                            $("#timeEntryView #pageTitleTop").text(`${arrEntriesTimers[activeTimerPosition].notes}`)
                            swpSettings.arrEntriesTimers[swpSettings.activeTimerPosition].notes = $("#timeEntryView #timerNameInput").val()

                        }
                    }
                }
            })

            return viewModel
        },

        newEventObject: function() {
            var objEvent = new kendo.data.ObservableObject({

                eventDT: "",
                client: "",
                matter: "",
                custom1: "",
                custom2: "",
                custom3: "",
                custom4: "",
                custom5: "",
                duration: "",
                timer: "",
                username: "",
                narrative: "",
                description: "",
                id: "",
                Key: swpSettings.swp_variable,
            })

            return objEvent;
        },

        updateReferences: function() {
            $("#liDate-phone").attr("href", "#datepicker-phone")
            $("#liClient-phone").attr("href", "#views/timeEntryViews/clientLookup.html")
            $("#liMatter-phone").attr("href", "#views/timeEntryViews/matterLookup.html")
            $("#liCustom1-phone").attr("href", "#views/timeEntryViews/custom1Lookup.html")
            $("#liCustom2-phone").attr("href", "#views/timeEntryViews/custom2Lookup.html")
            $("#liCustom3-phone").attr("href", "#views/timeEntryViews/custom3Lookup.html")
            $("#liCustom4-phone").attr("href", "#views/timeEntryViews/custom4Lookup.html")
            $("#liCustom5-phone").attr("href", "#views/timeEntryViews/custom5Lookup.html")
            //$("#liDuration-phone").attr("href", "#views/timeEntryViews/durationView.html")
        },

        updateTotals: function() {
            let arrEntries = swpSettings.arrEntries
            let set_DefaultRounding = swpSettings.set_DefaultRounding
            let arrEntriesTimers = swpSettings.arrEntriesTimers

            var saved = 0;
            var released = 0;
            var posted = 0;
            var total = 0;

            for (var i = 0; i < arrEntries.length; i++) {


                if (arrEntries[i].status == "Saved" && arrEntries[i].duration != "")
                    saved = saved + swp.utilities.parseNumValue(arrEntries[i].duration);

                if (arrEntries[i].status == "Released" && arrEntries[i].duration != "")
                    released = released + swp.utilities.parseNumValue(arrEntries[i].duration);

                if (arrEntries[i].status == "Posted" && arrEntries[i].duration != "")
                    posted = posted + swp.utilities.parseNumValue(arrEntries[i].duration);

                if (arrEntries[i].status != "New" && arrEntries[i].duration != "")
                    total = total + swp.utilities.parseNumValue(arrEntries[i].duration);
            }

            saved = saved.toFixed(set_DefaultRounding);
            released = released.toFixed(set_DefaultRounding);
            posted = posted.toFixed(set_DefaultRounding);
            total = total.toFixed(set_DefaultRounding);

            $("#spanSavedTotal").text(saved);
            $("#spanReleasedTotal").text(released);
            $("#spanPostedTotal").text(posted);
            $("#spanTotal").text(total);

            saved = 0;
            released = 0;
            total = 0;

            for (var j = 0; j < arrEntriesTimers.length; j++) {

                if (arrEntriesTimers[j].status == "Saved" && arrEntriesTimers[j].duration != "")
                    saved = saved + swp.utilities.parseNumValue(arrEntriesTimers[j].duration);

                if (arrEntriesTimers[j].status == "Released" && arrEntriesTimers[j].duration != "")
                    released = released + swp.utilities.parseNumValue(arrEntriesTimers[j].duration);

                if (arrEntriesTimers[j].status != "New" && arrEntriesTimers[j].duration != "")
                    total = total + swp.utilities.parseNumValue(arrEntriesTimers[j].duration);
            }

            saved = saved.toFixed(set_DefaultRounding);
            released = released.toFixed(set_DefaultRounding);
            total = total.toFixed(set_DefaultRounding);

            $("#spanSavedTotal_Timer").text(saved);
            $("#spanReleasedTotal_Timer").text(released);
            $("#spanTotal_Timer").text(total);
        },
    }

    return module

}()