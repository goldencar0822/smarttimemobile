//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.auth) { swp.auth = {} }

swp.auth = function () {
    "use strict";
    let activeEntryPosition = swpSettings.activeEntryPosition;
    let arrEntries = swpSettings.arrEntries
    let arrEntriesTimers = swpSettings.arrEntriesTimers
    let c1 = swpSettings.custom1
    let c2 = swpSettings.custom2
    let c3 = swpSettings.custom3
    let c4 = swpSettings.custom4
    let c5 = swpSettings.custom5


    var module = {

        appInit: function (user, pass, userSub) {
            //this will now first check for a url via email then make the auth calls
            //it will technically run after auth0 stuff, but that shouldn't matter for that setup
            //the email query will return "" and the app will init normally
            log.debug('Init url: ' + swpSettings.service_url + "init");
            var swp_variable = "No@30S9vj0@354;fsdwe[]||jvn(__sdfuhawn";
            $.when(this.setWSUrlFromEmail(user)).done(function () {
                //needs a little more time, adding timeout
                setTimeout(function () {
                    if (swpSettings._successsfullySetURL == false) {
                        swp.auth.makeAuthCallAndInit(user, pass, userSub)
                    }
                }, 1500)
            })
        },

        loginClick_Phone: function () {
            log.info('loginClick_Phone function hit')
            localStorage.setItem("swp_RememberUser", $("#checkRemember-phone").prop("checked")),
                "" !== $("#txtLoginUser-phone").val()
                ? $("#checkOffline-phone").prop("checked") === !0
                    ? swp.offline.offlineLogin($("#txtLoginUser-phone").val(), $("#txtLoginPass-phone").val())
                    : (localStorage.setItem("swp_RememberUser",
                        $("#checkRemember-phone").prop("checked")),
                        swp.auth.appInit($("#txtLoginUser-phone").val(), $("#txtLoginPass-phone").val()))
                : "admin" == $("#txtLoginPass-phone").val() &&
                    (swpSettings.swp_username = "",
                        $("#liEmailSets-phone").hide(),
                        $("#liTimersSets-phone").hide(),
                        $("#liMultTimersSets-phone").hide(),
                        $("#btnLogout-phone").hide(),
                        location.href = "#views/settingsViews/settingsView.html",
                        $(".km-tabstrip").hide()
                    )

            function hideTabstrip() {
                /*grant access to settings screen only*/
                $("#splash-screen").hide();
                $("#splash-screen-vertical").hide();

                /*hide all fields but the URL */
                $("#liEmail_tab").hide();
                $("#listview-settings-timer").hide();
                $("#liAuth_tab").hide();
                $('#liOfflinePass_tab').hide();

                location.href = "#tabstrip-phone-settings";

                $(".km-tabstrip").hide();

                $.when(this).done(function () {
                    if ($(".km-tabstrip").length === 0) {
                        setTimeout(function () {
                            hideTabstrip()
                        }, 500)
                    } else if ($(".km-tabstrip")[0].style.display !== "none") {
                        setTimeout(function () {
                            hideTabstrip()
                        }, 500)
                    }
                })
            }


            //if ($("#txtLoginUser-phone").val() === '' && $("#txtLoginPass-phone").val() === 'admin') {
            //    if ($(".km-tabstrip").length > 0) {
            //        hideTabstrip()
            //    } else {
            //        setTimeout(function () {
            //            hideTabstrip()
            //        }, 400)
            //    }
            //}
        },

        logout_Phone: function(a) {
            log.info('logout_Phone function hit')
            
            if("undefined" == typeof a) {
                a = "no";
            }
            if("tabstrip-phone-settings" != window.location.hash.substr(1) && "timeout" != a) {
                clearTimeout(swpSettings.TimeoutIntervalTimer);
                swpSettings.idleTime = new Date;
            }
            if(1 != $("#checkRemember-phone").prop("checked")) { 
                swpSettings.swp_username = ""; 
                $("#txtLoginUser-phone").val("");
                $("#txtLoginPass-phone").val("");
            };
            if(swpSettings.set_CachePass === !1) {
                $("#txtLoginPass-phone").val("");
                location.href = "#popover-login-phone";
                swpSettings.arrEntries = [];
                swpSettings.activeEntryPosition = 0;
            };
            //if ($("#session-entries-phone").data("kendoMobileListView") !== null) {
            //    $("#session-entries-phone").data("kendoMobileListView").setDataSource(arrEntries); 
            //    $("#session-entries-phone").data("kendoMobileListView").refresh();
            //};
            //if ($("#session-entries-list-timer").data("kendoMobileListView") != null) {
            //    $("#session-entries-list-timer").data("kendoMobileListView").setDataSource(arrEntriesTimers)
            //    $("#session-entries-list-timer").data("kendoMobileListView").refresh();
            //}
            $(".km-tabstrip").hide();
        },

        makeAuthCallAndInit: function (user, pass, userSub) {

            var swp_variable = ""

            var objLogin = new kendo.data.ObservableObject({
                Username: user,
                Password: pass,
                DeviceType: "mobile",
                Key: swp_variable
            });

            $.ajax({
                type: 'POST',
                data: JSON.stringify(objLogin),
                url: swpSettings.service_url + "init",
                contentType: "application/json; charset=utf-8",
                dataType: "json",/* -- if we set this to "json" then we don't get error messages but we might still need it to work */
                success: function (data) {
                    if (data === "") {
                        $("#loginFailureMessage").text('Login failed.  Invalid username and/or password.')
                        $("#loginFailureMessage").show()
                        location.href = "#popover-login-phone"
                        //swp.offline.customMessageBox("Login Failed", "Login failed.  Invalid username and/or password.");
                    }
                    else {
                        $("#loginFailureMessage").hide()
                        /* store with domain */
                        localStorage.setItem("swp_username", user);

                        setTimeout(function () {
                            $("#fieldNarrative-phone").attr("disabled", false);
                            document.activeElement.blur();
                        }, 500);

                        /* parse out the username if there is a domain */
                        if (user.indexOf('\\') !== -1)
                            user = user.substring(user.indexOf('\\') + 1, 99)

                        swpSettings.swp_username = user;

                        var obj = jQuery.parseJSON(data);

                        log.debug(obj);

                        /* store entire settings object locally for offline mode */
                        localStorage.setItem("swp_settings", JSON.stringify(obj));

                        swp.setup.setupApplication(obj);

                        /* 2017-4-7: start timer to monitor connectivity */
                        //for the moment just do this on the actions (save/release)
                        //setInterval(testConnectivity, 10000); //10 sec

                        /* start idle timer */
                        if (typeof obj.IdleTimeout !== undefined) {

                            swpSettings.idleTimeout = parseInt(obj.IdleTimeout);

                            if (swpSettings.idleTimeout > 0) {
                                swpSettings.idleTime = new Date(); //restart the timer
                                swpSettings.TimeoutIntervalTimer = setInterval(timerIncrement, 30000); // 30 sec

                                //Zero the idle timer on mouse movement.
                                $(document).bind('touchmove', function (e) {
                                    swpSettings.idleTime = new Date();
                                });
                                $(document).keypress(function (e) {
                                    swpSettings.idleTime = new Date();
                                });
                            }
                        }

                        /* cache pass */
                        // make sure password is not the same as user sub,
                        if (typeof userSub === "undefined" || typeof userSub !== "undefined" && userSub !== pass) {
                            if (typeof obj.CachePass !== 'undefined') { swpSettings.set_CachePass = obj.CachePass; }
                            if (swpSettings.set_CachePass === true && $("#checkRemember-phone").prop("checked") === true) {
                                localStorage.setItem("swp_password", pass);
                            }
                        }

                        /* check for offline entries to sync */
                        swp.offline.syncOfflineData();
                    }
                },
                error: function (x, textStatus, e) {
                    log.debug('init error: ' + textStatus);
                    $("#loginFailureMessage").text('Unable to authenticate because the web service could not be reached. Please contact the helpdesk for further assistance. ');
                }
            });

        },

        setWSUrlFromEmail: function(user) {
            log.info("setWSUrlFromEmail FUNCTION HIT, readin guser loggin credentials and applying url")
            //hard coding for now
            var successsfullySet = false;
            var currentUrl = localStorage.getItem("swp_wsURL");

            if (currentUrl === "" || currentUrl === "undefined" || currentUrl === null) {
                var makeTheCall = $.ajax({
                    type: "GET",
                    url: "https://mconfig.smarttimeapps.com/wsRest.svc/getUrl/" + user,
                    contentType: "application/json",
                    dataType: "json",
                    timeout: 1000,
                    success: function (data) {
                        //if "" is passed back, there is no url associated with the email.
                        if (data !== "") {
                            log.info("SETTTINGS LOCAL STORAGE ITEM TO WEB SERVICE URL PASSED HERE")
                            log.info(data.toString())
                            service_url = data;
                            localStorage.setItem("swp_wsURL", data);
                            alert("WebService is set, please relaunch and login")
                            swpSettings._successsfullySetURL = true
                        } else {
                            localStorage.setItem("swp_wsURL", "");
                            localStorage.setItem("swp_EnableAuth0", false)
                            log.info("No web service being passed by email && none in the cookie setting to guest system")
                            swpSettings._successsfullySetURL = false

                        }
                    },
                    error: function (errMsg) {
                        log.info("Could not pull in the url from the user email, error:", errMsg)
                        // alert("Mobile config service was unable to connect")
                        localStorage.setItem("swp_EnableAuth0", false)
                        // alert(errMsg)
                        swpSettings._successsfullySetURL = false
                        return false
                    }
                });

                $.when(makeTheCall).done(function () {
                    return swpSettings._successsfullySetURL;
                })
            } else {
                swpSettings._successsfullySetURL = false; //setting to false so the auth call will be made, eventhough the service is already set
                log.info("URL Already set, continue to login")
                return swpSettings._successsfullySetURL;
            }
        },

    }
    return module
}()







