//Copyright 2017 by Smart WebParts LLC
if (!swp) { var swp = {} }
if (!swp.dataSources) { swp.dataSources = {} }

swp.dataSources = function () {
    "use strict";
    let arrEntries = swpSettings.arrEntries,
        arrEntriesTimers = swpSettings.arrEntriesTimers,
        activeEntryPosition = swpSettings.activeEntryPosition,
        activeTimerPosition = swpSettings.activeTimerPosition,
        entryArray, entryPosition;

    var module = {

        stackDataSource: new kendo.data.DataSource(swpSettings.arrEntries),

        timersDataSource: new kendo.data.DataSource(swpSettings.arrEntriesTimers),

        custom1_dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    dataType: "json",
                    url: ""
                }
            },
            offlineStorage: {
                getItem: function () {

                    /* filter list based on matter plan */

                    /* get plan from active entry or timer */
                    var plan = "";

                    if (swpSettings.bTimers === true) {
                        plan = arrEntriesTimers[activeTimerPosition].plan1
                    }
                    else {
                        if (arrEntries.length > 0) { plan = arrEntries[activeEntryPosition].plan1; }
                    }

                    var data = JSON.parse(localStorage.getItem("swp-custom1"));
                    var results = [];

                    if (typeof plan === "undefined") { plan = ""; }

                    for (var i = 0; i < data.length; i++) {
                        if (data[i].plan.toString() === plan.toString()) {
                            results.push(data[i]);
                        }
                    }

                    log.debug("Returning " + results.length.toString() + " items");

                    return results;
                },
                setItem: function (item) { }
            },
            serverPaging: false,
            serverFiltering: true,
            change: function () {

                if (this._data.length > 0) {

                    swpSettings.bTimers === true
                        ? (entryArray = swpSettings.arrEntriesTimers,
                            entryPosition = swpSettings.activeTimerPosition)
                        : (entryArray = swpSettings.arrEntries,
                            entryPosition = swpSettings.activeEntryPosition)

                    entryArray[entryPosition].status === "Released" || entryArray[entryPosition].status === "Posted"
                        ? ($('#liCustom1-phone').removeAttr('href'),
                            $('#fieldCustom1_phone').css("color", "darkgray"),
                            $('.lblfieldCustom1').css("color", "darkgray"))
                        : ($('#liCustom1-phone').attr('href', '#views/timeEntryViews/custom1Lookup.html'),
                            $('#fieldCustom1_phone').css("color", "black"),
                            $('.lblfieldCustom1').css("color", "black"))

                } else {

                    $('#liCustom1-phone').removeAttr('href')
                    $('#fieldCustom1_phone').css("color", "darkgray")
                    $('.lblfieldCustom1').css("color", "darkgray")
                }

            }
        }),

        custom2_dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    dataType: "json",
                    url: ""
                }
            },
            offlineStorage: {
                getItem: function () {
                    /* filter list based on matter plan */

                    /* get plan from active entry or timer */
                    var plan = "";

                    if (bTimers === true) {
                        plan = arrEntriesTimers[activeTimerPosition].plan2
                    }
                    else {
                        if (arrEntries.length > 0) { plan = arrEntries[activeEntryPosition].plan2; }
                    }

                    var data = JSON.parse(localStorage.getItem("swp-custom2"));
                    var results = [];

                    if (typeof plan === "undefined") { plan = ""; }

                    for (var i = 0; i < data.length; i++) {
                        if (data[i].plan.toString() === plan.toString()) {
                            results.push(data[i]);
                        }
                    }

                    return results;
                },
                setItem: function (item) { }
            },
            serverPaging: false,
            serverFiltering: true,
            change: function () {

                if (this._data.length > 0) {

                    swpSettings.bTimers === true
                        ? (entryArray = swpSettings.arrEntriesTimers,
                            entryPosition = swpSettings.activeTimerPosition)
                        : (entryArray = swpSettings.arrEntries,
                            entryPosition = swpSettings.activeEntryPosition)

                    entryArray[entryPosition].status === "Released" || entryArray[entryPosition].status === "Posted"
                        ? ($('#liCustom2-phone').removeAttr('href'),
                            $('#fieldCustom2_phone').css("color", "darkgray"),
                            $('.lblfieldCustom2').css("color", "darkgray"))
                        : ($('#liCustom2-phone').attr('href', '#views/timeEntryViews/custom2Lookup.html'),
                            $('#fieldCustom2_phone').css("color", "black"),
                            $('.lblfieldCustom2').css("color", "black"))

                } else {

                    $('#liCustom2-phone').removeAttr('href')
                    $('#fieldCustom2_phone').css("color", "darkgray")
                    $('.lblfieldCustom2').css("color", "darkgray")
                }
            }
        }),

        custom3_dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    dataType: "json",
                    url: ""
                }
            },
            offlineStorage: {
                getItem: function () {
                    /* filter list based on matter plan */

                    /* get plan from active entry or timer */
                    var plan = "";

                    if (swpSettings.bTimers === true) {
                        plan = arrEntriesTimers[activeTimerPosition].plan3
                    }
                    else {
                        if (arrEntries.length > 0) { plan = arrEntries[activeEntryPosition].plan3; }
                    }

                    var data = JSON.parse(localStorage.getItem("swp-custom3"));
                    var results = [];

                    if (typeof plan === "undefined") { plan = ""; }

                    for (var i = 0; i < data.length; i++) {
                        if (data[i].plan === plan) {
                            results.push(data[i]);
                        }
                    }
                    return results;
                },
                setItem: function (item) { }
            },
            serverPaging: false,
            serverFiltering: true,
            change: function () {

                if (this._data.length > 0) {

                    swpSettings.bTimers === true
                        ? (entryArray = swpSettings.arrEntriesTimers,
                            entryPosition = swpSettings.activeTimerPosition)
                        : (entryArray = swpSettings.arrEntries,
                            entryPosition = swpSettings.activeEntryPosition)

                    entryArray[entryPosition].status === "Released" || entryArray[entryPosition].status === "Posted"
                        ? ($('#liCustom3-phone').removeAttr('href'),
                            $('#fieldCustom3_phone').css("color", "darkgray"),
                            $('.lblfieldCustom3').css("color", "darkgray"))
                        : ($('#liCustom3-phone').attr('href', '#views/timeEntryViews/custom3Lookup.html'),
                            $('#fieldCustom3_phone').css("color", "black"),
                            $('.lblfieldCustom3').css("color", "black"))

                } else {

                    $('#liCustom3-phone').removeAttr('href')
                    $('#fieldCustom3_phone').css("color", "darkgray")
                    $('.lblfieldCustom3').css("color", "darkgray")
                }
            }
        }),

        custom4_dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    dataType: "json",
                    url: ""
                }
            },
            offlineStorage: {
                getItem: function () {

                    /* filter list based on matter plan */

                    /* get plan from active entry or timer */
                    var plan = "";

                    if (swpSettings.bTimers === true) {
                        plan = arrEntriesTimers[activeTimerPosition].plan4
                    }
                    else {
                        plan = arrEntries[activeEntryPosition].plan4;
                    }

                    var data = JSON.parse(localStorage.getItem("swp-custom4"));
                    var results = [];

                    if (typeof plan === "undefined") { plan = ""; }

                    if (data !== null) {
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].plan === plan) {
                                results.push(data[i]);
                            }
                        }
                    }

                    return results;
                },
                setItem: function (item) { }
            },
            serverPaging: false,
            serverFiltering: true,
            change: function () {

                if (this._data.length > 0) {

                    swpSettings.bTimers === true
                        ? (entryArray = swpSettings.arrEntriesTimers,
                            entryPosition = swpSettings.activeTimerPosition)
                        : (entryArray = swpSettings.arrEntries,
                            entryPosition = swpSettings.activeEntryPosition)

                    entryArray[entryPosition].status === "Released" || entryArray[entryPosition].status === "Posted"
                        ? ($('#liCustom4-phone').removeAttr('href'),
                            $('#fieldCustom4_phone').css("color", "darkgray"),
                            $('.lblfieldCustom4').css("color", "darkgray"))
                        : ($('#liCustom4-phone').attr('href', '#views/timeEntryViews/custom4Lookup.html'),
                            $('#fieldCustom4_phone').css("color", "black"),
                            $('.lblfieldCustom4').css("color", "black"))

                } else {

                    $('#liCustom4-phone').removeAttr('href')
                    $('#fieldCustom4_phone').css("color", "darkgray")
                    $('.lblfieldCustom4').css("color", "darkgray")
                }
            }
        }),

        custom5_dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    dataType: "json",
                    url: ""
                }
            },
            offlineStorage: {
                getItem: function () {

                    /* filter list based on matter plan */

                    /* get plan from active entry or timer */
                    var plan = "";

                    if (swpSettings.bTimers === true) {
                        plan = arrEntriesTimers[activeTimerPosition].plan4
                    }
                    else {
                        plan = arrEntries[activeEntryPosition].plan4;
                    }

                    var data = JSON.parse(localStorage.getItem("swp-custom5"));
                    var results = [];

                    if (typeof plan === "undefined") { plan = ""; }

                    if (data !== null) {
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].plan === plan) {
                                results.push(data[i]);
                            }
                        }
                    }

                    return results;
                },
                setItem: function (item) { }
            },
            serverPaging: false,
            serverFiltering: true,
            change: function () {

                if (this._data.length > 0) {

                    swpSettings.bTimers === true
                        ? (entryArray = swpSettings.arrEntriesTimers,
                            entryPosition = swpSettings.activeTimerPosition)
                        : (entryArray = swpSettings.arrEntries,
                            entryPosition = swpSettings.activeEntryPosition)

                    entryArray[entryPosition].status === "Released" || entryArray[entryPosition].status === "Posted"
                        ? ($('#liCustom5-phone').removeAttr('href'),
                            $('#fieldCustom5_phone').css("color", "darkgray"),
                            $('.lblfieldCustom5').css("color", "darkgray"))
                        : ($('#liCustom5-phone').attr('href', '#views/timeEntryViews/custom5Lookup.html'),
                            $('#fieldCustom5_phone').css("color", "black"),
                            $('.lblfieldCustom5').css("color", "black"))

                } else {

                    $('#liCustom5-phone').removeAttr('href')
                    $('#fieldCustom5_phone').css("color", "darkgray")
                    $('.lblfieldCustom5').css("color", "darkgray")
                }
            }
        }),

        gridStats_dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: "",
                    dataType: "json",
                    cache: false
                }
            },
            schema: {
                type: "json",
                data: "Item",
                model: {
                    fields: {
                        Field1: { field: "All", type: "" },
                        Field2: { field: "MTD_AccountableActual", type: "number" },
                        Field3: { field: "MTD_AccountableBudget", type: "number" },
                        Field4: { field: "MTD_AccountableVariance", type: "number" },
                        Field5: { field: "MTD_BillableActual", type: "number" },
                        Field6: { field: "MTD_BillableBudget", type: "number" },
                        Field7: { field: "MTD_BillableVariance", type: "number" },
                        Field8: { field: "MTD_NonBillableActual", type: "number" },
                        Field9: { field: "MTD_NonBillableBudget", type: "number" },
                        Field10: { field: "MTD_NonBillableVariance", type: "number" },
                        Field11: { field: "Posted", type: "number" },
                        Field12: { field: "Released", type: "number" },
                        Field13: { field: "Saved", type: "number" },
                        Field14: { field: "YTD_AccountableActual", type: "number" },
                        Field15: { field: "YTD_AccountableBudget", type: "number" },
                        Field16: { field: "YTD_AccountableVariance", type: "number" },
                        Field17: { field: "YTD_BillableActual", type: "number" },
                        Field18: { field: "YTD_BillableBudget", type: "number" },
                        Field19: { field: "YTD_BillableVariance", type: "number" },
                        Field20: { field: "YTD_NonBillableActual", type: "number" },
                        Field21: { field: "YTD_NonBillableBudget", type: "number" },
                        Field22: { field: "YTD_NonBillableVariance", type: "number" },
                    }
                }
            }
        }),

        gridHours_dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: "",
                    dataType: "json",
                    cache: false
                }
            }
        }),

        mat_dataSource_tab: new kendo.data.DataSource({
            transport: {
                read: {
                    dataType: "json",
                    url: ""
                }
            },
            offlineStorage: {
                getItem: function () {

                    var client = arrEntries[activeEntryPosition].client;


                    if (swpSettings.bTimers === true) { client = arrEntriesTimers[activeTimerPosition].client; }

                    if (client.indexOf("|") !== -1) { client = client.substr(0, client.indexOf(" ")); }

                    var data = JSON.parse(localStorage.getItem("swp-matters"));
                    var results = [];

                    log.debug("Client = " + client);

                    if (client !== "") {
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].client === client) {
                                results.push(data[i]);
                            }
                        }
                    }
                    else {
                        return data;
                    }

                    return results;

                },
                setItem: function (item) { }
            },
            serverPaging: false,
            serverFiltering: true
        }),

        dataSourceEntry: function () {
            console.log("entry to get into datasource file")
        },
    }

    return module
}()





