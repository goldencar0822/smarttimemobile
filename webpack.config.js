var path = require('path')

module.exports = {
  entry: './www/js/pluginModules.js',
  output:{
    path: path.resolve(__dirname, './www/dist'),
    filename: 'main.js',
    libraryTarget: 'var',
    library: 'swpPlugins'

  }
}
